//
//  AppDelegate.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 12.11.21.

import Firebase
import FirebaseMessaging
import FirebaseCrashlytics

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var coordinator: MainCoordinator!
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIStylizer().setupUI()

        application.registerForRemoteNotifications()
        configureNotifications()

        coordinator = MainCoordinator()
        window = UIWindow(frame: UIScreen.main.bounds)
        coordinator.window = window
        coordinator.start()

        return true
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {

    private func configureNotifications() {
        FirebaseApp.configure()
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { _, _ in }
    }
}

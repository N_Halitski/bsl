//
//  StuffListEndpoints.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.12.2021.
//

import Foundation

struct StuffListEndpoints {
    static let stuffList = "/v1/account/employees/"
    static let positionsList = "/v1/account/employees/positions/"
}

//
//  StuffListAPI.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.12.2021.
//

import Moya

enum StuffListAPI {
    case readEmployeesList
    case readOneEmployee(userId: String)
    case employeePositionsList
}

extension StuffListAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .readEmployeesList:
            return StuffListEndpoints.stuffList
        case .employeePositionsList:
            return StuffListEndpoints.positionsList
        case .readOneEmployee(let userId):
            return StuffListEndpoints.stuffList + "\(userId)"
        }
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer \(KeychainManager.shared.accessToken ?? "")"]
    }
    
}

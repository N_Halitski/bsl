//
//  StuffListRepository.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.12.2021.
//

import Moya

enum StuffListAPIError: Error {
    case deserialization
    case accessError
    case unknownError
}

protocol StuffListRepositoryProtocol {
    func getPositionsList(completion: @escaping ((Result<[Position], StuffListAPIError>) -> Void))
    func getEmployees(completion: @escaping ((Result<[Employee], StuffListAPIError>) -> Void))
}

final class StuffListRepository: StuffListRepositoryProtocol {
    
    let provider = MoyaProvider<StuffListAPI>()
    
    func getPositionsList(completion: @escaping ((Result<[Position], StuffListAPIError>) -> Void)) {
        provider.request(.employeePositionsList) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map([Position].self) else {
                    completion(.failure(self.errorHandling(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func getEmployees(completion: @escaping ((Result<[Employee], StuffListAPIError>) -> Void)) {
        provider.request(.readEmployeesList) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map([Employee].self) else {
                    completion(.failure(self.errorHandling(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    private func errorHandling(_ errorCode: Int) -> StuffListAPIError {
        switch errorCode {
        case 403:
            return .accessError
        default:
            return .unknownError
        }
    }
}

//
//  VacationModel.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import UIKit

struct VacationModel: Codable {
    let id: String?
    let employee: EmployeeInfo?
    let status: Status?
    let dateStart: String?
    let dateEnd: String?
    let approvers: [Approver]?
    
    enum CodingKeys: String, CodingKey {
        case id, employee, status, approvers
        case dateStart = "date_start"
        case dateEnd = "date_end"
    }
    
}

struct Approver: Codable {
    let approver: EmployeeInfo?
    let status: Status?
}

struct EmployeeInfo: Codable {
    let id: String?
    let userId: String?
    let name: String?
    let position: String?
    let avatar: String?
    
    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case name, position, avatar, id
    }
    
}

enum Status: String, Codable {
    case approved
    case rejected
    case requested
    case cancelled
    case onHold
}

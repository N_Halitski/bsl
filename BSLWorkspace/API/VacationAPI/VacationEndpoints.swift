//
//  VacationEndpoints.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import Foundation

struct VacationEndpoints {
    static let vacation = "v1/account/vacations/"
    static let vacationRequest = "v1/account/vacations/request/"
    static let vacationDepartment = "v1/account/vacations/department/"
}

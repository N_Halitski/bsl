//
//  VacationAPI.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import Foundation
import Moya

enum VacationAPI {
    case readVacation
    case createVacationRequest(dateStart: String, dateEnd: String)
    case readVacationsOfColleagues
}

extension VacationAPI: TargetType {
    
    var baseURL: URL {
        URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .readVacation:
            return VacationEndpoints.vacation
        case .createVacationRequest:
            return VacationEndpoints.vacationRequest
        case .readVacationsOfColleagues:
            return VacationEndpoints.vacationDepartment
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .readVacation, .readVacationsOfColleagues:
            return .get
        case . createVacationRequest:
            return .post
        }
    }
    
    var task: Moya.Task {
        switch self {
        case .readVacation, .readVacationsOfColleagues:
                return .requestPlain
        case .createVacationRequest(let dateStart, let dateEnd):
            let parameters: [String: String] = ["date_start": dateStart, "date_end": dateEnd]
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer " + "\(KeychainManager.shared.accessToken ?? "")"]
    }
    
}

//
//  VacationRepository.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import Foundation
import Moya

enum VacationAPIError: Error {
    case deserialization
    case accessError
    case unknownError
}

protocol VacationsRepositoryProtocol: AnyObject {
    func getVacations(completion: @escaping ((Result<[VacationModel]?, VacationAPIError>) -> Void))
    func sendVacationRequest(dateStart: String, dateEnd: String, completion: @escaping (() -> Void))
    func getVacationsOfColleagues(completion: @escaping ((Result<[VacationModel]?, VacationAPIError>) -> Void))
}

final class VacationsRepository: VacationsRepositoryProtocol {
    
    private let provider = MoyaProvider<VacationAPI>()
    
    func getVacations(completion: @escaping ((Result<[VacationModel]?, VacationAPIError>) -> Void)) {
        provider.request(.readVacation) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map([VacationModel].self) else {
                    completion(.failure(self.handleError(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func sendVacationRequest(dateStart: String, dateEnd: String, completion: @escaping (() -> Void)) {
        provider.request(.createVacationRequest(dateStart: dateStart, dateEnd: dateEnd)) { result in
            switch result {
            case .success(let response):
                // swiftlint:disable all
                // TODO: экран который нужно закрыть в разработке
                // swiftlint:enable all
                print("response - \(response)")
            case .failure(let error):
                // swiftlint:disable all
                // TODO: экран в разработке у дизайнера
                // swiftlint:enable all
                print("eror - \(error)")
            }
        }
    }
    
    func getVacationsOfColleagues(completion: @escaping ((Result<[VacationModel]?, VacationAPIError>) -> Void)) {
        provider.request(.readVacationsOfColleagues) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map([VacationModel].self) else {
                    completion(.failure(self.handleError(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    private func handleError(_ errorCode: Int) -> VacationAPIError {
        switch errorCode {
        case 403:
            return .accessError
        default:
            return .unknownError
        }
    }
    
}

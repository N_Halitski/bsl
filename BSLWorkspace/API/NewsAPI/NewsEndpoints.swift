//
//  NewsEndpoints.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.12.21.
//

import Foundation

struct NewsEndpoints {
    static let news = "/v1/news/"
}

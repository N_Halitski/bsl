//
//  NewsAPI.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.12.21.
//

import Foundation
import Moya

enum NewsAPI {
    case readNews(limit: Int?, offset: Int?)
    case readNewsDetails(newsId: Int)
}

extension NewsAPI: TargetType {
    
    var baseURL: URL {
        URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .readNews:
            return NewsEndpoints.news
        case .readNewsDetails(let newsId):
            return  "\(NewsEndpoints.news)\(newsId)/"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Moya.Task {
        switch self {
        case .readNews(let limit, let offset):
            var parameters: [String: Any] = [:]
            if let limit = limit {
                if let offset = offset {
                    parameters = ["limit": limit, "offset": offset]
                } else {
                    parameters = ["limit": limit]
                }
            } else {
                return .requestPlain
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer " + "\(KeychainManager.shared.accessToken ?? "")" ]
    }
}

//
//  NewsModel.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.12.21.
//

import Foundation
import UIKit

struct NewsModel: Codable {
    let count: Int?
    let next: String?
    let previous: String?
    var results: [NewsList]?
}

struct NewsList: Codable {
    let id: Int?
    let createdAt: String?
    let type: String?
    let title: String?
    var text: String?
    var authorName: String?
    var authorAvatar: String?
    var newsAttachment: [String]?

    enum CodingKeys: String, CodingKey {
        case id, type, title, text
        case createdAt = "created_at"
    }
}

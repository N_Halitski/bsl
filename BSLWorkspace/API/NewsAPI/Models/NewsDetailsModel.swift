//
//  NewsDetailsModel.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.12.21.
//

import Foundation

struct NewsDetailsModel: Codable {
    let id: Int?
    let createdAt: String?
    let type: String?
    let title: String?
    let text: String?
    let attachments: [String]?
    let author: NewsAuthor?
    
    enum CodingKeys: String, CodingKey {
        case id, type, title, text, attachments, author
        case createdAt = "created_at"
    }
}

struct NewsAuthor: Codable {
    let name: String?
    let avatar: String?
}

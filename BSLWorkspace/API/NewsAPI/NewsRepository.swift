//
//  NewsRepository.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.12.21.
//

import Foundation
import Moya

enum NewsAPIError: Error {
    case deserialization
    case accessError
    case unknownError
}

protocol NewsRepositoryProtocol: AnyObject {
    func getNews(limit: Int?, offset: Int?, completion: @escaping ((Result<NewsModel?, NewsAPIError>) -> Void))
    func getNewsDetails(newsId: Int, completion: @escaping ((Result<NewsDetailsModel?, NewsAPIError>) -> Void))
}

final class NewsRepository: NewsRepositoryProtocol {
    
    private let provider = MoyaProvider<NewsAPI>()
    
    func getNews(limit: Int?, offset: Int?, completion: @escaping ((Result<NewsModel?, NewsAPIError>) -> Void)) {
        provider.request(.readNews(limit: limit, offset: offset)) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map(NewsModel.self) else {
                    completion(.failure(self.handleError(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func getNewsDetails(newsId: Int, completion: @escaping ((Result<NewsDetailsModel?, NewsAPIError>) -> Void)) {
        provider.request(.readNewsDetails(newsId: newsId)) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map(NewsDetailsModel.self) else {
                    completion(.failure(self.handleError(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    private func handleError(_ errorCode: Int) -> NewsAPIError {
        switch errorCode {
        case 403:
            return .accessError
        default:
            return .unknownError
        }
    }
}

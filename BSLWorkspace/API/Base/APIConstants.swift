//
//  APIConstants.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 2.12.21.
//

import Foundation

struct APIConstants {
    static let baseURL = "https://bslpln-dev.bsl.dev/api"
}

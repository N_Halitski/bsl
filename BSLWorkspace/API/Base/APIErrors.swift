//
//  APIErrors.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 16.12.21.
//

import Foundation

enum APIErrors: Error {
    case parametersError
    case unknownError
    case desearilization
}

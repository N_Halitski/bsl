//
//  InboxRepository.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import Foundation
import Moya

enum InboxAPIError: Error {
    case deserialization
    case accessError
    case unknownError
}

protocol InboxRepositoryProtocol: AnyObject {
    func getInbox(completion: @escaping ((Result<[InboxModel]?, InboxAPIError>) -> Void))
}

final class InboxRepository: InboxRepositoryProtocol {
    
    private let provider = MoyaProvider<InboxAPI>()
    
    func getInbox(completion: @escaping ((Result<[InboxModel]?, InboxAPIError>) -> Void)) {
        provider.request(.readInbox) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map([InboxModel].self) else {
                    completion(.failure(self.handleError(response.statusCode)))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    private func handleError(_ errorCode: Int) -> InboxAPIError {
        switch errorCode {
        case 403:
            return .accessError
        default:
            return .unknownError
        }
    }

}

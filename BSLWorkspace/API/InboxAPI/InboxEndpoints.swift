//
//  InboxEndpoints.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import Foundation

struct InboxEndpoints {
    static let inbox = "v1/account/inbox/"
}

//
//  InboxAPI.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import Foundation
import Moya

enum InboxAPI {
    case readInbox
}

extension InboxAPI: TargetType {
    
    var baseURL: URL {
        URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .readInbox:
            return InboxEndpoints.inbox
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Moya.Task {
        switch self {
        case .readInbox:
            return .requestPlain
        }
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer \(KeychainManager.shared.accessToken ?? "")" ]
    }

}

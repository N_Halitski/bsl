//
//  InboxModel.swift
//  BSLWorkspace
//
//  Created by Sergey on 10.01.22.
//

import UIKit

struct InboxModel: Codable {
    let fromUser: EmployeeInfo?
    let eventType: EventType?
    let objectId: String?
    let readAt: String?
    let createAt: String?
    let text: String?
    
    enum CodingKeys: String, CodingKey {
        case text
        case createAt = "created_at"
        case fromUser = "from_user"
        case eventType = "event_type"
        case objectId = "object_id"
        case readAt = "read_at"
    }
    
}

enum EventType: String, Codable {
    case vacationRequest = "vacation_request"
    case vacationApproved = "vacation_approved"
    case vacationRejected = "vacation_rejected"
    
    func getName() -> String {
        switch self {
        case .vacationRequest:
            return "InboxScreen.vacationRequest".localized
        case .vacationApproved, .vacationRejected:
            return ""
        }
    }
    
}

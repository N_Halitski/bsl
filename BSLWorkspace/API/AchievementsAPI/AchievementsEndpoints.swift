//
//  AchievementsEndpoints.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 26.01.22.
//

import Foundation

struct AchievementsEndpoints {
    static let awards = "/v1/account/award_templates/"
}

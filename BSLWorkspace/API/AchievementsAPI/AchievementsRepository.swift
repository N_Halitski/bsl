//
//  AchievementsRepository.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 26.01.22.
//

import Moya

protocol AchievementsRepositoryProtocol {
    func getAwards(completion: @escaping ((Result<[AchievementsModel]?, APIErrors>) -> Void))
}

final class AchievementsRepository: AchievementsRepositoryProtocol {
    
    private let provider = MoyaProvider<AchievementsAPI>()
    
    func getAwards(completion: @escaping ((Result<[AchievementsModel]?, APIErrors>) -> Void)) {
        provider.request(.awards) { result in
            switch result {
            case .success(let response):
                    switch response.statusCode {
                    case 200:
                        guard let responseModel = try? response.map([AchievementsModel].self) else {
                            completion(.failure(.desearilization))
                            return
                        }
                        completion(.success(responseModel))
                    default:
                        completion(.failure(.unknownError))
                }
            case .failure:
                    completion(.failure(.unknownError))
            }
        }
    }
}

//
//  AchievementsModel.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 26.01.22.
//

import Foundation

struct AchievementsModel: Codable {
    let id: String?
    let imageUrl: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case imageUrl = "image_url"
    }
}

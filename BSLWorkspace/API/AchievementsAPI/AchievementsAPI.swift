//
//  AchievementsAPI.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 26.01.22.
//

import Moya

enum AchievementsAPI {
    case awards
}

extension AchievementsAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        return AchievementsEndpoints.awards
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        .requestPlain
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer \(KeychainManager.shared.accessToken ?? "")"]
    }
}

//
//  ProjectsEndpoints.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 4.12.21.
//

struct ProjectsEndpoints {
    static let projects = "/v1/projects/"
}

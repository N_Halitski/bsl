//
//  ProjectResponseModel.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 2.12.21.
//

import Foundation

struct ProjectsRegistryResponseModel: Codable {
    let id, title, code: String?
    let imageURL: String?
    let dateStart, dateEnd, manager: String?
    let managerInfo: ManagerInfo?
    let status, welcomeDescription: String?
    let progress: Int?
    let team: [Team]?

    enum CodingKeys: String, CodingKey {
        case id, title, code
        case imageURL = "image_url"
        case dateStart = "date_start"
        case dateEnd = "date_end"
        case manager
        case managerInfo = "manager_info"
        case status
        case welcomeDescription = "description"
        case progress, team
    }
}

struct Team: Codable {
    
}

struct ManagerInfo: Codable {
    let name, id, position: String?
    let avatar: String?
}

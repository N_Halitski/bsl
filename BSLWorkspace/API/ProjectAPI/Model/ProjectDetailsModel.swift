//
//  ProjectDetailsModel.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 9.12.21.
//

import Foundation

struct ProjectDetailsModel: Codable {
    let id, title, code, imageURL: String?
    let jiraID, budget: String?
    let budgetReal: Int?
    let dateStart, dateEnd, manager, status: String?
    let welcomeDescription: String?
    let specialists: [Specialist]?
    let progress: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, title, code
        case imageURL = "image_url"
        case jiraID = "jira_id"
        case budget
        case budgetReal = "budget_real"
        case dateStart = "date_start"
        case dateEnd = "date_end"
        case manager, status
        case welcomeDescription = "description"
        case specialists, progress
    }
}

struct Specialist: Codable {
    let resourceKind: ResourceKind?
    let payment, id: Int?
    
    enum CodingKeys: String, CodingKey {
        case resourceKind = "resource_kind"
        case payment, id
    }
}

struct ResourceKind: Codable {
    let id: Int?
    let codename: String?
}

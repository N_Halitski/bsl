//
//  ProjectsRepository.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 4.12.21.
//

import Foundation
import Moya

enum ProjectsAPIError: Error {
    case deserialization
    case accessError
    case unknownError
}

protocol ProjectsRepositoryProtocol: AnyObject {
    func getProjects(completion: @escaping ((Result<[ProjectsRegistryResponseModel]?, ProjectsAPIError>) -> Void))
    func getProjectDetails(projectId: String, completion: @escaping ((Result<ProjectDetailsModel?, ProjectsAPIError>) -> Void))
}

final class ProjectsRepository: ProjectsRepositoryProtocol {
    
    private let provider = MoyaProvider<ProjectsAPI>()
    
    func getProjects(completion: @escaping ((Result<[ProjectsRegistryResponseModel]?, ProjectsAPIError>) -> Void)) {
        provider.request(.readProjectsRegistry) { [weak self] result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map([ProjectsRegistryResponseModel].self) else {
                    completion(.failure(self?.handleError(response.statusCode) ?? .deserialization))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func getProjectDetails(projectId: String, completion: @escaping ((Result<ProjectDetailsModel?, ProjectsAPIError>) -> Void)) {
        provider.request(.readProjectDetails(projectId: projectId)) { [weak self] result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map(ProjectDetailsModel.self) else {
                    completion(.failure(self?.handleError(response.statusCode) ?? .deserialization))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    private func handleError(_ errorCode: Int) -> ProjectsAPIError {
        switch errorCode {
        case 403:
            return .accessError
        default:
            return .unknownError
        }
    }
    
}

//
//  ProjectsAPI.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 2.12.21.
//

import Moya

enum ProjectsAPI {
    case readProjectsRegistry
    case readProjectDetails(projectId: String)
}

extension ProjectsAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .readProjectsRegistry:
            return ProjectsEndpoints.projects
        case .readProjectDetails(let projectId):
            return ProjectsEndpoints.projects + "\(projectId)"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer \(KeychainManager.shared.accessToken ?? "")"]
    }
    
}

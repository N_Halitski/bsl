//
//  AuthRepository.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 1.12.21.
//

import Moya

protocol AuthRepositoryProtocol {
    func auth(email: String, password: String, completion: @escaping ((Result<AuthResponseModel?, APIErrors>) -> Void))
    func resetPassword(email: String, completion: @escaping ((Result<Any?, APIErrors>) -> Void))
    func authWithToken(completion: @escaping ((Result<Any?, APIErrors>) -> Void))
    func refreshToken(refresh: String, completion: @escaping ((Result<RefreshTokenModel?, APIErrors>) -> Void))
}

final class AuthRepository: AuthRepositoryProtocol {
    
    private let provider = MoyaProvider<AuthAPI>()
    
    func auth(email: String, password: String, completion: @escaping ((Result<AuthResponseModel?, APIErrors>) -> Void)) {
        provider.request(.auth(email: email, password: password)) { result in
            switch result {
            case .success(let response):
                switch response.statusCode {
                case 200:
                    guard let responseModel = try? response.map(AuthResponseModel.self) else {
                        completion(.failure(.desearilization))
                        return
                    }
                    KeychainManager.shared.updateAccessToken(accessToken: responseModel.access)
                    KeychainManager.shared.updateRefreshToken(refreshToken: responseModel.refresh)
                    completion(.success(responseModel))
                case 401:
                    completion(.failure(.parametersError))
                default:
                    completion(.failure(.unknownError))
                }
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func resetPassword(email: String, completion: @escaping ((Result<Any?, APIErrors>) -> Void)) {
        provider.request(.resetPassword(email: email)) { result in
            switch result {
            case .success:
                completion(.success(nil))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func authWithToken(completion: @escaping ((Result<Any?, APIErrors>) -> Void)) {
        provider.request(.authWithToken) { result in
            switch result {
            case .success(let response):
                switch response.statusCode {
                case 401:
                    completion(.failure(.parametersError))
                case 404:
                    completion(.failure(.unknownError))
                default:
                    completion(.success(nil))
                }
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
    func refreshToken(refresh: String, completion: @escaping ((Result<RefreshTokenModel?, APIErrors>) -> Void)) {
        provider.request(.refreshToken(refresh: refresh)) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map(RefreshTokenModel.self) else {
                    completion(.failure(.desearilization))
                    return
                }
                KeychainManager.shared.updateRefreshToken(refreshToken: responseModel.refresh)
                KeychainManager.shared.updateAccessToken(accessToken: responseModel.access)
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
}

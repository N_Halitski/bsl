//
//  RefreshTokenModel.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 12.12.21.
//

import Foundation

struct RefreshTokenModel: Codable {
    let refresh: String?
    let access: String?
}

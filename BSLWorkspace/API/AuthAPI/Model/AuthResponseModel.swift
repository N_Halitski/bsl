//
//  AuthResponseModel.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 1.12.21.
//

import Foundation

struct AuthResponseModel: Codable {
    let access: String?
    let refresh: String?
}

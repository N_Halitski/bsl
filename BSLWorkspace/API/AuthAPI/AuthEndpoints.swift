//
//  AuthEndpoints.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 2.12.21.
//

import Foundation

struct AuthEndpoints {
    static let login = "/v1/auth/login"
    static let resetPassword = "/v1/account/issue_password_reset/"
    static let loginWithToken = "/v1/account/employees/me/"
    static let refreshToken = "/v1/auth/refresh"
    static let profileInfo = "/v1/account/employees/me/"
}

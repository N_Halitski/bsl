//
//  AuthAPI.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 1.12.21.
//

import Moya

enum AuthAPI {
    case auth(email: String, password: String)
    case resetPassword(email: String)
    case authWithToken
    case refreshToken(refresh: String)
}

extension AuthAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        switch self {
        case .auth:
            return AuthEndpoints.login
        case .resetPassword:
            return AuthEndpoints.resetPassword
        case .authWithToken:
            return AuthEndpoints.loginWithToken
        case .refreshToken:
            return AuthEndpoints.refreshToken
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .auth, .resetPassword, .refreshToken:
            return .post
        case .authWithToken:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .auth(let email, let password):
            let parameters: [String: Any] = ["email": email, "password": password]
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        case .resetPassword(let email):
            let parameters: [String: Any] = ["email": email]
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        case .authWithToken:
            return .requestPlain
        case .refreshToken(let refresh):
            return .requestParameters(parameters: ["refresh": refresh], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .auth, .resetPassword, .refreshToken:
            return nil
        case .authWithToken:
            return ["Authorization": "Bearer \(KeychainManager.shared.accessToken ?? "")"]
        }
        
    }
    
}

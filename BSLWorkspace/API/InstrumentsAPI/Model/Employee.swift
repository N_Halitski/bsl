//
//  InstrumentsResponseModel.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 16.12.21.
//

import Foundation

enum UserPermissions: String, Codable {
    case award = "employees_award"
    case projects
    case users
}

struct Employee: Codable {
    
    var userId: String?
    var name: String?
    var email: String?
    var office: String?
    var position: Position?
    var avatar: String?
    var title: String?
    var phone: String?
    var birthdate: String?
    var awards: [Award]?
    var city: String?
    var vacations: [Vacation]?
    var userPermissions: [UserPermissions]?
    
    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case name, email, office, position, avatar, title, phone, birthdate, awards, city
        case userPermissions = "user_permissions"
        case vacations
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let userId = try container.decodeIfPresent(String.self, forKey: .userId)
        let name = try container.decodeIfPresent(String.self, forKey: .name)
        let email = try container.decodeIfPresent(String.self, forKey: .email)
        let office = try container.decodeIfPresent(String.self, forKey: .office)
        let position = try container.decodeIfPresent(Position.self, forKey: .position)
        let title = try container.decodeIfPresent(String.self, forKey: .title)
        let phone = try container.decodeIfPresent(String.self, forKey: .phone)
        let birthdate = try container.decodeIfPresent(String.self, forKey: .birthdate)
        let awards = try container.decodeIfPresent([Award].self, forKey: .awards)
        let city = try container.decodeIfPresent(String.self, forKey: .city)
        let vacations = try container.decodeIfPresent([Vacation].self, forKey: .vacations)
        let avatar = try container.decodeIfPresent(String.self, forKey: .avatar)
        let userPermissions = try container.decodeIfPresent([String].self, forKey: .userPermissions)
        self.userId = userId
        self.name = name
        self.email = email
        self.office = office
        self.position = position
        self.avatar = avatar
        self.title = title
        self.phone = phone
        self.birthdate = birthdate
        self.awards = awards
        self.city = city
        self.vacations = vacations
        self.userPermissions = userPermissions?.compactMap { UserPermissions(rawValue: $0) }
    }
}

struct Position: Codable {
    let id: Int?
    let codename: String?
    let paymentPerHour: Int?

    enum CodingKeys: String, CodingKey {
        case id, codename
        case paymentPerHour = "payment_per_hour"
    }
}

struct Award: Codable {
    let imageURL, title, awardDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "image_url"
        case title
        case awardDescription = "description"
    }
}

struct Vacation: Codable {
    let id, status, dateStart, dateEnd: String?
    let days: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, status
        case dateStart = "date_start"
        case dateEnd = "date_end"
        case days
    }
}

//
//  InstrumentsAPI.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 13.12.21.
//

import Moya

enum InstrumentsAPI {
    case getInstrumentsScreenCells
}

extension InstrumentsAPI: TargetType {
    var baseURL: URL {
        return URL(string: APIConstants.baseURL)!
    }
    
    var path: String {
        return AuthEndpoints.profileInfo
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String: String]? {
        return ["Authorization": "Bearer \(KeychainManager.shared.accessToken ?? "")"]
    }
}

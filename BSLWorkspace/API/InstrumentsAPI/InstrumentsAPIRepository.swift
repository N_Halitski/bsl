//
//  InstrumentsAPIRepository.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 13.12.21.
//

import Moya
import UIKit

protocol InstrumentsAPIRepositoryProtocol {
    func getCelllsWithLevelAccess(completion: @escaping ((Result<Employee?, APIErrors>) -> Void))
}

final class InstrumentsAPIRepository: InstrumentsAPIRepositoryProtocol {
    
    private let provider = MoyaProvider<InstrumentsAPI>()
    
    func getCelllsWithLevelAccess(completion: @escaping ((Result<Employee?, APIErrors>) -> Void)) {
        provider.request(.getInstrumentsScreenCells) { result in
            switch result {
            case .success(let response):
                guard let responseModel = try? response.map(Employee.self) else {
                    completion(.failure(.desearilization))
                    return
                }
                completion(.success(responseModel))
            case .failure:
                completion(.failure(.unknownError))
            }
        }
    }
    
}

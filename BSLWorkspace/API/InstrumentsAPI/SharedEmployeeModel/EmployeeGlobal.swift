//
//  Singleton.swift
//  BSLWorkspace
//
//  Created by Nikita on 13.01.2022.
//

import Foundation

struct SharedEmployeeModel {
    
    static var shared = SharedEmployeeModel()
    var employee: Employee?
    
    private init() {}
}

//
//  UITableView + Extension.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 20.01.22.
//

import UIKit

extension UITableView {
    
    func registerCell<T>(_:T.Type) {
        register(UINib(nibName: String(describing: T.self), bundle: nil), forCellReuseIdentifier: String(describing: T.self))
    }
    
}

//
//  Date + Extension.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 12.01.22.
//

import Foundation

extension Date {
    
    static func getCurrentYear() -> Int {
        let date = Date()
        let year = Calendar.current.component(.year, from: date)
        return Int(year)
    } 
    
    static func getCurrentMonth() -> Int {
        let date = Date()
        let month = Calendar.current.component(.month, from: date)
        return Int(month)
    }
    
    static func getCurrentDay() -> Int {
        let date = Date()
        let month = Calendar.current.component(.day, from: date)
        return Int(month)
    }
    
}

//
//  NSLayoutConstraint + Extension.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 16.01.22.
//

import UIKit

extension NSLayoutConstraint {
    
     func prioritize(at priority: UILayoutPriority) -> NSLayoutConstraint {
        self.priority = priority
        return self
    }
    
}

//
//  String + Extension.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 18.11.21.
//

import Foundation
import UIKit

extension String {
    
    var isNotEmpty: Bool {
        return !isEmpty
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func splitStringInHalf() -> (firstHalf: String, secondHalf: String) {
        let words = self.components(separatedBy: " ")
        let halfLength = words.count / 2
        let firstHalf = words[0..<halfLength].joined(separator: " ")
        let secondHalf = words[halfLength..<words.count].joined(separator: " ")
        return (firstHalf: firstHalf, secondHalf: secondHalf)
    }
    
    func setupLineSpacing(lineSpacing: CGFloat) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineSpacing
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            // swiftlint:disable all
            range: NSMakeRange(0, attributedString.length))
            // swiftlint:enable all
        return attributedString
    }
}

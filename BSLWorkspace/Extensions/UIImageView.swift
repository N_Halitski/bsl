//
//  UIImageView.swift
//  BSLWorkspace
//
//  Created by Sergey on 19.01.22.
//

import UIKit

extension UIImageView {
    
    func setBorderColor(by status: Status) {
        switch status {
        case .requested:
            self.layer.borderColor = AppTheme.Colors.orangeStatus.cgColor
        case .approved:
            self.layer.borderColor = AppTheme.Colors.greenStatus.cgColor
        case .rejected:
            self.layer.borderColor = AppTheme.Colors.redStatus.cgColor
        case .onHold:
            self.layer.borderColor = AppTheme.Colors.gray100.cgColor
        default:
            self.layer.borderColor = AppTheme.Colors.orangeStatus.cgColor
        }
    }
    
}

//
//  UICollectionView + Extension.swift
//  BSLWorkspace
//
//  Created by Nikita on 20.01.2022.
//

import UIKit

extension UICollectionView {
    
    func registerCell<T>(_:T.Type) {
        register(UINib(nibName: String(describing: T.self), bundle: nil), forCellWithReuseIdentifier: String(describing: T.self))
    }
    
}

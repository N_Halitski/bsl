//
//  UIView + Extension.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 22.11.21.
//

import UIKit

extension UIView {
    
    func addShadow(shadowColor: CGColor,
                   shadowOffset: CGSize,
                   shadowOpacity: Float,
                   shadowRadius: CGFloat) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
    }
}

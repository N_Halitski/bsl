//
//  UIViewController + Extension.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 9.12.21.
//

import UIKit
import Lottie

extension UIViewController {
    final func showLoader(isShown: Bool) {
        view.subviews.first {$0.tag == 999}?.removeFromSuperview()
        view.isUserInteractionEnabled = true

        if isShown == true {
            let loaderName = "Loader"
            var loader: AnimationView
            loader = .init(name: loaderName)
            loader.center = view.center
            loader.loopMode = .loop
            loader.play()
            view.addSubview(loader)
            view.isUserInteractionEnabled = false
            loader.tag = 999
        }
    }
}

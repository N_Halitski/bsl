//
//  UIViewControllerExtension.swift
//  BSLWorkspace
//
//  Created by Sergey on 21.11.21.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

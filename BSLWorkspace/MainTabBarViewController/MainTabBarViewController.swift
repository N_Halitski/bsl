//
//  MainTabBarController.swift
//  BSLWorkspace
//
//  Created by Sergey on 22.11.21.
//

import UIKit

final class MainTabBarViewController: UITabBarController {
    
    var coordinators: [TabBarPresentableCoordinator]?
    
    var logOutCallback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createCoordinators()
        setupUI()
    }
    
    private func createCoordinators() {
        let workSpaceCoordinator = WorkSpaceCoordinator()
        
        workSpaceCoordinator.didFinish = { [weak self] in
            guard let self = self else { return }
            self.logOutCallback?()
        }
        
        let newsCoordinator = NewsCoordinator()
        let inboxCoordinator = InboxCoordinator()
        let profileCoordinator = ProfileCoordinator()
        
        profileCoordinator.didFinish = { [weak self] in
            guard let self = self else { return }
            self.logOutCallback?()
        }
        
        coordinators = [
            workSpaceCoordinator,
            newsCoordinator,
            inboxCoordinator,
            profileCoordinator
        ]

        coordinators?.forEach { $0.navigationController.tabBarItem = $0.tabBarItem }
        self.viewControllers = coordinators?.map { $0.navigationController }
        coordinators?.forEach { $0.start() }
    }
    
    private func setupUI() {
        UITabBar.appearance().tintColor = AppTheme.Colors.orange100
        let layer = CAShapeLayer()
        layer.path = UIBezierPath(roundedRect: CGRect(
            x: 16,
            y: self.tabBar.bounds.minY - 7,
            width: self.tabBar.bounds.width - 32,
            height: self.tabBar.bounds.height + 7), cornerRadius: 16).cgPath
        layer.shadowColor = AppTheme.Colors.lightGray8.cgColor
        layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        layer.shadowRadius = 30.0
        layer.shadowOpacity = 1.0
        layer.borderWidth = 1.0
        layer.opacity = 1.0
        layer.isHidden = false
        layer.masksToBounds = false
        layer.fillColor = AppTheme.Colors.white100.cgColor
        self.tabBar.layer.insertSublayer(layer, at: 0)
        
        if let items = self.tabBar.items {
            items.forEach { item in
                item.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
                item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -7)
            }
        }
        
        self.tabBar.itemPositioning = .centered
        self.tabBar.itemWidth = UIScreen.main.bounds.width / 7
    }
}

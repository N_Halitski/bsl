//
//  MainCoordinator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 16.11.21.
//

import UIKit

final class MainCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    var window: UIWindow?
    var didFinish: (() -> Void)?
    
    /// репозиторий для экрана авторизации, с помощью которого делается запрос
    private let authRepository: AuthRepositoryProtocol
    
    init(navigationController: UINavigationController = UINavigationController(), authRepository: AuthRepositoryProtocol = AuthRepository()) {
        self.navigationController = navigationController
        self.authRepository = authRepository
    }
    
    /// запуск авторизиациионного флоу
    func start() {
        let authCoordinator = createAuthCoordinator()
        if KeychainManager.shared.accessToken == nil {
            authCoordinator.start()
        } else {
            authWithToken(with: authCoordinator)
        }
    }
    
    func stop() {}
    
}

// MARK: - Extension. Сontains methods for creating various modules

private extension MainCoordinator {
    
    /// фабрика,  которая создает четыре тап бар контроллера
    func createTabBarController() {
        let mainTabBarController = MainTabBarViewController()
        mainTabBarController.logOutCallback = { [weak self] in
            guard let self = self else { return }
            KeychainManager.shared.clear()
            SharedEmployeeModel.shared.employee = nil
            self.resetNavigationStack()
        }
        window?.rootViewController = mainTabBarController
        window?.makeKeyAndVisible()
    }
    
    private func createNewsPhotosModule() -> UIViewController {
        let newsPhotosViewController = NewsPhotosModuleAssembler.createModule()
        return newsPhotosViewController
    }
    
    /// фабрика, которая запускает флоу авторизации
    /// - Returns: координатор авторизации
    func createAuthCoordinator() -> Coordinator {
        let authCoordinator = AuthCoordinator(navigationController: self.navigationController)
        window?.rootViewController = authCoordinator.navigationController
        window?.makeKeyAndVisible()
        authCoordinator.didFinish = { [weak self] in
            guard let self = self else { return }
            self.createTabBarController()
        }
        authCoordinator.openPasswordRecoveryScreen = { [weak self] in
            guard let self = self else { return }
            let passwordRecoveryViewController = PasswordRecoveryModuleAssembler.createModule {
                self.navigationController.popViewController(animated: true)
            }
            self.navigationController.pushViewController(passwordRecoveryViewController, animated: true)
        }
        return authCoordinator
    }
    
}

private extension MainCoordinator {
    
    /// метод, который осуществляет авторизацию в приложении с помощью аксес токена
    /// - Parameter authCoordinator: координатор авторизации
    func authWithToken(with authCoordinator: Coordinator) {
        let splashScreen = SplashScreen()
        navigationController.pushViewController(splashScreen, animated: true)
        authRepository.authWithToken { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.createTabBarController()
            case .failure:
                self.refreshToken(with: authCoordinator)
            }
        }
    }
    
    /// метод, который обновляет токен по прошествию времени и котороый позволяет авторизоваться
    /// - Parameter authCoordinator: координатор авторизации
    func refreshToken(with authCoordinator: Coordinator) {
        authRepository.refreshToken(refresh: KeychainManager.shared.refreshToken ?? "") { [weak self] result in
            switch result {
            case .success:
                self?.navigationController.navigationBar.isHidden = true
            case .failure:
                KeychainManager.shared.clear()
            }
            authCoordinator.start()
        }
    }
    
    func resetNavigationStack() {
        self.navigationController.popToRootViewController(animated: true)
        start()
    }
    
}

//
//  Coordinator.swift
//  BSLWorkspace
//
//  Created by Nikita on 18.11.2021.
//

import UIKit

protocol Coordinator: AnyObject {
    var navigationController: UINavigationController { get }
    var didFinish: (() -> Void)? { get set }
    
    func start()
    func stop()
}

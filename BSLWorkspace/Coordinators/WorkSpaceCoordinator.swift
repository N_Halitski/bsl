//
//  WorkSpaceCoordinator.swift
//  BSLWorkspace
//
//  Created by Nikita on 17.11.2021.
//

import UIKit

/// enum для правильной реализации перехода на экран реестра сотрудников и дальнейший флоу приложения. реестр сотрудников используется в двух разных флоу
enum StuffListOpenType {
    case achievements
    case instruments
}

final class WorkSpaceCoordinator: TabBarPresentableCoordinator {
    
    /// иконка таб бара с разными состояними
    var tabBarItem: UITabBarItem = {
        let title = "workspace"
        let image = UIImage(named: "workspace")
        let selectedImage = UIImage(named: "workspaceSelected")
        let item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        return item
    }()
    
    /// репозиторий для экрана отпусков, с помощью которого делается запрос
    private let vacationsRepository: VacationsRepositoryProtocol = VacationsRepository()
    
    var navigationController: UINavigationController
    var didFinish: (() -> Void)?
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    /// запуск флоу экрана workspace
    func start() {
        let workSpaceViewController = createWorkSpaceViewController()
        navigationController.pushViewController(workSpaceViewController, animated: true)
    }
    
    /// остановка флоу экрана workspace
    func stop() {
        navigationController.popToRootViewController(animated: false)
    }
}

// MARK: - Extension. Сontains methods for creating various modules

extension WorkSpaceCoordinator {
    
    /// фабрика, которая создает view controller воркспейс
    /// - Returns: возвращает workspace view controller
    private func createWorkSpaceViewController() -> UIViewController {
        let workSpaceViewController = InstrumentsAssembler.createModule(vacationRepository: self.vacationsRepository) { [weak self] list in
            guard let self = self else { return }
            let projectRegisterViewController = self.createProjectsListVC(list)
            self.navigationController.pushViewController(projectRegisterViewController, animated: true)
        }
        workSpaceViewController.openSharePainScreenCallBack = { [weak self] in
            guard let self = self else { return }
            self.navigationController.pushViewController(self.createSharePainVC(), animated: true)
        }
        workSpaceViewController.openStuffListScreenCallBack = { [weak self]  employeeList, positionsList, projectList in
            guard let self = self else { return }
            self.navigationController.pushViewController(self.createStuffListVC(
                type: .instruments,
                employees: employeeList,
                positions: positionsList,
                projects: projectList), animated: true)
        }
        workSpaceViewController.openVacationScreen = { [weak self] vacationsList, vacationsOfColleaguesList in
            guard let self = self else { return }
            self.navigationController.pushViewController(self.createVacationModule(vacationsList: vacationsList, vacationsOfColleaguesList: vacationsOfColleaguesList),
                                                         animated: true)
        }
        workSpaceViewController.openAchievementsScreen = { [weak self] employeeList, positionsList, projectList in
            guard let self = self else { return }
            self.navigationController.pushViewController(self.createAchievementsVC(
                type: .achievements,
                employees: employeeList,
                positions: positionsList,
                projects: projectList), animated: true)
        }
        return workSpaceViewController
    }
    
    ///  фабрика, которая возвращает view controller реестра проектов
    /// - Parameter list: массив проектов, который отображается в таблице
    /// - Returns: возвращает view controller реестра проектов
    private func createProjectsListVC(_ list: [ProjectsRegistryResponseModel]?) -> UIViewController {
        let projectRegisterViewController = ProjectsRegisterModuleAssembler.createModule(projectsList: list)
        
        projectRegisterViewController.didTapProject = { [weak self] project in
            guard let self = self, let projectModel = project else { return }
            let selectedProjectVC = self.createProjectViewController(projectModel)
            self.navigationController.pushViewController(selectedProjectVC, animated: true)
        }
        return projectRegisterViewController
    }
    
    /// фабрика, которая возвращает view controller реестра проектов
    /// - Parameters:
    ///   - type: enum, который сообщает, что запускается именно флоу реестра сотрудников
    ///   - employees: массив сотрудников для отображения в таблице
    ///   - positions: массив позиций для нэйминга секций
    ///   - projects: массив проектов для фильтров
    /// - Returns: возвращает view controller для реестра сотрудников
    private func createStuffListVC(type: StuffListOpenType, employees: [Employee], positions: [Position], projects: [ProjectsRegistryResponseModel]) -> UIViewController {
        
        let stuffVC = StuffListAssembler.createModule(employeeList: employees, positionsList: positions, projectsList: projects, type: type)
        
        stuffVC.openProfileScreen = { [weak self] employee in
            guard let self = self else { return }
            let profileVC = self.createProfileViewController(employee)
            self.navigationController.pushViewController(profileVC, animated: true)
        }
        stuffVC.dismissToAchievementsScreen = { [weak self] selectedEmployee in
            guard let self = self else { return }
            guard self.navigationController.viewControllers.indices.contains(self.navigationController.viewControllers.count - 2),
                let achievementsVC = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2] as? AchievementsViewController else { return }
            achievementsVC.presentSelectedEmployee(employee: selectedEmployee)
            self.navigationController.popViewController(animated: true)
        }
        return stuffVC
    }
    
    /// фабрика, которая возвращает профиль сотрудника по тапу в таблице реестра сотрудников
    /// - Parameter employee: сотрудник, который был выбран
    /// - Returns: возвращает view controller профиля
    private func createProfileViewController(_ employee: Employee) -> UIViewController {
        let profileVC = EmployeeProfileAssembler.createModule(isUserProfile: false,
                                                              employee: employee)
        profileVC.openSettings = { [weak self] in
            guard let self = self else { return }
            let settingsScreen = self.createSettingScreen()
            self.navigationController.pushViewController(settingsScreen, animated: true)
        }
        return profileVC
    }
    
    /// фабрика, которая возвращает view controller экрана настройки
    /// - Returns: возвращает view controller экрана настройки
    private func createSettingScreen() -> UIViewController {
        let settingsVC = ProfileSettingsAssembler.createModule()
        settingsVC.logOut = { [weak self] in
            guard let self = self else { return }
            self.navigationController.popToRootViewController(animated: true)
            self.didFinish?()
        }
        settingsVC.openLink = { [weak self] webLink in
            guard let self = self,
                  // swiftlint:disable all
                  let policyAndTermsVC = PolicyAndTermsViewController() as? PolicyAndTermsViewControllerProtocol else { return }
                  // swiftlint:enable all
            policyAndTermsVC.showWebView(webLink)
            guard let policyAndTermsVC = policyAndTermsVC as? PolicyAndTermsViewController else { return }
            self.navigationController.present(policyAndTermsVC, animated: true, completion: nil)
        }
        return settingsVC
    }
    
    private func createSharePainVC() -> UIViewController {
        let sharePainVC = SharePainAssembler.createModule()
        return sharePainVC
    }
    
    /// метод  запускает флоу по созданию нового отпуска и отображения прошедших отпусков
    /// - Parameters:
    ///   - vacationsList: список прошедших отпусков
    ///   - vacationsOfColleaguesList: список прошедших отпусков у коллег
    /// - Returns: метод возвращает экран со списком прошедших отпуском а так же запускает флоу по созданию нового отпуска
    private func createVacationModule(vacationsList: [VacationModel], vacationsOfColleaguesList: [VacationModel]) -> UIViewController {
        let vacationViewController = VacationBookingModuleAssembler.createModule(vacationsRepository: self.vacationsRepository,
                                                                                 vacationsList: vacationsList,
                                                                                 vacationsOfColleaguesList: vacationsOfColleaguesList)
        vacationViewController.didTapButton = { [weak self] in
            guard let self = self else { return }
            let calendarVC = self.createCalendarViewController()
            self.navigationController.present(calendarVC, animated: true, completion: nil)
        }
        
        vacationViewController.didTapDatePicker = { [weak self] in
            guard let self = self else { return }
            let calendarVC = self.createCalendarViewController()
            self.navigationController.present(calendarVC, animated: true, completion: nil)
        }
        
        return vacationViewController
    }
    
    /// фабрика, которая возвращает view controller экрана наградить сотрудника
    /// - Parameters:
    ///   - type: enum, который сообщает, что запускается именно флоу наград
    ///   - employees: массив сотрудников для отображения в таблице
    ///   - positions: массив позиций для нэйминга секций
    ///   - projects: массив проектов для фильтров
    /// - Returns: возвращает view controller экрана наградить сотрудника
    private func createAchievementsVC(type: StuffListOpenType, employees: [Employee], positions: [Position], projects: [ProjectsRegistryResponseModel]) -> UIViewController {
        let createAchievementsVC = AchievementsAssembler.createModule(employeeList: employees, positionsList: positions, projectsList: projects)
        createAchievementsVC.openStuffListScreen = { [weak self] in
            guard let self = self else { return }
            let stuffListScreen = self.createStuffListVC(type: .achievements, employees: employees, positions: positions, projects: projects)
            self.navigationController.pushViewController(stuffListScreen, animated: true)
        }
        return createAchievementsVC
    }
    
    /// фабрика, которая возвращает view controller экрана подробного описания проекта
    /// - Parameter project: проект, данные по которому будут отображаться
    /// - Returns: возвращает view controller экрана подробного описания проекта
    private func createProjectViewController(_ project: ProjectsRegistryResponseModel) -> UIViewController {
        let projectVC = AboutProjectScreenAssembler.createModule(project)
        return projectVC
    }
    
    /// фабрика, которая возвращает view controller (календарь)
    /// - Returns: возвращает view controller 
    private func createCalendarViewController() -> UIViewController {
        let calendarVC = CalendarScreenAssembler.createModule()
        calendarVC.didTapDateButton = { [weak self] startDate, finishDate in
            guard let self = self else { return }
            // swiftlint:disable all
            // TODO: тут Сергей прописываешь открытие своего модуля заявки на отпуск
            // swiftlint:enable all
        }
        return calendarVC
    }
    
}

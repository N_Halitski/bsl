//
//  AuthCoordinator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 18.11.21.
//

import UIKit

final class AuthCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    
    // MARK: - Callbacks
    
    var didFinish: (() -> Void)?
    var openPasswordRecoveryScreen: (() -> Void)?
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    ///  запускает флоу авторизации
    func start() {
        let authViewController = createAuthViewController()
        navigationController.pushViewController(authViewController, animated: true)
    }
    
    func stop() {
        navigationController.viewControllers.removeAll()
    }
    
}

// MARK: - Extension. Сontains methods for creating various modules

private extension AuthCoordinator {
    
    /// фабрика, которая возвращает контроллер авторизации
    /// - Returns: возвращает контроллер авторизации
    func createAuthViewController() -> UIViewController {
        let authViewController = LoginScreenAssembler.createModule(didFinish: didFinish, openPasswordRecoveryScreen: openPasswordRecoveryScreen)
        return authViewController
    }
    
}

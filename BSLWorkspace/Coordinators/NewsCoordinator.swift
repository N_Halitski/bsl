//
//  NewsCoordinator.swift
//  BSLWorkspace
//
//  Created by Nikita on 17.11.2021.
//

import UIKit

final class NewsCoordinator: TabBarPresentableCoordinator {
    
    /// иконка таб бара с разными состояними
    var tabBarItem: UITabBarItem = {
        let title = "news"
        let image = UIImage(named: "news")
        let selectedImage = UIImage(named: "newsSelected")
        let item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        return item
    }()
    
    /// репозиторий для экрана новостей, с помощью которого делается запрос
    private var newsRepository: NewsRepositoryProtocol = NewsRepository()
    
    var navigationController: UINavigationController
    
    // MARK: - Callbacks
    
    var didFinish: (() -> Void)?
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    /// запускает флоу новостей
    func start() {
        let newsFeedModule = createNewsFeedModule()
        navigationController.pushViewController(newsFeedModule, animated: true)
    }
    
    func stop() {
        navigationController.popToRootViewController(animated: false)
    }
}

// MARK: - Extension. Сontains methods for creating various modules

extension NewsCoordinator {
    /// фабрика, которая возвращает контроллер новостей
    /// - Returns: возвращает контроллер новостей
    private func createNewsFeedModule() -> UIViewController {
        let newsFeedViewController = NewsFeedModuleAssembler.createModule(newsRepository: self.newsRepository)
        newsFeedViewController.openNewsPageScreen = { [ weak self ] newsList in
            guard let self = self else { return }
            let newsPageModule = self.createNewsPageModule(newsList: newsList)
            self.navigationController.pushViewController(newsPageModule, animated: true)
        }
        return newsFeedViewController
    }
    
    /// фабрика, которая возвращает экран с подробным описанием новости
    /// - Parameter newsList: объект новости
    /// - Returns: озвращает экран с подробным описанием новости
    private func createNewsPageModule(newsList: NewsList) -> UIViewController {
        let newsPageViewController = NewsPageModuleAssembler.createModule(newsList: newsList)
        newsPageViewController.openNewsPhotosScreen = { [ weak self ] photoArray, displayedPhotoNumber in
            guard let self = self else { return }
            let newsPhotosModule = self.createNewsPhotosModule(photoArray: photoArray, displayedPhotoNumber: displayedPhotoNumber)
            self.navigationController.pushViewController(newsPhotosModule, animated: true)
        }
        return newsPageViewController
    }
    
    /// фабрика, которая возвращает экран с каруселью фото
    /// - Parameters:
    ///   - photoArray: массив url для отображения фото
    ///   - displayedPhotoNumber: номер фото, на которую нажали
    /// - Returns: возвращает экран с каруселью фото
    private func createNewsPhotosModule(photoArray: [String]?, displayedPhotoNumber: Int) -> UIViewController {
        let newsPhotosViewController = NewsPhotosModuleAssembler.createModule()
        newsPhotosViewController.popViewController = { [ weak self ] in
            guard let self = self else { return }
            self.navigationController.popViewController(animated: true)
        }
        return newsPhotosViewController
    }
}

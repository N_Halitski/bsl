//
//  ProfileCoordinator.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 22.11.21.
//

import UIKit

final class ProfileCoordinator: TabBarPresentableCoordinator {
    
    /// иконка таб бара с разными состояними
    var tabBarItem: UITabBarItem = {
        let title = "profile"
        let image = UIImage(named: "profile")
        let selectedImage = UIImage(named: "profileSelected")
        let item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        return item
    }()
    
    var navigationController: UINavigationController
    var isUserProfile = true
    
    // MARK: - Callbacks
    
    var didFinish: (() -> Void)?
    var logOut: (() -> Void)?
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    ///  запускает флоу профиля
    func start() {
        let profileVC = createProfileViewController()
        navigationController.pushViewController(profileVC, animated: true)
    }
    
    func stop() {
        navigationController.viewControllers.removeAll()
    }
    
}

// MARK: - Extension. Сontains methods for creating various modules

extension ProfileCoordinator {
    
    /// фабрика, которая возвращает view controller профиля
    /// - Returns: возвращает view controller профиля
    private func createProfileViewController() -> UIViewController {
        let profileVC = EmployeeProfileAssembler.createModule(isUserProfile: isUserProfile,
                                                              employee: nil)
        profileVC.openSettings = { [weak self] in
            guard let self = self else { return }
            let settingsScreen = self.createSettingScreen()
            self.navigationController.pushViewController(settingsScreen, animated: true)
        }
        return profileVC
    }
    
    /// фабрика, которая возвращает view controller настроек профиля
    /// - Returns: возвращает view controller настроек профиля
    private func createSettingScreen() -> UIViewController {
        let settingsVC = ProfileSettingsAssembler.createModule()
        settingsVC.logOut = { [weak self] in
            guard let self = self else { return }
            self.navigationController.popToRootViewController(animated: true)
            self.didFinish?()
        }
        settingsVC.openLink = { [weak self] webLink in
            guard let self = self,
                  // swiftlint:disable all
                  let policyAndTermsVC = PolicyAndTermsViewController() as? PolicyAndTermsViewControllerProtocol else { return }
                  // swiftlint:enable all
            policyAndTermsVC.showWebView(webLink)
            guard let policyAndTermsVC = policyAndTermsVC as? PolicyAndTermsViewController else { return }
            self.navigationController.present(policyAndTermsVC, animated: true, completion: nil)
        }
        return settingsVC
    }
    
}

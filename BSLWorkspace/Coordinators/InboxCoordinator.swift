//
//  InboxCoordinator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 18.11.21.
//

import UIKit

final class InboxCoordinator: TabBarPresentableCoordinator {
    
    /// иконка таб бара с разными состояними
    var tabBarItem: UITabBarItem = {
        let title = "inbox"
        let image = UIImage(named: "inbox")
        let selectedImage = UIImage(named: "inboxSelected")
        let item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        return item
    }()
    
    /// репозиторий для экрана inbox, с помощью которого делается запрос
    private var inboxRepository: InboxRepositoryProtocol = InboxRepository()
    
    var navigationController: UINavigationController
    
    var didFinish: (() -> Void)?
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    ///  запускает флоу инбокс
    func start() {
        let inboxVC = createInboxViewController()
        navigationController.pushViewController(inboxVC, animated: true)
    }
    
    func stop() {
        navigationController.viewControllers.removeAll()
    }
    
}

// MARK: - Extension. Сontains methods for creating various modules

private extension InboxCoordinator {
    
    /// фабрика, которая возвращает инбокс вью контроллер
    /// - Returns: возвращает инбокс вью контроллер
    func createInboxViewController() -> UIViewController {
        let inboxViewController = InboxModuleAssembler.createModule(inboxRepository: self.inboxRepository)
        inboxViewController.showRequestForVacation = { [weak self] in
            guard let self = self else { return }
            let processingRequestVacation = self.createRequestVacationViewController()
            self.navigationController.present(processingRequestVacation, animated: true, completion: nil)
        }
        return inboxViewController
    }
    
    ///  фабрика, которая возвращает экран отпусков
    /// - Returns: возвращает экран отпусков
    private func createRequestVacationViewController() -> UIViewController {
        let createRequestVacationVC = ProcessingRequestVacationAssembler.createModule()
        return createRequestVacationVC
    }
    
}

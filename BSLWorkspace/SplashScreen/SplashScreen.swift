//
//  SplashScreen.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 15.12.21.
//

import UIKit

final class SplashScreen: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigationBarSeparator()
    }
    
    func hideNavigationBarSeparator() {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.shadowColor = .clear
        navBarAppearance.shadowImage = UIImage()
        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
    }
}

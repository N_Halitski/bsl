//
//  InstrumentsInstrumentsPresenterOutput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol InstrumentsPresenterOutput: AnyObject {
    func openStuffListScreen(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel])
    func openVacationScreen(_ vacationsList: [VacationModel], _ vacationsOfColleaguesList: [VacationModel])
    func openSharePainScreen()
    func startLoader()
    func stopLoader()
    func didTapAchievements(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel])
}

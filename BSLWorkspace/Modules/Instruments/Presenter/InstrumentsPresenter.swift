//
//  InstrumentsInstrumentsPresenter.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import os.log

final class InstrumentsPresenter {
    
    private let interactor: InstrumentsInteractorInput
    private unowned let view: InstrumentsPresenterOutput
    
    // MARK: - Callbacks
    
    /// замыкание, которое реализует открытие экрана реестра проектов
    private var openProjectRegisterScreen: (([ProjectsRegistryResponseModel]?) -> Void)?
    
    init(_ interactor: InstrumentsInteractorInput,
         _ view: InstrumentsPresenterOutput,
         _ openProjectRegisterScreen: (([ProjectsRegistryResponseModel]?) -> Void)?) {
        self.interactor = interactor
        self.view = view
        self.openProjectRegisterScreen = openProjectRegisterScreen
    }
    
    // MARK: - Lifecycle
    
    func viewDidLoad() {
        interactor.attach(self)
        interactor.presentInstrumentsCell()
    }
    
}

extension InstrumentsPresenter: InstrumentsPresenterInput {
    
    /// перекидывает таблицу из view в interactor
    /// - Parameter table: таблица
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
    
}

extension InstrumentsPresenter: InstrumentsInteractorOutput {
    
    /// метод, который сообщает контроллеру, что будет открыт экран наградить
    /// - Parameters:
    ///   - employeeList: список сотрудников для выбора того, кому будет вручена награда
    ///   - positionsList: список позиций для заголовка секций в списке сотрудников
    ///   - projectList: список проектов для фильтров
    func didTapAchievements(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel]) {
        view.didTapAchievements(employeeList, positionsList, projectList)
    }
    
    /// метод, который сообщает контроллеру, что будет открыт экран поделиться болью
    func didTapSharePain() {
        view.openSharePainScreen()
    }
    
    /// метод сообщающий о старте работы лоудера
    func startLoader() {
        view.startLoader()
    }
    
    /// метод сообщающий об остановке лоудера
    func stopLoader() {
        view.stopLoader()
    }
    
    /// метод, который сообщает контроллеру, что будет открыт экран реестра сотрудников
    /// - Parameters:
    ///   - employeeList: список сотрудников для отображения в рессетре сотрудников
    ///   - positionsList: список позиций для секций в реестре сотрудников
    ///   - projectList: список проектов для фильтров в реестре сотрудников
    func didTapRoster(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel]) {
        view.openStuffListScreen(employeeList, positionsList, projectList)
    }
    
    /// метод, который сообщает контроллеру, что будет открыт экран отпуск
    /// - Parameters:
    ///   - vacationsList: список прошедших отпусков
    ///   - vacationsOfColleaguesList: список прошедших отпусков у коллег
    func didTapVacation(_ vacationsList: [VacationModel], _ vacationsOfColleaguesList: [VacationModel]) {
        view.openVacationScreen(vacationsList, vacationsOfColleaguesList)
    }
    
    /// метод, который сообщает, что будет открыт экран реестр проектов
    func didReceiveProjectsList(with result: Result<[ProjectsRegistryResponseModel]?, ProjectsAPIError>) {
        switch result {
        case .success(let projectsList):
            DispatchQueue.main.async {
                self.openProjectRegisterScreen?(projectsList)
                self.view.stopLoader()
            }
        case .failure(let error):
            os_log("@", error.localizedDescription)
        }
    }
}

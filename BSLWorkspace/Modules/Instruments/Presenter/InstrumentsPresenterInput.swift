//
//  InstrumentsInstrumentsPresenterInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol InstrumentsPresenterInput: BasePresenting {
    func attachTableView(_ table: UITableView)
}

//
//  InstrumentsCellModel.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.11.21.
//

import UIKit

struct InstrumentsCellModel {
    let icon: String
    let title: String
    let description: String
    let type: InstrumentsCellType?
}

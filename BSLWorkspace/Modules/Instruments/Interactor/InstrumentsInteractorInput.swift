//
//  InstrumentsInstrumentsInteractorInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol InstrumentsInteractorInput: AnyObject {
    func attach(_ output: InstrumentsInteractorOutput)
    func attachTableView(_ table: UITableView)
    func presentInstrumentsCell()
}

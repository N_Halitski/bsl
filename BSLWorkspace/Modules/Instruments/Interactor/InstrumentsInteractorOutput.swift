//
//  InstrumentsInstrumentsInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol InstrumentsInteractorOutput: AnyObject {
    func didTapRoster(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel])
    func didTapVacation(_ vacationsList: [VacationModel], _ vacationsOfColleaguesList: [VacationModel])
    func didReceiveProjectsList(with result: Result<[ProjectsRegistryResponseModel]?, ProjectsAPIError>)
    func startLoader()
    func stopLoader()
    func didTapSharePain()
    func didTapAchievements(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel])
}

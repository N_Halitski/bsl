//
//  InstrumentsInstrumentsInteractor.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import AVFoundation

final class InstrumentsInteractor {

    private weak var output: InstrumentsInteractorOutput?
    /// табличный менеджер
    private var tableManager: InstrumentsTableManagerProtocol
    /// репозиторий для запроса за моедлями для экрана инструментов
    private var instrumentsRepository: InstrumentsAPIRepositoryProtocol
    /// репозиторий для реестра сотрулников
    private var stuffListRepository: StuffListRepositoryProtocol
    /// репозиторий для реестра проектов
    private var projectsListRepository: ProjectsRepositoryProtocol
    /// репозиторий для экрана отпусков
    private var vacationRepository: VacationsRepositoryProtocol
    /// массив списка сотрудников
    private var employeeList: [Employee] = []
    /// массив позиций
    private var positionsList: [Position] = []
    /// массив проектов
    private var projectsList: [ProjectsRegistryResponseModel] = []
    /// массив отпусков авторизованного пользователя
    private var vacationsList: [VacationModel] = []
    /// массив отпусков команды авторизованного пользователя
    private var vacationsOfColleaguesList: [VacationModel] = []
    /// диспач груп для синхронизации нескольких запросов
    private let employeeRosterDispatchGroup = DispatchGroup()
    /// диспач груп для синхронизации нескольких запросов
    private let vacationsDispatchGroup = DispatchGroup()

    init(tableManager: InstrumentsTableManagerProtocol,
         vacationRepository: VacationsRepositoryProtocol,
         stuffListRepository: StuffListRepositoryProtocol,
         projectsListRepository: ProjectsRepositoryProtocol,
         instrumentsRepository: InstrumentsAPIRepositoryProtocol = InstrumentsAPIRepository()) {
        self.instrumentsRepository = instrumentsRepository
        self.stuffListRepository = stuffListRepository
        self.projectsListRepository = projectsListRepository
        self.vacationRepository = vacationRepository
        self.tableManager = tableManager
        self.tableManager.didTapProjects = { [weak self] in
            guard let self = self else { return }
            self.getProjectsForRoster()
            self.output?.startLoader()
        }
        self.tableManager.didTapRoster = { [weak self] in
            guard let self = self else { return }
            self.fetchDataForStuffRoster()
            self.output?.startLoader()
        }
        self.tableManager.didTapVacation = { [weak self] in
            guard let self = self else { return }
            self.fetchDataForVacationScreen()
        }
        self.tableManager.didTapSharePain = { [weak self] in
            guard let self = self else { return }
            self.output?.didTapSharePain()
        }
        self.tableManager.didTapAchievements = { [weak self] in
            guard let self = self else { return }
            self.fetchDataForAchievements()
            self.output?.startLoader()
        }
    }
    
    /// запрос для получения списка сотрудников
    private func getEmployees() {
        employeeRosterDispatchGroup.enter()
        self.stuffListRepository.getEmployees { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let employeeList):
                self.employeeRosterDispatchGroup.leave()
                self.employeeList = employeeList
            case .failure:
                self.employeeRosterDispatchGroup.leave()
            }
        }
    }

    ///  запрос для получения списка позиций
    private func getPositions() {
        employeeRosterDispatchGroup.enter()
        self.stuffListRepository.getPositionsList { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let positionsList):
                self.employeeRosterDispatchGroup.leave()
                self.positionsList = positionsList
            case .failure:
                self.employeeRosterDispatchGroup.leave()
            }
        }
    }
    
    /// запрос для получения списка проектов
    private func getProjects() {
        employeeRosterDispatchGroup.enter()
        self.projectsListRepository.getProjects { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let projectsList):
                guard let projectsList = projectsList else { return }
                self.projectsList = projectsList
                self.employeeRosterDispatchGroup.leave()
            case .failure:
                self.employeeRosterDispatchGroup.leave()
            }
        }
    }
    
    /// запрос для получения списка отпусков авторизованного пользователя
    private func getVacations() {
        self.vacationsDispatchGroup.enter()
        self.vacationRepository.getVacations { [weak self] result in
            guard let self = self else { return }
            self.vacationsDispatchGroup.leave()
            switch result {
            case .success(let data):
                guard let data = data else { return }
                self.vacationsList = data
            case .failure:
                break
            }
        }
    }
    
    /// запрос для получения списка отпусков команды авторизованного пользователя
    private func getVacationsOfColleagues() {
        self.vacationsDispatchGroup.enter()
        self.vacationRepository.getVacationsOfColleagues { [weak self] result in
            guard let self = self else { return }
            self.vacationsDispatchGroup.leave()
            switch result {
            case .success(let data):
                guard let data = data else { return }
                self.vacationsOfColleaguesList = data
            case .failure:
                break
            }
        }
    }
    
    /// метод для синхранизаци запросов для экрана отпусков
    func fetchDataForVacationScreen() {
        self.output?.startLoader()
        getVacations()
        getVacationsOfColleagues()
        vacationsDispatchGroup.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            self.output?.didTapVacation(self.vacationsList, self.vacationsOfColleaguesList)
            self.output?.stopLoader()
        }
    }
    
    /// метод для синхранизаци запросов для экрана реестра сотрудников
    func fetchDataForStuffRoster() {
        getEmployees()
        getPositions()
        getProjects()

        employeeRosterDispatchGroup.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            self.output?.didTapRoster(self.employeeList, self.positionsList, self.projectsList)
            self.output?.stopLoader()
        }
    }
    
    /// метод для синхранизаци запросов для экрана наградить сотрудника
    func fetchDataForAchievements() {
        getEmployees()
        getPositions()
        getProjects()
        employeeRosterDispatchGroup.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            self.output?.didTapAchievements(self.employeeList, self.positionsList, self.projectsList)
            self.output?.stopLoader()
        }
    }
    
    /// метод по запросу для реестра проектов
    private func getProjectsForRoster() {
        projectsListRepository.getProjects { [weak output] result in
            output?.didReceiveProjectsList(with: result)
        }
    }

}

extension InstrumentsInteractor: InstrumentsInteractorInput {
    
    /// метод, который создает ячейки таблицы исходя из данных, полученных от сервера
    func presentInstrumentsCell() {
        self.output?.startLoader()
        instrumentsRepository.getCelllsWithLevelAccess { [weak self] result in
            guard var cellDataWithLevelAccess = self?.instrumentsCellData else { return }
            switch result {
            case .success(let response):
                guard let responseModel = response?.userPermissions else { return }
                responseModel.forEach {
                    switch $0 {
                    case .award:
                        cellDataWithLevelAccess.insert(.init(icon: "🏆", title: "Наградить", description: "Награждение сотрудников", type: .awards), at: 2)
                    case .projects:
                        cellDataWithLevelAccess.append(.init(icon: "📁", title: "Реестр проектов", description: "Активные проекты компании", type: .projects))
                    case .users:
                        cellDataWithLevelAccess.append(.init(icon: "👨‍💻", title: "Реестр сотрудников", description: "Список сотрудников", type: .staff))
                    }
                }
                SharedEmployeeModel.shared.employee = response
            case .failure:
                break
            }
            self?.tableManager.presentInstrumentsCell(with: cellDataWithLevelAccess)
            self?.output?.stopLoader()
        }
    }
    
    /// передает таблицу из presenter в таболичный менеджер
    /// - Parameter table: таблица
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
    }

    func attach(_ output: InstrumentsInteractorOutput) {
        self.output = output
    }
}

private extension InstrumentsInteractor {
    
    /// дефолтный сет ячеек для отображения на экране инструментов
    var instrumentsCellData: [InstrumentsCellModel] {
        [
            .init(icon: "🌴", title: "Отпуск", description: "Бронирование отпусков", type: .vacation),
            .init(icon: "🤕", title: "Поделиться болью", description: "Расскажи о проблеме", type: .sharePain)
        ]
    }
}

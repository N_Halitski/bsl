//
//  InstrumentsTableConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.11.21.
//

import UIKit

enum InstrumentsCellType {
    case vacation
    case sharePain
    case awards
    case projects
    case staff
}

protocol InstrumentsConfiguratorProtocol: BaseConfigurator {
    var instrumentsCellModel: InstrumentsCellModel? { get set }
}

final class InstrumentsTableConfigurator: InstrumentsConfiguratorProtocol {
    
    /// хранит переиспользуемый id конкретной ячейки
    var reuseId: String = String(describing: InstrumentsTableViewCell.self)
    /// модель, которая передается и настраивается в классе ячейки
    var instrumentsCellModel: InstrumentsCellModel?
    
    /// метод, который передает модель в ячейку для настрйоки
    /// - Parameter instrumentsCell: модель для ячейки
    func setupCell(_ instrumentsCell: UIView) {
        guard let instrumentsCell = instrumentsCell as? InstrumentsTableViewCellProtocol,
              let instrumentsCellModel = instrumentsCellModel else { return }
        instrumentsCell.display(instrumentsCellModel)
    }
}

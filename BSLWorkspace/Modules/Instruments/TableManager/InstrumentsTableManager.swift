//
//  InstrumentsTableManager.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.11.21.
//

import UIKit

protocol InstrumentsTableManagerProtocol {
    var didTapProjects: (() -> Void)? { get set }
    var didTapRoster: (() -> Void)? { get set }
    var didTapVacation: (() -> Void)? { get set }
    var didTapSharePain: (() -> Void)? { get set }
    var didTapAchievements: (() -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func presentInstrumentsCell(with model: [InstrumentsCellModel])
}

final class InstrumentsTableManager: NSObject, InstrumentsTableManagerProtocol {
    
    // MARK: - Callbacks
    
    var didTapRoster: (() -> Void)?
    var didTapProjects: (() -> Void)?
    var didTapVacation: (() -> Void)?
    var didTapSharePain: (() -> Void)?
    var didTapAchievements: (() -> Void)?
    
    private weak var instrumentsTableView: UITableView?
    /// массив конфигураторов, который заполняет таблицу
    private var configurators: [InstrumentsConfiguratorProtocol] = [] {
        didSet {
            instrumentsTableView?.reloadData()
        }
    }
    
    /// метод, который передает таблицу в табличный менеджер
    /// - Parameter tableView: таблица
    func attachTable(_ tableView: UITableView) {
        tableView.backgroundColor = AppTheme.Colors.lightGray100
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        let instrumentsCellNib = UINib(nibName: String(describing: InstrumentsTableViewCell.self), bundle: nil)
        tableView.register(instrumentsCellNib, forCellReuseIdentifier: String(describing: InstrumentsTableViewCell.self))
        self.instrumentsTableView = tableView
    }
    
    ///  передает объекты для заполнения массива конфигураторов, чтобы отобразить таблицу
    /// - Parameter model: массив моделей для экрана инструментов
    func presentInstrumentsCell(with model: [InstrumentsCellModel]) {
        self.configurators = model.map {
            return createInstrumentsCellConfigurator($0)
        }
    }
    
    /// фабрика, которая создает конфигуратор для экрана инструментов
    /// - Parameter instrumentsCellModel: модель для настройки одной ячейки
    /// - Returns: конфигуратор  инструментов
    private func createInstrumentsCellConfigurator(_ instrumentsCellModel: InstrumentsCellModel) -> InstrumentsConfiguratorProtocol {
        let configurator = InstrumentsTableConfigurator()
        configurator.instrumentsCellModel = instrumentsCellModel
        return configurator
    }
}

extension InstrumentsTableManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cellType = configurators[indexPath.row].instrumentsCellModel?.type else { return }
        switch cellType {
        case .awards:
            didTapAchievements?()
        case .sharePain:
            didTapSharePain?()
        case .vacation:
            didTapVacation?()
        case .projects:
            didTapProjects?()
        case .staff:
            didTapRoster?()
        }
    }
}

extension InstrumentsTableManager: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let instrumentsTableViewCell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(instrumentsTableViewCell)
        return instrumentsTableViewCell
    }
}

//
//  InstrumentsInstrumentsViewController.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class InstrumentsViewController: BaseViewController {
    
    // MARK: - Outlets
    
    /// основная таблица
    @IBOutlet private var instrumentsTableView: UITableView!
    /// вью кастомного навигейшен бара
    @IBOutlet private var customNavigationBarView: UIView!
    /// лэйбл кастомного навигейшен бара
    @IBOutlet private var customNavigationBarTitleLabel: UILabel!
    
    // MARK: - Dependencies
    
    var presenter: InstrumentsPresenterInput!
    
    // MARK: - Callbacks
    
    /// колбэк на открытие реестра сотрудников
    var openStuffListScreenCallBack: ((_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel]) -> Void)?
    /// колбэк на открытие экрана отпусков
    var openVacationScreen: ((_ vacationsList: [VacationModel], _ vacationsOfColleaguesList: [VacationModel]) -> Void)?
    /// колбэк на открытие экрана поделиться болью
    var openSharePainScreenCallBack: (() -> Void)?
    /// колбэк на открытие экрана поделиться болью
    var openAchievementsScreen: ((_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel]) -> Void)?

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppTheme.Colors.lightGray100
        presenter.viewDidLoad()
        presenter.attachTableView(instrumentsTableView)
        setupCustomNavigationBar()
    }
    
}

     // MARK: - InstrumentsViewController UI

private extension InstrumentsViewController {
    
    func setupCustomNavigationBar() {
        customNavigationBarView.backgroundColor = AppTheme.Colors.lightGray100
        
        customNavigationBarTitleLabel.text = "Workspace"
        customNavigationBarTitleLabel.font = AppTheme.Fonts.SFSemiBold(30)
        customNavigationBarTitleLabel.textColor = AppTheme.Colors.black100
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = AppTheme.Colors.black100
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.shadowColor = .clear
        navBarAppearance.shadowImage = UIImage()
        navigationController?.navigationBar.standardAppearance = navBarAppearance
    }
    
}

extension InstrumentsViewController: InstrumentsPresenterOutput {
    
    /// метод, который сообщает контроллеру, что будет открыт экран наградить
    /// - Parameters:
    ///   - employeeList: список сотрудников для выбора того, кому будет вручена награда
    ///   - positionsList: список позиций для заголовка секций в списке сотрудников
    ///   - projectList: список проектов для фильтров
    func didTapAchievements(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel]) {
        openAchievementsScreen?(employeeList, positionsList, projectList)
    }
    
    /// метод, который сообщает контроллеру, что будет открыт экран реестра сотрудников
    /// - Parameters:
    ///   - employeeList: список сотрудников для отображения в рессетре сотрудников
    ///   - positionsList: список позиций для секций в реестре сотрудников
    ///   - projectList: список проектов для фильтров в реестре сотрудников
    func openStuffListScreen(_ employeeList: [Employee], _ positionsList: [Position], _ projectList: [ProjectsRegistryResponseModel]) {
        openStuffListScreenCallBack?(employeeList, positionsList, projectList)
    }
    
    /// метод, который сообщает контроллеру, что будет открыт экран поделиться болью
    func openSharePainScreen() {
        openSharePainScreenCallBack?()
    }
    
    /// метод, который сообщает контроллеру, что будет открыт экран отпуск
    /// - Parameters:
    ///   - vacationsList: список прошедших отпусков
    ///   - vacationsOfColleaguesList: список прошедших отпусков у коллег
    func openVacationScreen(_ vacationsList: [VacationModel], _ vacationsOfColleaguesList: [VacationModel]) {
        openVacationScreen?(vacationsList, vacationsOfColleaguesList)
    }
    
    /// функция начала анимации лоудера
    func startLoader() {
        showLoader(isShown: true)
    }
    
    /// функция остановки анимации лоудера
    func stopLoader() {
        showLoader(isShown: false)
    }
    
}

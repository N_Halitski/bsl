//
//  InstrumentsTableViewCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.11.21.
//

import UIKit

protocol InstrumentsTableViewCellProtocol {
    func display(_ instrumentsCellModel: InstrumentsCellModel)
}

final class InstrumentsTableViewCell: UITableViewCell, InstrumentsTableViewCellProtocol {
    
    // MARK: - Outlets
    
    /// view для ячеек
    @IBOutlet private var instrumentsCellView: UIView!
    /// лэйбл для названия ячейки
    @IBOutlet private var instrumentsCellTitleLabel: UILabel!
    /// описание ячейки
    @IBOutlet private var instrumentsCellDescriptionLabel: UILabel!
    /// икнока
    @IBOutlet private var iconLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
    }
    
    ///  метод, который заполняет ячейку
    /// - Parameter instrumentsCellModel: модель, которая содержит информацию для отображения
    func display(_ instrumentsCellModel: InstrumentsCellModel) {
        self.iconLabel.text = instrumentsCellModel.icon
        self.instrumentsCellTitleLabel.text = instrumentsCellModel.title
        self.instrumentsCellDescriptionLabel.text = instrumentsCellModel.description
    }
    
}

private extension InstrumentsTableViewCell {
    
    /// устанавливает вид ячейки
    func setupUI() {
        instrumentsCellView.backgroundColor = AppTheme.Colors.white100
        instrumentsCellView.layer.cornerRadius = 16
        instrumentsCellView.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: 0, height: 4), shadowOpacity: 0.6, shadowRadius: 36)
        
        iconLabel.backgroundColor = AppTheme.Colors.white100
        iconLabel.font = AppTheme.Fonts.SFMedium(26)
        iconLabel.layer.borderColor = AppTheme.Colors.gray100.cgColor
        iconLabel.layer.borderWidth = 1
        iconLabel.layer.cornerRadius = 8
        iconLabel.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: .zero, shadowOpacity: 1, shadowRadius: 18)
        
        instrumentsCellTitleLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        instrumentsCellDescriptionLabel.font = AppTheme.Fonts.SFRegular(14)
        instrumentsCellDescriptionLabel.textColor = AppTheme.Colors.darkGray
    }
    
}

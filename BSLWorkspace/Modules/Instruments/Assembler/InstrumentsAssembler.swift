//
//  InstrumentsInstrumentsAssembler.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class InstrumentsAssembler {
    
    /// метод создает контроллер инструментов
    /// - Parameters:
    ///   - tableManager: табличный менеджер инструментов
    ///   - stuffListRepository: репозиторий реестра сотрудников
    ///   - projectsListRepository: репозиторий реестра проектов
    ///   - vacationRepository: репозиторий отпусков
    ///   - openProjectRegisterScreen: замыкание для открытия экрана реестра проектов
    /// - Returns: контроллер инструментов
    static func createModule(
        tableManager: InstrumentsTableManagerProtocol = InstrumentsTableManager(),
        stuffListRepository: StuffListRepositoryProtocol = StuffListRepository(),
        projectsListRepository: ProjectsRepositoryProtocol = ProjectsRepository(),
        vacationRepository: VacationsRepositoryProtocol,
        openProjectRegisterScreen: (([ProjectsRegistryResponseModel]?) -> Void)?) -> InstrumentsViewController {
            let viewController = InstrumentsViewController()
            let interactor = InstrumentsInteractor(tableManager: tableManager,
                                                   vacationRepository: vacationRepository,
                                                   stuffListRepository: stuffListRepository,
                                                   projectsListRepository: projectsListRepository)
            let presenter = InstrumentsPresenter(interactor, viewController, openProjectRegisterScreen)
            viewController.presenter = presenter
            return viewController
        }
}

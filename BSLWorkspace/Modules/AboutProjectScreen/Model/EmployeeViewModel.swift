//
//  EmployeeModel.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 26.11.21.
//

import UIKit

struct EmployeeViewModel {
    var photo: UIImage
    var name: String
    var position: String
}

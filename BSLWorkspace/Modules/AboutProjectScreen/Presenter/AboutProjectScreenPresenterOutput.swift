//
//  AboutProjectScreenAboutProjectScreenPresenterOutput.swift
//  BSLWorkspace
//
//  Created by clear on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol AboutProjectScreenPresenterOutput: AnyObject {
    func setupUI()
    func configureProjectView(with project: ProjectsRegistryResponseModel)
}

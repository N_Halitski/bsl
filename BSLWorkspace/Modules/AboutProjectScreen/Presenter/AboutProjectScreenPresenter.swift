//
//  AboutProjectScreenAboutProjectScreenPresenter.swift
//  BSLWorkspace
//
//  Created by clear on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class AboutProjectScreenPresenter {
    
    private let interactor: AboutProjectScreenInteractorInput
    private unowned let view: AboutProjectScreenPresenterOutput
    
    init(_ interactor: AboutProjectScreenInteractorInput,
         _ view: AboutProjectScreenPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }
    
    func viewDidLoad() {
        interactor.attach(self)
        view.setupUI()
    }
    
}

extension AboutProjectScreenPresenter: AboutProjectScreenPresenterInput {
    
}

extension AboutProjectScreenPresenter: AboutProjectScreenInteractorOutput {
    
    func didReceiveModel(with model: ProjectsRegistryResponseModel) {
        view.configureProjectView(with: model)
    }
    
}

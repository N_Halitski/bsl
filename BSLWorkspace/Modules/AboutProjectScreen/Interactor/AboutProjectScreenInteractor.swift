//
//  AboutProjectScreenAboutProjectScreenInteractor.swift
//  BSLWorkspace
//
//  Created by clear on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class AboutProjectScreenInteractor {
    
    private weak var output: AboutProjectScreenInteractorOutput?
    var project: ProjectsRegistryResponseModel
    
    init(_ project: ProjectsRegistryResponseModel) {
        self.project = project
    }

}

extension AboutProjectScreenInteractor: AboutProjectScreenInteractorInput {
    
    func attach(_ output: AboutProjectScreenInteractorOutput) {
        self.output = output
        output.didReceiveModel(with: self.project)
    }
    
}

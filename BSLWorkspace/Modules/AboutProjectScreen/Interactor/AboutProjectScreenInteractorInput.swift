//
//  AboutProjectScreenAboutProjectScreenInteractorInput.swift
//  BSLWorkspace
//
//  Created by clear on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol AboutProjectScreenInteractorInput: AnyObject {
    func attach(_ output: AboutProjectScreenInteractorOutput)
}

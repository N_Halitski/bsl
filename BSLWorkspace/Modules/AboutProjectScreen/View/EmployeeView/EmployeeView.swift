//
//  EmployeeView.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 26.11.21.
//

import UIKit

final class EmployeeView: XibBasedView {
    
    @IBOutlet private var employeeImageView: UIImageView!
    @IBOutlet private var employeeNameLabel: UILabel!
    @IBOutlet private var employeePositionLabel: UILabel!
    
    private enum Constants {
        static let radius: CGFloat = 16
    }
    
    override func setupUI() {
        employeeImageView.layer.cornerRadius = Constants.radius
    }
    
    func configure(with employee: EmployeeViewModel) {
        employeeImageView.image = employee.photo
        employeeNameLabel.text = employee.name
        employeePositionLabel.text = employee.position
    }
}

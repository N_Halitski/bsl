//
//  AboutProjectScreenAboutProjectScreenViewController.swift
//  BSLWorkspace
//
//  Created by clear on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import Kingfisher

final class AboutProjectScreenViewController: BaseViewController {
    
    @IBOutlet private var projectLabel: UILabel!
    @IBOutlet private var typeProjectLabel: UILabel!
    @IBOutlet private var statusProjectLabel: UILabel!
    @IBOutlet private var teamListLabel: UILabel!
    @IBOutlet private var aboutProjectLabel: UILabel!
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var statusProjectView: UIView!
    @IBOutlet private var lowerView: UIView!
    
    @IBOutlet private var firstEmployeeView: EmployeeView!
    @IBOutlet private var secondEmployeeView: EmployeeView!
    @IBOutlet private var thirdEmployeeView: EmployeeView!
    
    @IBOutlet private var navigationBar: UINavigationBar!
    @IBOutlet private var projectImageView: UIImageView!
    @IBOutlet private var lookAllButton: UIButton!
    @IBOutlet private var aboutProjectTextView: UITextView!
    
    private enum Constants {
        static let cornerRadius8: CGFloat = 8
        static let cornerRadius16: CGFloat = 16
        static let isActiveStatus: String = "is_active"
        static let onHoldStatus: String = "on_hold"
        static let completedStatus: String = "completed"
    }
    
    var presenter: AboutProjectScreenPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    private func setStatusView(_ status: String?) {
        switch status {
        case Constants.isActiveStatus:
            configureStatusView(backgroundColor: AppTheme.Colors.lightGreen,
                                textColor: AppTheme.Colors.green100,
                                text: "AboutProjectScreen.isActive".localized)
        case Constants.onHoldStatus:
            configureStatusView(backgroundColor: AppTheme.Colors.redStatus2,
                                textColor: AppTheme.Colors.red100,
                                text: "AboutProjectScreen.onHold".localized)
        case Constants.completedStatus:
            configureStatusView(backgroundColor: AppTheme.Colors.lightYellow100,
                                textColor: AppTheme.Colors.darkYellow100,
                                text: "AboutProjectScreen.completed".localized)
        default:
            configureStatusView(backgroundColor: .clear,
                                textColor: .clear,
                                text: "")
        }
    }
    
    private func configureStatusView(backgroundColor: UIColor,
                                     textColor: UIColor,
                                     text: String) {
        statusProjectView.backgroundColor = backgroundColor
        statusProjectLabel.textColor = textColor
        statusProjectLabel.text = text
    }
}

extension AboutProjectScreenViewController: AboutProjectScreenPresenterOutput {
    
    func setupUI() {
        lookAllButton.setTitle("AboutProjectScreen.lookAll".localized, for: .normal)
        teamListLabel.text = "AboutProjectScreen.teamList".localized
        aboutProjectLabel.text = "AboutProjectScreen.aboutProject".localized
        
        projectImageView.layer.cornerRadius = Constants.cornerRadius16
        statusProjectView.layer.cornerRadius = Constants.cornerRadius8
        lowerView.layer.cornerRadius = Constants.cornerRadius16
        
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: AppTheme.Colors.black100]
        appearance.backgroundColor  = AppTheme.Colors.lightGray100
        appearance.shadowColor = .clear
        navigationBar.standardAppearance = appearance
    }
    
    func configureProjectView(with project: ProjectsRegistryResponseModel) {
        navigationItem.title = project.title
        if let projectImage = project.imageURL {
            let logo = URL(string: projectImage)
            let placeholder = UIImage(named: "unselectedCell")
            projectImageView.kf.setImage(with: logo, placeholder: placeholder)
        } else {
            projectImageView.image = UIImage(named: "BSL_Logo")
        }
        projectLabel.text = project.title
        statusProjectLabel.text = project.status
        typeProjectLabel.text = project.id
        aboutProjectTextView.text = project.welcomeDescription
        setStatusView(project.status)
    }
    
}

//
//  AboutProjectScreenAboutProjectScreenAssembler.swift
//  BSLWorkspace
//
//  Created by clear on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class AboutProjectScreenAssembler {
    static func createModule(_ project: ProjectsRegistryResponseModel) -> AboutProjectScreenViewController {
        let viewController = AboutProjectScreenViewController()
        let interactor = AboutProjectScreenInteractor(project)
        let presenter = AboutProjectScreenPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

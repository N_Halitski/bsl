//
//  StuffFiltrationStuffFiltrationViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class StuffFiltrationViewController: BaseViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var discardFiltersOutlet: UIButton!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var closeFilterPanalOutlet: UIButton!
    @IBOutlet weak var positionView: UIView!
    @IBOutlet weak var projectView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var showResultsView: UIView!
    @IBOutlet weak var showResultsButtonOutlet: UIButton!
    @IBOutlet weak var choosePositionButtonOutlet: UIButton!
    @IBOutlet weak var chooseProjectButtonOutlet: UIButton!
    
    var presenter: StuffFiltrationPresenterInput!
    
    var didFinishClosing: (() -> Void)?
    var showPositionsList: (() -> Void)?
    var showProjectsList: (() -> Void)?
    var discardChanges: (() -> Void)?
    var showResults: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
        
        let showPositions = UITapGestureRecognizer(target: self, action: #selector(self.showPositions(_:)))
        let showProjects = UITapGestureRecognizer(target: self, action: #selector(self.showProjects(_:)))
        positionView.addGestureRecognizer(showPositions)
        projectView.addGestureRecognizer(showProjects)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        closeFilterPanalOutlet.addTarget(self, action: #selector(closePanelTapped(_:)), for: .touchUpInside)
    }
    
    @objc func closePanelTapped(_ sender: Any) {
        didFinishClosing?()
    }
    
    @objc func showPositions(_ sender: UITapGestureRecognizer) {
        showPositionsList?()
    }
    
    @objc func showProjects(_ sender: UITapGestureRecognizer) {
        showProjectsList?()
    }
    
    @IBAction func choosePositionsClicked(_ sender: Any) {
        showPositionsList?()
    }
    
    @IBAction func chooseProjectsClicked(_ sender: Any) {
        showProjectsList?()
    }
    
    @IBAction func discardFiltersClicked(_ sender: Any) {
        discardChanges?()
    }
    
    @IBAction func showResultsClicked(_ sender: Any) {
        showResults?()
    }
    
    private func setupUI() {
        view.backgroundColor = AppTheme.Colors.lightGray100
        
        headerView.backgroundColor = AppTheme.Colors.lightGray100
        
        positionView.layer.cornerRadius = 16
        positionView.backgroundColor = .white
        
        projectView.layer.cornerRadius = 16
        projectView.backgroundColor = .white
        
        positionLabel.font = AppTheme.Fonts.SFRegular(16)
        positionLabel.textColor = AppTheme.Colors.darkGray
        
        projectLabel.font = AppTheme.Fonts.SFRegular(16)
        projectLabel.textColor = AppTheme.Colors.darkGray
        
        showResultsView.layer.cornerRadius = 16
        showResultsView.backgroundColor = AppTheme.Colors.darkBlue100
        
        showResultsButtonOutlet.tintColor = .white
        showResultsButtonOutlet.titleLabel?.font = AppTheme.Fonts.SFMedium(16)
        
        choosePositionButtonOutlet.setTitle("", for: .normal)
        choosePositionButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
        choosePositionButtonOutlet.tintColor = AppTheme.Colors.darkGray
        
        chooseProjectButtonOutlet.setTitle("", for: .normal)
        chooseProjectButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
        choosePositionButtonOutlet.tintColor = AppTheme.Colors.darkGray
        
        discardFiltersOutlet.setTitle("StuffFiltrationScreen.discardFilters".localized, for: .normal)
        discardFiltersOutlet.titleLabel?.font = AppTheme.Fonts.SFRegular(14)
        discardFiltersOutlet.tintColor = AppTheme.Colors.darkGray
        
        filterLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        closeFilterPanalOutlet.setTitle("", for: .normal)
    }
}

extension StuffFiltrationViewController: StuffFiltrationPresenterOutput {

}

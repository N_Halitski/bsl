//
//  StuffFiltrationStuffFiltrationInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class StuffFiltrationInteractor {
    private weak var output: StuffFiltrationInteractorOutput?

}

extension StuffFiltrationInteractor: StuffFiltrationInteractorInput {
    func attach(_ output: StuffFiltrationInteractorOutput) {
        self.output = output
    }
}

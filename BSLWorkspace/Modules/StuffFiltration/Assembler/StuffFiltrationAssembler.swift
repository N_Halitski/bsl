//
//  StuffFiltrationStuffFiltrationAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class StuffFiltrationAssembler {
    static func createModule() -> StuffFiltrationViewController {
        let viewController = StuffFiltrationViewController()
        let interactor = StuffFiltrationInteractor()
        let presenter = StuffFiltrationPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

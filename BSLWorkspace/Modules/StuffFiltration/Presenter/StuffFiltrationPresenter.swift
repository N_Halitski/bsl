//
//  StuffFiltrationStuffFiltrationPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class StuffFiltrationPresenter {

    private let interactor: StuffFiltrationInteractorInput
    private unowned let view: StuffFiltrationPresenterOutput

    init(_ interactor: StuffFiltrationInteractorInput, _ view: StuffFiltrationPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension StuffFiltrationPresenter: StuffFiltrationPresenterInput {

}

extension StuffFiltrationPresenter: StuffFiltrationInteractorOutput {

}

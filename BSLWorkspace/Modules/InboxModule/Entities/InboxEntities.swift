//
//  InboxEntities.swift
//  BSLWorkspace
//
//  Created by Sergey on 27.01.22.
//

import Foundation

struct InboxEntities {
    var newNotificationsList: [InboxModel] = []
    var oldNotificationsList: [InboxModel] = []
    var newConfirmationsList: [InboxModel] = []
    var oldConfirmationsList: [InboxModel] = []
}

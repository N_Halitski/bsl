//
//  InboxModuleInboxModuleInteractorInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 06/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol InboxModuleInteractorInput: AnyObject {
    func attach(_ output: InboxModuleInteractorOutput)
    func attachTableView(_ tableView: UITableView)
    func switchScreen(to number: Int)
}

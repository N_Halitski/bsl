//
//  InboxModuleInboxModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 06/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class InboxModuleInteractor {
    
    private weak var output: InboxModuleInteractorOutput?
    private var tableManager: InboxTableManagerProtocol
    private var  inboxRepository: InboxRepositoryProtocol
    private var inboxEntities = InboxEntities()
    
    init(tableManager: InboxTableManagerProtocol, inboxRepository: InboxRepositoryProtocol) {
        self.tableManager = tableManager
        self.inboxRepository = inboxRepository
        createNotificationTable()
    }
    
}

extension InboxModuleInteractor: InboxModuleInteractorInput {

    func attach(_ output: InboxModuleInteractorOutput) {
        self.output = output
    }
    
    func attachTableView(_ tableView: UITableView) {
        tableManager.attachTableView(tableView)
        getInbox()
        
        tableManager.openRequestVacation = { 
            guard let output = self.output else { return }
            output.openRequestScreen()
        }
    }
    
    func switchScreen(to number: Int) {
        number == 0 ? createNotificationTable() : createConfirmationTable()
    }
    
    private func createNotificationTable() {
        tableManager.setDataToNotificationsTable(newNotificationsList: inboxEntities.newNotificationsList,
                                                 oldNotificationsList: inboxEntities.oldNotificationsList)
    }
    
    private func createConfirmationTable() {
        tableManager.setDataToConfirmationsTable(newConfirmationsList: inboxEntities.newConfirmationsList,
                                                 oldConfirmationsList: inboxEntities.oldConfirmationsList)
    }
    
    private func getInbox() {
        self.output?.loaderStatus(is: true)
        inboxRepository.getInbox(completion: { [weak self] result in
            guard let self = self else { return }
            self.output?.loaderStatus(is: false)
            switch result {
            case .success(let data):
                self.inboxFiltering(inboxArray: data)
                self.createNotificationTable()
            case .failure :
                break
            }
        })
    }
    
    private func inboxFiltering(inboxArray: [InboxModel]?) {
        if let inboxArray = inboxArray {
            inboxArray.forEach { inbox in
                if let eventType = inbox.eventType {
                    switch eventType {
                    case .vacationRequest:
                        inbox.readAt == nil ? inboxEntities.newConfirmationsList.append(inbox) : inboxEntities.oldConfirmationsList.append(inbox)
                    case .vacationApproved, .vacationRejected:
                        inbox.readAt == nil ? inboxEntities.newNotificationsList.append(inbox) : inboxEntities.oldNotificationsList.append(inbox)

                    }
                }
            }
        }
    }

}

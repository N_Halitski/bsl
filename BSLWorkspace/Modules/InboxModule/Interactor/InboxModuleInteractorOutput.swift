//
//  InboxModuleInboxModuleInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 06/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

protocol InboxModuleInteractorOutput: AnyObject {
    func loaderStatus(is status: Bool)
    func openRequestScreen()
}

//
//  InboxModuleInboxModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 06/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class InboxModuleAssembler {
    static func createModule(tableManager: InboxTableManagerProtocol = InboxTableManager(),
                             inboxRepository: InboxRepositoryProtocol) -> InboxModuleViewController {
        let viewController = InboxModuleViewController()
        let interactor = InboxModuleInteractor(tableManager: tableManager, inboxRepository: inboxRepository)
        let presenter = InboxModulePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
    
}

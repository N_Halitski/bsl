//
//  InboxTableManager.swift
//  BSLWorkspace
//
//  Created by Sergey on 6.01.22.
//

import UIKit

protocol InboxTableManagerProtocol {
    func attachTableView(_ tableView: UITableView)
    func setDataToNotificationsTable(newNotificationsList: [InboxModel]?, oldNotificationsList: [InboxModel]?)
    func setDataToConfirmationsTable(newConfirmationsList: [InboxModel]?, oldConfirmationsList: [InboxModel]?)
    
    var openRequestVacation: (() -> Void)? { get set }
}

final class InboxTableManager: NSObject, InboxTableManagerProtocol {

    private weak var tableView: UITableView?
    private var configurators: [BaseConfigurator] = []
    private var sections: [InboxTableSection] = []
    
    var openRequestVacation: (() -> Void)?
    
    func attachTableView(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(NotificationCell.self)
        tableView.registerCell(ConfirmationCell.self)
        self.tableView = tableView
    }
    
}

extension InboxTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = sections[indexPath.section].configurators[indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionType = self.sections[section].sectionType else { return nil }
        let counter = self.sections[section].configurators.count
        let headerView = InboxHeaderView()
        headerView.configure(sectionType: sectionType, counter: counter)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard sections[indexPath.section].configurators[indexPath.row] is ConfirmationCellConfigurator else { return }
        openRequestVacation?()
    }
    
}

extension InboxTableManager {
    
    func setDataToNotificationsTable(newNotificationsList: [InboxModel]?, oldNotificationsList: [InboxModel]?) {
        var output: [InboxTableSection] = []
        if let newNotificationsList = newNotificationsList {
            var newNotificationsArray: [BaseConfigurator] = []
            newNotificationsList.forEach { newNotification in
                let configurator = newNotification.objectId == newNotificationsList.last?.objectId ?
                                createNotificationConfigurator(with: newNotification, isLast: true) :
                                createNotificationConfigurator(with: newNotification, isLast: false)
                                newNotificationsArray.append(configurator)
            }
            if !newNotificationsArray.isEmpty {
                let firstSection = InboxTableSection(sectionType: .new, configurators: newNotificationsArray)
                output.append(firstSection)
            }
        }
        if let oldNotificationsList = oldNotificationsList {
            var oldNotificationsArray: [BaseConfigurator] = []
            oldNotificationsList.forEach { oldNotification in
                let configurator = oldNotification.objectId == oldNotificationsList.last?.objectId ?
                createNotificationConfigurator(with: oldNotification, isLast: true) :
                createNotificationConfigurator(with: oldNotification, isLast: false)
                oldNotificationsArray.append(configurator)
            }
            if !oldNotificationsArray.isEmpty {
                let secondSection = InboxTableSection(sectionType: .old, configurators: oldNotificationsArray)
                output.append(secondSection)
            }
        }
        self.sections = output
        tableView?.reloadData()
    }
    
    func setDataToConfirmationsTable(newConfirmationsList: [InboxModel]?, oldConfirmationsList: [InboxModel]?) {
        var output: [InboxTableSection] = []
        if let newConfirmationsList = newConfirmationsList {
            var newConfirmationsArray: [BaseConfigurator] = []
            newConfirmationsList.forEach { newConfirmation in
                let configurator = newConfirmation.objectId == newConfirmationsList.last?.objectId ?
                createConfirmationConfigurator(with: newConfirmation, isLast: true) :
                createConfirmationConfigurator(with: newConfirmation, isLast: false)
                newConfirmationsArray.append(configurator)
            }
            if !newConfirmationsArray.isEmpty {
                let firstSection = InboxTableSection(sectionType: .new, configurators: newConfirmationsArray)
                output.append(firstSection)
            }
        }
        if let oldConfirmationsList = oldConfirmationsList {
            var oldConfirmationsArray: [BaseConfigurator] = []
            oldConfirmationsList.forEach { oldConfirmation in
                let configurator = oldConfirmation.objectId == oldConfirmationsList.last?.objectId ?
                createConfirmationConfigurator(with: oldConfirmation, isLast: true) :
                createConfirmationConfigurator(with: oldConfirmation, isLast: false)
                oldConfirmationsArray.append(configurator)
            }
            if !oldConfirmationsArray.isEmpty {
                let secondSection = InboxTableSection(sectionType: .old, configurators: oldConfirmationsArray)
                output.append(secondSection)
            }
        }
        self.sections = output
        tableView?.reloadData()
    }
    
    private func createNotificationConfigurator(with model: InboxModel, isLast: Bool) -> BaseConfigurator {
        let configurator = NotificationCellConfigurator()
        configurator.model = model
        configurator.isLast = isLast
        return configurator
    }
    
    private func createConfirmationConfigurator(with model: InboxModel, isLast: Bool) -> BaseConfigurator {
        let configurator = ConfirmationCellConfigurator()
        configurator.model = model
        configurator.isLast = isLast
        return configurator
    }
    
}

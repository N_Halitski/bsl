//
//  InboxTableConfigurators.swift
//  BSLWorkspace
//
//  Created by Sergey on 6.01.22.
//

import UIKit

final class NotificationCellConfigurator: BaseConfigurator {
                
    var reuseId: String = String(describing: NotificationCell.self)
    var model: InboxModel?
    var isLast: Bool?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? NotificationCellProtocol, let model = model, let isLast = isLast else { return }
        cell.configure(with: model, isLast: isLast)
    }
    
}

final class ConfirmationCellConfigurator: BaseConfigurator {
                
    var reuseId: String = String(describing: ConfirmationCell.self)
    var model: InboxModel?
    var isLast: Bool?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? ConfirmationCellProtocol, let model = model, let isLast = isLast else { return }
        cell.configure(with: model, isLast: isLast)
    }
    
}

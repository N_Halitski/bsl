//
//  InboxHeaderView.swift
//  BSLWorkspace
//
//  Created by Sergey on 7.01.22.
//

import UIKit

class InboxHeaderView: XibBasedView {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var sectionNameLabel: UILabel!
    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setupUI() {
        backView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func configure(sectionType: SectionType, counter: Int) {
        self.sectionNameLabel?.text = sectionType.getTitle()
        self.counterLabel?.text = "\(counter)"
        switch sectionType {
        case .new:
            self.counterView.backgroundColor = AppTheme.Colors.babyBlue100
        case .old, .processed:
            self.counterView.backgroundColor = AppTheme.Colors.green100
        }
    }
    
}

enum SectionType: String {
    case new
    case old
    case processed
    
    func getTitle() -> String {
        switch self {
        case .new:
            return "InboxScreen.new".localized
        case .old:
            return "InboxScreen.old".localized
        case .processed:
            return "InboxScreen.processed".localized
        }
    }
    
}

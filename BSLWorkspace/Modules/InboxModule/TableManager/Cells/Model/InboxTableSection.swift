//
//  InboxTableSection.swift
//  BSLWorkspace
//
//  Created by Sergey on 7.01.22.
//

import Foundation

final class InboxTableSection {
    var sectionType: SectionType?
    var configurators: [BaseConfigurator]
    
    init(sectionType: SectionType?, configurators: [BaseConfigurator]) {
        self.sectionType = sectionType
        self.configurators = configurators
    }
    
}

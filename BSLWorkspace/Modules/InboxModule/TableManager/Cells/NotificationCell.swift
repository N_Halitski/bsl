//
//  NotificationCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 6.01.22.
//

import UIKit
import Kingfisher

protocol NotificationCellProtocol {
    func configure(with model: InboxModel, isLast: Bool)
}

final class NotificationCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var checkboxImageView: UIImageView!
    
}

extension NotificationCell: NotificationCellProtocol {
    
    func configure(with model: InboxModel, isLast: Bool) {
        self.checkboxImageView.isHidden = false
        avatarImageView.kf.setImage(with: URL(string: model.fromUser?.avatar ?? ""), placeholder: UIImage(named: "person"))
        label.text = model.fromUser?.name
        dateLabel.text = DateFormatterManager.shared.date(createAt: model.createAt)
        
        switch model.eventType {
        case .vacationApproved:
            self.checkboxImageView.image = UIImage(named: "checkmarkGreenSquare")
        case .vacationRejected:
            self.checkboxImageView.image = UIImage(named: "xmarkRedSquare")
        case .vacationRequest, .none:
            self.checkboxImageView.isHidden = true
        }
        
        backViewHeightConstraint.constant = isLast ? 81.5 : 65.5
        backView.layer.cornerRadius = isLast ? 16 : 0
        backView.layer.maskedCorners = isLast ? [.layerMaxXMaxYCorner, .layerMinXMaxYCorner] : []
        separatorView.isHidden = isLast ? true : false
    }
    
}

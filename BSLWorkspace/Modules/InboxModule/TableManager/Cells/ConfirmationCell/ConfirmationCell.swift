//
//  ConfirmationCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 9.01.22.
//

import UIKit

protocol ConfirmationCellProtocol {
    func configure(with model: InboxModel, isLast: Bool)
}

final class ConfirmationCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var textLabelToBackViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
}

extension ConfirmationCell: ConfirmationCellProtocol {
    
    func configure(with model: InboxModel, isLast: Bool) {
        avatarImageView.kf.setImage(with: URL(string: model.fromUser?.avatar ?? ""), placeholder: UIImage(named: "person"))
        nameLabel.text = model.fromUser?.name
        dateLabel.text = DateFormatterManager.shared.date(createAt: model.createAt)
        typeLabel.text = model.eventType?.getName()
        commentLabel.text =  model.text
        commentLabel.isHidden = model.text != nil ? true : false
        switch model.eventType {
        case .vacationRequest, .vacationRejected, .vacationApproved, .none:
            self.typeLabel.textColor = AppTheme.Colors.darkGray
        }
        textLabelToBackViewConstraint.constant = isLast ? 16 : 8
        backView.layer.cornerRadius = isLast ? 16 : 0
        backView.layer.maskedCorners = isLast ? [.layerMaxXMaxYCorner, .layerMinXMaxYCorner] : []
        separatorView.isHidden = isLast ? true : false
    }
    
}

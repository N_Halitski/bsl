//
//  InboxModuleInboxModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 06/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class InboxModuleViewController: BaseViewController {
    
    @IBOutlet weak var customSegmentedControl: CustomSegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: InboxModulePresenterInput!
    
    var showRequestForVacation: (() -> Void)?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        setupUI()
        setupCallbacks()
    }

}

extension InboxModuleViewController: InboxModulePresenterOutput {
    
    func loaderStatus(is status: Bool) {
        self.showLoader(isShown: status)
    }
    
    func openRequestScreen() {
        showRequestForVacation?()
    }
    
}

extension InboxModuleViewController {
    
    func setupUI() {
        self.navigationItem.title = "InboxScreen.inbox".localized
        customSegmentedControl.setButtonNames(firstButton: "InboxScreen.notifications".localized, secondButton: "InboxScreen.confirmation".localized)
    }
    
    private func setupCallbacks() {
        self.customSegmentedControl.segmentedControlDidChange = { [weak self] numberOfButton in
            guard let self = self else { return }
            self.presenter.switchScreen(to: numberOfButton)
        }
    }
    
}

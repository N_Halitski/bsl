//
//  InboxModuleInboxModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 06/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class InboxModulePresenter {

    private let interactor: InboxModuleInteractorInput
    private unowned let view: InboxModulePresenterOutput

    init(_ interactor: InboxModuleInteractorInput, _ view: InboxModulePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension InboxModulePresenter: InboxModulePresenterInput {
    
    func attachTableView(_ tableView: UITableView) {
        interactor.attachTableView(tableView)
    }
    
    func switchScreen(to number: Int) {
        interactor.switchScreen(to: number)
    }

}

extension InboxModulePresenter: InboxModuleInteractorOutput {
    
    func loaderStatus(is status: Bool) {
        view.loaderStatus(is: status)
    }
    
    func openRequestScreen() {
        view.openRequestScreen()
    }
    
}

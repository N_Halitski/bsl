//
//  NewsFeedModuleNewsFeedModulePresenterInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 02/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol NewsFeedModulePresenterInput: BasePresenting {
    func attachTableView(_ tableView: UITableView)
}

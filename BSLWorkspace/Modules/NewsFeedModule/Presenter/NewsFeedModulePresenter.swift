//
//  NewsFeedModuleNewsFeedModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 02/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class NewsFeedModulePresenter {

    private let interactor: NewsFeedModuleInteractorInput
    private unowned let view: NewsFeedModulePresenterOutput

        init(_ interactor: NewsFeedModuleInteractorInput, _ view: NewsFeedModulePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension NewsFeedModulePresenter: NewsFeedModulePresenterInput {
    func attachTableView(_ tableView: UITableView) {
        interactor.attachTableView(tableView)
    }
}

extension NewsFeedModulePresenter: NewsFeedModuleInteractorOutput {
    func loaderStatus(is status: Bool) {
        view.loaderStatus(is: status)
    }
    
    func didTapNewsCell(_ newsList: NewsList) {
        view.passNewsToView(newsList)
    }
}

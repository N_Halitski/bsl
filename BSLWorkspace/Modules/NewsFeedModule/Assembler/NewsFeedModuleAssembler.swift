//
//  NewsFeedModuleNewsFeedModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 02/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class NewsFeedModuleAssembler {
    static func createModule(tableManager: NewsFeedTableManagerProtocol = NewsFeedTableManager(),
                             newsRepository: NewsRepositoryProtocol) -> NewsFeedModuleViewController {
        let viewController = NewsFeedModuleViewController()
        let interactor = NewsFeedModuleInteractor(tableManager: tableManager, newsRepository: newsRepository)
        let presenter = NewsFeedModulePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

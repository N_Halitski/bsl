//
//  NewsFeedModuleNewsFeedModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 02/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class NewsFeedModuleViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: NewsFeedModulePresenterInput!
    
    var openNewsPageScreen: ((NewsList) -> Void)?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        setupUI()
    }
    
}

extension NewsFeedModuleViewController: NewsFeedModulePresenterOutput {
    
    func loaderStatus(is status: Bool) {
        self.showLoader(isShown: status)
    }
    
    func passNewsToView(_ newsList: NewsList) {
        openNewsPageScreen?(newsList)
    }
    
}

private extension NewsFeedModuleViewController {
    
    func setupUI() {
        self.navigationItem.title = "NewsScreen.news".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.shadowColor = .clear
        navBarAppearance.shadowImage = UIImage()
        navigationController?.navigationBar.standardAppearance = navBarAppearance
    }
    
}

//
//  NewsFeedTableManager.swift
//  BSLWorkspace
//
//  Created by Sergey on 2.12.21.
//

import UIKit
import Kingfisher
import AVFoundation

protocol NewsFeedTableManagerProtocol {
    func attachTableView(_ tableView: UITableView)
    func setDataToTable(_ data: [NewsList])
    var showMoreNews: (() -> Void)? { get set }
    var didTapNewsCell: ((NewsList) -> Void)? { get set }
}

final class NewsFeedTableManager: NSObject, NewsFeedTableManagerProtocol {
    
    private weak var tableView: UITableView?
    private var configurators: [NewsConfiguratorProtocol] =  []
    
    var showMoreNews: (() -> Void)?
    var didTapNewsCell: ((NewsList) -> Void)?
    
    func attachTableView(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: NewsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: NewsTableViewCell.self))
        self.tableView = tableView
    }
    
    func setDataToTable(_ data: [NewsList]) {
        var output: [NewsConfiguratorProtocol] = []
        data.forEach { news in
            output.append(createNewsConfigurator(with: news))
        }
        
        self.configurators = output
        tableView?.reloadData()
    }
    
    private func createNewsConfigurator(with model: NewsList) -> NewsConfiguratorProtocol {
        let configurator = NewsCellConfigurator()
        configurator.model = model
        return configurator
    }
}

extension NewsFeedTableManager: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (configurators.count - 1) == indexPath.item {
            self.showMoreNews?()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let model = configurators[indexPath.row].model else { return }
        self.didTapNewsCell?(model)
    }
}

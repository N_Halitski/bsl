//
//  NewsFeedTableConfigurators.swift
//  BSLWorkspace
//
//  Created by Sergey on 2.12.21.
//

import UIKit

protocol NewsConfiguratorProtocol: BaseConfigurator {
    
    var model: NewsList? { get set }
}

final class NewsCellConfigurator: NewsConfiguratorProtocol {

    var reuseId: String = String(describing: NewsTableViewCell.self)
    var model: NewsList?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? NewsTableViewCellProtocol,
              let model = model else { return }
        cell.configure(with: model)
    }
}

//
//  NewsTableViewCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 3.12.21.
//

import UIKit
import Kingfisher

protocol NewsTableViewCellProtocol {
    func configure(with model: NewsList)
}

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var adminAvatarImageView: UIImageView!
    @IBOutlet weak var adminNameLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsTextLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
}

extension NewsTableViewCell: NewsTableViewCellProtocol {
    func configure(with model: NewsList) {
        if let modelAuthorAvatar = model.authorAvatar {
            if let authorAvatarUrl = URL(string: modelAuthorAvatar) {
                adminAvatarImageView.kf.setImage(with: authorAvatarUrl)
            } else {
                adminAvatarImageView.image = UIImage(named: "person")
            }
        }
        
        if let modelNewsAttachment = model.newsAttachment {
            if modelNewsAttachment.first != nil {
                if let newsImageUrl = URL(string: modelNewsAttachment[0]) {
                    newsImageView.kf.setImage(with: newsImageUrl)
                } else {
                    newsImageView.image = UIImage(named: "person")
                }
            }
        }
        adminNameLabel.text = model.authorName
        timeLabel.text = DateFormatterManager.shared.hourIntervalCalculation(dateOfPlacement: model.createdAt)
        newsTitleLabel.text = model.title
        newsTextLabel.attributedText = model.text?.setupLineSpacing(lineSpacing: 1.13)
    }
}

extension NewsTableViewCell {
    func setupUI() {
        self.separatorView.layer.cornerRadius = self.separatorView.frame.size.width / 2
        self.backView.addShadow(shadowColor: AppTheme.Colors.lightGray7.cgColor, shadowOffset: CGSize(width: 0, height: -3), shadowOpacity: 1, shadowRadius: 10)
    }
}

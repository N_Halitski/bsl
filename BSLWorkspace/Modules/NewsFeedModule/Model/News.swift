//
//  News.swift
//  BSLWorkspace
//
//  Created by Sergey on 9.12.21.
//

import UIKit

struct News {
    let adminAvatar: UIImage?
    let adminName: String?
    let time: String?
    let newsTitle: String?
    let newsText: String?
    let newsImage: UIImage?
}

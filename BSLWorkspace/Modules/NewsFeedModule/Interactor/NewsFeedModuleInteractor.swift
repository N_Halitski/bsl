//
//  NewsFeedModuleNewsFeedModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 02/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class NewsFeedModuleInteractor {
    
    private weak var output: NewsFeedModuleInteractorOutput?
    private var tableManager: NewsFeedTableManagerProtocol
    private var newsLimits = 2
    private var newsOffset = 0
    private var newsRepository: NewsRepositoryProtocol
    private var newsArray: [NewsList]?
    private var newsDetailsArray: [NewsDetailsModel]?
    private var loaderDispatchGroup = DispatchGroup()
    
    init(tableManager: NewsFeedTableManagerProtocol, newsRepository: NewsRepositoryProtocol) {
        self.tableManager = tableManager
        self.newsRepository = newsRepository
    }
}

extension NewsFeedModuleInteractor: NewsFeedModuleInteractorInput {
    func attach(_ output: NewsFeedModuleInteractorOutput) {
        self.output = output
    }
    
    func attachTableView(_ tableView: UITableView) {
        tableManager.attachTableView(tableView)
        self.tableManager.didTapNewsCell = { [weak self] news in
            guard let self = self else { return }
            self.output?.didTapNewsCell(news)
        }
        getNews()
    }
    
    // func getNews - receiving data from the network, sending data to a table, pagination
    
    private func getNews() {
        self.output?.loaderStatus(is: true)
        newsRepository.getNews(limit: self.newsLimits, offset: self.newsOffset) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                guard let dataResults = data?.results else { return }
                self.newsArray == nil ? self.newsArray = dataResults : self.newsArray?.append(contentsOf: dataResults)
                self.getEachNewsDetails()
            case .failure :
                break
            }
        }
    }
    
    private func getEachNewsDetails() {
        guard let newsArray = self.newsArray else { return }
        newsArray.forEach { news in
            guard let newsId = news.id else { return }
            self.loaderDispatchGroup.enter()
            newsRepository.getNewsDetails(newsId: newsId) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    guard let data = data else { return }
                    self.newsDetailsArray == nil ? self.newsDetailsArray = [data] : self.newsDetailsArray?.append(data)
                    self.loaderDispatchGroup.leave()
                case .failure:
                    self.loaderDispatchGroup.leave()
                }
            }
        }
        loaderDispatchGroup.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            guard var newsArray = self.newsArray, let newsDetailsArray = self.newsDetailsArray else { return }
            for newsNumber in 0..<newsArray.count {
                newsArray[newsNumber].text = newsDetailsArray[newsNumber].text
                newsArray[newsNumber].authorName = newsDetailsArray[newsNumber].author?.name
                newsArray[newsNumber].authorAvatar = newsDetailsArray[newsNumber].author?.avatar
                newsArray[newsNumber].newsAttachment = newsDetailsArray[newsNumber].attachments
            }
            self.newsArray = newsArray
            self.setDataToTable()
        }
        self.output?.loaderStatus(is: false)
    }
    
    private func setDataToTable() {
        guard let newsArray = self.newsArray else { return }
        self.tableManager.setDataToTable(newsArray)
        self.tableManager.showMoreNews = { [weak self] in
            guard let self = self else { return }
            self.newsOffset += 2
            self.getNews()
        }
    }
}

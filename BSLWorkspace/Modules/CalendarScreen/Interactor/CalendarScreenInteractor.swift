//
//  CalendarScreenCalendarScreenInteractor.swift
//  BSLWorkspace
//
//  Created by clear on 05/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class CalendarScreenInteractor {
    private weak var output: CalendarScreenInteractorOutput?

}

extension CalendarScreenInteractor: CalendarScreenInteractorInput {
    func attach(_ output: CalendarScreenInteractorOutput) {
        self.output = output
    }
}

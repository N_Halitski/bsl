//
//  CalendarScreenCalendarScreenPresenterOutput.swift
//  BSLWorkspace
//
//  Created by clear on 05/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

protocol CalendarScreenPresenterOutput: AnyObject {
    func setupUI()
}

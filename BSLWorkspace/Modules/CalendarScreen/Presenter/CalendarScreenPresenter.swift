//
//  CalendarScreenCalendarScreenPresenter.swift
//  BSLWorkspace
//
//  Created by clear on 05/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class CalendarScreenPresenter {

    private let interactor: CalendarScreenInteractorInput
    private unowned let view: CalendarScreenPresenterOutput

    init(_ interactor: CalendarScreenInteractorInput, _ view: CalendarScreenPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
        view.setupUI()
    }

}

extension CalendarScreenPresenter: CalendarScreenPresenterInput {

}

extension CalendarScreenPresenter: CalendarScreenInteractorOutput {

}

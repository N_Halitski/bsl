//
//  CalendarScreenCalendarScreenViewController.swift
//  BSLWorkspace
//
//  Created by clear on 05/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit
import HorizonCalendar

final class CalendarScreenViewController: CalendarType {
    
    @IBOutlet private var topRectangleView: UIView!
    @IBOutlet private var topLabel: UILabel!
    
    private enum Constants {
        static let activeButtonColor = AppTheme.Colors.darkBlue100
        static let inactiveButtonColor = AppTheme.Colors.grayBlue100
        static let buttonTextColor = AppTheme.Colors.white100
        static let selectedDayBackgroundColor = AppTheme.Colors.orange100
        static let selectedDayTextColor = AppTheme.Colors.white100
        static let dayColor = AppTheme.Colors.black100
        static let buttonFistLineFont = AppTheme.Fonts.SFMedium(16)
        static let buttonSecondLineFont = AppTheme.Fonts.SFRegular(12)
        static let calendarDayFont = AppTheme.Fonts.SFMedium(14)
        static let calendarDaySelectedColor = AppTheme.Colors.white100
        static let separatorColor = AppTheme.Colors.lightGray100
        static let deactivatedDateColor = AppTheme.Colors.gray100
        static let currentDayBorderColor = AppTheme.Colors.gray100
        static let monthFont = AppTheme.Fonts.SFMedium(20)
        static let monthColor = AppTheme.Colors.black100
        static let dayOfWeekColor = AppTheme.Colors.darkGray
        static let dayOfWeekFont = AppTheme.Fonts.SFRegular(12)
        static let separatorHeight: CGFloat = 1
        static let buttonHeight: CGFloat = 55
        static let selectedDayCornerRadius: CGFloat = 8
        static let buttonCornerRadius: CGFloat = 16
        static let calendarIdentifier = "ru_RU"
    }
    
    private let selectDateButton = UIButton()
    private var calendarSelection: CalendarSelection?
    
    var presenter: CalendarScreenPresenterInput!
    var startDate = String()
    var finishDate = String()
    var didTapDateButton: ((_ startDate: String, _ finishDate: String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    override func makeContent() -> CalendarViewContent {
        calendar.locale = Locale(identifier: Constants.calendarIdentifier)
        
        let startDate = calendar.date(from: DateComponents(year: Date.getCurrentYear(), month: Date.getCurrentMonth() ))!
        let endDate = calendar.date(from: DateComponents(year: Date.getCurrentYear() + 1, month: 12))!
        
        let calendarSelection = self.calendarSelection
        let dateRanges: Set<ClosedRange<Date>>
        
        if case .dayRange(let dayRange) = calendarSelection,
           let lowerBound = calendar.date(from: dayRange.lowerBound.components),
           let upperBound = calendar.date(from: dayRange.upperBound.components) {
            dateRanges = [lowerBound...upperBound]
        } else {
            dateRanges = []
        }
        
        return CalendarViewContent(
            calendar: calendar,
            visibleDateRange: startDate...endDate,
            monthsLayout: monthsLayout)
        
            .daysOfTheWeekRowSeparator(options: CalendarViewContent.DaysOfTheWeekRowSeparatorOptions.init(
                height: Constants.separatorHeight,
                color: Constants.separatorColor))
            .interMonthSpacing(50)
            .verticalDayMargin(8)
            .horizontalDayMargin(8)
            .dayItemProvider { [calendar, dayDateFormatter] day in
                var invariantViewProperties = DayView.InvariantViewProperties.baseInteractive
                invariantViewProperties.font = Constants.calendarDayFont
                invariantViewProperties.textColor = Constants.dayColor
                
                // Deactivate selection past days
                if day.day < Date.getCurrentDay(),
                   day.month.month == Date.getCurrentMonth(),
                   day.month.year == Date.getCurrentYear() {
                    invariantViewProperties.textColor = Constants.deactivatedDateColor
                    invariantViewProperties.interaction = .disabled
                }
                // Current day configuration
                if day.day == Date.getCurrentDay(),
                   day.month.month == Date.getCurrentMonth(),
                   day.month.year == Date.getCurrentYear() {
                    self.currentDayStyle(day: &invariantViewProperties)
                }
                
                let isSelectedStyle: Bool
                
                switch calendarSelection {
                case .singleDay(let selectedDay):
                    if day.day < Date.getCurrentDay(),
                       day.month.month == Date.getCurrentMonth(),
                       day.month.year == Date.getCurrentYear() {
                        isSelectedStyle = false
                    } else {
                        isSelectedStyle = day == selectedDay }
                case .dayRange(let selectedDayRange):
                    isSelectedStyle = day == selectedDayRange.lowerBound || day == selectedDayRange.upperBound
                case .none:
                    isSelectedStyle = false
                }
                
                if isSelectedStyle {
                    invariantViewProperties.textColor = Constants.calendarDaySelectedColor
                    invariantViewProperties.shape = .rectangle(cornerRadius: Constants.selectedDayCornerRadius)
                    invariantViewProperties.backgroundShapeDrawingConfig.fillColor = Constants.selectedDayBackgroundColor
                }
                
                let date = calendar.date(from: day.components)
                
                return CalendarItemModel<DayView>(
                    invariantViewProperties: invariantViewProperties,
                    viewModel: .init(
                        dayText: "\(day.day)",
                        accessibilityLabel: date.map { dayDateFormatter.string(from: $0) },
                        accessibilityHint: nil))
            }
        
            .monthHeaderItemProvider({ month in
                var invariantViewProperties = MonthHeaderView.InvariantViewProperties.base
                invariantViewProperties.font = Constants.monthFont
                invariantViewProperties.textColor = Constants.monthColor
                invariantViewProperties.edgeInsets.bottom = -30
                
                let monthHeaderText = "\(month.month) \(month.year)"
                let monthHeader = DateFormatterManager.shared.setMonthHeader(date: monthHeaderText)
                
                return CalendarItemModel<MonthHeaderView>(
                    invariantViewProperties: invariantViewProperties,
                    viewModel: .init(
                        monthText: " \(monthHeader.capitalized)",
                        accessibilityLabel: nil))
            })
        
            .dayRangeItemProvider(for: dateRanges) { dayRangeLayoutContext in
                CalendarItemModel<DayRangeIndicatorView>(
                    invariantViewProperties: .init(),
                    viewModel: .init(
                        framesOfDaysToHighlight: dayRangeLayoutContext.daysAndFrames.map { $0.frame }))
            }
        
            .dayOfWeekItemProvider { _, weekdayIndex in
                var invariantViewProperties = DayOfWeekView.InvariantViewProperties.base
                invariantViewProperties.font = Constants.dayOfWeekFont
                invariantViewProperties.textColor = Constants.dayOfWeekColor
                
                let dayOfWeekText = DateFormatterManager.shared.setWeekday(day: weekdayIndex)
                
                return CalendarItemModel<DayOfWeekView>(
                    invariantViewProperties: invariantViewProperties,
                    viewModel: .init(
                        dayOfWeekText: dayOfWeekText,
                        accessibilityLabel: dayOfWeekText)
                )
            }
    }
    
    private func configureButton() {
        selectDateButton.addTarget(self, action: #selector(selectedDateButtonTapped), for: .touchUpInside)
        
        calendarView.daySelectionHandler = { [weak self] day in
            guard let self = self else { return }
            
            switch self.calendarSelection {
            case .singleDay(let selectedDay):
                
                // Сhecking the relevance of the selected day
                if selectedDay.day < Date.getCurrentDay(),
                   selectedDay.month.month == Date.getCurrentMonth(),
                   selectedDay.month.year == Date.getCurrentYear() {
                    fallthrough
                }
                
                if day > selectedDay {
                    self.calendarSelection = .dayRange(selectedDay...day)
                    self.activateSelectedDateButton()
                    
                    // Setup number of days
                    let fromSelectedDay = "\(selectedDay.day) \(selectedDay.month.month) \(selectedDay.month.year)"
                    let toSelectedDay = "\(day.day) \(day.month.month) \(day.month.year)"
                    
                    self.startDate = DateFormatterManager.shared.changeDateFormat(date: fromSelectedDay)
                    self.finishDate = DateFormatterManager.shared.changeDateFormat(date: toSelectedDay)
                    
                    let rangeDate = DateFormatterManager.shared.setRangeOfDate(fromDate: fromSelectedDay, toDate: toSelectedDay)
                    let numberOfDays = DateFormatterManager.shared.setNumberOfDays(dateStart: fromSelectedDay, dateEnd: toSelectedDay)
                    
                    let firstLineOnButton = "\("CalendarScreenViewController.select".localized) \(rangeDate)"
                    
                    let buttonText = NSMutableAttributedString(string: "\(firstLineOnButton)\n\(numberOfDays)")
                    buttonText.addAttribute(NSAttributedString.Key.font,
                                            value: Constants.buttonFistLineFont,
                                            range: NSRange(location: 0, length: firstLineOnButton.count))
                    buttonText.addAttribute(NSAttributedString.Key.font,
                                            value: Constants.buttonSecondLineFont,
                                            range: NSRange(location: firstLineOnButton.count,
                                                           length: numberOfDays.count + 1))
                    
                    self.selectDateButton.setAttributedTitle(buttonText, for: .normal)
                } else {
                    self.calendarSelection = .singleDay(day)
                    self.deactivateSelectedDateButton()
                }
            case .none, .dayRange:
                self.calendarSelection = .singleDay(day)
                self.deactivateSelectedDateButton()
            }
            
            self.calendarView.setContent(self.makeContent())
        }
        
    }
    
    private func currentDayStyle(day: inout DayView.InvariantViewProperties) {
        day.backgroundShapeDrawingConfig.borderWidth = 1
        day.backgroundShapeDrawingConfig.borderColor = Constants.currentDayBorderColor
        day.shape = .rectangle(cornerRadius: Constants.selectedDayCornerRadius)
    }
    
    private func deactivateSelectedDateButton() {
        let firstLineText = NSMutableAttributedString(string: "CalendarScreenViewController.chooseDate".localized)
        selectDateButton.titleLabel?.numberOfLines = 1
        selectDateButton.backgroundColor = Constants.inactiveButtonColor
        selectDateButton.layer.cornerRadius = Constants.buttonCornerRadius
        selectDateButton.setAttributedTitle(firstLineText, for: .normal)
        selectDateButton.titleLabel?.textColor = Constants.buttonTextColor
        selectDateButton.isEnabled = false
    }
    
    private func activateSelectedDateButton() {
        selectDateButton.titleLabel?.numberOfLines = 2
        selectDateButton.backgroundColor = Constants.activeButtonColor
        selectDateButton.titleLabel?.textColor = Constants.buttonTextColor
        selectDateButton.titleLabel?.textAlignment = .center
        selectDateButton.layer.cornerRadius = Constants.buttonCornerRadius
        selectDateButton.isEnabled = true
    }
    
    private enum CalendarSelection {
        case singleDay(Day)
        case dayRange(DayRange)
    }
    
    @objc private func selectedDateButtonTapped() {
        didTapDateButton?(startDate, finishDate)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func crossButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension CalendarScreenViewController: CalendarScreenPresenterOutput {
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        configureButton()
        calendarView.addSubview(selectDateButton)
        selectDateButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            selectDateButton.heightAnchor.constraint(equalToConstant: Constants.buttonHeight),
            selectDateButton.bottomAnchor.constraint(equalTo: calendarView.bottomAnchor, constant: -18),
            selectDateButton.leftAnchor.constraint(equalTo: calendarView.leftAnchor, constant: 5),
            selectDateButton.rightAnchor.constraint(equalTo: calendarView.rightAnchor, constant: -5)
        ])
        
        deactivateSelectedDateButton()
        
        topLabel.text = "CalendarScreenViewController.when".localized
        topRectangleView.layer.cornerRadius = 2
    }
    
}

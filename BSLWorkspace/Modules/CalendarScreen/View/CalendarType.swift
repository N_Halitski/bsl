//
//  CalendarType.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 5.01.22.
//

import HorizonCalendar
import UIKit

class CalendarType: UIViewController {
    
    private enum Constants {
        static let calendarCornerRadius: CGFloat = 16
        static let calendarBackgroundColor = AppTheme.Colors.white100
    }
    
    // MARK: Lifecycle
    
    required init(monthsLayout: MonthsLayout) {
        self.monthsLayout = monthsLayout
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let monthsLayout: MonthsLayout
    
    lazy var calendarView = CalendarView(initialContent: makeContent())
    lazy var calendar = Calendar.current
    lazy var dayDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = calendar
        dateFormatter.locale = calendar.locale
        dateFormatter.dateFormat = DateFormatter.dateFormat(
            fromTemplate: "EEEE, MMM d, yyyy",
            options: 0,
            locale: calendar.locale ?? Locale.current)
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCalendarType()
    }
    
    func setupCalendarType() {
        view.addSubview(calendarView)
        
        calendarView.backgroundColor = Constants.calendarBackgroundColor
        calendarView.layer.cornerRadius = Constants.calendarCornerRadius
        
        calendarView.translatesAutoresizingMaskIntoConstraints = false
        
        // swiftlint:disable all
        switch monthsLayout {
        case .vertical:
            NSLayoutConstraint.activate([
                calendarView.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
                calendarView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10),
                calendarView.leadingAnchor.constraint(
                    greaterThanOrEqualTo: view.layoutMarginsGuide.leadingAnchor),
                calendarView.trailingAnchor.constraint(
                    lessThanOrEqualTo: view.layoutMarginsGuide.trailingAnchor),
                calendarView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                calendarView.widthAnchor.constraint(lessThanOrEqualToConstant: 375),
                calendarView.widthAnchor.constraint(equalToConstant: 375).prioritize(at: .defaultLow),
            ])
        case .horizontal:
            NSLayoutConstraint.activate([
                calendarView.centerYAnchor.constraint(equalTo: view.layoutMarginsGuide.centerYAnchor),
                calendarView.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor),
                calendarView.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor),
                calendarView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                calendarView.widthAnchor.constraint(lessThanOrEqualToConstant: 375),
                calendarView.widthAnchor.constraint(equalToConstant: 375).prioritize(at: .defaultLow),
            ])
        }
        // swiftlint:enable all
    }
    
    func makeContent() -> CalendarViewContent {
        fatalError("Must be implemented by a subclass.")
    }
    
}

//
//  CalendarScreenCalendarScreenAssembler.swift
//  BSLWorkspace
//
//  Created by clear on 05/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation
import HorizonCalendar

final class CalendarScreenAssembler {
    static func createModule() -> CalendarScreenViewController {
        let viewController = CalendarScreenViewController(monthsLayout: .vertical(options: VerticalMonthsLayoutOptions(pinDaysOfWeekToTop: false)))
        let interactor = CalendarScreenInteractor()
        let presenter = CalendarScreenPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

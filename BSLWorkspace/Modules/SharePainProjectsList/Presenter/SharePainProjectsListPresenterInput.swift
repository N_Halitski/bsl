//
//  SharePainProjectsListSharePainProjectsListPresenterInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol SharePainProjectsListPresenterInput: BasePresenting {
    func attachTableView(_ table: UITableView)
}

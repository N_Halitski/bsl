//
//  SharePainProjectsListSharePainProjectsListPresenter.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class SharePainProjectsListPresenter {

    private let interactor: SharePainProjectsListInteractorInput
    private unowned let view: SharePainProjectsListPresenterOutput

    init(_ interactor: SharePainProjectsListInteractorInput, _ view: SharePainProjectsListPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension SharePainProjectsListPresenter: SharePainProjectsListPresenterInput {

    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
    
}

extension SharePainProjectsListPresenter: SharePainProjectsListInteractorOutput {
    
    func getSelectedProject(with projectModel: ProjectsRegistryResponseModel) {
        view.getSelectedProject(with: projectModel)
    }
    
}

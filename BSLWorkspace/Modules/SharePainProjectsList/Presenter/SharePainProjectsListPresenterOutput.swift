//
//  SharePainProjectsListSharePainProjectsListPresenterOutput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

protocol SharePainProjectsListPresenterOutput: AnyObject {
    func getSelectedProject(with projectModel: ProjectsRegistryResponseModel)
}

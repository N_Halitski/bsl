//
//  SharePainProjectsListSharePainProjectsListAssembler.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class SharePainProjectsListAssembler {
    static func createModule(tableManager: SharePainProjectsListTableManagerProtocol = SharePainProjectsListTableManager()) -> SharePainProjectsListViewController {
        let viewController = SharePainProjectsListViewController()
        let interactor = SharePainProjectsListInteractor(tableManager: tableManager)
        let presenter = SharePainProjectsListPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

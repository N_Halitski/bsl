//
//  SharePainProjectsListTableManager.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 4.01.22.
//

import UIKit

protocol SharePainProjectsListTableManagerProtocol {
    var selectedProject: ((ProjectsRegistryResponseModel) -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func presentSharePainProjectsCell(with model: [ProjectsRegistryResponseModel])
}

final class SharePainProjectsListTableManager: NSObject, SharePainProjectsListTableManagerProtocol {
    
    private weak var tableView: UITableView?
    private var configurators: [SharePainProjectsListConfiguratorsProtocol] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var selectedProject: ((ProjectsRegistryResponseModel) -> Void)?
    
    func attachTable(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        
        let sharePainProjectsCellNib = UINib(nibName: String(describing: SharePainProjectsCell.self), bundle: nil)
        tableView.register(sharePainProjectsCellNib, forCellReuseIdentifier: String(describing: SharePainProjectsCell.self))
        
        self.tableView = tableView
        configureTableView()
    }
    
    func presentSharePainProjectsCell(with model: [ProjectsRegistryResponseModel]) {
        self.configurators = model.map {
            return createSharePainProjectsConfigurator($0)
        }
    }
    
    private func createSharePainProjectsConfigurator(_ sharePainProjectsModel: ProjectsRegistryResponseModel) -> SharePainProjectsListConfiguratorsProtocol {
        let configurator = SharePainProjectsListConfigurators()
        configurator.sharePainProjectsModel = sharePainProjectsModel
        configurator.getSelectedProject = { [weak self] selectedProject in
            self?.selectedProject?(selectedProject)
        }
        return configurator
    }
    
}

extension SharePainProjectsListTableManager: UITableViewDelegate {

}

extension SharePainProjectsListTableManager: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let sharePainProjectsCell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(sharePainProjectsCell)
        return sharePainProjectsCell
    }
    
}

private extension SharePainProjectsListTableManager {
    
    func configureTableView() {
        tableView?.backgroundColor = AppTheme.Colors.white100
        tableView?.separatorStyle = .none
    }
    
}

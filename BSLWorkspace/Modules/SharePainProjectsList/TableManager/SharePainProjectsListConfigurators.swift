//
//  SharePainProjectsListConfigurators.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 4.01.22.
//

import UIKit

protocol SharePainProjectsListConfiguratorsProtocol: BaseConfigurator {
    var sharePainProjectsModel: ProjectsRegistryResponseModel? { get set }
    var getSelectedProject: ((ProjectsRegistryResponseModel) -> Void)? { get set }
    var cell: SharePainProjectsCellProtocol? { get set }
}

final class SharePainProjectsListConfigurators: SharePainProjectsListConfiguratorsProtocol {
    
    var reuseId: String = String(describing: SharePainProjectsCell.self)
    var sharePainProjectsModel: ProjectsRegistryResponseModel?
    var cell: SharePainProjectsCellProtocol?
    var getSelectedProject: ((ProjectsRegistryResponseModel) -> Void)?
    
    func setupCell(_ sharePainProjectsCell: UIView) {
        guard let sharePainProjectsCell = sharePainProjectsCell as? SharePainProjectsCellProtocol,
              let sharePainProjectsModel = sharePainProjectsModel else { return }
        cell = sharePainProjectsCell
        sharePainProjectsCell.display(sharePainProjectsModel)
        cell?.projectSelected = self.getSelectedProject
    }
    
}

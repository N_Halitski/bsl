//
//  SharePainProjectsCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 4.01.22.
//

import UIKit
import Kingfisher

protocol SharePainProjectsCellProtocol {
    var projectSelected: ((ProjectsRegistryResponseModel) -> Void)? { get set }
    
    func display(_ projectsListCellModel: ProjectsRegistryResponseModel)
}

final class SharePainProjectsCell: UITableViewCell, SharePainProjectsCellProtocol {
    
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet weak var selectImage: UIImageView!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var specification: UILabel!
    @IBOutlet private weak var separator: UIView!
    
    var projectSelected: ((ProjectsRegistryResponseModel) -> Void)?
    var sharePainProjectsModel: ProjectsRegistryResponseModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        configureCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            selectImage.image = UIImage(named: "selectedCell")
            guard let sharePainProjectsModel = sharePainProjectsModel else { return }
            projectSelected?(sharePainProjectsModel)
        } else {
            selectImage.image = UIImage(named: "unselected")
        }
    }
    
    func display(_ projectsListModel: ProjectsRegistryResponseModel) {
        self.sharePainProjectsModel = projectsListModel
        title.text = projectsListModel.title
        guard let projectIcon = projectsListModel.imageURL else { return  }
        let iconURL = URL(string: projectIcon)
        icon.kf.setImage(with: iconURL)
    }
    
}

private extension SharePainProjectsCell {
    
    func configureCell() {
        icon.layer.cornerRadius = 8
        icon.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: 0, height: 4), shadowOpacity: 1, shadowRadius: 36)
        
        title.textColor = AppTheme.Colors.black100
        title.font = AppTheme.Fonts.SFMedium(16)
        
        specification.textColor = AppTheme.Colors.darkGray
        specification.font = AppTheme.Fonts.SFRegular(13)
        
        selectImage.image = UIImage(named: "unselected")
        
        separator.backgroundColor = AppTheme.Colors.gray100
    }
    
}

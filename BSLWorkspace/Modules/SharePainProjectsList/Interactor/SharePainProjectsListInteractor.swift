//
//  SharePainProjectsListSharePainProjectsListInteractor.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class SharePainProjectsListInteractor {
    
    private weak var output: SharePainProjectsListInteractorOutput?
    private var tableManager: SharePainProjectsListTableManagerProtocol
    private var projectsListRepository: ProjectsRepositoryProtocol
    
    init(tableManager: SharePainProjectsListTableManagerProtocol, projectsListRepository: ProjectsRepositoryProtocol = ProjectsRepository()) {
        self.tableManager = tableManager
        self.projectsListRepository = projectsListRepository
        self.tableManager.selectedProject = { [weak self] selectedProject in
            self?.output?.getSelectedProject(with: selectedProject)
        }
    }
    
}

extension SharePainProjectsListInteractor: SharePainProjectsListInteractorInput {
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
        projectsListRepository.getProjects { [weak self] result in
            switch result {
            case .success(let response):
                guard let responseModel = response else { return }
                self?.tableManager.presentSharePainProjectsCell(with: responseModel)
            case .failure:
                break
            }
        }
    }
    
    func attach(_ output: SharePainProjectsListInteractorOutput) {
        self.output = output
    }
    
}

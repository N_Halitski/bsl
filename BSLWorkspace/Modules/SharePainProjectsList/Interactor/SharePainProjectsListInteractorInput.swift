//
//  SharePainProjectsListSharePainProjectsListInteractorInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol SharePainProjectsListInteractorInput: AnyObject {
    func attach(_ output: SharePainProjectsListInteractorOutput)
    func attachTableView(_ table: UITableView)
}

//
//  SharePainProjectsListSharePainProjectsListViewController.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 04/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class SharePainProjectsListViewController: BaseViewController {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var readyButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var fringeLabel: UILabel!
    
    var presenter: SharePainProjectsListPresenterInput!
    var selectedProject: ProjectsRegistryResponseModel?
    
    var tapOnReadyButton: ((ProjectsRegistryResponseModel) -> Void)?
    var tapOnSelectButton: (() -> Void)?

	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        configure()
        navigationController?.navigationBar.isHidden = true
    }

    @IBAction func cancelButtonClicked(_ sender: Any) {
        tapOnSelectButton?()
        dismiss(animated: true)
    }
    
    @IBAction func readyButtonClicked(_ sender: Any) {
        guard let selectedProject = selectedProject else { return }
        tapOnReadyButton?(selectedProject)
        dismiss(animated: true)
    }
    
}

private extension SharePainProjectsListViewController {
    
    func configure() {
        cancelButton.setTitle("SharePainFlow.cancel".localized, for: .normal)
        cancelButton.setTitleColor(AppTheme.Colors.blue1, for: .normal)
        
        readyButton.setTitle("SharePainFlow.ready".localized, for: .normal)
        readyButton.setTitleColor(AppTheme.Colors.blue1, for: .normal)
        
        titleLabel.text = "SharePainFlow.project".localized
        titleLabel.textColor = AppTheme.Colors.black100
        titleLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        fringeLabel.text = ""
        fringeLabel.backgroundColor = AppTheme.Colors.gray100
        fringeLabel.layer.cornerRadius = 2
    }
    
}

extension SharePainProjectsListViewController: SharePainProjectsListPresenterOutput {
    
    func getSelectedProject(with projectModel: ProjectsRegistryResponseModel) {
        self.selectedProject = projectModel
    }
    
}

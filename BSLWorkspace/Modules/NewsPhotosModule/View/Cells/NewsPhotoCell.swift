//
//  NewsPhotoCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 7.12.21.
//

import UIKit

class NewsPhotoCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!

    func setupCell(image: UIImage) {
        self.imageView.image = image
    }
}

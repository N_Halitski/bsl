//
//  NewsPhotosModuleNewsPhotosModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 07/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class NewsPhotosModuleViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: NewsPhotosModulePresenterInput!
    let images: [UIImage?] = [
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed"),
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed"),
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed"),
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed")
    ]
    var currentImageNumber: Int = 0
    
    var popViewController: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupCollectionView()
        setupUI()
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.leftBarButtonItem = nil
        navigationItem.rightBarButtonItem = nil
        navigationController?.navigationBar.tintColor = AppTheme.Colors.black100
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: AppTheme.Colors.black100]
        tabBarController?.tabBar.isHidden = false
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: String(describing: NewsPhotoCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: NewsPhotoCell.self))
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.reloadData()
    }
}

extension NewsPhotosModuleViewController: NewsPhotosModulePresenterOutput {}

extension NewsPhotosModuleViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: NewsPhotoCell.self),
            for: indexPath) as? NewsPhotoCell else { return UICollectionViewCell() }
        guard let image = self.images[indexPath.item] else { return cell }
        cell.setupCell(image: image)
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let collectionView = collectionView else { return }
        for cell in collectionView.visibleCells {
            guard let indexPath = collectionView.indexPath(for: cell) else { return }
            self.navigationItem.title = ("\(indexPath.item + 1) " + "NewsScreen.from".localized + " \(images.count)")
            self.currentImageNumber = indexPath.item
        }
    }
}

extension NewsPhotosModuleViewController {
    func setupUI() {
        self.navigationItem.title = "\(self.currentImageNumber + 1) " + "NewsScreen.from".localized + " \(images.count)"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: AppTheme.Colors.white100]
        navigationController?.navigationBar.tintColor = AppTheme.Colors.white100
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "x"), style: .done, target: self, action: #selector(shutButtonPressed))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share"), style: .plain, target: self, action: #selector(shareButtonPressed))
    }
    
    @objc func shutButtonPressed() {
        popViewController?()
    }
    
    @objc func shareButtonPressed() {
        guard let image = self.images[self.currentImageNumber] else { return }
        let shareController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(shareController, animated: true, completion: nil)
    }
}

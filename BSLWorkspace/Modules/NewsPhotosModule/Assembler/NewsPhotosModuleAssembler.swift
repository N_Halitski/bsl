//
//  NewsPhotosModuleNewsPhotosModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 07/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class NewsPhotosModuleAssembler {
    static func createModule() -> NewsPhotosModuleViewController {
        let viewController = NewsPhotosModuleViewController()
        let interactor = NewsPhotosModuleInteractor()
        let presenter = NewsPhotosModulePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

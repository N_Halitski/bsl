//
//  NewsPhotosModuleNewsPhotosModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 07/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class NewsPhotosModuleInteractor {
    private weak var output: NewsPhotosModuleInteractorOutput?
}

extension NewsPhotosModuleInteractor: NewsPhotosModuleInteractorInput {
    func attach(_ output: NewsPhotosModuleInteractorOutput) {
        self.output = output
    }
}

//
//  NewsPhotosModuleNewsPhotosModuleInteractorInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 07/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol NewsPhotosModuleInteractorInput: AnyObject {
    func attach(_ output: NewsPhotosModuleInteractorOutput)
}

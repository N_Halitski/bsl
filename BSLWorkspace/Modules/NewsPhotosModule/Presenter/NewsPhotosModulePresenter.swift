//
//  NewsPhotosModuleNewsPhotosModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 07/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class NewsPhotosModulePresenter {

    private let interactor: NewsPhotosModuleInteractorInput
    private unowned let view: NewsPhotosModulePresenterOutput

    init(_ interactor: NewsPhotosModuleInteractorInput, _ view: NewsPhotosModulePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension NewsPhotosModulePresenter: NewsPhotosModulePresenterInput {}

extension NewsPhotosModulePresenter: NewsPhotosModuleInteractorOutput {}

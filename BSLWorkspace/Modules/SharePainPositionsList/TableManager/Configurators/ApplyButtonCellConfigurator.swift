//
//  ApplyButtonCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 9.01.22.
//

import UIKit

protocol ApplyButtonCellConfiguratorProtocol: BaseConfigurator {
    var cell: ApplyButtonCellProtocol? { get set }
    var applyButtonTapped: (() -> Void)? { get set }
}

final class ApplyButtonCellConfigurator: ApplyButtonCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: ApplyButtonCell.self)
    var cell: ApplyButtonCellProtocol?
    
    var applyButtonTapped: (() -> Void)?
    
    func setupCell(_ applyButtonCell: UIView) {
        guard let applyButtonCell = applyButtonCell as? ApplyButtonCellProtocol else { return }
        cell = applyButtonCell
        cell?.applyButtonTapped = self.applyButtonTapped
    }
    
}

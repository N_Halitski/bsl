//
//  SharePainPostitonsListConfigurators.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 7.01.22.
//

import UIKit

protocol SharePainPositionsConfiguratorsProtocol: BaseConfigurator {
    var sharePainPositionsModel: SharePainPositionsModel? { get set }
    var getPositionSelected: ((SharePainPositionsModel) -> Void)? { get set }
    var cell: SharePainPositionsCellProtocol? { get set }
    
    func didFinishDiscard()
    func discardByCloseButton()
}

final class SharePainPostitonsListConfigurators: SharePainPositionsConfiguratorsProtocol {
    
    var reuseId: String = String(describing: SharePainPositionsCell.self)
    var sharePainPositionsModel: SharePainPositionsModel?
    var cell: SharePainPositionsCellProtocol?
    
    var getPositionSelected: ((SharePainPositionsModel) -> Void)?
    
    func setupCell(_ sharePainPositionsCell: UIView) {
        guard let sharePainPositionsCell = sharePainPositionsCell as? SharePainPositionsCellProtocol,
              let sharePainPositionsCellModel = sharePainPositionsModel else { return }
        cell = sharePainPositionsCell
        sharePainPositionsCell.display(sharePainPositionsCellModel)
        cell?.positionSelected = getPositionSelected
    }
    
    func didFinishDiscard() {
        cell?.didFinishDiscard()
    }
    
    func discardByCloseButton() {
        cell?.discardByCloseButton()
    }
    
}

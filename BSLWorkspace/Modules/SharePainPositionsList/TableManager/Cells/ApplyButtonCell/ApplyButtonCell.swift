//
//  ApplyButtonCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 9.01.22.
//

import UIKit

protocol ApplyButtonCellProtocol {
    var applyButtonTapped: (() -> Void?)? { get set }
}

final class ApplyButtonCell: UITableViewCell, ApplyButtonCellProtocol {
    
    @IBOutlet private weak var applyButton: UIButton!
    
    var applyButtonTapped: (() -> Void?)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        directionalLayoutMargins = .zero
        configureCell()
    }
    
    @IBAction func applyButtonTapped(_ sender: Any) {
        applyButtonTapped?()
    }
    
}

private extension ApplyButtonCell {
    
    func configureCell() {
        applyButton.backgroundColor = AppTheme.Colors.darkBlue100
        applyButton.layer.cornerRadius = 16
        applyButton.setTitle("SharePainFlow.apply".localized, for: .normal)
        applyButton.setTitleColor(AppTheme.Colors.white100, for: .normal)
    }
    
}

//
//  SharePainPositionsCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 8.01.22.
//

import UIKit

protocol SharePainPositionsCellProtocol {
    var positionSelected: ((SharePainPositionsModel) -> Void)? { get set }
    
    func display(_ sharePainPositionsModel: SharePainPositionsModel)
    func didFinishDiscard()
    func discardByCloseButton()
}

final class SharePainPositionsCell: UITableViewCell, SharePainPositionsCellProtocol {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var selectImage: UIImageView!
    
    var sharePainPositionsModel: SharePainPositionsModel?
    
    var positionSelected: ((SharePainPositionsModel) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        configureCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            selectImage.image = UIImage(named: "selectedCell")
            guard let sharePainPositionsModel = sharePainPositionsModel else { return }
            positionSelected?(sharePainPositionsModel)
        } else {
            selectImage.image = UIImage(named: "unselected")
        }
    }
    
    func display(_ sharePainPositionsModel: SharePainPositionsModel) {
        self.sharePainPositionsModel = sharePainPositionsModel
        titleLabel.text = sharePainPositionsModel.title
    }
    
    func didFinishDiscard() {
        selectImage.image = UIImage(named: "unselected")
    }
    
    func discardByCloseButton() {
        selectImage.image = UIImage(named: "unselected")
    }

}

private extension SharePainPositionsCell {
    
    func configureCell() {
        titleLabel.textColor = AppTheme.Colors.black100
        titleLabel.font = AppTheme.Fonts.SFMedium(16)
        
        selectImage.image = UIImage(named: "unselectedCell")
    }
    
}

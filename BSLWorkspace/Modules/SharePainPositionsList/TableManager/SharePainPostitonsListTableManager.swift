//
//  SharePainPostitonsListTableManager.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 7.01.22.
//

import UIKit

protocol SharePainPostitonsTableManagerProtocol {
    var applyButtonTapped: ((SharePainPositionsModel) -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func presentSharePainPositionsCell(with model: [SharePainPositionsModel])
    func presentApplyButtonCell()
    func didFinishDiscard()
    func discardByCloseButton()
}

final class SharePainPostitonsListTableManager: NSObject, SharePainPostitonsTableManagerProtocol {
    
    private weak var tableView: UITableView?
    private var configurators: [BaseConfigurator] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    var sharePainPositionsModel: SharePainPositionsModel?
    
    var applyButtonTapped: ((SharePainPositionsModel) -> Void)?
    
    func attachTable(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        
        let sharePainPositionsCellNib = UINib(nibName: String(describing: SharePainPositionsCell.self), bundle: nil)
        tableView.register(sharePainPositionsCellNib, forCellReuseIdentifier: String(describing: SharePainPositionsCell.self))
        let applyButtonCellNib = UINib(nibName: String(describing: ApplyButtonCell.self), bundle: nil)
        tableView.register(applyButtonCellNib, forCellReuseIdentifier: String(describing: ApplyButtonCell.self))
        
        self.tableView = tableView
    }
    
    func didFinishDiscard() {
        for configurator in configurators {
            if let sharePainPositionConfigurator = configurator as? SharePainPositionsConfiguratorsProtocol {
                sharePainPositionConfigurator.didFinishDiscard()
            }
        }
        self.sharePainPositionsModel = nil
    }
    
    func discardByCloseButton() {
        for configurator in configurators {
            if let sharePainPositionConfigurator = configurator as? SharePainPositionsConfiguratorsProtocol {
                sharePainPositionConfigurator.discardByCloseButton()
            }
        }
        self.sharePainPositionsModel = nil
    }
    
    func presentSharePainPositionsCell(with model: [SharePainPositionsModel]) {
        self.configurators = model.map {
            return createSharePainPositionsCellConfigurator($0)
        }
    }
    
    func presentApplyButtonCell() {
        if let configurator = createApplyButtonConfigurator() {
            self.configurators.append(configurator)
        }
    }
    
    private func createSharePainPositionsCellConfigurator(_ sharePainPositionsCellModel: SharePainPositionsModel) -> SharePainPositionsConfiguratorsProtocol {
        let configurator = SharePainPostitonsListConfigurators()
        configurator.sharePainPositionsModel = sharePainPositionsCellModel
        configurator.getPositionSelected = { [weak self] getSelectedPosition in
            self?.sharePainPositionsModel = getSelectedPosition
        }
        return configurator
    }
    
    private func createApplyButtonConfigurator() -> ApplyButtonCellConfigurator? {
        let configurator = ApplyButtonCellConfigurator()
        configurator.applyButtonTapped = { [weak self] in
            guard let self = self, let model = self.sharePainPositionsModel else { return }
            self.applyButtonTapped?(model)
        }
        return configurator
    }
    
}

extension SharePainPostitonsListTableManager: UITableViewDelegate {

}

extension SharePainPostitonsListTableManager: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        return cell
    }
    
}

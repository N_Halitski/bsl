//
//  SharePainPositionsListSharePainPositionsListAssembler.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class SharePainPositionsListAssembler {
    static func createModule(tableManager: SharePainPostitonsTableManagerProtocol = SharePainPostitonsListTableManager()) -> SharePainPositionsListViewController {
        let viewController = SharePainPositionsListViewController()
        let interactor = SharePainPositionsListInteractor(tableManager: tableManager)
        let presenter = SharePainPositionsListPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

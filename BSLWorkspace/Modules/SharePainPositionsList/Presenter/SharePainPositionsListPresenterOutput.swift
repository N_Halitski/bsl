//
//  SharePainPositionsListSharePainPositionsListPresenterOutput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

protocol SharePainPositionsListPresenterOutput: AnyObject {
    func applyButtonTapped(with positions: SharePainPositionsModel)
}

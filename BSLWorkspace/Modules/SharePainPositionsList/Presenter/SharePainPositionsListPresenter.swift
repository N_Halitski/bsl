//
//  SharePainPositionsListSharePainPositionsListPresenter.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class SharePainPositionsListPresenter {

    private let interactor: SharePainPositionsListInteractorInput
    private unowned let view: SharePainPositionsListPresenterOutput

    init(_ interactor: SharePainPositionsListInteractorInput, _ view: SharePainPositionsListPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension SharePainPositionsListPresenter: SharePainPositionsListPresenterInput {
    
    func discardByCloseButton() {
        interactor.discardByCloseButton()
    }
    
    func didFinishDiscard() {
        interactor.didFinishDiscard()
    }
    
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
    
}

extension SharePainPositionsListPresenter: SharePainPositionsListInteractorOutput {
    
    func applyButtonTapped(with positions: SharePainPositionsModel) {
        view.applyButtonTapped(with: positions)
    }
    
}

//
//  SharePainPositionsListSharePainPositionsListInteractorInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol SharePainPositionsListInteractorInput: AnyObject {
    func attach(_ output: SharePainPositionsListInteractorOutput)
    func attachTableView(_ table: UITableView)
    func didFinishDiscard()
    func discardByCloseButton()
}

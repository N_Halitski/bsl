//
//  SharePainPositionsListSharePainPositionsListInteractor.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class SharePainPositionsListInteractor {
    
    private weak var output: SharePainPositionsListInteractorOutput?
    private var tableManager: SharePainPostitonsTableManagerProtocol
    
    init(tableManager: SharePainPostitonsTableManagerProtocol) {
        self.tableManager = tableManager
        self.tableManager.applyButtonTapped = { [weak self] selectedPositions in
            self?.output?.applyButtonTapped(with: selectedPositions)
        }
    }
    
}

extension SharePainPositionsListInteractor: SharePainPositionsListInteractorInput {
    
    func discardByCloseButton() {
        tableManager.discardByCloseButton()
    }
    
    func didFinishDiscard() {
        tableManager.didFinishDiscard()
    }
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
        tableManager.presentSharePainPositionsCell(with: sharePainPositionsData)
        tableManager.presentApplyButtonCell()
    }
    
    func attach(_ output: SharePainPositionsListInteractorOutput) {
        self.output = output
    }
    
}

private extension SharePainPositionsListInteractor {
    
    var sharePainPositionsData: [SharePainPositionsModel] {
        [
            .init(title: "Lead"),
            .init(title: "Project Manager"),
            .init(title: "HR")
        ]
    }
    
}

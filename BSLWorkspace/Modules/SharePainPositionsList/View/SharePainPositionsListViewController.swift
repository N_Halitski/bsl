//
//  SharePainPositionsListSharePainPositionsListViewController.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class SharePainPositionsListViewController: BaseViewController {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var fringeLabel: UILabel!
    @IBOutlet private weak var discardButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    
    var presenter: SharePainPositionsListPresenterInput!
    
    var didFinishClosing: (() -> Void)?
    var didFinishDiscard: (() -> Void)?
    var didFinishApplyButton: ((SharePainPositionsModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        configure()
        navigationController?.navigationBar.isHidden = true
        
        closeButton.addTarget(self, action: #selector(closeButtonTapped(_:)), for: .touchUpInside)
        discardButton.addTarget(self, action: #selector(discardButtonTapped(_:)), for: .touchUpInside)
    }
    
    @objc func closeButtonTapped(_ sender: Any) {
        didFinishClosing?()
        presenter.discardByCloseButton()
    }
    
    @objc func discardButtonTapped(_ sender: Any) {
        didFinishDiscard?()
        presenter.didFinishDiscard()
    }
 
}

private extension SharePainPositionsListViewController {
    
    func configure() {
        fringeLabel.backgroundColor = AppTheme.Colors.gray100
        fringeLabel.layer.cornerRadius = 2
        
        discardButton.setTitle("SharePainFlow.discard".localized, for: .normal)
        discardButton.tintColor = AppTheme.Colors.darkGray
        
        titleLabel.text = "SharePainFlow.sendingPain".localized
        titleLabel.textColor = AppTheme.Colors.black100
        titleLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        closeButton.setTitle("", for: .normal)
        closeButton.tintColor = AppTheme.Colors.black100
    }
    
}

extension SharePainPositionsListViewController: SharePainPositionsListPresenterOutput {
    
    func applyButtonTapped(with positions: SharePainPositionsModel) {
        self.didFinishApplyButton?(positions)
    }
    
}

//
//  RejectVacationRejectVacationAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 25/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class RejectVacationAssembler {
    static func createModule() -> RejectVacationViewController {
        let viewController = RejectVacationViewController()
        let interactor = RejectVacationInteractor()
        let presenter = RejectVacationPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

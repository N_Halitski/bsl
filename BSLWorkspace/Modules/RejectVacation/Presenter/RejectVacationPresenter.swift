//
//  RejectVacationRejectVacationPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 25/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class RejectVacationPresenter {

    private let interactor: RejectVacationInteractorInput
    private unowned let view: RejectVacationPresenterOutput

    init(_ interactor: RejectVacationInteractorInput, _ view: RejectVacationPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension RejectVacationPresenter: RejectVacationPresenterInput {

}

extension RejectVacationPresenter: RejectVacationInteractorOutput {

}

//
//  RejectVacationRejectVacationInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 25/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class RejectVacationInteractor {
    
    private weak var output: RejectVacationInteractorOutput?

}

extension RejectVacationInteractor: RejectVacationInteractorInput {
    func attach(_ output: RejectVacationInteractorOutput) {
        self.output = output
    }
}

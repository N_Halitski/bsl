//
//  RejectTextField.swift
//  BSLWorkspace
//
//  Created by Nikita on 25.01.2022.
//

import UIKit

final class RejectTextField: TextFieldEffects {
    
    private enum Constants {
        static let placeholderFontBig: UIFont = .systemFont(ofSize: 16)
        static let placeholderFontSmall: UIFont = .systemFont(ofSize: 12)
        static let placeholderColor: UIColor = AppTheme.Colors.darkGray
    }
    
    override var bounds: CGRect {
        didSet {
            updatePlaceholder()
        }
    }
    
    private let placeholderInsets = CGPoint(x: 16, y: 0)
    private let textFieldInsets = CGPoint(x: 16, y: 12)
    private let inactiveBorderLayer = CALayer()
    private let activeBorderLayer = CALayer()
    private var activePlaceholderPoint: CGPoint = CGPoint.zero
    
    // MARK: - TextFieldEffects
    
    override func drawViewsForRect(_ rect: CGRect) {
        let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: rect.size.width, height: rect.size.height))
        
        placeholderLabel.frame = frame.insetBy(dx: placeholderInsets.x, dy: placeholderInsets.y)
        placeholderLabel.font = Constants.placeholderFontBig
        updatePlaceholder()
        
        layer.addSublayer(inactiveBorderLayer)
        layer.addSublayer(activeBorderLayer)
        addSubview(placeholderLabel)
    }
    
    override func animateViewsForTextEntry() {
        if text!.isEmpty {
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                self.placeholderLabel.frame.origin = CGPoint(x: 10, y: self.placeholderLabel.frame.origin.y)
                self.placeholderLabel.alpha = 0
                self.placeholderLabel.font = Constants.placeholderFontSmall
            }), completion: { _ in
                self.animationCompletionHandler?(.textEntry)
            })
        }
        
        layoutPlaceholderInTextRect()
        placeholderLabel.frame.origin = activePlaceholderPoint
        
        UIView.animate(withDuration: 0.4, animations: {
            self.placeholderLabel.alpha = 1.0
        })
    }
    
    override func animateViewsForTextDisplay() {
        placeholderLabel.font = Constants.placeholderFontBig
        
        if text!.isEmpty {
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.0, options: .beginFromCurrentState, animations: ({
                self.layoutPlaceholderInTextRect()
                self.placeholderLabel.alpha = 1
            }), completion: { _ in
                self.animationCompletionHandler?(.textDisplay)
            })
        }
    }
    
    // MARK: - Private
    
    private func updatePlaceholder() {
        placeholderLabel.text = placeholder
        placeholderLabel.textColor = Constants.placeholderColor
        placeholderLabel.sizeToFit()
        layoutPlaceholderInTextRect()
        
        if isFirstResponder || text!.isNotEmpty {
            animateViewsForTextEntry()
        }
    }
    
    // Method to install location placeholder
    private func layoutPlaceholderInTextRect() {
        let textRect = self.textRect(forBounds: bounds)
        let originX = textRect.origin.x
        
        placeholderLabel.frame = CGRect(x: originX,
                                        y: textRect.height/3,
                                        width: placeholderLabel.bounds.width,
                                        height: placeholderLabel.bounds.height)
        activePlaceholderPoint = CGPoint(x: placeholderLabel.frame.origin.x,
                                         y: placeholderLabel.frame.minY - 9)
    }
    
    // MARK: - Overrides
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        placeholderLabel.font = Constants.placeholderFontSmall
        return bounds.offsetBy(dx: textFieldInsets.x, dy: textFieldInsets.y)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        placeholderLabel.sizeToFit()
        return bounds.offsetBy(dx: textFieldInsets.x, dy: textFieldInsets.y)
    }
    
}

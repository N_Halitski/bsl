//
//  RejectVacationRejectVacationViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 25/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class RejectVacationViewController: BaseViewController {
    
    @IBOutlet weak var rejectLabel: UILabel!
    @IBOutlet weak var rejectTextField: RejectTextField!
    @IBOutlet weak var viewBelowField: UIView!
    @IBOutlet weak var viewBelowApplyButton: UIView!
    @IBOutlet weak var applyButtonOutlet: UIButton!
    @IBOutlet weak var closeButtonOutlet: UIButton!
    
    var presenter: RejectVacationPresenterInput!
    var raiseUpFloatingPanel: (() -> Void)?
    var dropDownFloatingPanel: (() -> Void)?
    var didFinishClosing: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
        setupNotificationCenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.isHidden = true
        closeButtonOutlet.addTarget(self, action: #selector(closePanelTapped(_:)), for: .touchUpInside)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        dropDownFloatingPanel?()
        view.endEditing(true)
    }
    
    @objc func closePanelTapped(_ sender: Any) {
        didFinishClosing?()
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            raiseUpFloatingPanel?()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            dropDownFloatingPanel?()
        }
    }
    
    private func setupUI() {
        rejectLabel.text = "RejectVacation.rejection".localized
        rejectLabel.textColor = AppTheme.Colors.black100
        rejectLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        applyButtonOutlet.setTitle("RejectVacation.apply".localized, for: .normal)
        applyButtonOutlet.titleLabel?.font = AppTheme.Fonts.SFMedium(16)
        applyButtonOutlet.tintColor = AppTheme.Colors.white100
        
        viewBelowField.layer.cornerRadius = 16
        viewBelowApplyButton.layer.cornerRadius = 16
        
        closeButtonOutlet.setTitle("", for: .normal)
        
        rejectTextField.placeholder = "RejectVacation.resonForRejection".localized
        navigationController?.navigationBar.isHidden = true
    }
    
    private func setupNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func applyButton(_ sender: Any) {
        
    }
    
}

extension RejectVacationViewController: RejectVacationPresenterOutput {
    
}

//
//  VacationReviewer.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.01.2022.
//

import Foundation

struct VacationReviewer {
    var name: String?
    var position: String?
    var reviewStatus: SigningStatus?
}

enum SigningStatus: String {
    case approved = "SigningView.approved"
    case inProgress = "SigningView.inProgress"
    case rejected = "SigningView.rejected"
    case noSolution = "SigningView.noSolution"
}

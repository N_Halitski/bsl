//
//  EmployeeForVacationRequest.swift
//  BSLWorkspace
//
//  Created by Nikita on 08.01.2022.
//

import Foundation

struct EmployeeForVacationRequest {
    var name: String?
    var position: String?
    var lastVacation: String?
    var daysRemaining: String?
    var inCompanySince: String?
}

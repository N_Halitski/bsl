//
//  ReviewerCollectionViewCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.01.2022.
//

import UIKit

protocol ReviewerCollectionViewCellProtocol {
    func setupCell(_ reviewer: VacationReviewer)
}

final class ReviewerCollectionViewCell: UICollectionViewCell, ReviewerCollectionViewCellProtocol {
    
    @IBOutlet private var statusView: UIView!
    @IBOutlet private var reviewerImage: UIImageView!
    @IBOutlet private var reviewerName: UILabel!
    @IBOutlet private var reviewerPosition: UILabel!
    @IBOutlet private var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupCell (_ reviewer: VacationReviewer) {
        reviewerName.text = reviewer.name
        reviewerPosition.text = reviewer.position
        status.text = reviewer.reviewStatus?.rawValue.localized
        statusView.backgroundColor = setBackgroundColor(reviewer.reviewStatus ?? .noSolution)
    }
    
    private func setupUI() {
        statusView.backgroundColor = AppTheme.Colors.green1
        
        reviewerImage.layer.cornerRadius = 16
        
        reviewerName.font = AppTheme.Fonts.SFMedium(14)
        reviewerName.textColor = AppTheme.Colors.black100
        
        reviewerPosition.font = AppTheme.Fonts.SFRegular(11)
        reviewerPosition.textColor = AppTheme.Colors.darkGray
        
        statusView.layer.cornerRadius = 8
        statusView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        status.font = AppTheme.Fonts.SFRegular(12)
        status.textColor = AppTheme.Colors.white100
    }
    
    private func setBackgroundColor(_ signingStatus: SigningStatus) -> UIColor {
        switch signingStatus {
        case .approved:
            return AppTheme.Colors.green1
        case .inProgress:
            return AppTheme.Colors.yellow100
        case .rejected:
            return AppTheme.Colors.red100
        case .noSolution:
            return AppTheme.Colors.gray100
        }
    }

}

//
//  ProcessingVacationTableManager.swift
//  BSLWorkspace
//
//  Created by Nikita on 08.01.2022.
//

import UIKit

protocol ProcessingVacationTableManagerProtocol {
    func attach (_ tableView: UITableView)
    func setDataToTable(_ employee: EmployeeForVacationRequest, _ comment: Comment, _ reviewers: [VacationReviewer])
}

final class ProcessingVacationTableManager: NSObject, ProcessingVacationTableManagerProtocol {
    
    private var tableView: UITableView?
    private var employee: EmployeeForVacationRequest?
    private var comment: Comment?
    private var reviewers: [VacationReviewer]?
    private var configurators: [BaseConfigurator] = [] {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    func attach(_ tableView: UITableView) {
        self.tableView = tableView
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        setupEmployeeTableView()
    }
    
    func setDataToTable(_ employee: EmployeeForVacationRequest, _ comment: Comment, _ reviewers: [VacationReviewer]) {
        self.configurators.append(createEmployeeConfigurator(employee))
        self.configurators.append(createCommentConfigurator(comment))
        self.configurators.append(createWarningConfigurator())
        self.configurators.append(createReviewerConfigurator(reviewers))
    }
    
}

extension ProcessingVacationTableManager {
    
    private func createEmployeeConfigurator(_ employee: EmployeeForVacationRequest) -> ProcessingVacationTableConfigurator {
        let configurator = ProcessingVacationCellConfigurator()
        configurator.employee = employee
        return configurator
    }
    
    private func createCommentConfigurator(_ comment: Comment) -> CommentToRequestTableConfigurator {
        let configurator = CommentToRequestCellConfigurator()
        configurator.comment = comment
        return configurator
    }
    
    private func createWarningConfigurator() -> WarningTableConfigurator {
        let configurator = WarningCellConfigurator()
        return configurator
    }
    
    private func createReviewerConfigurator(_ reviewers: [VacationReviewer]) -> VacationReviewerTableConfigurator {
        let configurator = VacationReviewerCellConfigurator()
        configurator.reviewers = reviewers
        return configurator
    }
    
    private func setupEmployeeTableView() {
        tableView?.registerCell(RequestVacationEmployeeCell.self)
        tableView?.registerCell(CommentToRequestCell.self)
        tableView?.registerCell(WarningCell.self)
        tableView?.registerCell(VacationStatusCell.self)
        
        self.tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: UIScreen.main.bounds.height * 0.14, right: 0)
        self.tableView?.backgroundColor = AppTheme.Colors.lightGray100
    }
    
}

extension ProcessingVacationTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        
        return cell
    }
    
}

//
//  WarningCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 25.01.2022.
//

import UIKit

protocol WarningCellProtocol {
    
}

final class WarningCell: UITableViewCell, WarningCellProtocol {

    @IBOutlet private var warningView: UIView!
    @IBOutlet private var yellowWarningView: UIView!
    @IBOutlet private var vacationIntersectionLabel: UILabel!
    @IBOutlet private var intersectionOfVacationsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    private func setupUI() {
        warningView.layer.cornerRadius = 16
        warningView.backgroundColor = AppTheme.Colors.white100
        warningView.clipsToBounds = true
        
        yellowWarningView.backgroundColor = AppTheme.Colors.yellow100
        
        vacationIntersectionLabel.text = "ProcessingRequestVacationScreen.vacationIntersection".localized
        vacationIntersectionLabel.textColor = AppTheme.Colors.black100
        vacationIntersectionLabel.font = AppTheme.Fonts.SFMedium(16)
        
        intersectionOfVacationsLabel.text = "ProcessingRequestVacationScreen.intersectionOfVacations".localized
        intersectionOfVacationsLabel.textColor = AppTheme.Colors.darkGray
        intersectionOfVacationsLabel.font = AppTheme.Fonts.SFRegular(14)
        
        selectionStyle = .none
    }
    
}

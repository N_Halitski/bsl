//
//  CommentToRequestCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 25.01.2022.
//

import UIKit

protocol CommentToRequestCellProtocol {
    func display (_ comment: Comment)
}

final class CommentToRequestCell: UITableViewCell, CommentToRequestCellProtocol {

    @IBOutlet private var commentToRequestLabel: UILabel!
    @IBOutlet private var commentTextLabel: UILabel!
    @IBOutlet private var viewForComment: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func display(_ comment: Comment) {
        commentTextLabel.text = comment.text
    }
    
   private func setupUI() {
       viewForComment.layer.cornerRadius = 16
       viewForComment.backgroundColor = AppTheme.Colors.white100
       
       commentToRequestLabel.text = "ProcessingRequestVacationScreen.commentToRequest".localized
       commentToRequestLabel.font = AppTheme.Fonts.SFSemiBold(20)
       commentToRequestLabel.textColor = AppTheme.Colors.black100
       
       commentTextLabel.font = AppTheme.Fonts.SFRegular(16)
       commentTextLabel.textColor = AppTheme.Colors.black100
       selectionStyle = .none
    }
    
}

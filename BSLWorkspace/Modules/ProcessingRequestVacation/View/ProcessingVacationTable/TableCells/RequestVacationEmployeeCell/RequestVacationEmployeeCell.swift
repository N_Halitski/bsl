//
//  RequestVacationEmployeeCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 08.01.2022.
//

import UIKit

protocol RequestVacationEmployeeCellProtocol {
    func setupCell (with employee: EmployeeForVacationRequest)
}

final class RequestVacationEmployeeCell: UITableViewCell, RequestVacationEmployeeCellProtocol {

    @IBOutlet private var employeeDescrBackgroundView: UIView!
    @IBOutlet private var employeeName: UILabel!
    @IBOutlet private var employeePosition: UILabel!
    @IBOutlet private var employeeImage: UIImageView!
    @IBOutlet private var lastVacationView: UIView!
    @IBOutlet private var lastVacationImage: UILabel!
    @IBOutlet private var lastVacationLabel: UILabel!
    @IBOutlet private var lastVacationDates: UILabel!
    @IBOutlet private var remainDaysView: UIView!
    @IBOutlet private var remainDaysImage: UILabel!
    @IBOutlet private var remainDaysLabel: UILabel!
    @IBOutlet private var numberOfDays: UILabel!
    @IBOutlet private var worksInCompanyView: UIView!
    @IBOutlet private var worksInCompanyImage: UILabel!
    @IBOutlet private var worksInCompanyLabel: UILabel!
    @IBOutlet private var worksInCompanySince: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupCell(with employee: EmployeeForVacationRequest) {
        employeeName.text = employee.name
        employeePosition.text = employee.position
        lastVacationDates.text = employee.lastVacation
        numberOfDays.text = employee.daysRemaining
        worksInCompanySince.text = employee.inCompanySince
    }
    
    private func setupUI() {
        employeeDescrBackgroundView.layer.cornerRadius = 16
        employeeDescrBackgroundView.backgroundColor = AppTheme.Colors.white100
        employeeName.font = AppTheme.Fonts.SFSemiBold(20)
        employeeName.textColor = AppTheme.Colors.black100
        employeePosition.textColor = AppTheme.Colors.darkGray
        employeePosition.font = AppTheme.Fonts.SFMedium(14)
        employeeImage.layer.cornerRadius = 16
        
        setupViews(lastVacationView, lastVacationLabel, lastVacationDates)
        lastVacationImage.text = "🌴"
        lastVacationLabel.text = "ProcessingRequestVacationScreen.lastVacation".localized
        
        setupViews(remainDaysView, remainDaysLabel, numberOfDays)
        remainDaysImage.text = "🗓"
        remainDaysLabel.text = "ProcessingRequestVacationScreen.vacationDaysRemain".localized

        setupViews(worksInCompanyView, worksInCompanyLabel, worksInCompanySince)
        worksInCompanyImage.text = "👨‍💻"
        worksInCompanyLabel.text = "ProcessingRequestVacationScreen.inCompanySince".localized
        
        selectionStyle = .none
    }
    
    private func setupViews(_ view: UIView?, _ majorLabel: UILabel?, _ minorLabel: UILabel?) {
        view?.layer.cornerRadius = 8
        view?.backgroundColor = AppTheme.Colors.white100
        view?.layer.borderWidth = 1
        view?.layer.borderColor = AppTheme.Colors.gray100.cgColor
        
        majorLabel?.textColor = AppTheme.Colors.black100
        majorLabel?.font = AppTheme.Fonts.SFRegular(14)
        
        minorLabel?.textColor = AppTheme.Colors.darkGray
        minorLabel?.font = AppTheme.Fonts.SFRegular(12)
    }
    
}

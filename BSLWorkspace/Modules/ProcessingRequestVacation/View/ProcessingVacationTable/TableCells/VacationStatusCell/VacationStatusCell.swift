//
//  VacationStatusCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.01.2022.
//

import UIKit

protocol VacationStatusCellProtocol {
    func setupCell(_ reviewers: [VacationReviewer])
}

final class VacationStatusCell: UITableViewCell, VacationStatusCellProtocol {

    @IBOutlet private weak var vacationStatusBackgroundView: UIView!
    @IBOutlet private weak var vacationStatusMajorLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var reviewers: [VacationReviewer] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupCollectionView()
    }
    
    func setupCell(_ reviewers: [VacationReviewer]) {
        self.reviewers = reviewers
    }

    private func setupUI() {
        vacationStatusBackgroundView.backgroundColor = AppTheme.Colors.white100
        vacationStatusBackgroundView.layer.cornerRadius = 16
        
        vacationStatusMajorLabel.text = "ProcessingRequestVacationScreen.signingStatus".localized
        vacationStatusMajorLabel.textColor = AppTheme.Colors.black100
        vacationStatusMajorLabel.font = AppTheme.Fonts.SFSemiBold(20)
        selectionStyle = .none
        setupCollectionViewLayout()
    }
    
    private func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        let statusNib = UINib(nibName: String(describing: ReviewerCollectionViewCell.self), bundle: nil)
        collectionView.register(statusNib, forCellWithReuseIdentifier: String(describing: ReviewerCollectionViewCell.self))
    }
    
    private func setupCollectionViewLayout () {
        let layout = UICollectionViewFlowLayout()
        let inset: CGFloat = 8
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 116, height: 164)
        layout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.setCollectionViewLayout(layout, animated: false)
    }
}

extension VacationStatusCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        reviewers.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let reviewerCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: ReviewerCollectionViewCell.self),
            for: indexPath) as? ReviewerCollectionViewCellProtocol else { return UICollectionViewCell() }
        reviewerCell.setupCell(reviewers[indexPath.row])
        guard let reviewerCell = reviewerCell as? ReviewerCollectionViewCell else { return  UICollectionViewCell() }
        return reviewerCell
    }
    
}

//
//  VacationReviewerTableConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 09.01.2022.
//

import UIKit

protocol VacationReviewerTableConfigurator: BaseConfigurator {
    var reviewers: [VacationReviewer]? { get set }
}

final class VacationReviewerCellConfigurator: VacationReviewerTableConfigurator {
    
    var reuseId: String {String(describing: VacationStatusCell.self)}
    
    var reviewers: [VacationReviewer]?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? VacationStatusCellProtocol,
              let reviewers = reviewers else { return }
        cell.setupCell(reviewers)
    }
}

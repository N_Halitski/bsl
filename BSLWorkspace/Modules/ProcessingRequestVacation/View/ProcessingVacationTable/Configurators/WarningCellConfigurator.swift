//
//  WarningCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 25.01.2022.
//

import UIKit

protocol WarningTableConfigurator: BaseConfigurator {
    
}

final class WarningCellConfigurator: WarningTableConfigurator {
    
    var reuseId: String {String(describing: WarningCell.self)}
    
    func setupCell(_ cell: UIView) {
        
    }

}

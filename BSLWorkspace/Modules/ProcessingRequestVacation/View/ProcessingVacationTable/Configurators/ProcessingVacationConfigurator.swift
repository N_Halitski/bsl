//
//  ProcessingVacationConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 08.01.2022.
//

import UIKit

protocol ProcessingVacationTableConfigurator: BaseConfigurator {
    var employee: EmployeeForVacationRequest? { get set }
}

final class ProcessingVacationCellConfigurator: ProcessingVacationTableConfigurator {
    
    var reuseId: String {String(describing: RequestVacationEmployeeCell.self)}
    var employee: EmployeeForVacationRequest?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? RequestVacationEmployeeCellProtocol,
              let employee = employee else { return }
        cell.setupCell(with: employee)
    }
}

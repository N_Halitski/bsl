//
//  CommentToRequestConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 25.01.2022.
//

import UIKit

protocol CommentToRequestTableConfigurator: BaseConfigurator {
    var comment: Comment? { get set }
}

final class CommentToRequestCellConfigurator: CommentToRequestTableConfigurator {
    
    var reuseId: String {String(describing: CommentToRequestCell.self)}
    var comment: Comment?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? CommentToRequestCellProtocol,
              let comment = comment else { return }
        cell.display(comment)
    }
    
}

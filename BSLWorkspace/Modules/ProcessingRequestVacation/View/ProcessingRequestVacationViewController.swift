//
//  ProcessingRequestVacationProcessingRequestVacationViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit
import FloatingPanel

final class ProcessingRequestVacationViewController: BaseViewController {
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var headerView: UIView!
    @IBOutlet private var vacationInfoLabel: UILabel!
    @IBOutlet private var viewBelowButtons: UIView!
    @IBOutlet private var approveVacationView: UIView!
    @IBOutlet private var rejectVacationView: UIView!
    @IBOutlet private var viewBelowStack: UIView!
    
    private var fpc: FloatingPanelController?
    private var rejectViewController: RejectVacationViewController?
    
    var presenter: ProcessingRequestVacationPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attach(tableView)
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layoutIfNeeded()
    }
    
    private func setupUI() {
        headerView.backgroundColor = AppTheme.Colors.lightGray100
        
        vacationInfoLabel.font = AppTheme.Fonts.SFSemiBold(16)
        vacationInfoLabel.textColor = AppTheme.Colors.black100
        vacationInfoLabel.text = ("\("ProcessingRequestVacationScreen.vacationRequest".localized) 11-15 октября (5 дней)")
        
        approveVacationView.backgroundColor = AppTheme.Colors.darkBlue100
        approveVacationView.layer.cornerRadius = 16
        
        rejectVacationView.backgroundColor = AppTheme.Colors.white100
        rejectVacationView.layer.cornerRadius = 16
        
        rejectVacationView.addShadow(shadowColor: AppTheme.Colors.black100.cgColor, shadowOffset: CGSize(width: -1, height: 1), shadowOpacity: 0.1, shadowRadius: 16)
        rejectVacationView.layer.shadowPath = UIBezierPath(rect: rejectVacationView.bounds).cgPath
        rejectVacationView.layer.shouldRasterize = true
        rejectVacationView.layer.rasterizationScale = UIScreen.main.scale
        
        viewBelowStack.backgroundColor = AppTheme.Colors.white100
        viewBelowButtons.backgroundColor = AppTheme.Colors.white100
    }
    
    @IBAction func approveVacation(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func rejectVacation(_ sender: Any) {
        setupRejectVacationFloatingPannel()
    }
    
}

extension ProcessingRequestVacationViewController: ProcessingRequestVacationPresenterOutput {
    
}

extension ProcessingRequestVacationViewController: FloatingPanelControllerDelegate {
    
    func createRejectVacationVC() -> RejectVacationViewController {
        if rejectViewController == nil {
            let rejectViewController = RejectVacationAssembler.createModule()
            self.rejectViewController = rejectViewController
        }
        guard let rejectViewController = rejectViewController else { return  RejectVacationAssembler.createModule() }
        return rejectViewController
    }
    
    func setupRejectVacationFloatingPannel() {
        fpc = FloatingPanelController()
        guard let fpc = fpc else { return }
        fpc.delegate = self
        fpc.layout = RejectVacationFloatingPanelLayout()
        let rejectVacationVC = createRejectVacationVC()
        rejectVacationVC.raiseUpFloatingPanel = { [weak self] in
            guard let self = self else { return }
            self.fpc?.move(to: .half, animated: true, completion: nil)
        }
        rejectVacationVC.dropDownFloatingPanel = { [weak self] in
            guard let self = self else { return }
            self.fpc?.move(to: .tip, animated: true, completion: nil)
        }
        rejectVacationVC.didFinishClosing = { [weak self] in
            guard let self = self else { return }
            self.fpc!.hide(animated: true) {
                self.fpc!.removeFromParent()
            }
        }
        fpc.set(contentViewController: UINavigationController(rootViewController: rejectVacationVC))
        fpc.addPanel(toParent: self, at: -1, animated: true, completion: nil)
        let appearance = SurfaceAppearance()
        appearance.cornerRadius = 16.0
        fpc.surfaceView.appearance = appearance
        fpc.surfaceView.grabberHandle.isHidden = true
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(closeFloatingPanel))
        fpc.backdropView.addGestureRecognizer(recognizer)
        fpc.panGestureRecognizer.isEnabled = false
        self.fpc = fpc
    }
    
    @objc private func closeFloatingPanel() {
        fpc?.move(to: .hidden, animated: true)
        guard let rejectViewController = rejectViewController else { return }
        rejectViewController.view.endEditing(true)
    }
    
}

final class RejectVacationFloatingPanelLayout: FloatingPanelLayout {
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return [
            .tip: FloatingPanelLayoutAnchor(absoluteInset: 194, edge: .bottom, referenceGuide: .safeArea),
            .half: FloatingPanelLayoutAnchor(absoluteInset: 500, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
    
    func backdropAlpha(for state: FloatingPanelState) -> CGFloat {
        switch state {
        case .tip, .half : return 0.3
        default: return 0.0
        }
    }
    
}

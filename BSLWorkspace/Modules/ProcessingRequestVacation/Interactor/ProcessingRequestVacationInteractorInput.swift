//
//  ProcessingRequestVacationProcessingRequestVacationInteractorInput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol ProcessingRequestVacationInteractorInput: AnyObject {
    func attach(_ output: ProcessingRequestVacationInteractorOutput)
    func attach(_ tableView: UITableView)
}

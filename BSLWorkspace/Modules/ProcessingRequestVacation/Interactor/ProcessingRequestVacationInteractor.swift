//
//  ProcessingRequestVacationProcessingRequestVacationInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class ProcessingRequestVacationInteractor {
    
    private weak var output: ProcessingRequestVacationInteractorOutput?
    private var tableManager: ProcessingVacationTableManagerProtocol
    private var employee: EmployeeForVacationRequest?
    private var comment: Comment?
    private var reviewers: [VacationReviewer] = []
    
    init(tableManager: ProcessingVacationTableManagerProtocol) {
        self.tableManager = tableManager
    }
    
    private func createEmloyee() {
        let employee = EmployeeForVacationRequest(
            name: "Egor Gordey",
            position: "UI/UX Designer",
            lastVacation: "08.12.97 - 08.12.97",
            daysRemaining: "24",
            inCompanySince: "c 31.01.2022")
        self.employee = employee
    }
    
    private func createReviewers() {
        let reviewer1 = VacationReviewer(name: "Kirill Alimov", position: "Lead iOS Developer", reviewStatus: .some(.approved))
        let reviewer2 = VacationReviewer(name: "Vasiliy Volkov", position: "UI/UX Designer", reviewStatus: .some(.inProgress))
        let reviewer3 = VacationReviewer(name: "Daniil Tumash", position: "Analyst", reviewStatus: .some(.rejected))
        
        reviewers.append(reviewer1)
        reviewers.append(reviewer2)
        reviewers.append(reviewer3)
    }
    
    private func createComment() {
        let comment = Comment(
        // swiftlint:disable all
            text: "С другой стороны консультация с широким активом требуют определения и уточнения существенных финансовыхи административных условий. С другой стороны консультация с широким активом требуют определения и уточнения существенных финансовыхи административных условий.")
        // swiftlint:enable all
        self.comment = comment
    }
    
}

extension ProcessingRequestVacationInteractor: ProcessingRequestVacationInteractorInput {
    
    func attach(_ tableView: UITableView) {
        tableManager.attach(tableView)
        createEmloyee()
        createComment()
        createReviewers()
        guard let employee = employee,
              let comment = comment else { return }
        tableManager.setDataToTable(employee, comment, reviewers)
    }
    
    func attach(_ output: ProcessingRequestVacationInteractorOutput) {
        self.output = output
    }
    
}

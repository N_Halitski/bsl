//
//  ProcessingRequestVacationProcessingRequestVacationPresenterInput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol ProcessingRequestVacationPresenterInput: BasePresenting {
    func attach(_ tableView: UITableView)
}

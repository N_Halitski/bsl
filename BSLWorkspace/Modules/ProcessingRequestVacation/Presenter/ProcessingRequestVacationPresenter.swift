//
//  ProcessingRequestVacationProcessingRequestVacationPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class ProcessingRequestVacationPresenter {

    private let interactor: ProcessingRequestVacationInteractorInput
    private unowned let view: ProcessingRequestVacationPresenterOutput

    init(_ interactor: ProcessingRequestVacationInteractorInput, _ view: ProcessingRequestVacationPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
    
}

extension ProcessingRequestVacationPresenter: ProcessingRequestVacationPresenterInput {
    
    func attach(_ tableView: UITableView) {
        interactor.attach(tableView)
    }
    
}

extension ProcessingRequestVacationPresenter: ProcessingRequestVacationInteractorOutput {

}

//
//  ProcessingRequestVacationProcessingRequestVacationAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 07/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import Foundation

final class ProcessingRequestVacationAssembler {
    static func createModule(_ tableManager: ProcessingVacationTableManagerProtocol = ProcessingVacationTableManager()) -> ProcessingRequestVacationViewController {
        let viewController = ProcessingRequestVacationViewController()
        let interactor = ProcessingRequestVacationInteractor(tableManager: tableManager)
        let presenter = ProcessingRequestVacationPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

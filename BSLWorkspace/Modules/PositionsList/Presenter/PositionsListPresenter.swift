//
//  PositionsListPositionsListPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class PositionsListPresenter {

    private let interactor: PositionsListInteractorInput
    private unowned let view: PositionsListPresenterOutput

    init(_ interactor: PositionsListInteractorInput, _ view: PositionsListPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension PositionsListPresenter: PositionsListPresenterInput {
    
    func clearPositions(_ positions: [Position]) {
        interactor.clearPositions(positions)
    }
    
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
}

extension PositionsListPresenter: PositionsListInteractorOutput {
    
    func showPositions(_ positions: [Position]) {
        view.showPositions(positions)
    }
}

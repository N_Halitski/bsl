//
//  PositionsListPositionsListInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol PositionsListInteractorOutput: AnyObject {
    func showPositions(_ positions: [Position])
}

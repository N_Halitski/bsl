//
//  PositionsListPositionsListInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class PositionsListInteractor {
    
    private weak var output: PositionsListInteractorOutput?
    
    private var tableManager: PositionsTableManagerProtocol
    private var  positionsList: [Position]
    
    init(tableManager: PositionsTableManagerProtocol, positionsList: [Position]) {
        self.tableManager = tableManager
        self.positionsList = positionsList
        self.tableManager.showPositions(positionsList)
    }
}

extension PositionsListInteractor: PositionsListInteractorInput {

    func clearPositions(_ positions: [Position]) {
        tableManager.clearPositions(positions)
    }
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
        tableManager.showPositionOnFloatingPanel = { [weak output] positionList in
            output?.showPositions(positionList)
        }
    }
    
    func attach(_ output: PositionsListInteractorOutput) {
        self.output = output
    }
}

//
//  PositionsTableManager.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol PositionsTableManagerProtocol {
    var showPositionOnFloatingPanel: (([Position]) -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func showPositions(_ positions: [Position])
    func clearPositions(_ positions: [Position])
}

final class PositionsTableManager: NSObject, PositionsTableManagerProtocol {
    
    private var table: UITableView?
    
    var showPositionOnFloatingPanel: (([Position]) -> Void)?
    
    private var configurators: [PositionsTableConfigurator] = []
    private var presellectedPositions: [Position] = []
    
    private var positions: [Position] = [] {
        didSet {
            showPositionOnFloatingPanel?(positions)
        }
    }
    
    func attachTable(_ table: UITableView) {
        self.table = table
        table.delegate = self
        table.dataSource = self
        table.allowsSelection = true
        
        let positionCellNib = UINib(nibName: String(describing: PositionCell.self), bundle: nil)
        self.table?.register(positionCellNib, forCellReuseIdentifier: String(describing: PositionCell.self))
    }
    
    func showPositions(_ positions: [Position]) {
        var output: [PositionsTableConfigurator] = []
        positions.forEach { position in
            output.append(createPositionConfigurator(position))
        }
        self.configurators = output
        table?.reloadData()
    }
    
    private func createPositionConfigurator(_ positionModel: Position) -> PositionsTableConfigurator {
        let configurator = PositionCellConfigurator()
        configurator.positionModel = positionModel
        configurator.getPosition = { [weak self] position in
            self?.positions.append(position)
        }
        configurator.getDeselectedPosition = { [weak self] position in
            self?.positions.removeAll(where: { positionToDelete in
                positionToDelete.codename == position.codename
            })
        }
        return configurator
    }
    
    func clearPositions(_ positions: [Position]) {
        self.positions = positions
        for configurator in configurators {
            configurator.toggleForDeselection()
        }
    }
}

extension PositionsTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        
        if configurator.cellStatus {
            configurator.toggleToTrue()
        } else {
            configurator.toggleToFalse()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let configurator = configurators[indexPath.row]
        let position = configurator.positionModel
        guard let position = position else { return }
        
        if positions.isEmpty {
            
            positions.append(position)
            configurator.checkboxSelectedOnTap()
        } else {
            for (index, repeatedPosition) in positions.enumerated() where repeatedPosition.codename == position.codename {
//                if repeatedPosition.codename == position.codename {
                    positions.remove(at: index)
                    configurator.checkboxDeselectedOnTap()
                    return
//                }
            }
            positions.append(position)
            configurator.checkboxSelectedOnTap()
        }
    }
}

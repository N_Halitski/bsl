//
//  PositionsTableConfigurators.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol PositionsTableConfigurator {
    var reuseId: String { get }
    var positionModel: Position? { get set }
    var cell: PositionCellProtocol? { get }
    var cellStatus: Bool { get set }
    
    func setupCell(_ cell: UIView)
    func toggleForDeselection()
    func checkboxSelectedOnTap()
    func checkboxDeselectedOnTap()
    func toggleToTrue()
    func toggleToFalse()
}

final class PositionCellConfigurator: PositionsTableConfigurator {
    
    var reuseId: String = String(describing: PositionCell.self)
    
    var positionModel: Position?
    var cellStatus: Bool = false
    
    var getPosition: ((Position) -> Void?)?
    var getDeselectedPosition: ((Position) -> Void?)?
    
    var cell: PositionCellProtocol?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? PositionCellProtocol,
              let positionModel = positionModel else { return }
        self.cell = cell
        cell.display(positionModel)
        cell.positionSelected = { [weak self] position in
            self?.getPosition?(position)
        }
        cell.positionDeselected = { [weak self] position in
            self?.getDeselectedPosition?(position)
        }
        
        cell.informConfigurator = { [weak self] cellStatus in
            self?.cellStatus = cellStatus
        }
    }
    
    func toggleForDeselection() {
        if let cell = self.cell as? PositionCell {
            cell.toggleCheckmarkToFalse()
        }
    }
    
    func checkboxSelectedOnTap() {
        if let cell = self.cell as? PositionCell {
            cell.checkboxSelectedOnTap()
        }
    }
    
    func checkboxDeselectedOnTap() {
        if let cell = self.cell as? PositionCell {
            cell.checkboxDeselectedOnTap()
        }
    }
    
    func toggleToTrue() {
        if let cell = self.cell as? PositionCell {
            cell.toggleCellToTrue()
        }
    }
    func toggleToFalse() {
        if let cell = self.cell as? PositionCell {
            cell.toggleCellToFalse()
        }
    }
}

//
//  PositionCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol PositionCellProtocol: AnyObject {
    var positionSelected: ((Position) -> Void?)? { get set }
    var positionDeselected: ((Position) -> Void?)? { get set }
    var informConfigurator: ((Bool) -> Void?)? { get set }
    
    func display(_ positionModel: Position)
    func toggleCellToTrue()
    func toggleCellToFalse()
}

final class PositionCell: UITableViewCell, PositionCellProtocol {
    
    @IBOutlet private var positionLabel: UILabel!
    @IBOutlet private var selectButtonOutlet: UIButton!
    @IBOutlet private var separatorView: UIView!
    
    var positionSelected: ((Position) -> Void?)?
    var positionDeselected: ((Position) -> Void?)?
    var informConfigurator: ((Bool) -> Void?)?
    
    var position: Position?
    
    var cellStatus: Bool = false {
        didSet {
            informConfigurator?(cellStatus)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func display(_ positionModel: Position) {
        self.position = positionModel
        positionLabel.text = positionModel.codename
    }
    
    @IBAction private func selectButtonClicked(_ sender: Any) {
        if selectButtonOutlet.isSelected {
            selectButtonOutlet.isSelected = false
            cellStatus = false
            guard let position = position else { return }
            positionDeselected?(position)
        } else {
            selectButtonOutlet.isSelected = true
            cellStatus = true
            guard let position = position else { return }
            positionSelected?(position)
        }
    }
    
    func toggleCheckmarkToFalse() {
        if selectButtonOutlet.isSelected {
            cellStatus = false
            selectButtonOutlet.isSelected = false
        }
    }
    
    func checkboxSelectedOnTap() {
        if !selectButtonOutlet.isSelected {
            cellStatus = true
            selectButtonOutlet.isSelected = true
        }
    }
    
    func checkboxDeselectedOnTap() {
        if selectButtonOutlet.isSelected {
            cellStatus = false
            selectButtonOutlet.isSelected = false
        }
    }
    
    func toggleCellToTrue() {
        selectButtonOutlet.isSelected = true
    }
    
    func toggleCellToFalse() {
        selectButtonOutlet.isSelected = false
    }
    
}

extension PositionCell {
    
    private func setupUI() {
        
        selectButtonOutlet.setImage(UIImage(named: "unselectedCell"), for: .normal)
        selectButtonOutlet.setImage(UIImage(named: "selectedCell"), for: .selected)
        selectButtonOutlet.setTitle("", for: .normal)
        selectButtonOutlet.setTitle("", for: .selected)
        selectButtonOutlet.tintColor = .clear
        
        positionLabel.font = AppTheme.Fonts.SFRegular(16)
        positionLabel.textColor = AppTheme.Colors.black100
        
        separatorView.backgroundColor = AppTheme.Colors.gray100
        
        selectionStyle = .none
    }
    
}

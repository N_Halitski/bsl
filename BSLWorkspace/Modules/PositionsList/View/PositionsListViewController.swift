//
//  PositionsListPositionsListViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class PositionsListViewController: BaseViewController {

    @IBOutlet weak var cancelButtonOutlet: UIButton!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var applyButtonOutlet: UIButton!
    @IBOutlet weak var positionsTableView: UITableView!
    
    var applyButtonClicked: (([Position]) -> Void)?
    var cancelButtonClickedCallBack: (() -> Void)?
    var positions: [Position] = []
    
    var presenter: PositionsListPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(positionsTableView)
        setupUI()
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        positions = []
        presenter.clearPositions(positions)
        cancelButtonClickedCallBack?()
    }
    
    @IBAction func applyButtonClicked(_ sender: Any) {
        applyButtonClicked?(positions)
    }
    
    private func setupUI() {
        positionLabel.font = AppTheme.Fonts.SFSemiBold(16)
    }
}

extension PositionsListViewController: PositionsListPresenterOutput {
    func showPositions(_ positions: [Position]) {
        self.positions = positions
    }
}

extension PositionsListViewController {
    func clearSelection() {
        positions = []
        presenter.clearPositions(positions)
    }
}

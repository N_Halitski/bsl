//
//  PositionsListPositionsListAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class PositionsListAssembler {
    static func createModule(_ positionsList: [Position],
                             _ tableManager: PositionsTableManagerProtocol = PositionsTableManager()) -> PositionsListViewController {
        let viewController = PositionsListViewController()
        let interactor = PositionsListInteractor(tableManager: tableManager, positionsList: positionsList)
        let presenter = PositionsListPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

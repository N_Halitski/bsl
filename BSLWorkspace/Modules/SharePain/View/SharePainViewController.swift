//
//  SharePainSharePainViewController.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import FloatingPanel

final class SharePainViewController: BaseViewController, FloatingPanelControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var fpc: FloatingPanelController?
    var presenter: SharePainPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        title = "SharePainFlow.sharePain".localized + " 🤕"
        view.backgroundColor = AppTheme.Colors.lightGray100
        tableView.keyboardDismissMode = .onDrag
        hideKeyboardWhenTappedAround()
    }
    
    func setSelectedProject(_ project: ProjectsRegistryResponseModel?) {
        guard let project = project else { return }
        presenter.getSelectedProject(with: project)
    }
    
}

extension SharePainViewController: SharePainPresenterOutput {
    
    func openProjectsScreen() {
        let createSharePainProjectsVC = createSharePainProjectsVC()
        let navController = UINavigationController(rootViewController: createSharePainProjectsVC)
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
    
    func openPositionsFloatingPanel() {
        tabBarController?.tabBar.isHidden = true
        if fpc == nil {
            fpc = FloatingPanelController()
            guard let fpc = fpc else { return }
            fpc.delegate = self
            fpc.layout = PositionsFloatingPanelLayout()
            let sharePainPositionsViewController = createSharePainPositionsVC()
            fpc.set(contentViewController: sharePainPositionsViewController)
            fpc.addPanel(toParent: self)
            let appearance = SurfaceAppearance()
            appearance.cornerRadius = 16
            fpc.surfaceView.appearance = appearance
            fpc.surfaceView.grabberHandle.isHidden = true
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(closeFloatingPanel))
            fpc.backdropView.addGestureRecognizer(recognizer)
            fpc.panGestureRecognizer.isEnabled = false
            self.fpc = fpc
        } else {
            fpc?.move(to: .tip, animated: true)
        }
    }
    
    @objc private func closeFloatingPanel() {
        fpc?.move(to: .hidden, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
}

private extension SharePainViewController {
    
    func createSharePainProjectsVC() -> UIViewController {
        let sharePainProjectsViewController = SharePainProjectsListAssembler.createModule()
        sharePainProjectsViewController.tapOnReadyButton = { [weak self] project in
            guard let self = self else { return }
            self.setSelectedProject(project)
        }
        sharePainProjectsViewController.tapOnSelectButton = { [weak self] in
            guard let self = self else { return }
            self.presenter.clearProjectCell()
        }
        return sharePainProjectsViewController
    }
    
    func createSharePainPositionsVC() -> SharePainPositionsListViewController {
        let sharePainPositionsVC = SharePainPositionsListAssembler.createModule()
        sharePainPositionsVC.didFinishClosing = { [weak self] in
            guard let self = self else { return }
            self.fpc!.hide(animated: true) {
                self.fpc!.removeFromParent()
            }
            self.tabBarController?.tabBar.isHidden = false
        }
        sharePainPositionsVC.didFinishDiscard = { [weak self] in
            guard let self = self else { return }
            self.presenter.didFinishDiscard()
        }
        
        sharePainPositionsVC.didFinishApplyButton = { [weak self] selectedPositions in
            guard let self = self else { return }
            self.fpc!.hide(animated: true) {
                self.fpc!.removeFromParent()
            }
            self.tabBarController?.tabBar.isHidden = false
            self.presenter.applyButtonTapped(with: selectedPositions)
        }
        return sharePainPositionsVC
    }
    
}

class PositionsFloatingPanelLayout: FloatingPanelLayout {
    
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var heightOfFloatingPanel = UIScreen.main.bounds.height * 0.33
    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return [
            .tip: FloatingPanelLayoutAnchor(absoluteInset: heightOfFloatingPanel, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
    
    func backdropAlpha(for state: FloatingPanelState) -> CGFloat {
        switch state {
        case .tip: return 0.3
        default: return 0.0
        }
    }
    
}

//
//  SharePainCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 10.01.22.
//

import UIKit

protocol SharePainCellConfiguratorProtocol: BaseConfigurator {
    var sharePainPositionsModel: SharePainPositionsModel? { get set }
    var cell: SharePainCellProtocol? { get set }
    
    func markBorderOfCell()
    func unMarkBorderOfCell()
}

final class SharePainCellConfigurator: SharePainCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: SharePainCell.self)
    var sharePainPositionsModel: SharePainPositionsModel?
    var cell: SharePainCellProtocol?
    
    func setupCell(_ sharePainCell: UIView) {
        guard let sharePainCell = sharePainCell as? SharePainCellProtocol else { return }
        sharePainCell.display(with: sharePainPositionsModel)
        cell = sharePainCell
    }
    
    func markBorderOfCell() {
        cell?.markBorderOfCell()
    }
    
    func unMarkBorderOfCell() {
        cell?.unMarkBorderOfCell()
    }
    
}

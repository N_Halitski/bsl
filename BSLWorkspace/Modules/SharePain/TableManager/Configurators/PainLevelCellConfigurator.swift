//
//  PainLevelCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 10.01.22.
//

import UIKit

protocol PainLevelCellConfiguratorProtocol: BaseConfigurator {
    
}

final class PainLevelCellConfigurator: PainLevelCellConfiguratorProtocol {

    var reuseId: String = String(describing: PainLevelCell.self)

    func setupCell(_ cell: UIView) {

    }

}

//
//  ChoseProjectCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 28.12.21.
//

import UIKit

protocol ChoseProjectCellConfiguratorProtocol: BaseConfigurator {
    var shareChosenProject: ProjectsRegistryResponseModel? { get set }
}

final class ChoseProjectCellConfigurator: ChoseProjectCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: ChooseProjectCell.self)
    var shareChosenProject: ProjectsRegistryResponseModel?
    
    func setupCell(_ choseProjectCell: UIView) {
        guard let choseProjectCell = choseProjectCell as? ChooseProjectCellProtocol else { return }
        choseProjectCell.display(with: shareChosenProject)
    }
    
}

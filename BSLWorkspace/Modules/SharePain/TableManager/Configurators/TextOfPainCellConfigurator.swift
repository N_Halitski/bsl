//
//  TextOfPainCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 10.01.22.
//

import UIKit

protocol TextOfPainCellConfiguratorProtocol: BaseConfigurator {
    var didTextChanged: ((String) -> Void)? { get set }
    var textOfPainCell: TextOfPainCellProtocol? { get set }
    
    func markBorderOfCell()
    func unMarkBorderOfCell()
}

final class TextOfPainCellConfigurator: TextOfPainCellConfiguratorProtocol {

    var reuseId: String = String(describing: TextOfPainCell.self)
    var textOfPainCell: TextOfPainCellProtocol?
    
    var didTextChanged: ((String) -> Void)?

    func setupCell(_ cell: UIView) {
        guard let cell = cell as? TextOfPainCell else { return }
        cell.textChanged =  self.didTextChanged
        textOfPainCell = cell
    }
    
    func markBorderOfCell() {
        textOfPainCell?.markBorderOfCell()
    }
    
    func unMarkBorderOfCell() {
        textOfPainCell?.unMarkBorderOfCell()
    }

}

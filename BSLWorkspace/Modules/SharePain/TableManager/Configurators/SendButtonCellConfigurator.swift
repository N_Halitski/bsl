//
//  SendButtonCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 10.01.22.
//

import UIKit

protocol SendButtonCellConfiguratorProtocol: BaseConfigurator {
    var cell: SendButtonCellProtocol? { get set }
    var sendButtonTapped: (() -> Void)? { get set }
    
    func activateSendButton()
    func deactivateSencButton()
}

final class SendButtonCellConfigurator: SendButtonCellConfiguratorProtocol {

    var reuseId: String = String(describing: SendButtonCell.self)
    var cell: SendButtonCellProtocol?
    var sendButtonTapped: (() -> Void)?
    
    func setupCell(_ sendButtonCell: UIView) {
        guard let sendButtonCell = sendButtonCell as? SendButtonCellProtocol else { return }
        cell = sendButtonCell
        cell?.sendButtonTapped = self.sendButtonTapped
    }
    
    func activateSendButton() {
        cell?.activateSendButton()
    }
    
    func deactivateSencButton() {
        cell?.deactivateSencButton()
    }

}

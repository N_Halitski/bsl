//
//  TitleOfPainCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 10.01.22.
//

import UIKit

protocol TitleOfPainCellConfiguratorProtocol: BaseConfigurator {
    var didTextChanged: ((String) -> Void)? { get set }
    var titleOfPainCell: TitleOfPainCellProtocol? { get set }
    
    func markBorderOfCell()
    func unMarkBorderOfCell()
}

final class TitleOfPainCellConfigurator: TitleOfPainCellConfiguratorProtocol {

    var reuseId: String = String(describing: TitleOfPainCell.self)
    var titleOfPainCell: TitleOfPainCellProtocol?
    
    var didTextChanged: ((String) -> Void)?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? TitleOfPainCell else { return }
        cell.textChanged = self.didTextChanged
        titleOfPainCell = cell
    }
    
    func markBorderOfCell() {
        titleOfPainCell?.markBorderOfCell()
    }
    
    func unMarkBorderOfCell() {
        titleOfPainCell?.unMarkBorderOfCell()
    }
    
}

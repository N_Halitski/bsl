//
//  SharePainTableManager.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 28.12.21.
//

import UIKit

protocol SharePainTableManagerProtocol {
    var didTapProjects: (() -> Void)? { get set }
    var didtapPositions: (() -> Void)? { get set }
    var sendButtonTapped: (() -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func presentChosenProject(with model: ProjectsRegistryResponseModel)
    func presentSharePain(with chosenPositions: SharePainPositionsModel)
    func clearProjectCell()
    func clearPositionCell()
    func setupInitialState()
}

final class SharePainTableManager: NSObject, SharePainTableManagerProtocol {
    
    private weak var tableView: UITableView?
    private var configurators: [BaseConfigurator] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    private var observeTitleOfPainCell: String = ""
    private var observeTextOfPainCell: String = ""
    private var observeProjectCellModel: ProjectsRegistryResponseModel?
    private var observePositionCellModel: SharePainPositionsModel?
    
    var didTapProjects: (() -> Void)?
    var didtapPositions: (() -> Void)?
    var sendButtonTapped: (() -> Void)?
    
    func attachTable(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView = tableView
        configureTableView()
        registerCells()
    }
    
    func setupInitialState() {
        configurators.append(createChooseProjectCellConfigurator())
        configurators.append(createSharePainCellConfigurator())
        configurators.append(createTitleOfPainCellConfigurator())
        configurators.append(createTextOfPainCellConfigurator())
        configurators.append(createPainLevelCellConfigurator())
        configurators.append(createSendButtonCellConfigurator())
    }
    
    func clearPositionCell() {
        observePositionCellModel = nil
        controlStateOfSendButton()
        for (num, configurator) in configurators.enumerated() {
            if var sharePainPositionCellConfigurator = configurator as? SharePainCellConfiguratorProtocol {
                sharePainPositionCellConfigurator.sharePainPositionsModel = nil
                tableView?.reloadRows(at: [IndexPath(row: num, section: 0)], with: .none)
            }
        }
    }
    
    func clearProjectCell() {
        observeProjectCellModel = nil
        controlStateOfSendButton()
        for (num, configurator) in configurators.enumerated() {
            if var sharePainProjectCellConfigurator = configurator as? ChoseProjectCellConfiguratorProtocol {
                sharePainProjectCellConfigurator.shareChosenProject = nil
                tableView?.reloadRows(at: [IndexPath(row: num, section: 0)], with: .none)
            }
        }
    }
    
    func presentChosenProject(with model: ProjectsRegistryResponseModel) {
        self.observeProjectCellModel = model
        var output: ChoseProjectCellConfigurator?
        output = createChooseProjectCellConfiguratorWithModel(model)
        self.configurators.remove(at: 0)
        self.configurators.insert(output!, at: 0)
    }
    
    func presentSharePain(with chosenPositions: SharePainPositionsModel) {
        self.observePositionCellModel = chosenPositions
        var output: SharePainCellConfiguratorProtocol?
        output = createSharePainCellConfiguratorWithModel(chosenPositions)
        markBorderOfSharePainCell()
        self.configurators.remove(at: 1)
        self.configurators.insert(output!, at: 1)
    }
    
    private func markBorderOfSharePainCell() {
        for configurator in self.configurators {
            if let sharePainCellConfigurator = configurator as? SharePainCellConfiguratorProtocol {
                if self.observePositionCellModel == nil {
                    sharePainCellConfigurator.markBorderOfCell()
                } else {
                    sharePainCellConfigurator.unMarkBorderOfCell()
                }
            }
        }
    }
    
    private func markBorderOfTitleOfPainCell() {
        for configurator in self.configurators {
            if let titleOfPainCellConfigurator = configurator as? TitleOfPainCellConfiguratorProtocol {
                if self.observeTitleOfPainCell .isEmpty {
                    titleOfPainCellConfigurator.markBorderOfCell()
                } else {
                    titleOfPainCellConfigurator.unMarkBorderOfCell()
                }
            }
        }
    }
    
    private func markBorderOfTextOfPainCell() {
        for configurator in self.configurators {
            if let textOfPainCellConfigurator = configurator as? TextOfPainCellConfiguratorProtocol {
                if self.observeTextOfPainCell .isEmpty {
                    textOfPainCellConfigurator.markBorderOfCell()
                } else {
                    textOfPainCellConfigurator.unMarkBorderOfCell()
                }
            }
        }
    }
    
    private func controlStateOfSendButton() {
        for configurator in configurators {
            if let sendButtonCellConfigurator = configurator as? SendButtonCellConfiguratorProtocol {
                if observeTextOfPainCell .isNotEmpty && observePositionCellModel != nil && observeProjectCellModel != nil && observeTitleOfPainCell .isNotEmpty {
                    sendButtonCellConfigurator.activateSendButton()
                } else {
                    sendButtonCellConfigurator.deactivateSencButton()
                }
            }
        }
    }
    
    private func createChooseProjectCellConfiguratorWithModel(_ model: ProjectsRegistryResponseModel) -> ChoseProjectCellConfigurator {
        let configurator = ChoseProjectCellConfigurator()
        configurator.shareChosenProject = model
        self.controlStateOfSendButton()
        return configurator
    }
    
    private func createChooseProjectCellConfigurator() -> ChoseProjectCellConfigurator {
        let configurator = ChoseProjectCellConfigurator()
        return configurator
    }
    
    private func createSharePainCellConfiguratorWithModel(_ model: SharePainPositionsModel) -> SharePainCellConfiguratorProtocol {
        let configurator = SharePainCellConfigurator()
        configurator.sharePainPositionsModel = model
        self.controlStateOfSendButton()
        return configurator
    }
    
    private func createSharePainCellConfigurator() -> SharePainCellConfiguratorProtocol {
        let configurator = SharePainCellConfigurator()
        return configurator
    }
    
    private func createTitleOfPainCellConfigurator() -> TitleOfPainCellConfigurator {
        let configurator = TitleOfPainCellConfigurator()
        configurator.didTextChanged = { [weak self] text in
            self?.tableView?.beginUpdates()
            self?.tableView?.endUpdates()
            self?.observeTitleOfPainCell = text
            self?.controlStateOfSendButton()
            self?.markBorderOfTitleOfPainCell()
        }
        return configurator
    }
    
    private func createTextOfPainCellConfigurator() -> TextOfPainCellConfigurator {
        let configurator = TextOfPainCellConfigurator()
        configurator.didTextChanged = { [weak self] text in
            self?.tableView?.beginUpdates()
            self?.tableView?.endUpdates()
            self?.observeTextOfPainCell = text
            self?.controlStateOfSendButton()
            self?.markBorderOfTextOfPainCell()
        }
        return configurator
    }
    
    private func createPainLevelCellConfigurator() -> PainLevelCellConfigurator {
        let configurator = PainLevelCellConfigurator()
        return configurator
    }
    
    private func createSendButtonCellConfigurator() -> SendButtonCellConfigurator {
        let configurator = SendButtonCellConfigurator()
        configurator.sendButtonTapped = { [weak self] in
            self?.markBorderOfSharePainCell()
            self?.markBorderOfTitleOfPainCell()
            self?.markBorderOfTextOfPainCell()
        }
        return configurator
    }
    
}

extension SharePainTableManager: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            didTapProjects?()
        }
        if indexPath.row == 1 {
            didtapPositions?()
        }
    }
    
}

extension SharePainTableManager: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        return cell
    }
    
}

private extension SharePainTableManager {
    
    func configureTableView() {
        tableView?.backgroundColor = AppTheme.Colors.lightGray100
        tableView?.separatorStyle = .none
    }
    
}

private extension SharePainTableManager {
        
    func registerCells() {
        tableView?.registerCell(ChooseProjectCell.self)
        tableView?.registerCell(SharePainCell.self)
        tableView?.registerCell(TitleOfPainCell.self)
        tableView?.registerCell(TextOfPainCell.self)
        tableView?.registerCell(PainLevelCell.self)
        tableView?.registerCell(SendButtonCell.self)
    }
    
}

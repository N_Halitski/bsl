//
//  PainLevelCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 29.12.21.
//

import UIKit

protocol PainLevelCellProtocol {
    
}

final class PainLevelCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var lowButton: UIButton!
    @IBOutlet private weak var midButton: UIButton!
    @IBOutlet private weak var highButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupCell()
    }
    
    @IBAction func lowButtonClicked(_ sender: Any) {
        lowButton.backgroundColor = AppTheme.Colors.lightGreen
        lowButton.layer.borderWidth = 1
        lowButton.layer.borderColor = AppTheme.Colors.green100.cgColor
        
        midButton.backgroundColor = AppTheme.Colors.white100
        midButton.layer.borderWidth = 0
        
        highButton.backgroundColor = AppTheme.Colors.white100
        highButton.layer.borderWidth = 0
    }
    
    @IBAction func midButtonClicked(_ sender: Any) {
        lowButton.backgroundColor = AppTheme.Colors.white100
        lowButton.layer.borderWidth = 0
        
        midButton.backgroundColor = AppTheme.Colors.lightOrange100
        midButton.layer.borderWidth = 1
        midButton.layer.borderColor = AppTheme.Colors.orange100.cgColor
        
        highButton.backgroundColor = AppTheme.Colors.white100
        highButton.layer.borderWidth = 0
    }
    
    @IBAction func highButtonClicked(_ sender: Any) {
        lowButton.backgroundColor = AppTheme.Colors.white100
        lowButton.layer.borderWidth = 0
        
        midButton.backgroundColor = AppTheme.Colors.white100
        midButton.layer.borderWidth = 0
        
        highButton.backgroundColor = AppTheme.Colors.redStatus2
        highButton.layer.borderWidth = 1
        highButton.layer.borderColor = AppTheme.Colors.red100.cgColor
    }
    
}

private extension PainLevelCell {
    
    func setupCell() {
        titleLabel.text = "SharePainFlow.painLevel".localized
        titleLabel.font = AppTheme.Fonts.SFSemiBold(20)
        titleLabel.textColor = AppTheme.Colors.black100
        
        lowButton.setTitle("Low", for: .normal)
        lowButton.setTitleColor(AppTheme.Colors.black100, for: .normal)
        lowButton.layer.cornerRadius = 16
        lowButton.backgroundColor = AppTheme.Colors.white100
        lowButton.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
        
        midButton.setTitle("Mid", for: .normal)
        midButton.setTitleColor(AppTheme.Colors.black100, for: .normal)
        midButton.layer.cornerRadius = 16
        midButton.backgroundColor = AppTheme.Colors.lightOrange100
        midButton.layer.borderWidth = 1
        midButton.layer.borderColor = AppTheme.Colors.orange100.cgColor
        midButton.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
        
        highButton.setTitle("High", for: .normal)
        highButton.setTitleColor(AppTheme.Colors.black100, for: .normal)
        highButton.layer.cornerRadius = 16
        highButton.backgroundColor = AppTheme.Colors.white100
        highButton.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
    }
    
}

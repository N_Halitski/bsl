//
//  TitleOfPainCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 29.12.21.
//

import UIKit

protocol TitleOfPainCellProtocol {
    var textChanged: ((String) -> Void)? { get set }
    
    func markBorderOfCell()
    func unMarkBorderOfCell()
}

final class TitleOfPainCell: UITableViewCell, TitleOfPainCellProtocol {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleTextField: UITextField!
    
    var textChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        configureCell()
        titleTextField.addTarget(self, action: #selector(TitleOfPainCell.textFieldDidChange(_:)),
                                  for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        textChanged?(textField.text ?? "")
    }
    
    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
    func markBorderOfCell() {
        containerView.layer.borderColor = AppTheme.Colors.red100.cgColor
        containerView.layer.borderWidth = 1
    }
    
    func unMarkBorderOfCell() {
        containerView.layer.borderWidth = 0
    }
    
}

private extension TitleOfPainCell {
    
    func configureCell() {
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
        
        titleTextField.placeholder = "SharePainFlow.title".localized
        titleTextField.font = AppTheme.Fonts.SFRegular(16)
        titleTextField.textColor = AppTheme.Colors.black100
    }
    
}

//
//  ChooseProjectCellTableViewCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 28.12.21.
//

import UIKit

protocol ChooseProjectCellProtocol {
    func display(with chosenProjectModel: ProjectsRegistryResponseModel?)
}

final class ChooseProjectCell: UITableViewCell, ChooseProjectCellProtocol {    
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    private var dashedBorder = CAShapeLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        selectionStyle = .none
    }
    
    func display(with chosenProjectModel: ProjectsRegistryResponseModel?) {
        titleLabel.text = chosenProjectModel == nil ? "SharePainFlow.choseProject".localized : chosenProjectModel?.title
        titleLabel.textColor = chosenProjectModel == nil ? AppTheme.Colors.gray100 : AppTheme.Colors.black100
        titleLabel.font = chosenProjectModel == nil ? AppTheme.Fonts.SFRegular(18) : AppTheme.Fonts.SFMedium(18)
        
        dashedBorder.lineDashPattern = [2, 2]
        dashedBorder.fillColor = nil
        dashedBorder.strokeColor = chosenProjectModel == nil ? AppTheme.Colors.gray100.cgColor : UIColor.clear.cgColor
        let dashedBorderRect = CGRect(
            x: 0, y: 0, width: UIScreen.main.bounds.width - 32,
            height: containerView.bounds.height - (chosenProjectModel == nil ? 2 : 0))
        dashedBorder.path = UIBezierPath(roundedRect: dashedBorderRect, cornerRadius: 16).cgPath
        containerView.layer.addSublayer(dashedBorder)
    }
    
    private func setupUI() {
        backgroundColor = AppTheme.Colors.lightGray100
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
    }
    
}

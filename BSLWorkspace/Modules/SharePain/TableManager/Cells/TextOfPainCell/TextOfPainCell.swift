//
//  TextOfPainCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 30.12.21.
//

import UIKit

protocol TextOfPainCellProtocol {
    var textChanged: ((String) -> Void)? { get set }
    
    func markBorderOfCell()
    func unMarkBorderOfCell()
}

final class TextOfPainCell: UITableViewCell, TextOfPainCellProtocol {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet weak var painTextView: UITextView!
    @IBOutlet weak var titleOfTextView: UILabel!
    
    private let screenSize = UIScreen.main.bounds
    
    var textChanged: ((String) -> Void)?
    var observerOnTextView: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        configureCell()
        painTextView.delegate = self
    }
    
    func markBorderOfCell() {
        containerView.layer.borderColor = AppTheme.Colors.red100.cgColor
        containerView.layer.borderWidth = 1
    }
    
    func unMarkBorderOfCell() {
        containerView.layer.borderWidth = 0
    }
    
}

extension TextOfPainCell: UITextViewDelegate {
    
    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textChanged?(textView.text)
        let maxHeightOfTextView = screenSize.height * 0.14
        textView.isScrollEnabled = textView.contentSize.height >= maxHeightOfTextView ? true : false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if painTextView.text == "SharePainFlow.textOfPain".localized {
            painTextView.text = ""
            titleOfTextView.textColor = AppTheme.Colors.darkGray
        }
        painTextView.textColor = AppTheme.Colors.black100
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if painTextView.text == "" {
            painTextView.text = "SharePainFlow.textOfPain".localized
            painTextView.textColor = AppTheme.Colors.darkGray
            painTextView.font = AppTheme.Fonts.SFRegular(16)
            titleOfTextView.textColor = .clear
        }
    }
    
}

private extension TextOfPainCell {
    
    func configureCell() {
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
        
        painTextView.text = "SharePainFlow.textOfPain".localized
        painTextView.textColor = AppTheme.Colors.darkGray
        painTextView.font = AppTheme.Fonts.SFRegular(16)
        
        titleOfTextView.text = "SharePainFlow.textOfPain".localized
        titleOfTextView.font = AppTheme.Fonts.SFRegular(12)
        titleOfTextView.textColor = .clear
        titleOfTextView.backgroundColor = AppTheme.Colors.white100
    }
    
}

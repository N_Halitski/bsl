//
//  SharePainCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 29.12.21.
//

import UIKit

protocol SharePainCellProtocol {
    func display(with chosenPostionsModel: SharePainPositionsModel?)
    func markBorderOfCell()
    func unMarkBorderOfCell()
}

final class SharePainCell: UITableViewCell, SharePainCellProtocol {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var titleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
    }
    
    func display(with chosenPostionsModel: SharePainPositionsModel?) {
        titleLabel.text = chosenPostionsModel == nil ? "SharePainFlow.recipientOfPain".localized : chosenPostionsModel?.title
        titleLabel.textColor = chosenPostionsModel == nil ? AppTheme.Colors.darkGray : AppTheme.Colors.black100
        titleImage.image = chosenPostionsModel == nil ? UIImage(named: "choosePositionOrProject") : UIImage(named: "positionOrProjectChosen")
    }
    
    func markBorderOfCell() {
        containerView.layer.borderColor = AppTheme.Colors.red100.cgColor
        containerView.layer.borderWidth = 1
    }
    
    func unMarkBorderOfCell() {
        containerView.layer.borderWidth = 0
    }
    
    private func setupUI() {
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
    }
    
}

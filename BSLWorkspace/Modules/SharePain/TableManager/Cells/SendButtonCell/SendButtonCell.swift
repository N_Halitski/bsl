//
//  SendButtonCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 29.12.21.
//

import UIKit

protocol SendButtonCellProtocol {
    var sendButtonTapped: (() -> Void)? { get set }
    
    func activateSendButton()
    func deactivateSencButton()
}

final class SendButtonCell: UITableViewCell, SendButtonCellProtocol {
    
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var sendButtonTopConstraint: NSLayoutConstraint!
    
    var sendButtonTapped: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
    }
    
    func activateSendButton() {
        sendButton.backgroundColor = AppTheme.Colors.darkBlue100
    }
    
    func deactivateSencButton() {
        sendButton.backgroundColor = AppTheme.Colors.grayBlue100
    }
    
    @IBAction func sendButtonClicked(_ sender: Any) {
        sendButtonTapped?()
    }
    
}

private extension SendButtonCell {
    
    func setupUI() {
//TODO: костыль для того, чтобы кнопка отображалась чуть выше таббар на разных размерах экрана, пофиксить, если появится более красивое решение
        sendButtonTopConstraint.constant = UIScreen.main.bounds.height - 700
        if UIScreen.main.bounds.height == 667 {
            sendButtonTopConstraint.constant = UIScreen.main.bounds.height - 630
        }
        sendButton.backgroundColor = AppTheme.Colors.grayBlue100
        sendButton.layer.cornerRadius = 16
        sendButton.setTitle("SharePainFlow.send".localized, for: .normal)
        sendButton.setTitleColor(AppTheme.Colors.white100, for: .normal)
    }
    
}

//
//  SharePainSharePainAssembler.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class SharePainAssembler {
    static func createModule(tableManager: SharePainTableManagerProtocol = SharePainTableManager()) -> SharePainViewController {
        let viewController = SharePainViewController()
        let interactor = SharePainInteractor(tableManager: tableManager)
        let presenter = SharePainPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

//
//  SharePainSharePainInteractor.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class SharePainInteractor {
    
    private weak var output: SharePainInteractorOutput?
    private var tableManager: SharePainTableManagerProtocol
    
    init(tableManager: SharePainTableManagerProtocol, projectsListRepository: ProjectsRepositoryProtocol = ProjectsRepository()) {
        self.tableManager = tableManager
        self.tableManager.didTapProjects = { [weak self]  in
            guard let self = self else { return }
            self.output?.didTapProjects()
        }
        self.tableManager.didtapPositions = { [weak self] in
            guard let self = self else { return }
            self.output?.didTapPositions()
        }
    }
    
}

extension SharePainInteractor: SharePainInteractorInput {
    
    func clearProjectCell() {
        tableManager.clearProjectCell()
    }
    
    func didFinishDiscard() {
        tableManager.clearPositionCell()
    }
    
    func getSelectedProject(with project: ProjectsRegistryResponseModel) {
        tableManager.presentChosenProject(with: project)
    }
    
    func applyButtonTapped(with positions: SharePainPositionsModel) {
        tableManager.presentSharePain(with: positions)
    }
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
        tableManager.setupInitialState()
    }
    
    func attach(_ output: SharePainInteractorOutput) {
        self.output = output
    }
    
}

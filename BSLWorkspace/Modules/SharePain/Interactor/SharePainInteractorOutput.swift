//
//  SharePainSharePainInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol SharePainInteractorOutput: AnyObject {
    func didTapProjects()
    func didTapPositions()
}

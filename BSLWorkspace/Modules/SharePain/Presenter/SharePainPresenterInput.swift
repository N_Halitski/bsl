//
//  SharePainSharePainPresenterInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol SharePainPresenterInput: BasePresenting {
    func attachTableView(_ table: UITableView)
    func applyButtonTapped(with positions: SharePainPositionsModel)
    func getSelectedProject(with project: ProjectsRegistryResponseModel)
    func didFinishDiscard()
    func clearProjectCell()
}

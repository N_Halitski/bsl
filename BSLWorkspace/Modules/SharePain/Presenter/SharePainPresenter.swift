//
//  SharePainSharePainPresenter.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class SharePainPresenter {

    private let interactor: SharePainInteractorInput
    private unowned let view: SharePainPresenterOutput

    init(_ interactor: SharePainInteractorInput, _ view: SharePainPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension SharePainPresenter: SharePainPresenterInput {
    
    func clearProjectCell() {
        interactor.clearProjectCell()
    }
    
    func didFinishDiscard() {
        interactor.didFinishDiscard()
    }
    
    func getSelectedProject(with project: ProjectsRegistryResponseModel) {
        interactor.getSelectedProject(with: project)
    }
    
    func applyButtonTapped(with positions: SharePainPositionsModel) {
        interactor.applyButtonTapped(with: positions)
    }
    
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
    
}

extension SharePainPresenter: SharePainInteractorOutput {
    
    func didTapProjects() {
        view.openProjectsScreen()
    }
    
    func didTapPositions() {
        view.openPositionsFloatingPanel()
    }

}

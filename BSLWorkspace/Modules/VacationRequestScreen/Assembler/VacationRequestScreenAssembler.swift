//
//  VacationRequestScreenVacationRequestScreenAssembler.swift
//  BSLWorkspace
//
//  Created by clear on 16/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class VacationRequestScreenAssembler {
    static func createModule() -> VacationRequestScreenViewController {
        let viewController = VacationRequestScreenViewController()
        let interactor = VacationRequestScreenInteractor()
        let presenter = VacationRequestScreenPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

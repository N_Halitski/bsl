//
//  SigningModel.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 17.12.21.
//

import UIKit

struct SigningModel {
    let status: String
    let photo: UIImage
    let name: String
    let position: String
}

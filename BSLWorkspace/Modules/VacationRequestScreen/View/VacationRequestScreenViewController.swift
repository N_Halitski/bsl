//
//  VacationRequestScreenVacationRequestScreenViewController.swift
//  BSLWorkspace
//
//  Created by clear on 16/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class VacationRequestScreenViewController: BaseViewController {
    
    @IBOutlet private var vacationRequestLabel: UILabel!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var positionLabel: UILabel!
    @IBOutlet private var palmLabel: UILabel!
    @IBOutlet private var calendarLabel: UILabel!
    @IBOutlet private var laptopLabel: UILabel!
    @IBOutlet private var lastVacationLabel: UILabel!
    @IBOutlet private var lastVacationFromToLabel: UILabel!
    @IBOutlet private var daysLeftVacationLabel: UILabel!
    @IBOutlet private var daysVacationLabel: UILabel!
    @IBOutlet private var inCompanyLabel: UILabel!
    @IBOutlet private var inCompanyFromDateLabel: UILabel!
    @IBOutlet private var statusLabel: UILabel!
    
    @IBOutlet private var centralSigningView: SigningView!
    @IBOutlet private var leftSigningView: SigningView!
    @IBOutlet private var rightSigningView: SigningView!
    
    @IBOutlet private var employeeInfoView: UIView!
    @IBOutlet private var statusView: UIView!
    
    @IBOutlet private var photoImageView: UIImageView!
    @IBOutlet private var withdrawButton: UIButton!
    
    private enum Constants {
        static let palmUnicode: String = "\u{1F334}"
        static let calendarUnicode: String = "\u{1F5D3}"
        static let laptopUnicode: String = "\u{1F468}\u{200D}\u{1F4BB}"
        static let cornerRadius8: CGFloat = 8
        static let cornerRadius16: CGFloat = 16
        static let border: CGFloat = 1
        static let shadowRGBA = AppTheme.Colors.lightGray8.cgColor
        static let shadowOffset = CGSize(width: 0, height: 4)
        static let shadowOpacity: Float = 0.07
        static let shadowRadius: CGFloat = 36
    }
    
    var presenter: VacationRequestScreenPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    private func setupEmojiUI(label: UILabel) {
        label.layer.cornerRadius = Constants.cornerRadius8
        label.layer.borderWidth = Constants.border
        label.layer.borderColor = AppTheme.Colors.gray100.cgColor
        label.addShadow(shadowColor: Constants.shadowRGBA,
                        shadowOffset: Constants.shadowOffset,
                        shadowOpacity: Constants.shadowOpacity,
                        shadowRadius: Constants.cornerRadius8)
    }
    
    @IBAction private func withdrawButtonTapped() {}
    
}

extension VacationRequestScreenViewController: VacationRequestScreenPresenterOutput {
    
    func setupUI() {
        withdrawButton.setTitle("VacationRequestScreen.withdraw".localized, for: .normal)
        lastVacationLabel.text = "VacationRequestScreen.lastVacation".localized
        daysLeftVacationLabel.text = "VacationRequestScreen.daysLeftVacation".localized
        inCompanyLabel.text = "VacationRequestScreen.inCompany".localized
        statusLabel.text = "VacationRequestScreen.status".localized
        
        palmLabel.text = Constants.palmUnicode
        calendarLabel.text = Constants.calendarUnicode
        laptopLabel.text = Constants.laptopUnicode
        
        photoImageView.layer.cornerRadius = Constants.cornerRadius16
        employeeInfoView.layer.cornerRadius = Constants.cornerRadius16
        statusView.layer.cornerRadius = Constants.cornerRadius16
        withdrawButton.layer.cornerRadius = Constants.cornerRadius16
        
        setupEmojiUI(label: palmLabel)
        setupEmojiUI(label: calendarLabel)
        setupEmojiUI(label: laptopLabel)
    }
    
}

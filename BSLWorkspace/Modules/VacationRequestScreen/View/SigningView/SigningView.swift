//
//  SigningView.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 17.12.21.
//

import UIKit

final class SigningView: XibBasedView {
    
    @IBOutlet private var statusLabel: UILabel!
    @IBOutlet private var statusView: UIView!
    @IBOutlet private var employeeImageView: UIImageView!
    @IBOutlet private var employeeNameLabel: UILabel!
    @IBOutlet private var employeePositionLabel: UILabel!
    
    private enum Constants {
        static let cornerRadius16: CGFloat = 16
        static let cornerRadius8: CGFloat = 8
    }
    
    override func setupUI() {
        employeeImageView.layer.cornerRadius = Constants.cornerRadius16
        employeeImageView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        statusView.layer.cornerRadius = Constants.cornerRadius8
        statusView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    func configure(with signing: SigningModel) {
        statusLabel.text = signing.status
        employeeImageView.image = signing.photo
        employeeNameLabel.text = signing.name
        employeePositionLabel.text = signing.position
    }
    
}

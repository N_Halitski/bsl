//
//  VacationRequestScreenVacationRequestScreenPresenter.swift
//  BSLWorkspace
//
//  Created by clear on 16/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class VacationRequestScreenPresenter {

    private let interactor: VacationRequestScreenInteractorInput
    private unowned let view: VacationRequestScreenPresenterOutput

    init(_ interactor: VacationRequestScreenInteractorInput, _ view: VacationRequestScreenPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
        view.setupUI()
    }

}

extension VacationRequestScreenPresenter: VacationRequestScreenPresenterInput {}

extension VacationRequestScreenPresenter: VacationRequestScreenInteractorOutput {}

//
//  VacationRequestScreenVacationRequestScreenInteractor.swift
//  BSLWorkspace
//
//  Created by clear on 16/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class VacationRequestScreenInteractor {
    private weak var output: VacationRequestScreenInteractorOutput?

}

extension VacationRequestScreenInteractor: VacationRequestScreenInteractorInput {
    func attach(_ output: VacationRequestScreenInteractorOutput) {
        self.output = output
    }
}

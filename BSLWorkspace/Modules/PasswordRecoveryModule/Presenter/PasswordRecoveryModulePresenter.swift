//
//  PasswordRecoveryModulePasswordRecoveryModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class PasswordRecoveryModulePresenter {
    
    //MARK: - Dependencies
    
    private let interactor: PasswordRecoveryModuleInteractorInput
    private unowned let view: PasswordRecoveryModulePresenterOutput
    
    //MARK: - Callbacks
    
    private var openLoginScreen: (() -> Void)?
    
    init(_ interactor: PasswordRecoveryModuleInteractorInput, _ view: PasswordRecoveryModulePresenterOutput, openLoginScreen: (() -> Void)?) {
        self.interactor = interactor
        self.view = view
        self.openLoginScreen = openLoginScreen
    }
    
    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension PasswordRecoveryModulePresenter: PasswordRecoveryModulePresenterInput {
    
    ///  метод передает пароль в интерактор
    /// - Parameter email: email для востановления пароля
    func passwordRecovery(with email: String) {
        interactor.passwordRecovery(with: email)
    }
}

extension PasswordRecoveryModulePresenter: PasswordRecoveryModuleInteractorOutput {
    
    func didRecievePasswordRecovery(with result: Result<Any?, APIErrors>) {
        switch result {
        case .success:
            openLoginScreen?()
        case .failure:
            break
        }
    }
}

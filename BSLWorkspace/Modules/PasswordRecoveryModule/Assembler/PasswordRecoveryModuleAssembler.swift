//
//  PasswordRecoveryModulePasswordRecoveryModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class PasswordRecoveryModuleAssembler {
    /// создает модуль востановления пароля
    /// - Parameters:
    ///   - authRepository: репозиторий для запроса за востановлением пароля
    ///   - openLoginScreen: замыкание, которое открывает экран логина
    /// - Returns: view controller востановления пароля
    static func createModule(authRepository: AuthRepositoryProtocol = AuthRepository(), openLoginScreen: (() -> Void)?) -> PasswordRecoveryModuleViewController {
        let viewController = PasswordRecoveryModuleViewController()
        let interactor = PasswordRecoveryModuleInteractor(authRepository: authRepository)
        let presenter = PasswordRecoveryModulePresenter(interactor, viewController, openLoginScreen: openLoginScreen)
        viewController.presenter = presenter
        return viewController
    }
}

//
//  PasswordRecoveryModulePasswordRecoveryModuleInteractorInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol PasswordRecoveryModuleInteractorInput: AnyObject {
    func attach(_ output: PasswordRecoveryModuleInteractorOutput)
    func passwordRecovery(with email: String)
}

//
//  PasswordRecoveryModulePasswordRecoveryModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class PasswordRecoveryModuleInteractor {
    
    //MARK: - Dependencies
    
    private weak var output: PasswordRecoveryModuleInteractorOutput?
    
    /// репозиторий для запроса
    private let authRepository: AuthRepositoryProtocol
    
    init(authRepository: AuthRepositoryProtocol) {
        self.authRepository = authRepository
    }
}

extension PasswordRecoveryModuleInteractor: PasswordRecoveryModuleInteractorInput {
    
    func attach(_ output: PasswordRecoveryModuleInteractorOutput) {
        self.output = output
    }
    
    ///  метод, для запроса на востановление пароля
    /// - Parameter email: имейл для востановление пароля
    func passwordRecovery(with email: String) {
        self.authRepository.resetPassword(email: email) { [weak self] result in
            self?.output?.didRecievePasswordRecovery(with: result)
        }
    }
    
}

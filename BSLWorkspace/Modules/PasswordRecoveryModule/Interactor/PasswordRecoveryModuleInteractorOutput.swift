//
//  PasswordRecoveryModulePasswordRecoveryModuleInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol PasswordRecoveryModuleInteractorOutput: AnyObject {
    func didRecievePasswordRecovery(with result: Result<Any?, APIErrors>)
}

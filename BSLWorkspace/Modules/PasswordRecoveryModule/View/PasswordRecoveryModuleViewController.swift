//
//  PasswordRecoveryModulePasswordRecoveryModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class PasswordRecoveryModuleViewController: BaseViewController {
    
    /// задний фон
    @IBOutlet var backbroundView: UIView!
    /// скролл
    @IBOutlet weak var scrollView: UIScrollView!
    /// верхний лэйбл
    @IBOutlet weak var titleLabel: UILabel!
    /// описание требований
    @IBOutlet weak var textView: UITextView!
    /// кнопка Отправить
    @IBOutlet weak var button: UIButton!
    /// поле ввода имэйла
    @IBOutlet weak var textFieldView: TextFieldWithPlaceholderAndText!
    
    //MARK: - Dependencies
    
    var presenter: PasswordRecoveryModulePresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        presenter.viewDidLoad()
        setupUI()
        setupCallbacks()
        addKeyboardObservers()
        configureNavigationBar()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        guard let email = textFieldView.textField.text else { return }
        presenter.passwordRecovery(with: email)
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow),
                                               name: UIResponder.keyboardDidShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidHide),
                                               name: UIResponder.keyboardDidHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.isScrollEnabled = true
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 30
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        scrollView.isScrollEnabled = false
        scrollView.contentInset.bottom = 0
    }
}

// MARK: - Extensions
extension PasswordRecoveryModuleViewController: PasswordRecoveryModulePresenterOutput {}

extension PasswordRecoveryModuleViewController: UITextFieldDelegate {}

extension PasswordRecoveryModuleViewController {
    private func setupUI() {
        self.textFieldView.setup(placeHolder: "E-mail")
        setupTextView(
            textView: self.textView,
            text: "Введите адрес электронной почты, связанный с вашей учетной записью и мы отправим электронное письмо с инструкциями на адрес сбросить пароль.")
    }
    
    func setupTextView(textView: UITextView, text: String) {
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineHeightMultiple = 1.13
        paragraph.alignment = NSTextAlignment.left
        let font = AppTheme.Fonts.SFRegular(16)
        let attributes = [ NSAttributedString.Key.paragraphStyle: paragraph, NSAttributedString.Key.font: font ]
        textView.attributedText = NSAttributedString(string: text, attributes: attributes)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let padding = textView.textContainer.lineFragmentPadding
        textView.textContainerInset =  UIEdgeInsets(top: 0, left: -padding, bottom: 0, right: -padding)
        textView.clipsToBounds = false
        textView.textColor = AppTheme.Colors.darkGray
    }
    
    private func setupCallbacks() {
        self.textFieldView.textFieldDidChangeCompletion = { [weak self] email in
            guard let self = self else { return }
            self.button.isEnabled = email.lowercased().contains("@bsl.dev") ? true : false
            self.button.backgroundColor = email.lowercased().contains("@bsl.dev") ? AppTheme.Colors.darkBlue100 : AppTheme.Colors.grayBlue100
        }
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.topItem?.title = ""
        navigationController?.navigationBar.tintColor = AppTheme.Colors.black100
    }
}

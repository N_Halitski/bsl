//
//  TextFieldWithPlaceholderAndText.swift
//  BSLWorkspace
//
//  Created by Sergey on 11/17/21.
//

import Foundation
import UIKit

class TextFieldWithPlaceholderAndText: XibBasedView {

    // MARK: - Outlets
    
    @IBOutlet var backView: UIView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet var labelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var textFieldBottomConstraint: UIView!
    
    // MARK: - Callbacks
    
    var textFieldDidChangeCompletion: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(TextFieldWithPlaceholderAndText.tapFunction))
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tap)
        setupUI()
        self.textField.delegate = self
    }
    
    private func resetView() {
        self.labelTopConstraint.constant = 18
        self.label.font = AppTheme.Fonts.SFRegular(16)
        self.textField.isEnabled = false
    }
    
    override func setupUI() {
        self.textField.isEnabled = false
        self.backView.layer.shadowColor = UIColor.black.cgColor
        self.backView.layer.shadowOpacity = 0.07
        self.backView.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.backView.layer.shadowRadius = 36
        self.view.layer.cornerRadius = 16
    }
    
    @objc func tapFunction(_ sender: Any) {
        self.labelTopConstraint.constant = 9
        self.label.font = AppTheme.Fonts.SFRegular(12)
        self.textField.isEnabled = true
        self.textField.becomeFirstResponder()
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.textFieldDidChangeCompletion?(text)
    }
    
    func setup(placeHolder: String) {
        self.label.text = placeHolder
    }
}

// MARK: - Extensions

extension TextFieldWithPlaceholderAndText: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !self.textField.hasText {
            resetView()
        }
    }
}

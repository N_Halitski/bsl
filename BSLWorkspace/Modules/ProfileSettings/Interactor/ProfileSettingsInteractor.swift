//
//  ProfileSettingsProfileSettingsInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class ProfileSettingsInteractor {
    
    private weak var output: ProfileSettingsInteractorOutput?

}

extension ProfileSettingsInteractor: ProfileSettingsInteractorInput {
    func attach(_ output: ProfileSettingsInteractorOutput) {
        self.output = output
    }
}

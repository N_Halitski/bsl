//
//  PolicyAndTermsViewController.swift
//  BSLWorkspace
//
//  Created by Nikita on 19.01.2022.
//

import UIKit
import WebKit

protocol PolicyAndTermsViewControllerProtocol {
    func showWebView (_ url: String)
}

final class PolicyAndTermsViewController: UIViewController, PolicyAndTermsViewControllerProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func showWebView(_ url: String) {
        lazy var webView: WKWebView = {
            let web = WKWebView.init(frame: UIScreen.main.bounds)
            let url = URL.init(string: url)!
            let request = URLRequest.init(url: url)
            web.load(request)
            return web
        }()
        self.view.addSubview(webView)
    }
   
}

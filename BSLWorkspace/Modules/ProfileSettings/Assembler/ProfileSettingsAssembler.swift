//
//  ProfileSettingsProfileSettingsAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class ProfileSettingsAssembler {
    static func createModule() -> ProfileSettingsViewController {
        let viewController = ProfileSettingsViewController()
        let interactor = ProfileSettingsInteractor()
        let presenter = ProfileSettingsPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

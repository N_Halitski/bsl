//
//  ProfileSettingsProfileSettingsPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class ProfileSettingsPresenter {

    private let interactor: ProfileSettingsInteractorInput
    private unowned let view: ProfileSettingsPresenterOutput

    init(_ interactor: ProfileSettingsInteractorInput, _ view: ProfileSettingsPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension ProfileSettingsPresenter: ProfileSettingsPresenterInput {

}

extension ProfileSettingsPresenter: ProfileSettingsInteractorOutput {

}

//
//  ProfileSettingsProfileSettingsViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 28/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import UserNotifications

final class ProfileSettingsViewController: BaseViewController, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var pushNotificationsView: UIView!
    @IBOutlet weak var pushNotifications: UILabel!
    @IBOutlet weak var toggleSwitcherLabel: UISwitch!
    @IBOutlet weak var usingDataPolicyView: UIView!
    @IBOutlet weak var usingDataPolicyLabel: UILabel!
    @IBOutlet weak var termsOfUsageView: UIView!
    @IBOutlet weak var termsOfUsageLabel: UILabel!
    @IBOutlet weak var accountSettingsLabel: UILabel!
    @IBOutlet weak var accountSettingsView: UIView!
    @IBOutlet weak var deleteAccountLabel: UIButton!
    @IBOutlet weak var deleteAccountView: UIView!
    @IBOutlet weak var logOutLabel: UIButton!
    @IBOutlet weak var logOutView: UIView!
    @IBOutlet weak var separatorView: UIView!
    
    private let policyLink = "https://www.google.com"
    private let termsLink = "https://stackoverflow.com"
    
    var presenter: ProfileSettingsPresenterInput!
    
    var logOut: (() -> Void)?
    var deleteProfile: (() -> Void)?
    var openLink: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.getNotificationSettings { permission in
            DispatchQueue.main.async {
                self.toggleSwitcherLabel.isOn = permission.authorizationStatus == .authorized ? true : false
            }
        }
    }
    
    @IBAction func togglePushNotifications(_ sender: UISwitch) {
        if let bundle = Bundle.main.bundleIdentifier,
           let settings = URL(string: UIApplication.openSettingsURLString + bundle) {
            if UIApplication.shared.canOpenURL(settings) {
                UIApplication.shared.open(settings)
            }
        }
    }
    
    @objc func showPolicy() {
        openLink?(policyLink)
    }
    
    @objc func showTermsOfUsage() {
        openLink?(termsLink)
    }
    
    @IBAction func deleteAccountClicked(_ sender: Any) {
        deleteProfile?()
    }
    
    @IBAction func logOutClicked(_ sender: Any) {
        logOut?()
    }
    
    private func setupUI() {
        title = "ProfileSettings.settings".localized
        
        view.backgroundColor = AppTheme.Colors.lightGray100
        separatorView.backgroundColor = AppTheme.Colors.gray100
        
        pushNotificationsView.backgroundColor = AppTheme.Colors.lightGray100
        pushNotifications.font = AppTheme.Fonts.SFRegular(16)
        
        usingDataPolicyView.backgroundColor = AppTheme.Colors.lightGray100
        usingDataPolicyLabel.font = AppTheme.Fonts.SFRegular(16)
        
        termsOfUsageView.backgroundColor = AppTheme.Colors.lightGray100
        termsOfUsageLabel.font = AppTheme.Fonts.SFRegular(16)
        
        accountSettingsView.backgroundColor = AppTheme.Colors.lightGray100
        accountSettingsLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        deleteAccountView.backgroundColor = AppTheme.Colors.lightGray100
        deleteAccountLabel.tintColor = AppTheme.Colors.babyBlue1
        
        logOutView.backgroundColor = AppTheme.Colors.lightGray100
        logOutLabel.tintColor = AppTheme.Colors.babyBlue1
        
        let policy = UITapGestureRecognizer(target: self, action: #selector(self.showPolicy))
        usingDataPolicyView.addGestureRecognizer(policy)
        
        let showTermsOfUsage = UITapGestureRecognizer(target: self, action: #selector(self.showTermsOfUsage))
        termsOfUsageView.addGestureRecognizer(showTermsOfUsage)
        
       navigationController?.navigationBar.topItem?.title = " "
       navigationController?.navigationBar.tintColor = AppTheme.Colors.black100
    }

}

extension ProfileSettingsViewController: ProfileSettingsPresenterOutput {
    
}

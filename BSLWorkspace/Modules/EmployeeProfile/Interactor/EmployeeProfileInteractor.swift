//
//  EmployeeProfileEmployeeProfileInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 15/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class EmployeeProfileInteractor {
    
    private weak var output: EmployeeProfileInteractorOutput?
    private var tableManager: EmployeeTableManagerProtocol
    
    var profileModel: Employee?
    var employeeTestProjectArray: [EmployeeActiveProjectsTest] = []
    var vacationTestArray: [EmployeeVacationTestModel] = []
    var isUserProfile: Bool
    var stuffRosterEmployeeModel: Employee?
    
    init(tableManager: EmployeeTableManagerProtocol, isUserProfile: Bool, employee: Employee?) {
        self.tableManager = tableManager
        self.isUserProfile = isUserProfile
        self.stuffRosterEmployeeModel = employee
    }
    
    private func setupTableWithEmployee(_ stuffRosterEmployeeModel: Employee?) {
        if stuffRosterEmployeeModel?.userId == SharedEmployeeModel.shared.employee?.userId || self.isUserProfile == true {
            if let loggedInEmployee = SharedEmployeeModel.shared.employee {
                tableManager.getUserOrEmployeeForTable(loggedInEmployee)
            }
            output?.settingsIsHidden(condition: false)
        } else {
            guard let stuffRosterEmployeeModel = stuffRosterEmployeeModel else { return }
            tableManager.getUserOrEmployeeForTable(stuffRosterEmployeeModel)
            output?.settingsIsHidden(condition: true)
        }
    }
    
}

extension EmployeeProfileInteractor: EmployeeProfileInteractorInput {
    
    func attachTable(_ tableView: UITableView) {
        setupTableWithEmployee(stuffRosterEmployeeModel)
        tableManager.attachTable(tableView)
    }
    
    func attach(_ output: EmployeeProfileInteractorOutput) {
        self.output = output
    }
    
}

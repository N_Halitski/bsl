//
//  EmployeeProfileEmployeeProfileInteractorInput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 15/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol EmployeeProfileInteractorInput: AnyObject {
    func attach(_ output: EmployeeProfileInteractorOutput)
    func attachTable(_ tableView: UITableView)
}

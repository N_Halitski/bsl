//
//  ActiveProjectsCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit

protocol ActiveProjectsCellProtocol {
    func getProjectsForCollectionView(_ projects: [EmployeeActiveProjectsTest])
}

final class ActiveProjectsCell: UITableViewCell, ActiveProjectsCellProtocol {

    @IBOutlet weak var projectsCellView: UIView!
    @IBOutlet weak var activeProjectsLabel: UILabel!
    @IBOutlet weak var activeProjectsCollectionView: UICollectionView!
    
    private var projects: [EmployeeActiveProjectsTest] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        self.activeProjectsCollectionView.delegate = self
        self.activeProjectsCollectionView.dataSource = self
        activeProjectsCollectionView.registerCell(ActiveProjectsCollectionCell.self.self)
    }
    
    private func setupUI() {
        projectsCellView.layer.cornerRadius = 16
        projectsCellView.backgroundColor = AppTheme.Colors.white100
        
        activeProjectsLabel.font = AppTheme.Fonts.SFSemiBold(20)
        activeProjectsLabel.textColor = AppTheme.Colors.black100
        
        selectionStyle = .none
    }
    
    func getProjectsForCollectionView(_ projects: [EmployeeActiveProjectsTest]) {
        self.projects = projects
    }
    
}

extension ActiveProjectsCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let activeProjectsCell = activeProjectsCollectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: ActiveProjectsCollectionCell.self),
            for: indexPath) as? ActiveProjectsCollectionCellProtocol else { return UICollectionViewCell() }
        let project = projects[indexPath.row]
        activeProjectsCell.setupCell(with: project)
        guard let activeProjectsCell = activeProjectsCell as? ActiveProjectsCollectionCell else { return UICollectionViewCell() }
        return activeProjectsCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 232, height: 135)
    }
    
}

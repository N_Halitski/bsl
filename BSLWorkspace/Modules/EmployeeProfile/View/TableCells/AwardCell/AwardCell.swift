//
//  AwardCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit

protocol AwardsCellProtocol {
    func getAwardsForCollectionView(_ employeeAwards: [Award])
}

final class AwardCell: UITableViewCell, AwardsCellProtocol {
    
    @IBOutlet private var awardCellView: UIView!
    @IBOutlet private var awardsLabel: UILabel!
    @IBOutlet weak var awardsCollectionView: UICollectionView!
    
    private var awards: [EmployeeTestAward] = []
    private var employeeAwards: [Award] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupCollectionView()
    }
    
    func getAwardsForCollectionView(_ employeeAwards: [Award]) {
        self.employeeAwards = employeeAwards
    }
    
    private func setupUI() {
        awardCellView.layer.cornerRadius = 16
        awardCellView.backgroundColor = AppTheme.Colors.white100
        
        awardsLabel.font = AppTheme.Fonts.SFSemiBold(20)
        awardsLabel.textColor = AppTheme.Colors.black100
        
        selectionStyle = .none
    }
    
    private func setupCollectionView() {
        self.awardsCollectionView.delegate = self
        self.awardsCollectionView.dataSource = self
        awardsCollectionView.registerCell( AwardCollectionCell.self.self)
    }
    
}

extension AwardCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeeAwards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let awardCell = awardsCollectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: AwardCollectionCell.self),
            for: indexPath) as? AwardCellProtocol else { return UICollectionViewCell() }
        let employeeAward = employeeAwards[indexPath.row]
        awardCell.setupCell(with: employeeAward)
        guard let awardCell = awardCell as? AwardCollectionCell else { return UICollectionViewCell() }
        return awardCell 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 116, height: 127)
    }
    
}

//
//  EmployeeInfoCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 15.12.2021.
//

import UIKit
import Kingfisher

protocol EmployeeInfoCellProtocol {
    func display(_ employee: Employee)
}

final class EmployeeInfoCell: UITableViewCell, EmployeeInfoCellProtocol {
    
    @IBOutlet weak var employeePhoto: UIImageView!
    @IBOutlet weak var viewBelowPhoto: UIView!
    @IBOutlet weak var nameSurnameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func display(_ employee: Employee) {
        self.nameSurnameLabel.text = employee.name
        self.positionLabel.text = employee.title
        self.cityLabel.text = employee.city
        self.dateOfBirthLabel.text = employee.birthdate
        self.phoneNumberLabel.text = employee.phone
        self.emailLabel.text = employee.email
        if let employeePicture = employee.avatar {
            let avatar = URL(string: employeePicture)
            let placeholder = UIImage(named: "unselectedCell")
            employeePhoto.kf.setImage(with: avatar, placeholder: placeholder)
        } else {
            employeePhoto.image = UIImage(named: "person")
        }
    }
    
    private func setupUI() {
        employeePhoto.layer.cornerRadius = 16
        
        viewBelowPhoto.backgroundColor = AppTheme.Colors.lightGray100
        
        nameSurnameLabel.font = AppTheme.Fonts.SFBold(20)
        nameSurnameLabel.textColor = AppTheme.Colors.black100
        
        positionLabel.font = AppTheme.Fonts.SFRegular(14)
        positionLabel.textColor = AppTheme.Colors.darkGray
        
        cityLabel.font = AppTheme.Fonts.SFRegular(14)
        cityLabel.textColor = AppTheme.Colors.black100
        
        dateOfBirthLabel.font = AppTheme.Fonts.SFRegular(14)
        dateOfBirthLabel.textColor = AppTheme.Colors.black100
        
        phoneNumberLabel.font = AppTheme.Fonts.SFRegular(14)
        phoneNumberLabel.textColor = AppTheme.Colors.babyBlue
        
        emailLabel.font = AppTheme.Fonts.SFRegular(14)
        emailLabel.textColor = AppTheme.Colors.babyBlue
        
        selectionStyle = .none
        backgroundColor = AppTheme.Colors.lightGray100
        
    }
}

//
//  VacationCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit

protocol VacationCellProtocol {
    func getVacationsForCollectionView (_ employeeVacations: [Vacation])
}

final class VacationCell: UITableViewCell, VacationCellProtocol {
    
    @IBOutlet weak var vacationCellView: UIView!
    @IBOutlet weak var vacationLabel: UILabel!
    @IBOutlet weak var vacationCollectionView: UICollectionView!
    
    private var vacations: [EmployeeVacationTestModel] = []
    private var employeeVacations: [Vacation] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func getVacationsForCollectionView (_ employeeVacations: [Vacation]) {
        self.employeeVacations = employeeVacations
    }
    
    private func setupUI() {
        vacationCellView.layer.cornerRadius = 16
        vacationCellView.backgroundColor = AppTheme.Colors.white100
        vacationLabel.font = AppTheme.Fonts.SFSemiBold(20)
        vacationLabel.textColor = AppTheme.Colors.black100
        selectionStyle = .none
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.vacationCollectionView.delegate = self
        self.vacationCollectionView.dataSource = self
        self.vacationCollectionView.isPagingEnabled = false
        vacationCollectionView.registerCell(RequestForVacationCell.self.self)
        
        let layout = UICollectionViewFlowLayout()
        let inset: CGFloat = 8
        layout.minimumLineSpacing = -10
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 16, height: 138)
        layout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        vacationCollectionView.collectionViewLayout.invalidateLayout()
        vacationCollectionView.setCollectionViewLayout(layout, animated: false)
    }
    
}

extension VacationCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeeVacations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let vacationCell = vacationCollectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: RequestForVacationCell.self),
            for: indexPath) as? RequestForVacationCellProtocol else { return UICollectionViewCell() }
        let employeeVacation = employeeVacations[indexPath.row]
        vacationCell.setupCell(with: employeeVacation)
        guard let vacationCell = vacationCell as? RequestForVacationCell else { return UICollectionViewCell() }
        return vacationCell
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let layout = vacationCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        let cellWidthWithSpacing = layout.itemSize.width + layout.minimumLineSpacing
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthWithSpacing
        let roundedIndex = round(index)
        offset = CGPoint(x: roundedIndex * cellWidthWithSpacing - scrollView.contentInset.left,
                         y: scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
}

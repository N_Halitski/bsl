//
//  RequestForVacationCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 21.12.2021.
//

import UIKit

protocol RequestForVacationCellProtocol {
    func setupCell (with employeeVacation: Vacation)
}

final class RequestForVacationCell: UICollectionViewCell, RequestForVacationCellProtocol {
    
    @IBOutlet weak var datesLabel: UILabel!
    @IBOutlet weak var numberOfDays: UILabel!
    @IBOutlet weak var vacationBackgroundView: UIView!
    @IBOutlet weak var firstReviewerImage: UIImageView!
    @IBOutlet weak var secondReviewerImage: UIImageView!
    @IBOutlet weak var thirdReviewerImage: UIImageView!
    @IBOutlet weak var vacationStatusView: UIView!
    @IBOutlet weak var vacationStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupCell (with employeeVacation: Vacation) {
        numberOfDays.text = DateFormatterManager.shared.dayIntervalCalculation(dateStart: employeeVacation.dateEnd, dateEnd: employeeVacation.dateEnd)
        datesLabel.text = DateFormatterManager.shared.dateInterval(dateStart: employeeVacation.dateStart, dateEnd: employeeVacation.dateEnd)
        vacationStatusLabel.text = employeeVacation.status
    }
    
    private func setupUI() {
        vacationBackgroundView.addShadow(shadowColor: AppTheme.Colors.shadowColor.cgColor, shadowOffset: CGSize(width: 0, height: 0), shadowOpacity: 1, shadowRadius: 6)
        
        vacationBackgroundView.layer.cornerRadius = 16
        
        firstReviewerImage.layer.cornerRadius = self.firstReviewerImage.layer.frame.width / 2
        firstReviewerImage.layer.borderWidth = 2
        firstReviewerImage.layer.borderColor = AppTheme.Colors.greenStatus.cgColor
        
        secondReviewerImage.layer.cornerRadius = self.secondReviewerImage.layer.frame.width / 2
        secondReviewerImage.layer.borderWidth = 2
        secondReviewerImage.layer.borderColor = AppTheme.Colors.greenStatus.cgColor
        
        thirdReviewerImage.layer.cornerRadius = self.thirdReviewerImage.layer.frame.width / 2
        thirdReviewerImage.layer.borderWidth = 2
        thirdReviewerImage.layer.borderColor = AppTheme.Colors.greenStatus.cgColor
        vacationStatusView.backgroundColor = AppTheme.Colors.greenStatus
    }
    
}

//
//  AwardCollectionCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit
import Kingfisher

protocol AwardCellProtocol {
    func setupCell(with employeeAward: Award)
}

final class AwardCollectionCell: UICollectionViewCell, AwardCellProtocol {
    
    @IBOutlet weak var awardView: UIView!
    @IBOutlet weak var awardDescription: UILabel!
    @IBOutlet weak var awardIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupCell(with employeeAward: Award) {
        awardDescription.text = employeeAward.title
        if let employeePhoto = employeeAward.imageURL {
            let avatar = URL(string: "\(APIConstants.baseURL)\(employeePhoto)")
            let placeholder = UIImage(named: "unselectedCell")
            awardIcon.kf.setImage(with: avatar, placeholder: placeholder)
        } else {
            awardIcon.image = UIImage(named: "person")
        }
    }
    
    private func setupUI() {
        awardView.backgroundColor = AppTheme.Colors.creamColor
        awardView.layer.borderWidth = 1
        awardView.layer.borderColor = AppTheme.Colors.sandColor.cgColor
        awardView.layer.cornerRadius = 16
        
        awardDescription.font = AppTheme.Fonts.SFMedium(14)
        awardDescription.textColor = AppTheme.Colors.black100
    }
    
}

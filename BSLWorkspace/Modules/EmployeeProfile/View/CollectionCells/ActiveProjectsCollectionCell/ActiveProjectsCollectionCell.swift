//
//  ActiveProjectsCollectionCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 20.12.2021.
//

import UIKit

protocol ActiveProjectsCollectionCellProtocol {
    func setupCell(with activeProject: EmployeeActiveProjectsTest)
}

final class ActiveProjectsCollectionCell: UICollectionViewCell, ActiveProjectsCollectionCellProtocol {
    
    @IBOutlet weak var projectBackgroundView: UIView!
    @IBOutlet weak var projectIcon: UIImageView!
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var positionOnProjectLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupCell(with activeProject: EmployeeActiveProjectsTest) {
        projectNameLabel.text = activeProject.projectame
        positionOnProjectLabel.text = activeProject.position
    }
    
    private func setupUI() {
        projectBackgroundView.addShadow(shadowColor: AppTheme.Colors.shadowColor.cgColor, shadowOffset: CGSize(width: 0, height: 3), shadowOpacity: 1, shadowRadius: 6)
        projectBackgroundView.layer.masksToBounds = false
        
        projectBackgroundView.backgroundColor = AppTheme.Colors.white100
        projectBackgroundView.layer.cornerRadius = 16
        
        projectIcon.layer.cornerRadius = 8
        
        projectNameLabel.textColor = AppTheme.Colors.black100
        projectNameLabel.font = AppTheme.Fonts.SFSemiBold(16)
        
        positionOnProjectLabel.textColor = AppTheme.Colors.black100
        positionOnProjectLabel.font = AppTheme.Fonts.SFMedium(14)
    }
}

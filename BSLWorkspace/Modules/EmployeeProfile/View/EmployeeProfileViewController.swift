//
//  EmployeeProfileEmployeeProfileViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 15/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class EmployeeProfileViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: EmployeeProfilePresenterInput!
    
    var openSettings: (() -> Void)?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTable(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "EmployeeProfile.title".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    private func setupNavUI() {
        let profileSettingsButton = UIButton(type: UIButton.ButtonType.custom)
        profileSettingsButton.setImage(UIImage(named: "profileSettings"), for: .normal)
        profileSettingsButton.addTarget(self, action: #selector(openSettingsScreen), for: .touchUpInside)
        profileSettingsButton.frame = CGRect(x: 16, y: 10, width: 44, height: 44)
        let barButton = UIBarButtonItem(customView: profileSettingsButton)
        self.navigationItem.rightBarButtonItem = barButton
        
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.shadowColor = .clear
        navBarAppearance.shadowImage = UIImage()
        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
    }
    
    @objc func openSettingsScreen() {
       openSettings?()
    }
    
}

extension EmployeeProfileViewController: EmployeeProfilePresenterOutput {
    
    func hideSettingsButton(condition: Bool) {
        if !condition {
            setupNavUI()
        }
    }

}

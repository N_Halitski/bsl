//
//  EmployeeTableManager.swift
//  BSLWorkspace
//
//  Created by Nikita on 15.12.2021.
//

import UIKit

protocol EmployeeTableManagerProtocol {
    func attachTable(_ tableView: UITableView)
    func createActiveProjectsCell(_ activeProjects: [EmployeeActiveProjectsTest])
    func getUserOrEmployeeForTable(_ employee: Employee)
}

final class EmployeeTableManager: NSObject, EmployeeTableManagerProtocol {
    
    private var tableView: UITableView?
    private var configurators: [BaseConfigurator] = []
    private var employee: Employee?
    
    func attachTable(_ tableView: UITableView) {
        self.tableView = tableView
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
        setupEmployeeTableView()
        setupCellsForTableView()
    }
    
    func getUserOrEmployeeForTable(_ employee: Employee) {
        var output: EmployeeTableConfigurator?
        self.employee = employee
        output = createUserConfigurator(employee)
        self.configurators.append(output!)
        if !(employee.awards?.isEmpty ?? true) {
            createAwardCell(employee.awards)
        }
        if !(employee.vacations?.isEmpty ?? true) {
            createVacationCell(employee.vacations)
        }
        tableView?.reloadData()
    }
   
    func createAwardCell(_ employeeAwards: [Award]?) {
        guard let employeeAwards = employeeAwards else { return }
        var output: AwardsTableConfigurator?
        output = createAwardConfigurator(employeeAwards)
        self.configurators.append(output!)
    }
    
    func createActiveProjectsCell(_ activeProjects: [EmployeeActiveProjectsTest]) {
        var output: ActiveProjectsTableConfigurator?
        output = createActiveProjectsConfigurator(activeProjects)
        self.configurators.append(output!)
    }
    
    func createVacationCell(_ employeeVacations: [Vacation]?) {
        guard let employeeVacations = employeeVacations else { return }
        var output: VacationTableConfigurator?
        output = createVacationConfigurator(employeeVacations)
        self.configurators.append(output!)
    }
    
}

extension EmployeeTableManager {
    
    private func createUserConfigurator(_ employee: Employee) -> EmployeeTableConfigurator {
        let configurator = EmployeeProfileCellConfigurator()
        configurator.employee = employee
        return configurator
    }
    
    private func createAwardConfigurator(_ employeeAwards: [Award]) -> AwardsTableConfigurator {
        let configurator = AwardsCellConfigurator()
        configurator.employeeAwards = employeeAwards
        return configurator
    }
    
    private func createActiveProjectsConfigurator(_ activeProjects: [EmployeeActiveProjectsTest]) -> ActiveProjectsTableConfigurator {
        let configurator = ActriveProjectsCellConfigurator()
        configurator.activeProjects = activeProjects
        return configurator
    }
    
    private func createVacationConfigurator(_ employeeVacations: [Vacation]) -> VacationTableConfigurator {
        let configurator = VacationCellConfigurator()
        configurator.employeeVacations = employeeVacations
        return configurator
    }
    
    private func setupEmployeeTableView() {
        self.tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        self.tableView?.backgroundColor = AppTheme.Colors.lightGray100
    }
    
}

extension EmployeeTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        
        return cell
    }
    
}

extension EmployeeTableManager {
    
    private func setupCellsForTableView() {
        self.tableView?.registerCell(EmployeeInfoCell.self.self)
        self.tableView?.registerCell(AwardCell.self.self)
        self.tableView?.registerCell(ActiveProjectsCell.self.self)
        self.tableView?.registerCell(VacationCell.self.self)
    }
    
}

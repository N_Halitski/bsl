//
//  EmployeeCellConfigurators.swift
//  BSLWorkspace
//
//  Created by Nikita on 15.12.2021.
//

import UIKit

protocol EmployeeTableConfigurator: BaseConfigurator {
    var employee: Employee? { get set }
}

final class EmployeeProfileCellConfigurator: EmployeeTableConfigurator {
    
    var reuseId: String { String(describing: EmployeeInfoCell.self) }
    var employee: Employee?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? EmployeeInfoCellProtocol,
              let employee = employee else { return }
        cell.display(employee)
    }
}

//
//  VacationTableConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit

protocol VacationTableConfigurator: BaseConfigurator {
    var employeeVacations: [Vacation]? { get set }
}

final class VacationCellConfigurator: VacationTableConfigurator {
    
    var reuseId: String = String(describing: VacationCell.self)
    var employeeVacations: [Vacation]?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? VacationCellProtocol,
              let employeeVacations = employeeVacations else { return }
        cell.getVacationsForCollectionView(employeeVacations)
    }
    
}

//
//  AwardTableConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit

protocol ActiveProjectsTableConfigurator: BaseConfigurator {
    var activeProjects: [EmployeeActiveProjectsTest]? { get set }
}

final class ActriveProjectsCellConfigurator: ActiveProjectsTableConfigurator {
    
    var reuseId: String = String(describing: ActiveProjectsCell.self)
    
    var activeProjects: [EmployeeActiveProjectsTest]?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? ActiveProjectsCellProtocol,
              let activeProjects = activeProjects else { return }
        cell.getProjectsForCollectionView(activeProjects)
    }
    
}

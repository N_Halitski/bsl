//
//  AwardsTableConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import UIKit

protocol AwardsTableConfigurator: BaseConfigurator {
    var employeeAwards: [Award]? { get set }
}

final class AwardsCellConfigurator: AwardsTableConfigurator {
    
    var reuseId: String = String(describing: AwardCell.self)
    var employeeAwards: [Award]?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? AwardsCellProtocol,
              let employeeAwards = employeeAwards else { return }
        cell.getAwardsForCollectionView(employeeAwards)
    }
    
}

//
//  EmployeeProfileModel.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.12.2021.
//

import Foundation

struct EmployeeProfile {
    var nameAndSurname: String?
    var position: String?
    var city: String?
    var dateOfBirth: String?
    var phoneNumber: String?
    var email: String?
}

//
//  EmployeeVacationTestModel.swift
//  BSLWorkspace
//
//  Created by Nikita on 24.12.2021.
//

import Foundation

struct EmployeeVacationTestModel {
    var datePeriod: String?
    var totalNumberOfDays: String?
}

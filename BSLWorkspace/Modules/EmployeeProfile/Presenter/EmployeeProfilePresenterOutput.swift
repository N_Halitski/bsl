//
//  EmployeeProfileEmployeeProfilePresenterOutput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 15/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol EmployeeProfilePresenterOutput: AnyObject {
    func hideSettingsButton(condition: Bool)
}

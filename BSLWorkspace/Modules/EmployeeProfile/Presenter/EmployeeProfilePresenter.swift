//
//  EmployeeProfileEmployeeProfilePresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 15/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class EmployeeProfilePresenter {

    private let interactor: EmployeeProfileInteractorInput
    private unowned let view: EmployeeProfilePresenterOutput

    init(_ interactor: EmployeeProfileInteractorInput, _ view: EmployeeProfilePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
    
}

extension EmployeeProfilePresenter: EmployeeProfilePresenterInput {
    
    func attachTable(_ tableView: UITableView) {
        interactor.attachTable(tableView)
    }
    
}

extension EmployeeProfilePresenter: EmployeeProfileInteractorOutput {
    
    func settingsIsHidden(condition: Bool) {
        view.hideSettingsButton(condition: condition)
    }
    
}

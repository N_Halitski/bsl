//
//  EmployeeProfileEmployeeProfileAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 15/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class EmployeeProfileAssembler {
    static func createModule(_ tableManager: EmployeeTableManagerProtocol = EmployeeTableManager(),
                             isUserProfile: Bool = false,
                             employee: Employee?) -> EmployeeProfileViewController {
        let viewController = EmployeeProfileViewController()
        let interactor = EmployeeProfileInteractor(tableManager: tableManager, isUserProfile: isUserProfile, employee: employee)
        let presenter = EmployeeProfilePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

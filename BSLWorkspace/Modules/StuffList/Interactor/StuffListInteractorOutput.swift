//
//  StuffListStuffListInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol StuffListInteractorOutput: AnyObject {
    func sendPositionsAndProjectsToFilterPanel (positionsList: [Position], projectsList: [ProjectsRegistryResponseModel])
    func showEmployeeProfile (_ employee: Employee)
}

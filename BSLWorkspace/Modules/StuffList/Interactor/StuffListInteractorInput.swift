//
//  StuffListStuffListInteractorInput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol StuffListInteractorInput: AnyObject {
    func attach(_ output: StuffListInteractorOutput)
    func attachTableView(_ table: UITableView)
    func filterData(by searchText: String)
    func sendPositionsAndProjectsToTableManager(_ positions: [Position])
    func clearFilters()
}

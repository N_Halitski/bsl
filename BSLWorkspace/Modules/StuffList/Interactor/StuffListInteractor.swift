//
//  StuffListStuffListInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class StuffListInteractor {
    
    private weak var output: StuffListInteractorOutput?
    private var tableManager: StuffTableManagerProtocol
    private var employeeList: [Employee]
    private var positionsList: [Position]
    private var projectsList: [ProjectsRegistryResponseModel]
    
    init(tableManager: StuffTableManagerProtocol, employeeList: [Employee], positionsList: [Position], projectsList: [ProjectsRegistryResponseModel]) {
        self.tableManager = tableManager
        self.employeeList = employeeList
        self.positionsList = positionsList
        self.projectsList = projectsList
        self.tableManager.showEmployees(employees: employeeList, positions: positionsList, projects: projectsList)
    }
}

extension StuffListInteractor: StuffListInteractorInput {
    func sendPositionsAndProjectsToTableManager(_ positions: [Position]) {
        tableManager.filterByOptions(positions)
    }
    
    func filterData(by searchText: String) {
        tableManager.filterData(by: searchText)
    }
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
    }
    
    func attach(_ output: StuffListInteractorOutput) {
        self.output = output
        output.sendPositionsAndProjectsToFilterPanel(positionsList: self.positionsList, projectsList: self.projectsList)
        tableManager.didSelectEmployee = { employee in
            output.showEmployeeProfile(employee)
        }
    }
    
    func clearFilters() {
        tableManager.clearFilters()
    }
}

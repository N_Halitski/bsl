//
//  EmployeeInSection.swift
//  BSLWorkspace
//
//  Created by Nikita on 17.12.2021.
//

import Foundation

struct EmployeeInSection {
    var section: String?
    var employees: [Employee]
}

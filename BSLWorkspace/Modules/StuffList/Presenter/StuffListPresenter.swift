//
//  StuffListStuffListPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class StuffListPresenter {
    
    private let interactor: StuffListInteractorInput
    private unowned let view: StuffListPresenterOutput
    private let openType: StuffListOpenType
    
    init(_ interactor: StuffListInteractorInput, _ view: StuffListPresenterOutput, openType: StuffListOpenType) {
        self.interactor = interactor
        self.view = view
        self.openType = openType
    }
    
    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension StuffListPresenter: StuffListPresenterInput {
    
    func sendPositionsAndProjectsToTableManager(_ positions: [Position]) {
        interactor.sendPositionsAndProjectsToTableManager(positions)
    }
    
    func filterData(by searchText: String) {
        interactor.filterData(by: searchText)
    }
    
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
    
    func clearFilters() {
        interactor.clearFilters()
    }
}

extension StuffListPresenter: StuffListInteractorOutput {
    
    func sendPositionsAndProjectsToFilterPanel(positionsList: [Position], projectsList: [ProjectsRegistryResponseModel]) {
        view.sendPositionsAndProjectsToFilterPanel(positionsList: positionsList, projectsList: projectsList)
    }
    
    func showEmployeeProfile(_ employee: Employee) {
        switch openType {
            case .achievements:
                view.returnToAchivmentts(with: employee)
            case .instruments:
                view.showEmployeeProfile(employee)
        }
    }
    
}

//
//  StuffListStuffListViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import FloatingPanel

final class StuffListViewController: BaseViewController {
    
    @IBOutlet weak var stuffTableView: UITableView!
    @IBOutlet weak var filterButtonOutlet: UIButton!
    @IBOutlet weak var searchView: SearchPanel!
    @IBOutlet weak var viewForSearchbarAndButton: UIView!
    
    var presenter: StuffListPresenterInput!
    
    private var fpc: FloatingPanelController?
    var filtersVC: StuffFiltrationViewController?
    var positionsVC: PositionsListViewController?
    var projectsVC: ProjectsListViewController?
    
    var clearSelectionForPositionsVC: (() -> Void)?
    var clearSelectionForProjectsVC: (() -> Void)?
    var openProfileScreen: ((Employee) -> Void)?
    var dismissToAchievementsScreen: ((Employee) -> Void)?
    
    private var positionsList: [Position] = []
    private var projectsList: [ProjectsRegistryResponseModel] = []
    
    var positionsForFiltering: [Position] = []
    var projectsForFiltering: [ProjectsRegistryResponseModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(stuffTableView)
        searchView.textField.delegate = self
        setupCallbacks()
        
        setupUI()
        stuffTableView.keyboardDismissMode = .onDrag
        hideKeyboardWhenTappedAround()
        guard let fpc = fpc else { return }
        self.view.addSubview(fpc.view)
    }
    
    private func setupUI() {
        
        viewForSearchbarAndButton.backgroundColor = AppTheme.Colors.lightGray100
        
        navigationController?.navigationBar.backgroundColor = AppTheme.Colors.lightGray100
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = AppTheme.Colors.black100
        view.backgroundColor = AppTheme.Colors.lightGray100
        
        title = ("\("StuffListScreen.stuffRoster".localized)🧑‍💻")
    }
    
    private func setupCallbacks() {
        self.searchView.textFieldDidChangeCompletion = { [weak self] searchText in
            guard let self = self else { return }
            self.presenter.filterData(by: searchText)
        }
        
        self.searchView.filtersClicked = { [weak self] in
            guard let self = self else { return }
            self.navigationController?.navigationBar.backgroundColor = .clear
            self.setupFiltrationFloatingPannel()
            self.tabBarController?.tabBar.isHidden = true
        }
    }
}

// MARK: - Presenter Output

extension StuffListViewController: StuffListPresenterOutput {
    
    func returnToAchivmentts(with profile: Employee) {
        dismissToAchievementsScreen?(profile)
    }
    
    func sendPositionsAndProjectsToFilterPanel(positionsList: [Position], projectsList: [ProjectsRegistryResponseModel]) {
        self.positionsList = positionsList
        self.projectsList = projectsList
    }
    
    func sendProjectsToFilterPanel(projectsList: [ProjectsRegistryResponseModel]) {

    }
    
    func showEmployeeProfile(_ employee: Employee) {
        openProfileScreen?(employee)
    }
    
}

extension StuffListViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

extension StuffListViewController: FloatingPanelControllerDelegate {
    
    private func createFiltrationVC() -> StuffFiltrationViewController {
        if filtersVC == nil {
            let filtersVC = StuffFiltrationAssembler.createModule()
            filtersVC.didFinishClosing = { [weak self] in
                guard let self = self else { return }
                self.fpc!.hide(animated: true) {
                    self.fpc!.removeFromParent()
                }
                self.tabBarController?.tabBar.isHidden = false
            }
            filtersVC.showPositionsList = { [weak self] in
                guard let self = self else { return }
                let positionsVC = self.createPositionsVC()
                self.fpc?.isRemovalInteractionEnabled = false
                self.fpc?.set(contentViewController: positionsVC)
                self.fpc?.move(to: .full, animated: true, completion: nil)
            }
            filtersVC.showProjectsList = { [weak self] in
                guard let self = self else { return }
                let projectsVC = self.createProjectsVC()
                projectsVC.modalPresentationStyle = .formSheet
                self.present(projectsVC, animated: true, completion: nil)
            }
            filtersVC.discardChanges = { [weak self] in
                guard let self = self else { return }
                self.searchView.orangeMarkForFilter.isHidden = true
                self.positionsForFiltering.removeAll()
                self.projectsForFiltering.removeAll()
                self.presenter.clearFilters()
                self.clearSelectionForPositionsVC?()
                self.clearSelectionForProjectsVC?()

                filtersVC.positionLabel.text = "StuffFiltrationScreen.choosePosition".localized
                filtersVC.choosePositionButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
                filtersVC.positionLabel.textColor = AppTheme.Colors.darkGray
                
                filtersVC.projectLabel.text = "StuffFiltrationScreen.chooseProject".localized
                filtersVC.chooseProjectButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
                filtersVC.projectLabel.textColor = AppTheme.Colors.darkGray
                
                filtersVC.discardFiltersOutlet.tintColor = AppTheme.Colors.darkGray
            }
            filtersVC.showResults = { [weak self] in
                guard let self = self else { return }
    
                if !self.positionsForFiltering.isEmpty {
                    self.searchView.orangeMarkForFilter.isHidden = false
                    self.presenter.sendPositionsAndProjectsToTableManager(self.positionsForFiltering)
                }
                self.tabBarController?.tabBar.isHidden = false
                self.fpc!.hide(animated: true) {
                    self.fpc!.removeFromParent()
                }
            }
            self.filtersVC = filtersVC
        }
        guard let filtersVC = filtersVC else { return StuffFiltrationAssembler.createModule() }
        return filtersVC
    }
    
    // MARK: - Positions List Logics
    
    private func createPositionsVC() -> PositionsListViewController {
        if positionsVC == nil {
            let positionsVC = PositionsListAssembler.createModule(positionsList)
            positionsVC.applyButtonClicked = { [weak self] positions in
                guard let self = self else { return }
                let stuffFilterVC = self.createFiltrationVC()
                if positions.isEmpty {
                    stuffFilterVC.positionLabel.text = "StuffFiltrationScreen.choosePosition".localized
                    self.filtersVC?.choosePositionButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
                    self.filtersVC?.positionLabel.textColor = AppTheme.Colors.darkGray
                    self.filtersVC?.discardFiltersOutlet.tintColor = AppTheme.Colors.darkGray
                } else {
                    self.positionsForFiltering = positions
                    let positionsNamesText = positions.map { $0.codename!}.joined(separator: ", ")
                    stuffFilterVC.positionLabel.text = positionsNamesText
                    self.filtersVC?.choosePositionButtonOutlet.setImage(UIImage(named: "positionOrProjectChosen"), for: .normal)
                    self.filtersVC?.positionLabel.textColor = AppTheme.Colors.black100
                    self.filtersVC?.discardFiltersOutlet.tintColor = AppTheme.Colors.blue1
                }
                self.fpc?.set(contentViewController: stuffFilterVC)
                self.fpc?.move(to: .tip, animated: true, completion: nil)
            }
            positionsVC.cancelButtonClickedCallBack = { [weak self] in
                guard let self = self else { return }
                let stuffFilterVC = self.createFiltrationVC()
                stuffFilterVC.positionLabel.text = "StuffFiltrationScreen.choosePosition".localized
                self.filtersVC?.choosePositionButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
                self.filtersVC?.positionLabel.textColor = AppTheme.Colors.darkGray
                self.filtersVC?.discardFiltersOutlet.tintColor = AppTheme.Colors.darkGray
                self.fpc?.set(contentViewController: stuffFilterVC)
                self.fpc?.move(to: .tip, animated: true)
            }
            clearSelectionForPositionsVC = { [weak self] in
                guard let self = self else { return }
                self.positionsVC?.clearSelection()
            }
            self.positionsVC = positionsVC
        }
        guard let positionsVC = positionsVC else { return PositionsListAssembler.createModule(positionsList) }
        return positionsVC
    }
    
    // MARK: - Project List Logics
    
    private func createProjectsVC() -> ProjectsListViewController {
        if projectsVC == nil {
            let projectsVC = ProjectsListAssembler.createModule(projectsList)
            projectsVC.applyButtonClicked = { [weak self] projects in
                guard let self = self else { return }
                let stuffFilterVC = self.createFiltrationVC()
                if projects.isEmpty {
                    stuffFilterVC.projectLabel.text = "StuffFiltrationScreen.chooseProject".localized
                    self.filtersVC?.chooseProjectButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
                    self.filtersVC?.projectLabel.textColor = AppTheme.Colors.darkGray
                    self.filtersVC?.discardFiltersOutlet.tintColor = AppTheme.Colors.darkGray
                } else {
                    self.projectsForFiltering = projects
                    let projectsNamesText = projects.map { $0.title ?? "" }.joined(separator: ", ")
                    stuffFilterVC.projectLabel.text = projectsNamesText
                    self.filtersVC?.chooseProjectButtonOutlet.setImage(UIImage(named: "positionOrProjectChosen"), for: .normal)
                    self.filtersVC?.projectLabel.textColor = AppTheme.Colors.black100
                    self.filtersVC?.discardFiltersOutlet.tintColor = AppTheme.Colors.blue1
                }
                self.fpc?.set(contentViewController: stuffFilterVC)
                self.dismiss(animated: true, completion: nil)
            }
            projectsVC.onCloseTapped = { [weak self] in
                guard let self = self else { return }
                let stuffFilterVC = self.createFiltrationVC()
                stuffFilterVC.projectLabel.text = "StuffFiltrationScreen.chooseProject".localized
                self.filtersVC?.chooseProjectButtonOutlet.setImage(UIImage(named: "choosePositionOrProject"), for: .normal)
                self.filtersVC?.projectLabel.textColor = AppTheme.Colors.darkGray
                self.filtersVC?.discardFiltersOutlet.tintColor = AppTheme.Colors.darkGray
                self.fpc?.set(contentViewController: stuffFilterVC)
            }
            clearSelectionForProjectsVC = { [weak self] in
                guard let self = self else { return }
                self.projectsVC?.clearSelection()
            }
            self.projectsVC = projectsVC
        }
        guard let projectsVC = projectsVC else { return ProjectsListAssembler.createModule(projectsList)}
        return projectsVC
    }
    
    func setupFiltrationFloatingPannel() {
        if fpc == nil {
            fpc = FloatingPanelController()
            guard let fpc = fpc else { return }
            fpc.delegate = self
            fpc.layout = ChooseFiltersFloatingPanelLayout()
            let filtersVC = createFiltrationVC()
            fpc.set(contentViewController: UINavigationController(rootViewController: filtersVC))
            fpc.addPanel(toParent: self, at: -1, animated: true, completion: nil)
            let appearance = SurfaceAppearance()
            appearance.cornerRadius = 16
            fpc.surfaceView.appearance = appearance
            fpc.surfaceView.grabberHandleSize = .init(width: 41.0, height: 4.0)
            fpc.surfaceView.backgroundColor = AppTheme.Colors.gray100
            fpc.surfaceView.grabberHandle.isHidden = true
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(closeFloatingPanel))
            fpc.backdropView.addGestureRecognizer(recognizer)
            fpc.panGestureRecognizer.isEnabled = false
            self.fpc = fpc
        } else {
            fpc?.move(to: .tip, animated: true)
        }
    }
    
    @objc private func closeFloatingPanel() {
        fpc?.move(to: .hidden, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func floatingPanelDidChangeState(_ fpc: FloatingPanelController) {
        if fpc.contentViewController as? PositionsListViewController != nil {
            switch fpc.state {
            case .hidden:
                self.clearSelectionForPositionsVC?()
                self.positionsForFiltering.removeAll()
                
                fpc.set(contentViewController: filtersVC)
            default:
                return
            }
        }
    }
    
}

class ChooseFiltersFloatingPanelLayout: FloatingPanelLayout {
    
    let heightForPositionsFilter = UIScreen.main.bounds.size.height * 0.75
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return [
            .tip: FloatingPanelLayoutAnchor(absoluteInset: 250.0, edge: .bottom, referenceGuide: .safeArea),
            .full: FloatingPanelLayoutAnchor(absoluteInset: heightForPositionsFilter, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
    func backdropAlpha(for state: FloatingPanelState) -> CGFloat {
        switch state {
        case .full, .tip: return 0.3
        default: return 0.0
        }
    }
    
}

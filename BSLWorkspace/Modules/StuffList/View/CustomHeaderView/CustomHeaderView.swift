//
//  CustomHeaderView.swift
//  BSLWorkspace
//
//  Created by Nikita on 17.12.2021.
//

import UIKit

class CustomHeaderView: XibBasedView {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var sectionHeaderName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setTitleForHeader(section: Int, _ name: String) {
        if section == 0 {
            backgroundView.backgroundColor = AppTheme.Colors.lightGray100
        } else {
            backgroundView.backgroundColor = AppTheme.Colors.white100
        }
        sectionHeaderName.text = name
    }
    
    override func setupUI() {
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        sectionHeaderName.font = AppTheme.Fonts.SFMedium(14)
        sectionHeaderName.textColor = AppTheme.Colors.black100
    }
}

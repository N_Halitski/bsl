//
//  EmployeeCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit
import Kingfisher

protocol EmployeeCellProtocol {
    func display(_ employee: Employee)
    func hideSeparator()
}

final class EmployeeCell: UITableViewCell, EmployeeCellProtocol {
    
    // MARK: - UIElements outlets
    
    @IBOutlet weak var employeeImage: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpLayoutForCell()
    }
    
    func display(_ employee: Employee) {
        self.firstNameLabel.text = employee.name
        self.positionLabel.text = employee.position?.codename
        
        if let employeePhoto = employee.avatar {
            let avatar = URL(string: employeePhoto)
            let placeholder = UIImage(named: "unselectedCell")
           employeeImage.kf.setImage(with: avatar, placeholder: placeholder)
            
        } else {
            employeeImage.image = UIImage(named: "person")
        }
    }
    
    func hideSeparator() {
        separatorView.isHidden = true
    }
}

extension EmployeeCell {
    private func setUpLayoutForCell() {
        separatorView.backgroundColor = AppTheme.Colors.gray100
        firstNameLabel.font = AppTheme.Fonts.SFMedium(16)
        firstNameLabel.textColor = AppTheme.Colors.black100
        positionLabel.font = AppTheme.Fonts.SFRegular(13)
        positionLabel.textColor = AppTheme.Colors.darkGray
        employeeImage.layer.cornerRadius = 8
        selectionStyle = .none
    }
}

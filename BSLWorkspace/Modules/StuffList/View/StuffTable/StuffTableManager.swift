//
//  StuffTableManager.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol StuffTableManagerProtocol {
    var didSelectEmployee: ((Employee) -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func filterData(by seachText: String)
    func showEmployees(employees: [Employee], positions: [Position], projects: [ProjectsRegistryResponseModel])
    func filterByOptions(_ positions: [Position])
    func clearFilters()
}

final class StuffTableManager: NSObject, StuffTableManagerProtocol {
    
    private var table: UITableView?
    private var employees: [Employee] = []
    private var positions: [Position] = []
    private var projects: [ProjectsRegistryResponseModel] = []
    private var initialDataSource: [[StuffTableConfigurator]] = []
    private var filteredDataSource: [[StuffTableConfigurator]] = []
    private var dataSourceWithFilters: [[StuffTableConfigurator]] = []
    
    var didSelectEmployee: ((Employee) -> Void)?
    
    func attachTable(_ table: UITableView) {
        self.table = table
        table.delegate = self
        table.dataSource = self
        table.estimatedSectionHeaderHeight = 0
        table.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 14, right: 0)
        let employeeNib = UINib(nibName: String(describing: EmployeeCell.self), bundle: nil)
        self.table?.register(employeeNib, forCellReuseIdentifier: String(describing: EmployeeCell.self))
    }
    
    func showEmployees(employees: [Employee], positions: [Position], projects: [ProjectsRegistryResponseModel]) {
        self.positions = positions
        self.employees = employees
        self.projects = projects
        
        let filteredEmployees = filterEmployeesByPosition(employees: employees, positions: positions)
        
        var oneSectionOutput: [StuffTableConfigurator] = []
        var outputOfSections: [[StuffTableConfigurator]] = []
        
        filteredEmployees.forEach { oneListPositions in
            oneSectionOutput = []
            oneListPositions.forEach { employee in
                oneSectionOutput.append(createUserConfigurator(employee))
            }
            outputOfSections.append(oneSectionOutput)
        }
        
        self.initialDataSource = outputOfSections
        self.filteredDataSource = self.initialDataSource
        
        table?.reloadData()
    }
    
    private func filterEmployeesByPosition(employees: [Employee], positions: [Position]) -> [[Employee]] {
        var sectionsWithEmployees: [[Employee]] = []
        positions.forEach { position in
            var positionsSections: [Employee] = []
            positionsSections = employees.filter { $0.position?.codename == position.codename }
            sectionsWithEmployees.append(positionsSections)
        }
        return sectionsWithEmployees
    }
    
    func filterByOptions(_ positions: [Position]) {
        
        var dataSourceWithFilters: [[StuffTableConfigurator]] = []
        
        for position in positions {
            for configuratorsArray in initialDataSource {
                if configuratorsArray.first?.employeeModel?.position?.codename == position.codename {
                    dataSourceWithFilters.append(configuratorsArray)
                    break
                } else {
                    continue
                }
            }
        }
        self.dataSourceWithFilters = dataSourceWithFilters
        self.filteredDataSource = dataSourceWithFilters
        self.table?.reloadData()
    }
    
    func clearFilters() {
        self.filteredDataSource = self.initialDataSource
        table?.reloadData()
    }
    
}

extension StuffTableManager: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = CustomHeaderView()
        let headerName = filteredDataSource[section].first?.employeeModel?.position?.codename
        headerView.setTitleForHeader(section: section, headerName ?? "")
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredDataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = filteredDataSource[indexPath.section][indexPath.row]
        let lastConfigurator = filteredDataSource.last?.last
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        lastConfigurator?.hideSeparator()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let employee = filteredDataSource[indexPath.section][indexPath.row].employeeModel else { return }
           self.didSelectEmployee?(employee)
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if filteredDataSource[section].isEmpty {
            return 0
        }
        return 25
    }
    
}

extension StuffTableManager {
    
    private func createUserConfigurator(_ employee: Employee) -> StuffTableConfigurator {
        let configurator = EmployeeCellConfigurator()
        configurator.employeeModel = employee
        return configurator
    }
    
}

extension StuffTableManager {
    
    func filterData(by searchText: String) {
        guard let tableView = table else { return }
        
        if searchText.isEmpty {
            if self.filteredDataSource.count < self.initialDataSource.count {
                self.filteredDataSource = self.dataSourceWithFilters
            } else {
                self.filteredDataSource = initialDataSource
            }
        } else {
            if self.filteredDataSource.count < self.initialDataSource.count {
                var filteredDataSource: [[StuffTableConfigurator]] = []
                for oneSection in self.filteredDataSource {
                    let filteredSection = oneSection.filter { employee in
                        if (employee.employeeModel?.name?.lowercased().prefix(searchText.count))! == searchText.lowercased() {
                            return true
                        }
                        return false
                    }
                    filteredDataSource.append(filteredSection)
                }
                self.filteredDataSource = filteredDataSource
            } else {
                var filteredDataSource: [[StuffTableConfigurator]] = []
                for oneSection in initialDataSource {
                    let filteredSection = oneSection.filter { employee in
                        if (employee.employeeModel?.name?.lowercased().prefix(searchText.count))! == searchText.lowercased() {
                            return true
                        }
                        return false
                    }
                    filteredDataSource.append(filteredSection)
                }
                self.filteredDataSource = filteredDataSource
            }
        }
        tableView.reloadData()
    }
    
}

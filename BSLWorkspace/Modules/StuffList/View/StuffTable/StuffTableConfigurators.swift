//
//  StuffTableConfigurator.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol StuffTableConfigurator {
    var reuseId: String { get }
    var employeeModel: Employee? { get set }
    
    func setupCell(_ cell: UIView)
    func hideSeparator()
}

final class EmployeeCellConfigurator: StuffTableConfigurator {
    
    var reuseId: String { String(describing: EmployeeCell.self) }
    var cell: UIView?
    var employeeModel: Employee?
    
    func setupCell(_ cell: UIView) {
        self.cell = cell
        guard let cell = cell as? EmployeeCellProtocol,
        let employee = employeeModel else { return }
        cell.display(employee)
    }
    
    func hideSeparator() {
        guard let cell = cell as? EmployeeCellProtocol else { return }
        cell.hideSeparator()
    }
}

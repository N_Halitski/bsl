//
//  StuffListStuffListAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class StuffListAssembler {
    static func createModule(
        employeeList: [Employee] = [],
        positionsList: [Position] = [],
        projectsList: [ProjectsRegistryResponseModel] = [],
        tableManager: StuffTableManagerProtocol = StuffTableManager(),
        type: StuffListOpenType) -> StuffListViewController {
            let viewController = StuffListViewController()
            let interactor = StuffListInteractor(tableManager: tableManager, employeeList: employeeList, positionsList: positionsList, projectsList: projectsList)
            let presenter = StuffListPresenter(interactor, viewController, openType: type)
            viewController.presenter = presenter
            return viewController
        }
}

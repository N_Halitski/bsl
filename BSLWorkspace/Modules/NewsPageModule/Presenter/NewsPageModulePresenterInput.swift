//
//  NewsPageModuleNewsPageModulePresenterInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 05/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol NewsPageModulePresenterInput: BasePresenting {}

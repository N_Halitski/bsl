//
//  NewsPageModuleNewsPageModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 05/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class NewsPageModulePresenter {

    private let interactor: NewsPageModuleInteractorInput
    private unowned let view: NewsPageModulePresenterOutput

    init(_ interactor: NewsPageModuleInteractorInput, _ view: NewsPageModulePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension NewsPageModulePresenter: NewsPageModulePresenterInput {}

extension NewsPageModulePresenter: NewsPageModuleInteractorOutput {
    func transferNewsData(newsList: NewsList) {
        view.transferNewsData(newsList)
    }
}

//
//  NewsPageModuleNewsPageModulePresenterOutput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 05/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol NewsPageModulePresenterOutput: AnyObject {
    func transferNewsData(_ newsList: NewsList)
}

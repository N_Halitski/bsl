//
//  NewsPageModuleNewsPageModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 05/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class NewsPageModuleAssembler {
    static func createModule(newsList: NewsList) -> NewsPageModuleViewController {
        let viewController = NewsPageModuleViewController()
        let interactor = NewsPageModuleInteractor(newsList: newsList)
        let presenter = NewsPageModulePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

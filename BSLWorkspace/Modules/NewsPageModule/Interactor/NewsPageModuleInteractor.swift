//
//  NewsPageModuleNewsPageModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 05/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation
import Kingfisher

final class NewsPageModuleInteractor {
    private weak var output: NewsPageModuleInteractorOutput?
    private let newsList: NewsList
    
    init(newsList: NewsList) {
        self.newsList = newsList
    }
}

extension NewsPageModuleInteractor: NewsPageModuleInteractorInput {
    func attach(_ output: NewsPageModuleInteractorOutput) {
        self.output = output
        self.output?.transferNewsData(newsList: self.newsList)
    }
}

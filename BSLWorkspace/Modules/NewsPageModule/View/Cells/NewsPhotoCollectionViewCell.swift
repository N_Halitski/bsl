//
//  NewsPhotoCollectionViewCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 5.12.21.
//

import UIKit

class NewsPhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var blackoutView: UIView!
    @IBOutlet weak var label: UILabel!
    
//    if there are more than three photos, then we make the third cell with a hint
    
    func setupCell(withImage image: UIImage, imageNumber: Int, numberOfImages: Int) {

        if imageNumber == 2 && numberOfImages > 3 {
            self.imageView.image = image
            self.blackoutView.isHidden = false
            self.label.isHidden = false
            self.label.text = "+\(numberOfImages - 3)"
        } else {
            self.imageView.image = image
            self.blackoutView.isHidden = true
            self.label.isHidden = true
        }
    }
}

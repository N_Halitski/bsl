//
//  NewsPageModuleNewsPageModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 05/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit
import Kingfisher

final class NewsPageModuleViewController: BaseViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var photosView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var firstPartOfTextLabel: UILabel!
    @IBOutlet weak var secondPartOfTextLabel: UILabel!
    
    var numberOfImages: Int = 4
    var presenter: NewsPageModulePresenterInput!
    let images: [UIImage?] = [
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed"),
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed"),
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed"),
        UIImage(named: "Rectangle 171") ?? UIImage(systemName: "book.closed")
    ]
    var newsList: NewsList?
    
    var openNewsPhotosScreen: (([String]?, Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
        setupCollectionView()
    }
    
    func createLayout(numberOfImages: Int) -> UICollectionViewCompositionalLayout {
        switch numberOfImages {
        case 1:
            return createLayoutWithOneImage()
        case 2:
            return createLayoutWithTwoImages()
        default:
            return createLayoutWithThreeImages()
        }
    }
}

extension NewsPageModuleViewController: NewsPageModulePresenterOutput {
    func transferNewsData(_ newsList: NewsList) {
        self.newsList = newsList
    }
}

extension NewsPageModuleViewController {
    func setupUI() {
        guard let newsList = self.newsList else { return }
        self.title = "#новый сотрудник"
        if let authorAvatar = newsList.authorAvatar {
            if let authorAvatarUrl = URL(string: authorAvatar) {
                avatarImageView.kf.setImage(with: authorAvatarUrl)
            }
        }
        self.titleLabel.text = newsList.title
        self.nameLabel.text = newsList.authorName
        self.timeLabel.text = DateFormatterManager.shared.hourIntervalCalculation(dateOfPlacement: newsList.createdAt)
        
        var firstPartOfText = ""
        var secondPartOfText = ""
        if let newsText = newsList.text {
            let text = newsText.splitStringInHalf()
            firstPartOfText = text.firstHalf
            secondPartOfText = text.secondHalf
        }
        self.separatorView.layer.cornerRadius = self.separatorView.frame.size.width / 2
        self.firstPartOfTextLabel.attributedText = firstPartOfText.setupLineSpacing(lineSpacing: 1.13)
        self.secondPartOfTextLabel.attributedText = secondPartOfText.setupLineSpacing(lineSpacing: 1.13)
        self.photosView.addShadow(shadowColor: AppTheme.Colors.lightGray7.cgColor, shadowOffset: CGSize(width: 0, height: -3), shadowOpacity: 1, shadowRadius: 10)
        navigationController?.navigationBar.tintColor = AppTheme.Colors.black100
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share"), style: .plain, target: self, action: #selector(shareButtonPressed))
    }
    
    func setupCollectionView() {
        self.collectionView.register(UINib(nibName: String(describing: NewsPhotoCollectionViewCell.self), bundle: nil),
                                     forCellWithReuseIdentifier: String(describing: NewsPhotoCollectionViewCell.self))
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.collectionViewLayout = createLayout(numberOfImages: self.numberOfImages)
    }
    
    @objc func shareButtonPressed() {
        let shareController = UIActivityViewController(activityItems: [1], applicationActivities: nil)
        present(shareController, animated: true, completion: nil)
    }
}

extension NewsPageModuleViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.numberOfImages {
        case 1:
            return 1
        case 2:
            return 2
        default:
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: NewsPhotoCollectionViewCell.self),
            for: indexPath) as? NewsPhotoCollectionViewCell else { return UICollectionViewCell() }
        guard let image = self.images[indexPath.item] else { return cell }
        cell.setupCell(withImage: image, imageNumber: indexPath.item, numberOfImages: self.numberOfImages)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let newsAttachments = newsList?.newsAttachment else { return }
        let displayedPhotoNumber = indexPath.item
        openNewsPhotosScreen?(newsAttachments, displayedPhotoNumber)
    }
    
    private func createLayoutWithOneImage() -> UICollectionViewCompositionalLayout {
        // Item
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            )
        )
        // Group
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            ),
            subitem: item,
            count: 1
        )
        // Sections
        let section = NSCollectionLayoutSection(group: group)
        // Return
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    private func createLayoutWithTwoImages() -> UICollectionViewCompositionalLayout {
        // Item
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            )
        )
        // Group
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            ),
            subitem: item,
            count: 2
        )
        group.interItemSpacing = .fixed(3)
        // Sections
        let section = NSCollectionLayoutSection(group: group)
        // Return
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    private func createLayoutWithThreeImages() -> UICollectionViewCompositionalLayout {
        // Item
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(2/3),
                heightDimension: .fractionalHeight(1)
            )
        )
        item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 3)
        let verticalStackItem = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1/2)
            )
        )
        verticalStackItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
        let verticalStackGroup = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1/3),
                heightDimension: .fractionalHeight(1)
            ),
            subitem: verticalStackItem,
            count: 2
        )
        verticalStackGroup.interItemSpacing = .fixed(3)
        // Group
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            ),
            subitems: [
                item,
                verticalStackGroup
            ]
        )
        //        threeGroup.interItemSpacing = .fixed(3)
        // Sections
        let section = NSCollectionLayoutSection(group: group)
        // Return
        return UICollectionViewCompositionalLayout(section: section)
    }
}

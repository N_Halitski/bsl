//
//  ProjectsListProjectsListViewController.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class ProjectsListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    
    var onCloseTapped: (() -> Void)?
    var applyButtonClicked: (([ProjectsRegistryResponseModel]) -> Void)?
    var projects: [ProjectsRegistryResponseModel] = []
    
    var presenter: ProjectsListPresenterInput!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        setupUI()
    }
    
    @IBAction func closeFloatingPanel(_ sender: Any) {
        projects = []
        presenter.clearProjects(projects)
        onCloseTapped?()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyButtonClicked(_ sender: Any) {
        applyButtonClicked?(projects)
    }
    
    private func setupUI() {
        projectLabel.font = AppTheme.Fonts.SFSemiBold(16)
    }
}

extension ProjectsListViewController: ProjectsListPresenterOutput {
    func showProjects(_ projects: [ProjectsRegistryResponseModel]) {
        self.projects = projects
    }
}

extension ProjectsListViewController {
    func clearSelection() {
        projects = []
        presenter.clearProjects(projects)
    }
}

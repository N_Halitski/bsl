//
//  ProjectCell.swift
//  BSLWorkspace
//
//  Created by Nikita on 01.12.2021.
//

import UIKit
import Kingfisher

protocol ProjectCellProtocol: AnyObject {
    var projectSelected: ((ProjectsRegistryResponseModel) -> Void?)? { get set }
    var projectDeselected: ((ProjectsRegistryResponseModel) -> Void?)? { get set }
    var informConfigurator: ((Bool) -> Void?)? { get set }
    
    func display(_ projectModel: ProjectsRegistryResponseModel)
    func toggleCellToTrue()
    func toggleCellToFalse()
}

final class ProjectCell: UITableViewCell, ProjectCellProtocol {
    
    @IBOutlet private var projectNameLabel: UILabel!
    @IBOutlet private var projectTypeLabel: UILabel!
    @IBOutlet private var separatorView: UIView!
    @IBOutlet private var projectsIcon: UIImageView!
    @IBOutlet private var selectButton: UIButton!
    
    var projectSelected: ((ProjectsRegistryResponseModel) -> Void?)?
    var projectDeselected: ((ProjectsRegistryResponseModel) -> Void?)?
    var informConfigurator: ((Bool) -> Void?)?
    
    var projectModel: ProjectsRegistryResponseModel?
    
    var cellStatus: Bool = false {
        didSet {
            informConfigurator?(cellStatus)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    func display(_ projectModel: ProjectsRegistryResponseModel) {
        self.projectModel = projectModel
        projectNameLabel.text = projectModel.title
        if let projectIcon = projectModel.imageURL {
            let icon = URL(string: projectIcon)
            projectsIcon.kf.setImage(with: icon)
        } else {
            projectsIcon.image = UIImage(named: "BSL_Logo")
        }
        
    }
    
    @IBAction private func selectButtonClicked(_ sender: Any) {
        if selectButton.isSelected {
            selectButton.isSelected = false
            cellStatus = false
            guard let projectModel = projectModel else { return }
            projectDeselected?(projectModel)
        } else {
            selectButton.isSelected = true
            cellStatus = true
            guard let projectModel = projectModel else { return }
            projectSelected?(projectModel)
        }
    }
    
    func toggleCheckmarkToFalse() {
        if selectButton.isSelected {
            cellStatus = false
            selectButton.isSelected = false
        }
    }
    
    func checkboxSelectedOnTap() {
        if !selectButton.isSelected {
            cellStatus = true
            selectButton.isSelected = true
        }
    }
    
    func checkboxDeselectedOnTap() {
        if selectButton.isSelected {
            cellStatus = false
            selectButton.isSelected = false
        }
    }
    
    func toggleCellToTrue() {
        selectButton.isSelected = true
    }
    
    func toggleCellToFalse() {
        selectButton.isSelected = false
    }
    
}

extension ProjectCell {
    
    private func setupUI() {
        
        selectButton.setTitle("", for: .normal)
        selectButton.setTitle("", for: .selected)
        selectButton.setImage(UIImage(named: "unselectedCell"), for: .normal)
        selectButton.setImage(UIImage(named: "selectedCell"), for: .selected)
        selectButton.tintColor = .clear
        
        projectNameLabel.font = AppTheme.Fonts.SFMedium(16)
        projectNameLabel.textColor = AppTheme.Colors.black100
        
        projectTypeLabel.font = AppTheme.Fonts.SFRegular(13)
        projectTypeLabel.textColor = AppTheme.Colors.darkGray
        
        separatorView.backgroundColor = AppTheme.Colors.gray100
        
        selectionStyle = .none
    }
    
}

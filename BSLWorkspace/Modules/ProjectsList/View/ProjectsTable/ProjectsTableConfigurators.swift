//
//  ProjectsTableConfigurators.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol ProjectsTableConfigurator {
    var reuseId: String { get }
    var projectModel: ProjectsRegistryResponseModel? { get set }
    var cell: ProjectCellProtocol? { get }
    var cellStatus: Bool { get set }
    
    func setupCell(_ cell: UIView)
    func toggleForDeselection()
    func checkboxSelectedOnTap()
    func checkboxDeselectedOnTap()
    func toggleToTrue()
    func toggleToFalse()
}

final class ProjectCellConfigurator: ProjectsTableConfigurator {
    
    var reuseId: String = String(describing: ProjectCell.self)
    
    var projectModel: ProjectsRegistryResponseModel?
    var cellStatus: Bool = false
    
    var getProject: ((ProjectsRegistryResponseModel) -> Void)?
    var getDeselectedProject: ((ProjectsRegistryResponseModel) -> Void)?
    
    var cell: ProjectCellProtocol?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? ProjectCellProtocol,
              let projectModel = projectModel else { return }
        self.cell = cell
        cell.display(projectModel)
        cell.projectSelected = { [weak self] projectModel in
            self?.getProject?(projectModel)
        }
        cell.projectDeselected = { [weak self] projectModel in
            self?.getDeselectedProject?(projectModel)
        }
        
        cell.informConfigurator = { [weak self] cellStatus in
            self?.cellStatus = cellStatus
        }
    }
    
    func toggleForDeselection() {
        if let cell = self.cell as? ProjectCell {
            cell.toggleCheckmarkToFalse()
        }
    }
    
    func checkboxSelectedOnTap() {
        if let cell = self.cell as? ProjectCell {
            cell.checkboxSelectedOnTap()
        }
    }
    
    func checkboxDeselectedOnTap() {
        if let cell = self.cell as? ProjectCell {
            cell.checkboxDeselectedOnTap()
        }
    }
    
    func toggleToTrue() {
        if let cell = self.cell as? ProjectCell {
            cell.toggleCellToTrue()
        }
    }
    func toggleToFalse() {
        if let cell = self.cell as? ProjectCell {
            cell.toggleCellToFalse()
        }
    }
}

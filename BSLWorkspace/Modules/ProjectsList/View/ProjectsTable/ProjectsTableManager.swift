//
//  ProjectsTableManager.swift
//  BSLWorkspace
//
//  Created by Nikita on 06.12.2021.
//

import UIKit

protocol ProjectsTableManagerProtocol {
    var showProjectsOnFloatingPanel: (([ProjectsRegistryResponseModel]) -> Void)? { get set }
    
    func attachTable(_ tableView: UITableView)
    func showProjects(_ projects: [ProjectsRegistryResponseModel])
    func clearProjects(_ projects: [ProjectsRegistryResponseModel])
}

final class ProjectsTableManager: NSObject, ProjectsTableManagerProtocol {
    
    private weak var tableView: UITableView?
    
    var showProjectsOnFloatingPanel: (([ProjectsRegistryResponseModel]) -> Void)?
    
    private var configurators: [ProjectsTableConfigurator] = []
    
    private var selectedProjects: [ProjectsTableConfigurator] = []
    
    private var projects: [ProjectsRegistryResponseModel] = [] {
        didSet {
            showProjectsOnFloatingPanel?(projects)
        }
    }
    
    func attachTable(_ tableView: UITableView) {
        self.tableView = tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        
        let projectCellNib = UINib(nibName: String(describing: ProjectCell.self), bundle: nil)
        self.tableView?.register(projectCellNib, forCellReuseIdentifier: String(describing: ProjectCell.self))
    }
    
    func showProjects(_ projects: [ProjectsRegistryResponseModel]) {
        var output: [ProjectsTableConfigurator] = []
        projects.forEach { project in
            output.append(createProjectConfigurator(project))
        }
        self.configurators = output
        tableView?.reloadData()
    }
    
    private func createProjectConfigurator(_ projectModel: ProjectsRegistryResponseModel) -> ProjectsTableConfigurator {
        let configurator = ProjectCellConfigurator()
        configurator.projectModel = projectModel
        
        configurator.getProject = { [weak self] projectModel in
            self?.projects.append(projectModel)
        }
        configurator.getDeselectedProject = { [weak self] projectModel in
            self?.projects.removeAll(where: { projectToDelete in
                projectToDelete.title == projectModel.title
            })
        }
        return configurator
    }
    
    func clearProjects(_ projects: [ProjectsRegistryResponseModel]) {
        self.projects = projects
        for configurator in configurators {
            configurator.toggleForDeselection()
        }
    }
}

extension ProjectsTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        
        if configurator.cellStatus {
            configurator.toggleToTrue()
        } else {
            configurator.toggleToFalse()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let configurator = configurators[indexPath.row]
        let project = configurator.projectModel
        guard let project = project else { return }
        
        if projects.isEmpty {
            projects.append(project)
            configurator.checkboxSelectedOnTap()
        } else {
            for (index, repeatedProject) in projects.enumerated() where repeatedProject.title == project.title {
                projects.remove(at: index)
                configurator.checkboxDeselectedOnTap()
                return
            }
            projects.append(project)
            configurator.checkboxSelectedOnTap()
        }
    }
}

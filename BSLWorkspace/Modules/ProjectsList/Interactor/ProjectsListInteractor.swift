//
//  ProjectsListProjectsListInteractor.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class ProjectsListInteractor {
    
    private weak var output: ProjectsListInteractorOutput?
    
    private var tableManager: ProjectsTableManagerProtocol
    private var projectsList: [ProjectsRegistryResponseModel]
    
    init(tableManager: ProjectsTableManagerProtocol, projectsList: [ProjectsRegistryResponseModel]) {
        self.tableManager = tableManager
        self.projectsList = projectsList
        self.tableManager.showProjects(projectsList)
    }
}

extension ProjectsListInteractor: ProjectsListInteractorInput {
    func clearProjects(_ projects: [ProjectsRegistryResponseModel]) {
        tableManager.clearProjects(projects)
    }
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
        tableManager.showProjectsOnFloatingPanel = { [weak output] projects in
            output?.showProjects(projects)
        }
    }
    
    func attach(_ output: ProjectsListInteractorOutput) {
        self.output = output
    }
}

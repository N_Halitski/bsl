//
//  ProjectsListProjectsListInteractorInput.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol ProjectsListInteractorInput: AnyObject {
    func attach(_ output: ProjectsListInteractorOutput)
    func attachTableView(_ table: UITableView)
    func clearProjects(_ projects: [ProjectsRegistryResponseModel])
}

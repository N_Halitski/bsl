//
//  ProjectsListProjectsListAssembler.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class ProjectsListAssembler {
    static func createModule(
        _ projectsList: [ProjectsRegistryResponseModel],
        _ tableManager: ProjectsTableManagerProtocol = ProjectsTableManager()) -> ProjectsListViewController {
        let viewController = ProjectsListViewController()
        let interactor = ProjectsListInteractor(tableManager: tableManager, projectsList: projectsList)
        let presenter = ProjectsListPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

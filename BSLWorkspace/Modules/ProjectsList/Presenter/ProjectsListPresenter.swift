//
//  ProjectsListProjectsListPresenter.swift
//  BSLWorkspace
//
//  Created by Nikita Halitsky on 06/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class ProjectsListPresenter {

    private let interactor: ProjectsListInteractorInput
    private unowned let view: ProjectsListPresenterOutput

    init(_ interactor: ProjectsListInteractorInput, _ view: ProjectsListPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension ProjectsListPresenter: ProjectsListPresenterInput {
    
    func clearProjects(_ projects: [ProjectsRegistryResponseModel]) {
        interactor.clearProjects(projects)
    }
    
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }
}

extension ProjectsListPresenter: ProjectsListInteractorOutput {
    
    func showProjects(_ projetcs: [ProjectsRegistryResponseModel]) {
        view.showProjects(projetcs)
    }
}

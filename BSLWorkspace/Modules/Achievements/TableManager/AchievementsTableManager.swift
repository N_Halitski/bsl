//
//  AchievementsTableManager.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.01.22.
//

import UIKit

protocol AchievementsTableManagerProtocol {
    var didTapSelectEmployee: (() -> Void)? { get set }
    
    func attachTable(_ table: UITableView)
    func setupInitialState(with responseModel: [AchievementsModel])
    func presentChooseEmployeeCell(with selectedEmployee: Employee?)
}

final class AchievementsTableManager: NSObject, AchievementsTableManagerProtocol {
    
    private weak var tableView: UITableView?
    private var configurators: [BaseConfigurator] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    private var observeAwardTitleText: String = ""
    private var observeAwardDescriptionText: String = ""
    
    var selectedEmployee: Employee?
    
    var didTapSelectEmployee: (() -> Void)?
    
    func attachTable(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView = tableView
        configureTableView()
        registerCells()
    }
    
    func setupInitialState(with responseModel: [AchievementsModel]) {
        configurators.append(createChooseEmployeeCellConfigurator())
        configurators.append(createAwardNameCellConfigurator())
        configurators.append(createAwardDescriptionCellConfigurator())
        configurators.append(createAwardsListCellConfigurator(responseModel))
        configurators.append(createRewardButtonCellConfigurator())
    }
    
    func presentChooseEmployeeCell(with selectedEmployee: Employee?) {
        self.selectedEmployee = selectedEmployee
        var output: ChooseEmployeeCellConfiguratorProtocol?
        output = createEmployeeCellConfigurator(with: selectedEmployee)
        self.configurators.remove(at: 0)
        self.configurators.insert(output!, at: 0)
    }
    
    private func controleRewardButtonState() {
        for configurator in configurators {
            if let rewardButtonCellConfigurator = configurator as? RewardButtonCellConfigurator {
                if observeAwardTitleText .isNotEmpty && selectedEmployee != nil && observeAwardDescriptionText .isNotEmpty {
                    rewardButtonCellConfigurator.activateSendButton()
                } else {
                    rewardButtonCellConfigurator.deactivateSencButton()
                }
            }
        }
    }
    
    private func createEmployeeCellConfigurator(with model: Employee?) -> ChooseEmployeeCellConfigurator {
        let configurator = ChooseEmployeeCellConfigurator()
        configurator.setSelectedEmployee(employee: model)
        return configurator
    }

    private func createChooseEmployeeCellConfigurator() -> ChooseEmployeeCellConfigurator {
        let configurator = ChooseEmployeeCellConfigurator()
        return configurator
    }
    
    private func createAwardNameCellConfigurator() -> AwardNameCellConfigurator {
        let configurator = AwardNameCellConfigurator()
        configurator.didTextChanged = { [weak self] text in
            self?.tableView?.beginUpdates()
            self?.tableView?.endUpdates()
            self?.observeAwardTitleText = text
            self?.controleRewardButtonState()
        }
        return configurator
    }
    
    private func createAwardDescriptionCellConfigurator() -> AwardDescriptionCellConfigurator {
        let configurator = AwardDescriptionCellConfigurator()
        configurator.didTextChanged = { [weak self] text in
            self?.tableView?.beginUpdates()
            self?.tableView?.endUpdates()
            self?.observeAwardDescriptionText = text
            self?.controleRewardButtonState()
        }
        return configurator
    }
    
    private func createAwardsListCellConfigurator(_ model: [AchievementsModel]) -> AwardsListCellConfigurator {
        let configurator = AwardsListCellConfigurator()
        configurator.achievementsModel = model
        return configurator
    }
    
    private func createRewardButtonCellConfigurator() -> RewardButtonCellConfigurator {
        let configurator = RewardButtonCellConfigurator()
        return configurator
    }
    
}

extension AchievementsTableManager: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            didTapSelectEmployee?()
        }
    }
    
}

extension AchievementsTableManager: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        return cell
    }
    
}

private extension AchievementsTableManager {
    
    func configureTableView() {
        tableView?.backgroundColor = AppTheme.Colors.lightGray100
        tableView?.separatorStyle = .none
    }
    
    func registerCells() {
        tableView?.registerCell(ChooseEmployeeCell.self)
        tableView?.registerCell(AwardNameCell.self)
        tableView?.registerCell(AwardDescriptionCell.self)
        tableView?.registerCell(AwardsListCell.self)
        tableView?.registerCell(RewardButtonCell.self)
    }
    
}

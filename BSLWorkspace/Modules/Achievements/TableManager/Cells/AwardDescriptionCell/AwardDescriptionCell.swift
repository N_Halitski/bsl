//
//  AwardDescriptionCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.01.22.
//

import UIKit

protocol AwardDescriptionCellProtocol {
    var textChanged: ((String) -> Void)? { get set }
}

final class AwardDescriptionCell: UITableViewCell, AwardDescriptionCellProtocol {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleOfTextView: UILabel!
    @IBOutlet private weak var descriptionTextView: UITextView!
    
    var textChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
        descriptionTextView.delegate = self
    }
    
}

extension AwardDescriptionCell: UITextViewDelegate {
    
    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textChanged?(textView.text)
        let maxHeightOfTextView = UIScreen.main.bounds.height * 0.14
        textView.isScrollEnabled = textView.contentSize.height >= maxHeightOfTextView ? true : false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.text == "AchievementsFlow.awardDescription".localized {
            descriptionTextView.text = ""
            titleOfTextView.textColor = AppTheme.Colors.darkGray
        }
        descriptionTextView.textColor = AppTheme.Colors.black100
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if descriptionTextView.text == "" {
            descriptionTextView.text = "AchievementsFlow.awardDescription".localized
            descriptionTextView.textColor = AppTheme.Colors.darkGray
            descriptionTextView.font = AppTheme.Fonts.SFRegular(16)
            titleOfTextView.textColor = .clear
        }
    }
    
}

private extension AwardDescriptionCell {
    
    func setupUI() {
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
        
        descriptionTextView.text = "AchievementsFlow.awardDescription".localized
        descriptionTextView.textColor = AppTheme.Colors.darkGray
        descriptionTextView.font = AppTheme.Fonts.SFRegular(16)
        
        titleOfTextView.text = "AchievementsFlow.awardDescription".localized
        titleOfTextView.font = AppTheme.Fonts.SFRegular(12)
        titleOfTextView.textColor = .clear
        titleOfTextView.backgroundColor = AppTheme.Colors.white100
    }
    
}

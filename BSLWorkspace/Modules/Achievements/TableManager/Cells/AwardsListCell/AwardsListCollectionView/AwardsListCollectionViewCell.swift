//
//  AwardsListCollectionViewCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 25.01.22.
//

import UIKit
import Kingfisher

protocol AwardsListCollectionViewCellProtocol {
    func setupCell(with achievementsModel: AchievementsModel)
    func markSelectedItem()
    func markUnselectedItem()
}

final class AwardsListCollectionViewCell: UICollectionViewCell, AwardsListCollectionViewCellProtocol {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet private weak var awardImage: UIImageView!
    @IBOutlet private weak var awardTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupCell(with achievementsModel: AchievementsModel) {
        awardTitleLabel.text = achievementsModel.id
        
        let imageIcon = achievementsModel.imageUrl
        let url = URL(string: "\(APIConstants.baseURL)\(String(describing: imageIcon))")
        self.awardImage.kf.setImage(with: url)
    }
    
    func markSelectedItem() {
        containerView.backgroundColor = AppTheme.Colors.yellowStatus
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = AppTheme.Colors.yellowStatus2.cgColor
    }
    
    func markUnselectedItem() {
        containerView.backgroundColor = AppTheme.Colors.white100
        containerView.layer.borderWidth = 0
    }
    
}

private extension AwardsListCollectionViewCell {
    
    func setupUI() {
        awardTitleLabel.textColor = AppTheme.Colors.black100
        awardTitleLabel.font = AppTheme.Fonts.SFMedium(14)
        
        containerView.layer.cornerRadius = 16
        containerView.backgroundColor = AppTheme.Colors.white100
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
    }
    
}

//
//  AwardsListCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.01.22.
//

import UIKit

protocol AwardsListCellProtocol {
    func display(with achievementsModel: [AchievementsModel]?)
}

final class AwardsListCell: UITableViewCell, AwardsListCellProtocol {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var awardsListCollectionView: UICollectionView!
    
    var achievementsModel: [AchievementsModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
        setupCollectionView()
    }
    
    func setupCollectionView() {
        awardsListCollectionView.delegate = self
        awardsListCollectionView.dataSource = self
        awardsListCollectionView.registerCell(AwardsListCollectionViewCell.self)
    }
    
    func display(with achievementsModel: [AchievementsModel]?) {
        guard let achievementsModel = achievementsModel else { return }
        self.achievementsModel = achievementsModel
    }
    
}

private extension AwardsListCell {
    
    func setupUI() {
        titleLabel.text = "AchievementsFlow.awards".localized
        titleLabel.textColor = AppTheme.Colors.black100
        titleLabel.font = AppTheme.Fonts.SFSemiBold(20)
        
        awardsListCollectionView.backgroundColor = AppTheme.Colors.lightGray100
    }
    
}

extension AwardsListCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? AwardsListCollectionViewCell else { return }
        if cell.isSelected {
            cell.markSelectedItem()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? AwardsListCollectionViewCell else { return }
        if cell.isSelected == false {
            cell.markUnselectedItem()
        }
    }
    
}

extension AwardsListCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return achievementsModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let awardsCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: AwardsListCollectionViewCell.self),
            for: indexPath) as? AwardsListCollectionViewCell else { return UICollectionViewCell() }
        let achievementsModel = achievementsModel[indexPath.row]
        awardsCell.setupCell(with: achievementsModel)
        
        return awardsCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 116, height: 127)
    }
    
}

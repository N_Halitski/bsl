//
//  RewardButtonCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.01.22.
//

import UIKit

protocol RewardButtonCellProtocol {
    func activateSendButton()
    func deactivateSencButton()
}

final class RewardButtonCell: UITableViewCell, RewardButtonCellProtocol {
    
    @IBOutlet private weak var rewardButton: UIButton!
    @IBOutlet private weak var rewardButtonTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
    }
    
    func activateSendButton() {
        rewardButton.backgroundColor = AppTheme.Colors.darkBlue100
    }
    
    func deactivateSencButton() {
        rewardButton.backgroundColor = AppTheme.Colors.grayBlue100
    }
    
}

private extension RewardButtonCell {
    
    func setupUI() {
//TODO: костыль для того, чтобы кнопка отображалась чуть выше таббар на разных размерах экрана, пофиксить, если появится более красивое решение
        rewardButtonTopConstraint.constant = UIScreen.main.bounds.height - 630
        if UIScreen.main.bounds.height == 667 {
            rewardButtonTopConstraint.constant = UIScreen.main.bounds.height - 570
        }
        rewardButton.backgroundColor = AppTheme.Colors.grayBlue100
        rewardButton.layer.cornerRadius = 16
        rewardButton.setTitle("AchievementsFlow.reward".localized, for: .normal)
        rewardButton.setTitleColor(AppTheme.Colors.white100, for: .normal)
    }
    
}

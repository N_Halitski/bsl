//
//  AwardNameCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.01.22.
//

import UIKit

protocol AwardNameCellProtocol {
    var textChanged: ((String) -> Void)? { get set }
}

final class AwardNameCell: UITableViewCell, AwardNameCellProtocol {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleTextField: UITextField!
    
    var textChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
        titleTextField.addTarget(self, action: #selector(TitleOfPainCell.textFieldDidChange(_:)),
                                  for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        textChanged?(textField.text ?? "")
    }
    
    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
}

private extension AwardNameCell {
    
    func setupUI() {
        containerView.backgroundColor = AppTheme.Colors.white100
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
        
        titleTextField.placeholder = "AchievementsFlow.awardTitle".localized
        titleTextField.font = AppTheme.Fonts.SFRegular(16)
        titleTextField.textColor = AppTheme.Colors.black100
    }
    
}

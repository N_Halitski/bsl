//
//  ChooseEmployeeCell.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 24.01.22.
//

import UIKit

protocol ChooseEmployeeCellProtocol {
    func display()
    func setSelectedEmployee(employee: Employee?)
}

final class ChooseEmployeeCell: UITableViewCell, ChooseEmployeeCellProtocol {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleImage: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = AppTheme.Colors.lightGray100
        selectionStyle = .none
        setupUI()
    }
    
    func setSelectedEmployee(employee: Employee?) {
        titleLabel.text = employee?.name
        titleLabel.textColor = AppTheme.Colors.black100
    }
    
    func display() {
        titleLabel.text = "AchievementsFlow.chooseEmployee".localized
        titleLabel.textColor = AppTheme.Colors.darkGray
        titleLabel.font = AppTheme.Fonts.SFRegular(16)
        
        titleImage.image = UIImage(named: "positionOrProjectChosen")
    }
    
}

private extension ChooseEmployeeCell {
    
    func setupUI() {
        containerView.backgroundColor = AppTheme.Colors.white100
        containerView.layer.cornerRadius = 16
        containerView.addShadow(
            shadowColor: AppTheme.Colors.lightGray4.cgColor,
            shadowOffset: CGSize(width: 0, height: 4),
            shadowOpacity: 1, shadowRadius: 36)
    }
    
}

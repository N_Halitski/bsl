//
//  AwardNameCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 25.01.22.
//

import UIKit

protocol AwardNameCellConfiguratorProtocol: BaseConfigurator {
    var didTextChanged: ((String) -> Void)? { get set }
}

final class AwardNameCellConfigurator: AwardNameCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: AwardNameCell.self)
    
    var didTextChanged: ((String) -> Void)?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? AwardNameCell else { return }
        cell.textChanged = self.didTextChanged
    }
    
}

//
//  AwardDescriptionCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 25.01.22.
//

import UIKit

protocol AwardDescriptionCellConfiguratorProtocol: BaseConfigurator {
    var didTextChanged: ((String) -> Void)? { get set }
}

final class AwardDescriptionCellConfigurator: AwardNameCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: AwardDescriptionCell.self)
    
    var didTextChanged: ((String) -> Void)?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? AwardDescriptionCell else { return }
        cell.textChanged =  self.didTextChanged
    }
    
}

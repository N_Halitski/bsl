//
//  RewardButtonCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 25.01.22.
//

import UIKit

protocol RewardButtonCellConfiguratorProtocol: BaseConfigurator {
    var cell: RewardButtonCellProtocol? { get set }
    
    func activateSendButton()
    func deactivateSencButton()
}

final class RewardButtonCellConfigurator: RewardButtonCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: RewardButtonCell.self)
    var cell: RewardButtonCellProtocol?
    
    func setupCell(_ rewardButtonCell: UIView) {
        guard let rewardButtonCell = rewardButtonCell as? RewardButtonCellProtocol else { return }
        cell = rewardButtonCell
    }
    
    func activateSendButton() {
        cell?.activateSendButton()
    }
    
    func deactivateSencButton() {
        cell?.deactivateSencButton()
    }
    
}

//
//  ChooseEmployeeCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 25.01.22.
//

import UIKit

protocol ChooseEmployeeCellConfiguratorProtocol: BaseConfigurator {
    func setSelectedEmployee(employee: Employee?)
}

final class ChooseEmployeeCellConfigurator: ChooseEmployeeCellConfiguratorProtocol {
    
    var choosenEmployee: Employee?
    var reuseId: String = String(describing: ChooseEmployeeCell.self)
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? ChooseEmployeeCellProtocol else { return }
        cell.display()
        if choosenEmployee != nil {
            cell.setSelectedEmployee(employee: choosenEmployee)
        }
    }
    
    func setSelectedEmployee(employee: Employee?) {
        self.choosenEmployee = employee
    }
    
}

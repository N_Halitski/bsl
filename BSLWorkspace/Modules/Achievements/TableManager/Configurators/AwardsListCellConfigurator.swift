//
//  AwardsListCellConfigurator.swift
//  BSLWorkspace
//
//  Created by Karaimchuk on 25.01.22.
//

import UIKit

protocol AwardsListCellConfiguratorProtocol: BaseConfigurator {
    var achievementsModel: [AchievementsModel]? { get set }
    var cell: AwardsListCellProtocol? { get set }
}

final class AwardsListCellConfigurator: AwardsListCellConfiguratorProtocol {
    
    var reuseId: String = String(describing: AwardsListCell.self)
    var achievementsModel: [AchievementsModel]?
    var cell: AwardsListCellProtocol?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? AwardsListCellProtocol,
              let achievementsModel = achievementsModel else { return }
        self.cell = cell
        cell.display(with: achievementsModel)
    }
    
}

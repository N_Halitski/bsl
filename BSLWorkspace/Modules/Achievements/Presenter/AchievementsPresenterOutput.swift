//
//  AchievementsAchievementsPresenterOutput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol AchievementsPresenterOutput: AnyObject {
    func didTapSelectEmployee()
    func startLoader()
    func stopLoader()
}

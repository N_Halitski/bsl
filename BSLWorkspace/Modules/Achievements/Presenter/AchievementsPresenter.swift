//
//  AchievementsAchievementsPresenter.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class AchievementsPresenter {

    private let interactor: AchievementsInteractorInput
    private unowned let view: AchievementsPresenterOutput

    init(_ interactor: AchievementsInteractorInput, _ view: AchievementsPresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }

}

extension AchievementsPresenter: AchievementsPresenterInput {
    
    func presentSelectedEmployee(employee: Employee?) {
        interactor.presentSelectedEmployee(employee: employee)
    }
    
    func attachTableView(_ table: UITableView) {
        interactor.attachTableView(table)
    }

}

extension AchievementsPresenter: AchievementsInteractorOutput {
    
    func startLoader() {
        view.startLoader()
    }
    
    func stopLoader() {
        view.stopLoader()
    }
    
    func didTapSelectEmployee() {
        view.didTapSelectEmployee()
    }
    
}

//
//  AchievementsAchievementsInteractorInput.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

protocol AchievementsInteractorInput: AnyObject {
    func attach(_ output: AchievementsInteractorOutput)
    func attachTableView(_ table: UITableView)
    func presentSelectedEmployee(employee: Employee?)
}

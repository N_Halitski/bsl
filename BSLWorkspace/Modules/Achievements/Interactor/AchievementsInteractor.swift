//
//  AchievementsAchievementsInteractor.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class AchievementsInteractor {
    
    private weak var output: AchievementsInteractorOutput?
    private var tableManager: AchievementsTableManagerProtocol
    private var achievementsRepository: AchievementsRepositoryProtocol
    private var employeeList: [Employee]
    private var positionsList: [Position]
    private var projectsList: [ProjectsRegistryResponseModel]
    
    init(
        tableManager: AchievementsTableManagerProtocol,
        achievementsRepository: AchievementsRepositoryProtocol = AchievementsRepository(),
        employeeList: [Employee],
        positionsList: [Position],
        projectsList: [ProjectsRegistryResponseModel]) {
        self.tableManager = tableManager
        self.employeeList = employeeList
        self.positionsList = positionsList
        self.projectsList = projectsList
        self.achievementsRepository = achievementsRepository
        self.tableManager.didTapSelectEmployee = { [weak self] in
            self?.output?.didTapSelectEmployee()
        }
    }
    
}

extension AchievementsInteractor: AchievementsInteractorInput {
    
    func presentSelectedEmployee(employee: Employee?) {
        tableManager.presentChooseEmployeeCell(with: employee)
    }
    
    func attachTableView(_ table: UITableView) {
        tableManager.attachTable(table)
        output?.startLoader()
        achievementsRepository.getAwards { [weak self] result in
            switch result {
            case .success(let response):
                guard let responseModel = response else { return }
                self?.tableManager.setupInitialState(with: responseModel)
            case .failure:
                break
            }
            self?.output?.stopLoader()
        }
    }
    
    func attach(_ output: AchievementsInteractorOutput) {
        self.output = output
    }
    
}

//
//  AchievementsAchievementsViewController.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class AchievementsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: AchievementsPresenterInput!
    
    var openStuffListScreen: (() -> Void)?

	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        title = "AchievementsFlow.reward".localized + "🏆"
        view.backgroundColor = AppTheme.Colors.lightGray100
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.keyboardDismissMode = .onDrag
        hideKeyboardWhenTappedAround()
    }
    
    func presentSelectedEmployee(employee: Employee?) {
        presenter.presentSelectedEmployee(employee: employee)
    }

}

extension AchievementsViewController: AchievementsPresenterOutput {
    
    func didTapSelectEmployee() {
        openStuffListScreen?()
    }
    
    func startLoader() {
        showLoader(isShown: true)
    }
    
    func stopLoader() {
        showLoader(isShown: false)
    }
    
}

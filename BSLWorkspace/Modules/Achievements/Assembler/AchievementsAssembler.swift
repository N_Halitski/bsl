//
//  AchievementsAchievementsAssembler.swift
//  BSLWorkspace
//
//  Created by Dmitry_Karaimchuk on 24/01/2022.
//  Copyright © 2022 BSL. All rights reserved.
//

import UIKit

final class AchievementsAssembler {
    static func createModule(
        tableManager: AchievementsTableManagerProtocol = AchievementsTableManager(),
        employeeList: [Employee] = [],
        positionsList: [Position] = [],
        projectsList: [ProjectsRegistryResponseModel] = []) -> AchievementsViewController {
        let viewController = AchievementsViewController()
        let interactor = AchievementsInteractor(tableManager: tableManager, employeeList: employeeList, positionsList: positionsList, projectsList: projectsList)
        let presenter = AchievementsPresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

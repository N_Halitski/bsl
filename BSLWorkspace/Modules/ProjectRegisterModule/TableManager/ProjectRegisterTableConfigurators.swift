//
//  ProjectRegisterTableConfigurators.swift
//  BSLWorkspace
//
//  Created by Sergey on 25.11.21.
//

import UIKit

protocol ProjectRegisterConfiguratorProtocol: BaseConfigurator {
    var projectModel: ProjectsRegistryResponseModel? { get set }
    
    func hideSeparator()
}

final class ProjectRegisterCellConfigurator: ProjectRegisterConfiguratorProtocol {
    
    var reuseId: String = String(describing: ProjectRegisterTableViewCell.self)
    var cell: UIView?
    var projectModel: ProjectsRegistryResponseModel?
    
    func setupCell(_ cell: UIView) {
        self.cell = cell
        guard let cell =  cell as? ProjectRegisterTableViewCellProtocol,
              let model  = projectModel else { return }
        cell.configure(with: model)
    }
    
    func hideSeparator() {
        guard let cell = cell as? ProjectRegisterTableViewCellProtocol else { return }
        cell.hideSeparator()
    }
    
}

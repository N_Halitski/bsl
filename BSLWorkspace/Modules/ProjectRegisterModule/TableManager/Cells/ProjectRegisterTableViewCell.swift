//
//  ProjectRegisterTableViewCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 26.11.21.
//

import UIKit
import Kingfisher

protocol ProjectRegisterTableViewCellProtocol {
    func configure(with model: ProjectsRegistryResponseModel)
    func hideSeparator()
}

final class ProjectRegisterTableViewCell: UITableViewCell {
    
    @IBOutlet private var logoImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var typeLabel: UILabel!
    @IBOutlet private var separatorView: UIView!
}

extension ProjectRegisterTableViewCell: ProjectRegisterTableViewCellProtocol {
    
    func configure(with model: ProjectsRegistryResponseModel) {
        nameLabel.text = model.title
        typeLabel.text = model.id
        
        if let projectImage = model.imageURL {
            let logo = URL(string: projectImage)
            let placeholder = UIImage(named: "unselectedCell")
            logoImageView.kf.setImage(with: logo, placeholder: placeholder)
        } else {
            logoImageView.image = UIImage(named: "BSL_Logo")
        }
    }
    
    func hideSeparator() {
        separatorView.isHidden = true
    }
    
}

//
//  ProjectRegisterTableManager.swift
//  BSLWorkspace
//
//  Created by Sergey on 25.11.21.
//

import UIKit

protocol ProjectRegisterTableManagerProtocol: AnyObject {
    var didProjectTapped: ((ProjectsRegistryResponseModel?) -> Void)? { get set }
    
    func attachTableView(_ tableView: UITableView)
    func setDataToTable(_ data: [ProjectsRegistryResponseModel])
    func filterData(by seachText: String)
}

final class ProjectRegisterTableManager: NSObject, ProjectRegisterTableManagerProtocol {
    
    private enum Constants {
        static let isActiveStatus: String = "is_active"
        static let onHoldStatus: String = "on_hold"
        static let completedStatus: String = "completed"
        static let edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 14, right: 0)
    }
    
    private weak var tableView: UITableView?
    
    private var configurators: [[ProjectRegisterConfiguratorProtocol]] = []
    private var filteredConfigurators: [[ProjectRegisterConfiguratorProtocol]] = []
    
    var didProjectTapped: ((ProjectsRegistryResponseModel?) -> Void)?
    
    func attachTableView(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = Constants.edgeInsets
        tableView.register(UINib(nibName: String(describing: ProjectRegisterTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: ProjectRegisterTableViewCell.self))
        tableView.separatorStyle = .none
        self.tableView = tableView
    }
    
    func setDataToTable(_ data: [ProjectsRegistryResponseModel]) {
        
        var output: [[ProjectRegisterConfiguratorProtocol]] = []
        var tempOutput: [ProjectRegisterConfiguratorProtocol] = []
        
        let isActiveData = data.filter { $0.status == Constants.isActiveStatus }
        let onHoldData = data.filter { $0.status == Constants.onHoldStatus }
        let completedData = data.filter { $0.status == Constants.completedStatus }
        
        isActiveData.forEach { isActiveProjects in
            tempOutput.append(createProjectRegisterConfigurator(with: isActiveProjects))
        }
        output.append(tempOutput)
        tempOutput = []
        
        onHoldData.forEach { onHoldProjects in
            tempOutput.append(createProjectRegisterConfigurator(with: onHoldProjects))
        }
        output.append(tempOutput)
        tempOutput = []
        
        completedData.forEach { completedProjects in
            tempOutput.append(createProjectRegisterConfigurator(with: completedProjects))
        }
        output.append(tempOutput)
        
        self.configurators = output
        self.filteredConfigurators = output
        tableView?.reloadData()
    }
    
    private func createProjectRegisterConfigurator(with model: ProjectsRegistryResponseModel) -> ProjectRegisterConfiguratorProtocol {
        let configurator = ProjectRegisterCellConfigurator()
        configurator.projectModel = model
        return configurator
    }
}

// MARK: - Extensions. Сontains methods for working with a table and its data

extension ProjectRegisterTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredConfigurators.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredConfigurators[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = filteredConfigurators[indexPath.section][indexPath.row]
        let lastConfigurator = filteredConfigurators.last?.last
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        lastConfigurator?.hideSeparator()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedCellConfigurator = self.filteredConfigurators[indexPath.section][indexPath.row].projectModel else { return }
        didProjectTapped?(selectedCellConfigurator)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.backgroundView?.backgroundColor = AppTheme.Colors.lightGray100
        header.textLabel?.textColor = AppTheme.Colors.black100
        header.textLabel?.font = AppTheme.Fonts.SFRegular(14)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName = filteredConfigurators[section].first?.projectModel?.status
        
        switch sectionName {
        case Constants.isActiveStatus:
            return "ProjectRegisterTableManager.isActive".localized
        case Constants.onHoldStatus:
            return "ProjectRegisterTableManager.onHold".localized
        case Constants.completedStatus:
            return "ProjectRegisterTableManager.completed".localized
        default:
            return ""
        }
    }
    
}

extension ProjectRegisterTableManager {
    
    func filterData(by searchText: String) {
        guard let tableView = tableView else { return }
        
        var filteredSection: [ProjectRegisterConfiguratorProtocol] = []
        var filteredData: [[ProjectRegisterConfiguratorProtocol]] = []
        
        if searchText.isEmpty {
            filteredConfigurators = configurators
        } else {
            configurators.forEach {
                filteredSection = $0.filter { $0.projectModel?.title?.lowercased().hasPrefix(searchText.lowercased()) ?? false }
                filteredData.append(filteredSection)
            }
            filteredConfigurators = filteredData
        }
        tableView.reloadData()
    }
    
}

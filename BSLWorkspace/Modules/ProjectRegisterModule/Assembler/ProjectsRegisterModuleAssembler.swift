//
//  ProjectRegisterModuleProjectRegisterModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class ProjectsRegisterModuleAssembler {
    
    static func createModule(projectsList: [ProjectsRegistryResponseModel]?,
                             tableManager: ProjectRegisterTableManagerProtocol = ProjectRegisterTableManager()) -> ProjectRegisterModuleViewController {
        let viewController = ProjectRegisterModuleViewController()
        let interactor = ProjectRegisterModuleInteractor(tableManager, projectsList)
        let presenter = ProjectRegisterModulePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
    
}

//
//  ProjectRegisterModuleProjectRegisterModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class ProjectRegisterModuleViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: SearchPanel!
    
    var presenter: ProjectRegisterModulePresenterInput!
    
    var didTapProject: ((ProjectsRegistryResponseModel?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        self.hideKeyboardWhenTappedAround()
        setupUI()
        setupCallbacks()
        searchView.textField.delegate = self
        view.backgroundColor = AppTheme.Colors.lightGray100
    }
    
    private func setupUI() {
        self.navigationTitle = "Реестр проектов 📁"
        self.searchView.showfilterButton(condition: false)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func setupCallbacks() {
        self.searchView.textFieldDidChangeCompletion = { [weak self] searchText in
            guard let self = self else { return }
            self.presenter.filterData(by: searchText)
        }
    }
}

extension ProjectRegisterModuleViewController: ProjectRegisterModulePresenterOutput {
    
    func didProjectTapped(_ project: ProjectsRegistryResponseModel?) {
        self.didTapProject?(project)
    }
    
}

extension ProjectRegisterModuleViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}

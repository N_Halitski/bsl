//
//  HeaderView.swift
//  BSLWorkspace
//
//  Created by Sergey on 20.12.21.
//

import UIKit

class HeaderView: XibBasedView {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    init(name: String) {
        super.init(frame: UIScreen.main.bounds)
        self.label?.text = name
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

//
//  SearchPanel.swift
//  BSLWorkspace
//
//  Created by Sergey on 28.11.21.
//

import UIKit

class SearchPanel: XibBasedView {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var searchViewToBackViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchViewToButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var orangeMarkForFilter: UIView!
    
    var textFieldDidChangeCompletion: ((String) -> Void)?
    var filtersClicked:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        showfilterButton()
    }

    override func setupUI() {
        self.button.setTitle("", for: .normal)
        orangeMarkForFilter.backgroundColor = AppTheme.Colors.orange100
    }
    
    @IBAction func filterButtonClicked(_ sender: UIButton) {
        filtersClicked?()
    }
    
    func showfilterButton(condition: Bool = true) {
        self.searchViewToButtonTrailingConstraint.priority = condition ? UILayoutPriority(1000) : UILayoutPriority(999)
        self.searchViewToBackViewTrailingConstraint.priority = condition ? UILayoutPriority(999) : UILayoutPriority(1000)
        self.button.isHidden = !condition
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.textFieldDidChangeCompletion?(text)
    }
}

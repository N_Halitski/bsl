//
//  ProjectRegisterModuleProjectRegisterModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class ProjectRegisterModulePresenter {
    
    private let interactor: ProjectRegisterModuleInteractorInput
    private unowned let view: ProjectRegisterModulePresenterOutput
    
    init(_ interactor: ProjectRegisterModuleInteractorInput, _ view: ProjectRegisterModulePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }
    
    func viewDidLoad() {
        interactor.attach(self)
    }
}

extension ProjectRegisterModulePresenter: ProjectRegisterModulePresenterInput {
    func attachTableView(_ tableView: UITableView) {
        interactor.attachTableView(tableView)
    }
    
    func filterData(by searchText: String) {
        interactor.filterData(by: searchText)
    }
}

extension ProjectRegisterModulePresenter: ProjectRegisterModuleInteractorOutput {
    
    func didProjectTapped(_ project: ProjectsRegistryResponseModel?) {
        view.didProjectTapped(project)
    }
    
}

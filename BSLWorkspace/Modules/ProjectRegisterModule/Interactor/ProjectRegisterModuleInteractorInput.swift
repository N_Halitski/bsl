//
//  ProjectRegisterModuleProjectRegisterModuleInteractorInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol ProjectRegisterModuleInteractorInput: AnyObject {
    func attach(_ output: ProjectRegisterModuleInteractorOutput)
    func attachTableView(_ tableView: UITableView)
    func filterData(by searchText: String)
}

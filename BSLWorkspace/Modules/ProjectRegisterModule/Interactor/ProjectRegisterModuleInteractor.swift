//
//  ProjectRegisterModuleProjectRegisterModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class ProjectRegisterModuleInteractor {
    
    private weak var output: ProjectRegisterModuleInteractorOutput?
    private let tableManager: ProjectRegisterTableManagerProtocol
    private let projectsList: [ProjectsRegistryResponseModel]?
    
    init(_ tableManager: ProjectRegisterTableManagerProtocol,
         _ projectsList: [ProjectsRegistryResponseModel]?) {
        self.tableManager = tableManager
        self.projectsList = projectsList
    }
}

extension ProjectRegisterModuleInteractor: ProjectRegisterModuleInteractorInput {
    
    func attach(_ output: ProjectRegisterModuleInteractorOutput) {
        self.output = output
    }
    
    func attachTableView(_ tableView: UITableView) {
        tableManager.attachTableView(tableView)
        guard let projectsList = projectsList else {
            tableManager.setDataToTable([])
            return
        }
        
        tableManager.didProjectTapped = { [weak output] project in
            output?.didProjectTapped(project)
        }
        
        tableManager.setDataToTable(projectsList)
    }
    
    func filterData(by searchText: String) {
        tableManager.filterData(by: searchText)
    }
    
}

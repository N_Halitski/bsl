//
//  ProjectRegisterModuleProjectRegisterModuleInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 25/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol ProjectRegisterModuleInteractorOutput: AnyObject {
    func didProjectTapped(_ project: ProjectsRegistryResponseModel?)
}

//
//  VacationBookingModuleVacationBookingModuleViewController.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 17/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class VacationBookingModuleViewController: BaseViewController {
    
    @IBOutlet weak var customSegmentedControl: CustomSegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: VacationBookingModulePresenterInput!
    var didTapButton: (() -> Void)?
    var didTapDatePicker: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        presenter.attachTableView(tableView)
        setupUI()
        setupCallbacks()
    }
    
}

extension VacationBookingModuleViewController: VacationBookingModulePresenterOutput {
    
    func didButtonTapped() {
        self.didTapButton?()
    }
    
    func didDatePickerTapped() {
        self.didTapDatePicker?()
    }
    
}

extension VacationBookingModuleViewController {
    
    func setupUI() {
        self.title = "VacationBookingScreen.vacation".localized
        self.navigationController?.navigationBar.tintColor = AppTheme.Colors.darkBlue100
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "info.square"), style: .plain, target: self, action: #selector(infoButtonPressed))
        self.customSegmentedControl.setButtonNames(firstButton: "VacationBookingScreen.myVacations".localized, secondButton: "VacationBookingScreen.myTeam".localized)
    }
    
    private func setupCallbacks() {
        self.customSegmentedControl.segmentedControlDidChange = { [weak self] numberOfButton in
            guard let self = self else { return }
            self.presenter.switchScreen(to: numberOfButton)
        }
    }
    
    @objc func infoButtonPressed() {
        
    }
    
}

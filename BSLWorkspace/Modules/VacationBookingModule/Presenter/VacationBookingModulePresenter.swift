//
//  VacationBookingModuleVacationBookingModulePresenter.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 17/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class VacationBookingModulePresenter {

    private let interactor: VacationBookingModuleInteractorInput
    private unowned let view: VacationBookingModulePresenterOutput

    init(_ interactor: VacationBookingModuleInteractorInput, _ view: VacationBookingModulePresenterOutput) {
        self.interactor = interactor
        self.view = view
    }

    func viewDidLoad() {
        interactor.attach(self)
    }
    
}

extension VacationBookingModulePresenter: VacationBookingModulePresenterInput {
    
    func switchScreen(to number: Int) {
        interactor.switchScreen(to: number)
    }
    
    func attachTableView(_ tableView: UITableView) {
        interactor.attachTableView(tableView)
    }
    
}

extension VacationBookingModulePresenter: VacationBookingModuleInteractorOutput {
    
    func didTapButton() {
        view.didButtonTapped()
    }
    
    func didTapDatePicker() {
        view.didDatePickerTapped()
    }
    
}

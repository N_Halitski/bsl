//
//  VacationBookingModuleVacationBookingModulePresenterInput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 17/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

protocol VacationBookingModulePresenterInput: BasePresenting {
    func attachTableView(_ tableView: UITableView)
    func switchScreen(to number: Int)
}

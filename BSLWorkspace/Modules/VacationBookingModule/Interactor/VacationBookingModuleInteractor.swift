//
//  VacationBookingModuleVacationBookingModuleInteractor.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 17/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class VacationBookingModuleInteractor {
    
    private weak var output: VacationBookingModuleInteractorOutput?
    private var tableManager: VacationBookingTableManagerProtocol
    var teamData: [String] = []
    var oldVacations: [String] = []
    private var vacationsRepository: VacationsRepositoryProtocol
    private var vacationsList: [VacationModel]
    private var actualVacationsList: [VacationModel] = []
    private var completedVacationsList: [VacationModel] = []
    private var vacationsOfColleaguesList: [VacationModel]
    
    init(tableManager: VacationBookingTableManagerProtocol,
         vacationsRepository: VacationsRepositoryProtocol,
         vacationsList: [VacationModel],
         vacationsOfColleaguesList: [VacationModel]) {
        self.tableManager = tableManager
        self.vacationsRepository = vacationsRepository
        self.vacationsList = vacationsList
        self.vacationsOfColleaguesList = vacationsOfColleaguesList
        self.tableManager.didTapButton = { [weak self] in
            self?.output?.didTapButton()
        }
    }
    
}

extension VacationBookingModuleInteractor: VacationBookingModuleInteractorInput {
    
    func attach(_ output: VacationBookingModuleInteractorOutput) {
        self.output = output
    }
    
    func attachTableView(_ tableView: UITableView) {
        tableManager.attachTableView(tableView)
        self.tableManager.didTapDatePicker = { [weak output] in
            output?.didTapDatePicker()
        }
        vacationsFiltering(vacationsArray: vacationsList)
        createMyVacationsTable()
    }
    
    func switchScreen(to number: Int) {
        switch number {
        case 0:
            createMyVacationsTable()
        case 1:
            createMyTeamTable()
        default:
            break
        }
    }
    
    private func createMyVacationsTable() {
        tableManager.setDataToMyVacationsTable(actualVacationsApplications: self.actualVacationsList, completedVacationsApplications: self.completedVacationsList)
    }
    
    private func createMyTeamTable() {
        tableManager.setDataToMyTeamTable(vacationApplicationsOfMyTeam: self.vacationsOfColleaguesList)
    }
    
    private func vacationsFiltering(vacationsArray: [VacationModel]?) {
       if let vacationsArray = vacationsArray {
           vacationsArray.forEach { vacation in
               if let dateEnd = vacation.dateEnd {
                   DateFormatterManager.shared.isActual(date: dateEnd) ? actualVacationsList.append(vacation) : completedVacationsList.append(vacation)
               }
           }
       }
    }
        
}

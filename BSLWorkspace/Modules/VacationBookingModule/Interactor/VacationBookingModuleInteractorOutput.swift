//
//  VacationBookingModuleVacationBookingModuleInteractorOutput.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 17/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol VacationBookingModuleInteractorOutput: AnyObject {
    func didTapButton()
    func didTapDatePicker()
}

//
//  VacationBookingTableConfigurators.swift
//  BSLWorkspace
//
//  Created by Sergey on 17.12.21.
//

import UIKit

final class DatePickerCellConfigurator: BaseConfigurator {
    
    var reuseId: String = String(describing: DatePickerTableViewCell.self)
    
    func setupCell(_ cell: UIView) {}
}

final class SectionNameCellConfigurator: BaseConfigurator {
    
    var reuseId: String = String(describing: SectionNameTableViewCell.self)
    var model: String?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? SectionNameTableViewCellProtocol,
              let model = model else { return }
        cell.configure(with: model)
    }
}

final class ButtonCellConfigurator: BaseConfigurator {
    
    var reuseId: String = String(describing: ButtonTableViewCell.self)
    var didTapButton: (() -> Void)?
    
    func setupCell(_ cell: UIView) {
        guard var cell = cell as? ButtonCellProtocol else { return }
        cell.didTapButton = self.didTapButton
    }
}

final class OldVacationApplicationCellConfigurator: BaseConfigurator {
    
    var reuseId: String = String(describing: OldVacationApplicationCell.self)
    var model: VacationModel?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? OldVacationApplicationCellProtocol,
              let model = model else { return }
        cell.configure(with: model)
    }
}

final class MemberOfTeamCellConfigurator: BaseConfigurator {
    
    var reuseId: String = String(describing: MemberOfTeamCell.self)
    var model: VacationModel?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? MemberOfTeamCellProtocol,
              let model = model else { return }
        cell.configure(with: model)
    }
}
// swiftlint:disable all
final class MultipleVacationApplicationsCellConfigurator: BaseConfigurator {
// swiftlint:enable all
    var reuseId: String = String(describing: MultipleVacationApplicationsCell.self)
    var model: [VacationModel]?
    
    func setupCell(_ cell: UIView) {
        guard let cell = cell as? MultipleVacationApplicationsCellProtocol,
              let model = model else { return }
        cell.configure(with: model)
    }
}

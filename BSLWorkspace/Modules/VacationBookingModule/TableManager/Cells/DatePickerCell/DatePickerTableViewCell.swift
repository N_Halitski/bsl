//
//  DatePickerTableViewCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 17.12.21.
//

import UIKit

protocol DatePickerTableViewCellProtocol {}

final class DatePickerTableViewCell: UITableViewCell {

    @IBOutlet weak var view: RectangularDashedView!
    @IBOutlet weak var label: UILabel!
    
    var calendarDidPressed: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
}

extension DatePickerTableViewCell {
    
    func setupUI() {
        self.label.text = "VacationBookingScreen.pickDates".localized
    }
    
    @objc private func viewDidPressed() {
        self.calendarDidPressed?()
    }
    
}

extension DatePickerTableViewCell: DatePickerTableViewCellProtocol {}

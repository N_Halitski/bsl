//
//  SectionNameTableViewCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 17.12.21.
//

import UIKit

protocol SectionNameTableViewCellProtocol {
    func configure(with model: String)
}

final class SectionNameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
}

extension SectionNameTableViewCell: SectionNameTableViewCellProtocol {
    
    func configure(with model: String) {
        self.label.text = model
    }
    
}

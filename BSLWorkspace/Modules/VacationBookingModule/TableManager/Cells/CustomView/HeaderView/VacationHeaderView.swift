//
//  VacationHeaderView.swift
//  BSLWorkspace
//
//  Created by Sergey on 3.01.22.
//

import UIKit

class VacationHeaderView: XibBasedView {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    init(name: String) {
        super.init(frame: UIScreen.main.bounds)
        self.label?.text = name
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

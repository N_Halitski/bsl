//
//  RectangularDashedView.swift
//  BSLWorkspace
//
//  Created by Sergey on 17.12.21.
//

import UIKit

class RectangularDashedView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 1
    @IBInspectable var dashColor: UIColor = AppTheme.Colors.darkBlueGray100
    @IBInspectable var dashLength: CGFloat = 2
    @IBInspectable var betweenDashesSpace: CGFloat = 2
    
    var dashBorder: CAShapeLayer?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        dashBorder.path = cornerRadius > 0 ? UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath : UIBezierPath(rect: bounds).cgPath
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}

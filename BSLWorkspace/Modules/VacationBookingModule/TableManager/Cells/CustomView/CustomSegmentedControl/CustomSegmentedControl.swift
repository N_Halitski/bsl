//
//  CustomSegmentedControl.swift
//  BSLWorkspace
//
//  Created by Sergey on 22.12.21.
//

import UIKit

final class CustomSegmentedControl: XibBasedView {
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var firstButtonView: UIView!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var secondButtonView: UIView!
    
    var segmentedControlDidChange: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    @IBAction func firstButtonPressed(_ sender: UIButton) {
        self.firstButtonView.isHidden = false
        self.firstButton.setTitleColor(AppTheme.Colors.darkBlue100, for: .normal)
        self.secondButtonView.isHidden = true
        self.secondButton.setTitleColor(AppTheme.Colors.darkGray, for: .normal)
        segmentedControlDidChange?(0)
    }
    
    @IBAction func secondButtonPressed(_ sender: UIButton) {
        self.firstButtonView.isHidden = true
        self.firstButton.setTitleColor(AppTheme.Colors.darkGray, for: .normal)
        self.secondButtonView.isHidden = false
        self.secondButton.setTitleColor(AppTheme.Colors.darkBlue100, for: .normal)
        segmentedControlDidChange?(1)
    }
    
    override func setupUI() {
        self.firstButtonView.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: 7, height: 0), shadowOpacity: 1, shadowRadius: 16)
        self.secondButtonView.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: -7, height: 0), shadowOpacity: 1, shadowRadius: 16)
        self.secondButtonView.isHidden = true
    }
    
    func setButtonNames(firstButton: String, secondButton: String) {
        self.firstButton.setTitle(firstButton, for: .normal)
        self.secondButton.setTitle(secondButton, for: .normal)
    }
    
}

//
//  VacationTableSection.swift
//  BSLWorkspace
//
//  Created by Sergey on 3.01.22.
//

import Foundation

class VacationTableSection {
    var sectionName: String?
    var configurators: [BaseConfigurator]
    
    init(sectionName: String?, configurators: [BaseConfigurator]) {
        self.sectionName = sectionName
        self.configurators = configurators
    }
}

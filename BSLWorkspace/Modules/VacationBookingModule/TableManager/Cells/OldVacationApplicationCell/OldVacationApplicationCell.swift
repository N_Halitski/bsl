//
//  OldVacationApplicationCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 19.12.21.
//

import UIKit

protocol OldVacationApplicationCellProtocol {
    func configure(with model: VacationModel)
}

final class OldVacationApplicationCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var datesAndDaysLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
}

extension OldVacationApplicationCell: OldVacationApplicationCellProtocol {
    
    func setupUI() {
        self.backView.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: 0, height: 4), shadowOpacity: 0.5, shadowRadius: 36)
        self.statusLabel.text = "VacationBookingScreen.statusСompleted".localized
    }
    
    func configure(with model: VacationModel) {
        let daysIntervalText = DateFormatterManager.shared.dayIntervalCalculation(dateStart: model.dateStart, dateEnd: model.dateEnd)
        let datesIntervalText = DateFormatterManager.shared.dateInterval(dateStart: model.dateStart, dateEnd: model.dateEnd)
        self.datesAndDaysLabel.text = datesIntervalText + " " + daysIntervalText
    }
    
}

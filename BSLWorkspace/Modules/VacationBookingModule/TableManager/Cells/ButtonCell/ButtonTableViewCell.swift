//
//  ButtonTableViewCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 19.12.21.
//

import UIKit

protocol ButtonCellProtocol {
    var didTapButton: (() -> Void)? { get set }
    
    func configure(with model: String)
}

final class ButtonTableViewCell: UITableViewCell {
    
    var didTapButton: (() -> Void)?
    
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        didTapButton?()
    }
    
}

extension ButtonTableViewCell: ButtonCellProtocol {
    
    func configure(with model: String) {}
    
    func setupUI() {
        self.button.setTitle("VacationBookingScreen.pickDates".localized, for: .normal)
    }
}

//
//  MultipleVacationApplicationsCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 19.12.21.
//

import UIKit

protocol MultipleVacationApplicationsCellProtocol {
    func configure(with model: [VacationModel])
}

final class MultipleVacationApplicationsCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var vacationsList: [VacationModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: String(describing: VacationApplicationCell.self), bundle: nil),
                                forCellWithReuseIdentifier: String(describing: VacationApplicationCell.self))
        let layout = UICollectionViewFlowLayout()
        let inset: CGFloat = 16
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 32, height: self.collectionView.frame.height)
        layout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.setCollectionViewLayout(layout, animated: false)
    }
    
}

extension MultipleVacationApplicationsCell: MultipleVacationApplicationsCellProtocol {
    
    func configure(with model: [VacationModel]) {
        self.vacationsList = model
    }
    
}

extension MultipleVacationApplicationsCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vacationsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: VacationApplicationCell.self),
            for: indexPath) as? VacationApplicationCell else { return UICollectionViewCell() }
        cell.configure(with: self.vacationsList[indexPath.row])
        return cell
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        let cellWidthWithSpacing = layout.itemSize.width + layout.minimumLineSpacing
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthWithSpacing
        let roundedIndex = round(index)
        offset = CGPoint(x: roundedIndex * cellWidthWithSpacing - scrollView.contentInset.left, y: scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
}

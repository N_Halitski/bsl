//
//  MemberOfTeamCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 19.12.21.
//

import UIKit
import Kingfisher

protocol MemberOfTeamCellProtocol {
    func configure(with model: VacationModel)
}

final class MemberOfTeamCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var datesAndDaysLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var checkerImageView: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
}

extension MemberOfTeamCell: MemberOfTeamCellProtocol {
    
    func setupUI() {
        self.backView.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: 0, height: 4), shadowOpacity: 0.5, shadowRadius: 36)
        self.avatarImageView.layer.borderColor = AppTheme.Colors.gray100.cgColor
        self.checkerImageView.forEach { imageView in
            imageView.layer.cornerRadius = imageView.layer.frame.width / 2
            imageView.layer.borderColor = AppTheme.Colors.greenStatus.cgColor
        }
        self.statusLabel.text = "VacationBookingScreen.statusApproved".localized
        self.statusView.backgroundColor = AppTheme.Colors.greenStatus
    }
    
    func configure(with model: VacationModel) {
        let daysIntervalText = DateFormatterManager.shared.dayIntervalCalculation(dateStart: model.dateStart, dateEnd: model.dateEnd)
        let datesIntervalText = DateFormatterManager.shared.dateInterval(dateStart: model.dateStart, dateEnd: model.dateEnd)
        self.datesAndDaysLabel.text = datesIntervalText + " " + daysIntervalText
        nameLabel.text = model.employee?.name
        avatarImageView.kf.setImage(with: URL(string: model.employee?.avatar ?? ""), placeholder: UIImage(named: "person"))
        
        if let approversArray = model.approvers {
            let approversNumber = approversArray.count
            for (index, imageView) in checkerImageView.enumerated() {
                imageView.isHidden = false
                if index < approversNumber {
                    let approver = approversArray[index]
                    guard let status = approver.status else {
                        imageView.layer.borderColor = AppTheme.Colors.orangeStatus.cgColor
                        continue
                    }
                    imageView.setBorderColor(by: status)
                    imageView.kf.setImage(with: URL(string: approver.approver?.avatar ?? ""), placeholder: UIImage(named: "person"))
                } else {
                    imageView.isHidden = true
                }
            }
        } else {
            for imageView in self.checkerImageView {
                imageView.isHidden = true
            }
        }
    }
    
}

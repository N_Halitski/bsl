//
//  VacationApplicationCell.swift
//  BSLWorkspace
//
//  Created by Sergey on 19.12.21.
//

import UIKit
import Kingfisher

final class VacationApplicationCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var datesLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet var checkerImageView: [UIImageView]!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {}
    
}

extension VacationApplicationCell {
    
    func setupUI() {
        self.editButton.setTitle("", for: .normal)
        self.backView.addShadow(shadowColor: AppTheme.Colors.lightGray4.cgColor, shadowOffset: CGSize(width: 0, height: 2), shadowOpacity: 0.5, shadowRadius: 36)
        self.checkerImageView.forEach { imageView in
            imageView.layer.cornerRadius = imageView.layer.frame.width / 2
            imageView.layer.borderColor = AppTheme.Colors.orangeStatus.cgColor
        }
    }
    
    func configure(with model: VacationModel) {
        switch model.status {
        case .requested:
            self.statusLabel.text = "VacationBookingScreen.statusUnderApproval".localized
            self.statusView.backgroundColor = AppTheme.Colors.orangeStatus
        case .approved:
            self.statusLabel.text = "VacationBookingScreen.statusApproved".localized
            self.statusView.backgroundColor = AppTheme.Colors.greenStatus
        case .rejected:
            self.statusLabel.text = "VacationBookingScreen.statusRejected".localized
            self.statusView.backgroundColor = AppTheme.Colors.redStatus
        case .onHold:
            self.statusLabel.text = "VacationBookingScreen.onHold".localized
            self.statusView.backgroundColor = AppTheme.Colors.gray100
        default:
            break
        }
                
        self.daysLabel.text = DateFormatterManager.shared.dayIntervalCalculation(dateStart: model.dateStart, dateEnd: model.dateEnd)
        self.datesLabel.text = DateFormatterManager.shared.dateInterval(dateStart: model.dateStart, dateEnd: model.dateEnd)
        
        if let approversArray = model.approvers {
            let approversNumber = approversArray.count
            for (index, imageView) in checkerImageView.enumerated() {
                imageView.isHidden = false
                if index < approversNumber {
                    let approver = approversArray[index]
                    guard let status = approver.status else {
                        imageView.layer.borderColor = AppTheme.Colors.orangeStatus.cgColor
                        continue
                    }
                    imageView.setBorderColor(by: status)
                    imageView.kf.setImage(with: URL(string: approver.approver?.avatar ?? ""), placeholder: UIImage(named: "person"))
                } else {
                    imageView.isHidden = true
                }
            }
        } else {
            for imageView in self.checkerImageView {
                imageView.isHidden = true
            }
        }
    }
    
}

//
//  VacationBookingTableManager.swift
//  BSLWorkspace
//
//  Created by Sergey on 17.12.21.
//

import UIKit

protocol VacationBookingTableManagerProtocol {
    var didTapDatePicker: (() -> Void)? { get set }
    var didTapButton: (() -> Void)? { get set }
    
    func attachTableView(_ tableView: UITableView)
    func setDataToMyVacationsTable(actualVacationsApplications: [VacationModel], completedVacationsApplications: [VacationModel])
    func setDataToMyTeamTable(vacationApplicationsOfMyTeam: [VacationModel]?)
}

final class VacationBookingTableManager: NSObject, VacationBookingTableManagerProtocol {

    private weak var tableView: UITableView?
    private var configurators: [BaseConfigurator] = []
    private var sections: [VacationTableSection] = []
    private var sectionsName: [String] = []
    
    var didTapDatePicker: (() -> Void)?
    var didTapButton: (() -> Void)?
    
    func attachTableView(_ tableView: UITableView) {
        self.tableView = tableView
        tableView.delegate = self
        tableView.dataSource = self
        registerTableCells(for: tableView)
    }
    
}

extension VacationBookingTableManager: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let configurator = sections[indexPath.section].configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseId, for: indexPath)
        configurator.setupCell(cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionName = self.sections[section].sectionName else { return nil }
        let headerView = VacationHeaderView(name: sectionName)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.sections[section].sectionName == nil ? 0 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if sections[indexPath.section].configurators[indexPath.row] as? DatePickerCellConfigurator != nil {
            self.didTapDatePicker?()
        }
    }
    
}

extension VacationBookingTableManager {
    
    func registerTableCells(for tableView: UITableView) {
        tableView.register(UINib(nibName: String(describing: DatePickerTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: DatePickerTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: SectionNameTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: SectionNameTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: OldVacationApplicationCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: OldVacationApplicationCell.self))
        tableView.register(UINib(nibName: String(describing: ButtonTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: ButtonTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: MemberOfTeamCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: MemberOfTeamCell.self))
        tableView.register(UINib(nibName: String(describing: MultipleVacationApplicationsCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: MultipleVacationApplicationsCell.self))
    }
    
    func setDataToMyVacationsTable(actualVacationsApplications: [VacationModel], completedVacationsApplications: [VacationModel]) {
        var tableSectionsArray: [VacationTableSection] = []
        let firstSections = self.createFirstSectionOfTable(actualVacationsApplications: actualVacationsApplications)
        tableSectionsArray.append(firstSections)
        let secondSection = self.createSecondSectionOfTable(completedVacationsApplications: completedVacationsApplications)
        tableSectionsArray.append(secondSection)
        self.sections = tableSectionsArray
        tableView?.reloadData()
    }
    
    func createSecondSectionOfTable(completedVacationsApplications: [VacationModel]) -> VacationTableSection {
        var secondSectionConfigurators: [BaseConfigurator] = []
        completedVacationsApplications.forEach { item in
            let configurator = createOldVacationApplicationConfigurator(with: item)
            secondSectionConfigurators.append(configurator)
        }
        let secondSection = VacationTableSection(sectionName: "VacationBookingScreen.vacationHistory".localized, configurators: secondSectionConfigurators)
        return secondSection
    }
    
    func createFirstSectionOfTable(actualVacationsApplications: [VacationModel]) -> VacationTableSection {
        var firstSectionConfigurators: [BaseConfigurator] = []
        if actualVacationsApplications.isEmpty {
            let datePickerConfigurator = createDatePickerConfigurator()
            firstSectionConfigurators.append(datePickerConfigurator)
        } else {
            let multipleVacationApplicationsConfigurator = createMultipleVacationApplicationsConfigurator(with: actualVacationsApplications)
            firstSectionConfigurators.append(multipleVacationApplicationsConfigurator)
            let buttonConfigurator = createButtonConfigurator()
            firstSectionConfigurators.append(buttonConfigurator)
        }
        let firstSection = VacationTableSection(sectionName: "VacationBookingScreen.vacationApplications".localized, configurators: firstSectionConfigurators)
        return firstSection
    }
    
    func setDataToMyTeamTable(vacationApplicationsOfMyTeam: [VacationModel]?) {
        var output: [VacationTableSection] = []
        if  let vacationApplicationsOfMyTeam = vacationApplicationsOfMyTeam {
            var myTeamVacations: [BaseConfigurator] = []
            vacationApplicationsOfMyTeam.forEach { item in
                let configurator = createMemberOfTeamConfigurator(with: item)
                myTeamVacations.append(configurator)
            }
            let section = VacationTableSection(sectionName: nil, configurators: myTeamVacations)
            output.append(section)
        } else {
            return
        }
        self.sections = output
        tableView?.reloadData()
    }
    
}

extension VacationBookingTableManager {
    
    private func createDatePickerConfigurator() -> BaseConfigurator {
        let configurator = DatePickerCellConfigurator()
        return configurator
    }
    
    private func createSectionNameConfigurator(with model: String) -> BaseConfigurator {
        let configurator = SectionNameCellConfigurator()
        configurator.model = model
        return configurator
    }
    
    private func createOldVacationApplicationConfigurator(with model: VacationModel) -> BaseConfigurator {
        let configurator = OldVacationApplicationCellConfigurator()
        configurator.model = model
        return configurator
    }
    
    private func createButtonConfigurator() -> BaseConfigurator {
        let configurator = ButtonCellConfigurator()
        configurator.didTapButton = didTapButton
        return configurator
    }
    
    private func createMemberOfTeamConfigurator(with model: VacationModel) -> BaseConfigurator {
        let configurator = MemberOfTeamCellConfigurator()
        configurator.model = model
        return configurator
    }
    
    private func createMultipleVacationApplicationsConfigurator(with model: [VacationModel]) -> BaseConfigurator {
        let configurator = MultipleVacationApplicationsCellConfigurator()
        configurator.model = model
        return configurator
    }
    
}

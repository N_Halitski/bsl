//
//  VacationBookingModuleVacationBookingModuleAssembler.swift
//  BSLWorkspace
//
//  Created by Sergey Metelsky on 17/12/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class VacationBookingModuleAssembler {
    static func createModule(tableManager: VacationBookingTableManagerProtocol = VacationBookingTableManager(),
                             vacationsRepository: VacationsRepositoryProtocol,
                             vacationsList: [VacationModel],
                             vacationsOfColleaguesList: [VacationModel]) -> VacationBookingModuleViewController {
        let viewController = VacationBookingModuleViewController()
        let interactor = VacationBookingModuleInteractor(tableManager: tableManager,
                                                         vacationsRepository: vacationsRepository,
                                                         vacationsList: vacationsList,
                                                         vacationsOfColleaguesList: vacationsOfColleaguesList)
        let presenter = VacationBookingModulePresenter(interactor, viewController)
        viewController.presenter = presenter
        return viewController
    }
}

//
//  LoginScreenLoginScreenPresenterInput.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol LoginScreenPresenterInput: BasePresenting {
    func continueTapped(with email: String, with password: String)
    func forgotPassButtonTap()
}

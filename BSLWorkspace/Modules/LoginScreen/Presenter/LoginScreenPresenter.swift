//
//  LoginScreenLoginScreenPresenter.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

final class LoginScreenPresenter {
    
    private let interactor: LoginScreenInteractorInput
    private unowned let view: LoginScreenPresenterOutput
    
    // MARK: - Callbacks
    
    private var didFinish: (() -> Void)?
    private var openPasswordRecoveryScreen: (() -> Void)?
    
    init(_ interactor: LoginScreenInteractorInput, _ view: LoginScreenPresenterOutput, didFinish: (() -> Void)?, openPasswordRecoveryScreen: (() -> Void)?) {
        self.interactor = interactor
        self.view = view
        self.didFinish = didFinish
        self.openPasswordRecoveryScreen = openPasswordRecoveryScreen
    }
    
    func viewDidLoad() {
        interactor.attach(self)
        view.setupUI()
    }
    
}

extension LoginScreenPresenter: LoginScreenPresenterInput {
    
    /// сообщает, что будет открыт экран востановления пароля
    func forgotPassButtonTap() {
        openPasswordRecoveryScreen?()
    }
    
    /// метод передает в интерактор пароль и логин для пароля
    /// - Parameters:
    ///   - email: имэйл
    ///   - password: пароль
    func continueTapped(with email: String, with password: String) {
        interactor.auth(with: email, with: password)
    }
}

extension LoginScreenPresenter: LoginScreenInteractorOutput {
    
    func startLoader() {
        view.startLoader()
    }
    
    func stopLoader() {
        view.stopLoader()
    }
    
    /// метод собщает будет ли пользователь пропущен внутрь приложения или нет
    func didRecieveAuthResult(with result: Result<AuthResponseModel?, APIErrors>) {
        switch result {
        case .success:
            didFinish?()
        case .failure(let error):
            switch error {
            case .parametersError:
                view.showErrorAuth()
            case .unknownError:
                break
            case .desearilization:
                break
            }
        }
    }
}

//
//  LoginScreenLoginScreenPresenterOutput.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol LoginScreenPresenterOutput: AnyObject {
    func setupUI()
    func showErrorAuth()
    func startLoader()
    func stopLoader()
}

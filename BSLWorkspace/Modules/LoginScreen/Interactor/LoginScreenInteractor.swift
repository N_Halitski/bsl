//
//  LoginScreenLoginScreenInteractor.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class LoginScreenInteractor {
    
    private weak var output: LoginScreenInteractorOutput?
    /// репозиторий необходимый для выполнения запроса
    private let authRepository: AuthRepositoryProtocol
    
    init(authRepository: AuthRepositoryProtocol) {
        self.authRepository = authRepository
    }

}

extension LoginScreenInteractor: LoginScreenInteractorInput {
    
    func attach(_ output: LoginScreenInteractorOutput) {
        self.output = output
    }
    
    /// метод совершает проверку пароля и логина введенные пользователем
    /// - Parameters:
    ///   - email: имэйл
    ///   - password: пароль
    func auth(with email: String, with password: String) {
        self.output?.startLoader()
        self.authRepository.auth(email: email, password: password) { [weak self] result in
            self?.output?.didRecieveAuthResult(with: result)
            self?.output?.stopLoader()
        }
    }
    
}

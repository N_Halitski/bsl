//
//  LoginScreenLoginScreenInteractorOutput.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol LoginScreenInteractorOutput: AnyObject {
    func didRecieveAuthResult(with result: Result<AuthResponseModel?, APIErrors>)
    func startLoader()
    func stopLoader()
}

//
//  LoginScreenLoginScreenInteractorInput.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

protocol LoginScreenInteractorInput: AnyObject {
    func attach(_ output: LoginScreenInteractorOutput)
    func auth(with email: String, with password: String)
}

//
//  LoginScreenLoginScreenViewController.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import UIKit

final class LoginScreenViewController: BaseViewController {
    
    // MARK: - Outlets
    
    /// текст филд для ввода почты
    @IBOutlet private var emailTextField: UITextField!
    /// текст филд для ввода пароля
    @IBOutlet private var passwordTextField: UITextField!
    /// лэйбл востановления пароля
    @IBOutlet private var forgotPasswordButton: UIButton!
    /// аутлет кнопки войти
    @IBOutlet private var loginButton: UIButton!
    /// лэйбл с ошибкой
    @IBOutlet private var errorLabel: UILabel!
    /// лэйбл надписи Вход
    @IBOutlet private var entryLabel: UILabel!
    ///  лэйбл описания того, что нужно сделать
    @IBOutlet private var instructionLabel: UILabel!
    
    /// енум с константами для UI элементов экрана логин
    private enum Constants {
        static let distanceFromKeyboardToLoginButton: CGFloat = 60
        static let borderRadius: CGFloat = 16
        static let activeButtonColor = AppTheme.Colors.darkBlue100
        static let inactiveButtonColor = AppTheme.Colors.grayBlue100
        static let shadowRGBA = AppTheme.Colors.black100.cgColor
        static let shadowOffset = CGSize(width: 0, height: 4)
        static let shadowOpacity: Float = 0.07
        static let shadowRadius: CGFloat = 16
        static let emailSuffix: String = "@bsl.dev"
        static let minPasswordCount: Int = 6
    }
    
    // MARK: - Dependencies
    
    var presenter: LoginScreenPresenterInput!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupNotificationCenter()
        subscribeOnDelegate()
        hideNavigationBarSeparator()
    }
    
    // Сlose the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // Pull up the screen with keyboard
    private func setupNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if emailTextField.isFirstResponder || passwordTextField.isFirstResponder {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                let distanceFromBottomToButton = self.view.frame.maxY - loginButton.frame.maxY - Constants.distanceFromKeyboardToLoginButton
                
                if self.view.frame.origin.y == .zero, distanceFromBottomToButton < keyboardSize.height {
                    self.view.frame.origin.y -= keyboardSize.height - distanceFromBottomToButton
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = .zero
    }
    
    /// метод, который сообщает что делегатом текст филдов будет данный экран
    private func subscribeOnDelegate() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    /// аквтивирует кнопку логин
    private func activateLoginButton() {
        loginButton.backgroundColor = Constants.activeButtonColor
        loginButton.isEnabled = true
    }
    
    /// деактивирует кнопку логин
    private func inactivateLoginButton() {
        loginButton.backgroundColor = Constants.inactiveButtonColor
        loginButton.isEnabled = false
    }
    
    @IBAction private func loginButtonTapped() {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        presenter.continueTapped(with: email, with: password)
    }
    
    @IBAction private func forgotPasswordButtonTapped() {
        presenter.forgotPassButtonTap()
    }
    
}

// MARK: - Extensions

extension LoginScreenViewController: LoginScreenPresenterOutput {
    
    func startLoader() {
        showLoader(isShown: true)
    }
    
    func stopLoader() {
        showLoader(isShown: false)
    }
    
    func setupUI() {
        entryLabel.text = "LoginScreen.entry".localized
        instructionLabel.text = "LoginScreen.instruction".localized
        emailTextField.placeholder = "LoginScreen.email".localized
        passwordTextField.placeholder = "LoginScreen.password".localized
        loginButton.setTitle("LoginScreen.login".localized, for: .normal)
        forgotPasswordButton.setTitle("LoginScreen.forgotPassword".localized, for: .normal)
        
        emailTextField.layer.cornerRadius = Constants.borderRadius
        passwordTextField.layer.cornerRadius = Constants.borderRadius
        loginButton.layer.cornerRadius = Constants.borderRadius
        
        emailTextField.clipsToBounds = true
        passwordTextField.clipsToBounds = true
        
        emailTextField.addShadow(shadowColor: Constants.shadowRGBA,
                                 shadowOffset: Constants.shadowOffset,
                                 shadowOpacity: Constants.shadowOpacity,
                                 shadowRadius: Constants.shadowRadius)
        passwordTextField.addShadow(shadowColor: Constants.shadowRGBA,
                                    shadowOffset: Constants.shadowOffset,
                                    shadowOpacity: Constants.shadowOpacity,
                                    shadowRadius: Constants.shadowRadius)
        navigationItem.setHidesBackButton(true, animated: true)
    }
    
    /// метод подсвечивает текст филды, если была допущена ошибка ввода пароля/логина
    func showErrorAuth() {
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = AppTheme.Colors.red100.cgColor
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.borderColor = AppTheme.Colors.red100.cgColor
        errorLabel.text = "LoginScreen.checkData".localized
    }
    
}

extension LoginScreenViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if emailTextField.text?.hasSuffix(Constants.emailSuffix) ?? false,
           (passwordTextField.text?.count ?? 0) >= Constants.minPasswordCount {
            activateLoginButton()
        } else {
            inactivateLoginButton()
        }
    }
    
}

private extension LoginScreenViewController {
    
    func hideNavigationBarSeparator() {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.shadowColor = .clear
        navBarAppearance.shadowImage = UIImage()
        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
    }
    
}

//
//  LoginScreenLoginScreenAssembler.swift
//  BSLWorkspace
//
//  Created by clear on 15/11/2021.
//  Copyright © 2021 BSL. All rights reserved.
//

import Foundation

final class LoginScreenAssembler {
    /// метод возвращает логин view controller
    /// - Parameters:
    ///   - authRepository: репозиторий необходимый для входа
    ///   - didFinish: замыкание, которое сообщает об успешной авторизации и передает событие в координатор
    ///   - openPasswordRecoveryScreen: замыкание сообщащее что будет открыт экран востановления пароля
    /// - Returns: логин view controller
    static func createModule(
        authRepository: AuthRepositoryProtocol = AuthRepository(),
        didFinish: (() -> Void)?,
        openPasswordRecoveryScreen: (() -> Void)?) -> LoginScreenViewController {
            let viewController = LoginScreenViewController()
            let interactor = LoginScreenInteractor(authRepository: authRepository)
            let presenter = LoginScreenPresenter(
                interactor,
                viewController,
                didFinish: didFinish,
                openPasswordRecoveryScreen: openPasswordRecoveryScreen)
            viewController.presenter = presenter
            return viewController
        }
}

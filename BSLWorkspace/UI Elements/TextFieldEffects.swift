//
//  TextFieldEffects.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 15.11.21.
//

import UIKit
import os.log

// Helper class for creating the effect TextField
class TextFieldEffects: UITextField {
    
    enum AnimationType: Int {
        case textEntry
        case textDisplay
    }
    
    typealias AnimationCompletionHandler = (_ type: AnimationType) -> Void
    
    let placeholderLabel = UILabel()
    
    func animateViewsForTextEntry() {
        os_log("@", "\(#function) must be overridden")
    }
    
    func animateViewsForTextDisplay() {
        os_log("@", "\(#function) must be overridden")
    }
    
    var animationCompletionHandler: AnimationCompletionHandler?
    
    func drawViewsForRect(_ rect: CGRect) {
        os_log("@", "\(#function) must be overridden")
    }
    
    func updateViewsForBoundsChange(_ bounds: CGRect) {
        os_log("@", "\(#function) must be overridden")
    }
    
    // MARK: - Overrides
    
    override func draw(_ rect: CGRect) {
        guard isFirstResponder == false else { return }
        drawViewsForRect(rect)
    }
    
    override func drawPlaceholder(in rect: CGRect) {
        // Don't draw any placeholders
    }
    
    override var text: String? {
        didSet {
            if let text = text, text.isNotEmpty || isFirstResponder {
                animateViewsForTextEntry()
            } else {
                animateViewsForTextDisplay()
            }
        }
    }
    
    // MARK: - UITextField Observing
    
    override func willMove(toSuperview newSuperview: UIView?) {
        if newSuperview != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidEndEditing), name: UITextField.textDidEndEditingNotification, object: self)
            
            NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidBeginEditing), name: UITextField.textDidBeginEditingNotification, object: self)
        } else {
            // swiftlint:disable all
            NotificationCenter.default.removeObserver(self)
            // swiftlint:enable all
        }
    }
    
    /**
     The textfield has started an editing session.
     */
    @objc private func textFieldDidBeginEditing() {
        animateViewsForTextEntry()
    }
    
    /**
     The textfield has ended an editing session.
     */
    @objc private func textFieldDidEndEditing() {
        animateViewsForTextDisplay()
    }
    
    // MARK: - Interface Builder
    
    override func prepareForInterfaceBuilder() {
        drawViewsForRect(frame)
    }

}

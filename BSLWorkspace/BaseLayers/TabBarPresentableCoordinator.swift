//
//  TabBarPresentableCoordinator.swift
//  BSLWorkspace
//
//  Created by Nikita on 17.11.2021.
//

import UIKit

protocol TabBarPresentableCoordinator: Coordinator {
  var tabBarItem: UITabBarItem { get }
}

//
//  UIStylizer.swift
//  BSLWorkspace
//
//  Created by Sergey on 20.12.21.
//

import UIKit

class UIStylizer {
    func setupUI () {
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = 0.0
        }
    }
}

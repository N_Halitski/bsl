//
//  BaseConfigurator.swift
//  BSLWorkspace
//
//  Created by Sergey on 1.12.21.
//

import UIKit

protocol BaseConfigurator {
    var reuseId: String { get }
    func setupCell(_ cell: UIView)
}

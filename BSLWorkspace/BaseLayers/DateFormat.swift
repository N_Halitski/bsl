//
//  DateFormat.swift
//  BSLWorkspace
//
//  Created by Sergey on 24.12.21.
//

import UIKit

final class DateFormatterManager {
    
    private let locale = String(Locale.preferredLanguages[0].prefix(2))
    
    public static let shared = DateFormatterManager()
    private let currentSecondsFromGMT = TimeZone.current.secondsFromGMT()
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        return formatter
    }()
    
    private init() {
    }
    
    private func configureFormatter(formatter: DateFormatter, format: String) {
        formatter.locale = Locale(identifier: self.locale)
        formatter.dateFormat = format
    }
    
    func setWeekday(day: Int) -> String {
        return formatter.shortWeekdaySymbols[day].description.uppercased()
    }
    
    func changeDateFormat(date: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "d m yyyy")
        guard let date = date,
              let dateHeader = self.formatter.date(from: date) else { return "" }
        self.configureFormatter(formatter: self.formatter, format: "dd.mm.yyyy")
        return formatter.string(from: dateHeader)
    }
    
    func setMonthHeader(date: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "M yyyy")
        guard let date = date,
              let dateHeader = self.formatter.date(from: date) else { return "" }
        self.configureFormatter(formatter: self.formatter, format: "LLLL yyyy")
        return formatter.string(from: dateHeader)
    }
    
    func setRangeOfDate(fromDate: String?, toDate: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "d M yyyy")
        guard let fromDate = fromDate,
              let toDate = toDate,
              let firstDate = self.formatter.date(from: fromDate),
              let secondDate = self.formatter.date(from: toDate) else { return "" }
        self.configureFormatter(formatter: self.formatter, format: "d MMMM")
        return ("\(formatter.string(from: firstDate)) - \(formatter.string(from: secondDate))")
    }
    
    func hourIntervalCalculation(dateOfPlacement: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "dd.MM.yyyy HH:mm:ss")
        guard let dateOfPlacement = dateOfPlacement, let date = self.formatter.date(from: dateOfPlacement) else { return ""}
        let timeInterval = -date.timeIntervalSinceNow
        let numberOfHours = (Int(round(timeInterval / 60 / 60)))
        if numberOfHours > 24 {
            let numberOfDays = (Int(numberOfHours / 24))
            return ("\(numberOfDays) \(createDaysString(from: numberOfDays))")
        }
        return ("\(numberOfHours) \(createHoursString(from: numberOfHours))")
    }
    
    func dayIntervalCalculation(dateStart: String?, dateEnd: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "dd.MM.yyyy")
        guard let dateStart = dateStart,
              let dateEnd = dateEnd,
              let dateFirst = self.formatter.date(from: dateStart),
              let dateSecond = self.formatter.date(from: dateEnd) else { return "" }
        let timeInterval = dateSecond.timeIntervalSince(dateFirst)
        let numberOfDays = Int(round(timeInterval / 60 / 60 / 24)) + 1
        return ("(\(numberOfDays) \(createDaysString(from: numberOfDays)))")
    }
    
    func setNumberOfDays(dateStart: String?, dateEnd: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "dd.MM.yyyy")
        guard let dateStart = dateStart,
              let dateEnd = dateEnd,
              let dateFirst = self.formatter.date(from: dateStart),
              let dateSecond = self.formatter.date(from: dateEnd) else { return "" }
        let timeInterval = dateSecond.timeIntervalSince(dateFirst)
        let numberOfDays = Int(round(timeInterval / 60 / 60 / 24)) + 1
        return ("\(numberOfDays) \(createDaysString(from: numberOfDays))")
    }
    
    func dateInterval(dateStart: String?, dateEnd: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "dd.MM.yyyy")
        guard let dateStart = dateStart,
                let dateEnd = dateEnd,
                let dateFirst = self.formatter.date(from: dateStart),
                let dateSecond = self.formatter.date(from: dateEnd) else { return ""}
                self.configureFormatter(formatter: self.formatter, format: "dd MMM")
        return ("\(formatter.string(from: dateFirst)) - \(formatter.string(from: dateSecond))")
    }
    
    func date(createAt: String?) -> String {
        self.configureFormatter(formatter: self.formatter, format: "dd.MM.yyyy HH:mm:ss")
        guard let createAt = createAt,
              let date = self.formatter.date(from: createAt) else { return ""}
        let timeInterval = Int(-date.timeIntervalSinceNow) - currentSecondsFromGMT
        if timeInterval <= 60 {
            return("DateFormatterManager.now".localized)
        }
        if timeInterval > 60 && timeInterval < (60 * 5) {
            return("DateFormatterManager.5MinutesAgo".localized)
        }
        if timeInterval >= (60 * 5) && timeInterval < (60 * 10) {
            return("DateFormatterManager.10MinutesAgo".localized)
        }
        if timeInterval >= (60 * 10) && timeInterval < (60 * 60 * 8) {
            return("DateFormatterManager.8HoursAgo".localized)
        }
        self.configureFormatter(formatter: self.formatter, format: "dd MMM")
        return ("\(formatter.string(from: date))")
    }
    
    func isActual(date: String) -> Bool {
        self.configureFormatter(formatter: self.formatter, format: "dd.MM.yyyy")
        guard let date = self.formatter.date(from: date) else { return false }
        let timeInterval = date.timeIntervalSinceNow
        return timeInterval < 0 ? false : true
    }
    
    func createDaysString(from days: Int) -> String {
        switch self.locale {
        case "ru":
            if 11...14 ~= days % 100 { return "VacationBookingScreen.manyDays".localized }
            if "234".contains("\(days % 10)") { return "VacationBookingScreen.fewDays".localized }
            if "567890".contains("\(days % 10)") { return "VacationBookingScreen.manyDays".localized }
            return "VacationBookingScreen.oneDay".localized
        default:
            return ""
        }
    }
    
    func createHoursString(from hours: Int) -> String {
        switch self.locale {
        case "ru":
            if 11...14 ~= hours % 100 { return "NewsScreen.manyHours".localized }
            if "234".contains("\(hours % 10)") { return "NewsScreen.fewHours".localized }
            if "567890".contains("\(hours % 10)") { return "NewsScreen.manyHours".localized }
            return "NewsScreen.oneHour".localized
        default:
            return ""
        }
    }
    
}

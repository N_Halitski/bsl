//
//  KeyStorage.swift
//  BSLWorkspace
//
//  Created by Andrei Harnashevich on 29.11.21.
//

import SwiftKeychainWrapper

final class KeychainManager {
    
    static let shared = KeychainManager()
    
    private struct Keys {
        static let accessToken = "access_token"
        static let refreshToken = "refresh_token"
    }
    
    var accessToken: String? {
        if let accessToken = KeychainWrapper.standard.string(forKey: Keys.accessToken) {
            return accessToken
        }
        return nil
    }
    
    var refreshToken: String? {
        if let refreshToken = KeychainWrapper.standard.string(forKey: Keys.refreshToken) {
            return refreshToken
        }
        return nil
    }
    
    func updateAccessToken(accessToken: String?) {
        if accessToken?.isEmpty == true || accessToken == nil {
            KeychainWrapper.standard.removeObject(forKey: Keys.accessToken, withAccessibility: .alwaysThisDeviceOnly)
        } else {
            KeychainWrapper.standard.set(accessToken!, forKey: Keys.accessToken, withAccessibility: .alwaysThisDeviceOnly)
        }
    }
    
    func updateRefreshToken(refreshToken: String?) {
        if refreshToken?.isEmpty == true || refreshToken == nil {
            KeychainWrapper.standard.removeObject(forKey: Keys.refreshToken, withAccessibility: .alwaysThisDeviceOnly)
        } else {
            KeychainWrapper.standard.set(refreshToken!, forKey: Keys.refreshToken, withAccessibility: .alwaysThisDeviceOnly)
        }
    }
        
    func clear() {
        updateRefreshToken(refreshToken: nil)
        updateAccessToken(accessToken: nil)
    }
    
}

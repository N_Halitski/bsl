//
//  BaseViewController.swift
//  HelpToMama
//
//  Created by Yaroslav Skachkov on 18.05.21.
//

import UIKit.UIViewController

class BaseViewController: UIViewController {
    
    private let leftButton = UIButton(type: .custom)
    private let rightButton = UIButton(type: .custom)
    
    var navigationTitle: String? {
        didSet {
            self.navigationItem.title = navigationTitle
        }
    }
    
    var backButtonTapped: (() -> Void)?
    var closeButtonTapped: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private lazy var spinner = BaseLoaderViewController()
    
    func showLoader() {
        addChild(spinner)
        spinner.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(spinner.view)
        spinner.spinner.startAnimation()
        spinner.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        spinner.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        spinner.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        spinner.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        spinner.didMove(toParent: self)
    }
    
    func hideLoader() {
        spinner.spinner.stopAnimation()
        spinner.willMove(toParent: nil)
        spinner.view.removeFromSuperview()
        spinner.removeFromParent()
    }
    
    func setupNavigationUI() {
        let navigationBar = navigationController!.navigationBar
//        navigationBar.barTintColor = AppTheme.Colors.gray4
        navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: AppTheme.Fonts.interSemiBold(18)]
        
        leftButton.setImage(UIImage(named: "BackArrow"), for: .normal)
        leftButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: leftButton)
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        rightButton.setImage(UIImage(named: "Close"), for: .normal)
        rightButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func setLeftButtonIsHidden(_ isHidden: Bool) {
        leftButton.isHidden = isHidden
    }
    
    func setRightButtonIsHidden(_ isHidden: Bool) {
        rightButton.isHidden = isHidden
    }
    
    @objc private func backButtonPressed() {
        backButtonTapped?()
    }
    
    @objc private func closeButtonPressed() {
        closeButtonTapped?()
    }
    
    func configureNavigationBarAppearance(with backgroundColor: UIColor, shouldConfigureButtons: Bool? = false) {
//        navigationController?.navigationBar.tintColor = AppTheme.Colors.gray1
        navigationController?.navigationBar.barTintColor = backgroundColor
        navigationController?.navigationBar.shadowImage = (shouldConfigureButtons == true) ? nil : UIImage()
        navigationController?.navigationBar.isTranslucent = (shouldConfigureButtons == true) ? true : false
        navigationController?.navigationBar.isOpaque = true
    }
}

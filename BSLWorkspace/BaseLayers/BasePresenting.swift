//
//  BasePresenting.swift
//  HelpToMama
//
//  Created by Yaroslav Skachkov on 18.05.21.
//

import Foundation

protocol BasePresenting: AnyObject {
    func viewDidLoad()
    func viewDidLayoutSubviews()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()
//    func showLoader()
//    func hideLoader()
}

extension BasePresenting {
    func viewDidLoad() {}
    func viewDidLayoutSubviews() {}
    func viewWillAppear() {}
    func viewDidAppear() {}
    func viewWillDisappear() {}
    func viewDidDisappear() {}
//    func showLoader() {}
//    func hideLoader() {}
}

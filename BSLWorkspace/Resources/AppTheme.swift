//
//  AppTheme.swift
//  BSLWorkspace
//
//  Created by Nikita on 16.11.2021.
//

import UIKit

public enum AppTheme {
    
    enum Colors {
        public static let orange100 = UIColor(red: 1, green: 0.451, blue: 0.012, alpha: 1)
        public static let orange50 = UIColor(red: 1, green: 0.451, blue: 0.012, alpha: 0.5)
        public static let lightOrange100 = UIColor(red: 1, green: 0.924, blue: 0.854, alpha: 1)
        public static let red100 = UIColor(red: 0.942, green: 0.365, blue: 0.365, alpha: 1)
        public static let lightRed100 = UIColor(red: 0.942, green: 0.365, blue: 0.365, alpha: 1)
        public static let black100 = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)
        public static let darkGray = UIColor(red: 0.522, green: 0.522, blue: 0.522, alpha: 1)
        public static let darkBlueGray100 = UIColor(red: 0.624, green: 0.659, blue: 0.69, alpha: 1)
        public static let grayBlue100 = UIColor(red: 0.843, green: 0.859, blue: 0.878, alpha: 1)
        public static let gray100 = UIColor(red: 0.897, green: 0.897, blue: 0.897, alpha: 1)
        public static let gray35 = UIColor(red: 0.929, green: 0.929, blue: 0.929, alpha: 1)
        public static let lightGray100 = UIColor(red: 0.976, green: 0.976, blue: 0.976, alpha: 1)
        public static let darkBlue100 = UIColor(red: 0.031, green: 0.133, blue: 0.251, alpha: 1)
        public static let babyBlue100 = UIColor(red: 0.263, green: 0.667, blue: 0.796, alpha: 1)
        public static let lightBlue100 = UIColor(red: 0.886, green: 0.988, blue: 1, alpha: 1)
        public static let green100 = UIColor(red: 0.263, green: 0.796, blue: 0.584, alpha: 1)
        public static let green1 = UIColor(red: 0.294, green: 0.702, blue: 0.294, alpha: 1)
        public static let lightGreen = UIColor(red: 0.886, green: 1, blue: 0.941, alpha: 1)
        public static let lightGray8 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08)
        public static let white100 = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        public static let lightGray4 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.04)
        public static let lightGray7 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.07)
        public static let blue1 = UIColor(red: 0.184, green: 0.502, blue: 0.929, alpha: 1)
        public static let babyBlue = UIColor(red: 0.44, green: 0.65, blue: 0.86, alpha: 1)
        public static let creamColor = UIColor(red: 1, green: 0.98, blue: 0.92, alpha: 1)
        public static let sandColor = UIColor(red: 1, green: 0.91, blue: 0.7, alpha: 1)
        public static let yellow100 = UIColor(red: 1, green: 0.757, blue: 0.027, alpha: 1)
        public static let darkYellow100 = UIColor(red: 0.879, green: 0.733, blue: 0.216, alpha: 1)
        public static let lightYellow100 = UIColor(red: 1, green: 0.978, blue: 0.779, alpha: 1)
        public static let greenStatus = UIColor(red: 0.294, green: 0.702, blue: 0.294, alpha: 1)
        public static let orangeStatus = UIColor(red: 1, green: 0.757, blue: 0.027, alpha: 1)
        public static let redStatus = UIColor(red: 0.902, green: 0.275, blue: 0.275, alpha: 1)
        public static let babyBlue1 = UIColor(red: 0.435, green: 0.655, blue: 0.859, alpha: 1)
        public static let shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.07)
        public static let redStatus2 = UIColor(red: 1, green: 0.918, blue: 0.918, alpha: 1)
        public static let yellowStatus = UIColor(red: 1, green: 0.976, blue: 0.917, alpha: 1)
        public static let yellowStatus2 = UIColor(red: 1, green: 0.913, blue: 0.696, alpha: 1)
    }
    
    enum Fonts {
        public static func SFHeavy(_ size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.heavy)
        }
        
        public static func SFBold(_ size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.bold)
        }
        
        public static func SFSemiBold(_ size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
        }
        
        public static func SFMedium(_ size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.medium)
        }
        
        public static func SFRegular(_ size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.regular)
        }
        
        public static func SFLight(_ size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.light)
        }
    }
}
